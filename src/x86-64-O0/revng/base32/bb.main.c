typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_11_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_4_ret_type;
struct indirect_placeholder_10_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_11_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_4_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_10_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_14(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_7(void);
extern void indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0);
extern struct indirect_placeholder_11_ret_type indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_4_ret_type indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_10_ret_type indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
void bb_main(uint64_t rdi, uint64_t rsi) {
    uint64_t var_21;
    uint32_t var_22;
    uint64_t r9_1;
    uint64_t local_sp_1;
    uint64_t var_47;
    struct indirect_placeholder_5_ret_type var_52;
    struct indirect_placeholder_15_ret_type var_16;
    uint64_t var_37;
    uint64_t *var_38;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_13;
    uint32_t var_23;
    uint64_t var_56;
    struct indirect_placeholder_12_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t *_pre_phi151;
    uint64_t *var_24;
    uint64_t var_27;
    uint64_t local_sp_3;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t local_sp_4_be;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_17;
    uint64_t local_sp_2;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    unsigned char *var_7;
    unsigned char *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t *var_12;
    uint64_t local_sp_4;
    uint64_t rcx_0;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t r8_1;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_10_ret_type var_35;
    uint64_t *_pre_phi153;
    uint64_t var_39;
    uint64_t var_36;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-76L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-88L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (unsigned char *)(var_0 + (-41L));
    *var_7 = (unsigned char)'\x00';
    var_8 = (unsigned char *)(var_0 + (-42L));
    *var_8 = (unsigned char)'\x00';
    var_9 = (uint64_t *)(var_0 + (-56L));
    *var_9 = 76UL;
    var_10 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-96L)) = 4206377UL;
    indirect_placeholder_3(var_10);
    *(uint64_t *)(var_0 + (-104L)) = 4206392UL;
    indirect_placeholder_1();
    var_11 = var_0 + (-112L);
    *(uint64_t *)var_11 = 4206402UL;
    indirect_placeholder_1();
    var_12 = (uint32_t *)(var_0 + (-60L));
    r8_1 = 0UL;
    local_sp_4 = var_11;
    while (1U)
        {
            var_13 = *var_6;
            var_14 = (uint64_t)*var_4;
            var_15 = local_sp_4 + (-8L);
            *(uint64_t *)var_15 = 4206655UL;
            var_16 = indirect_placeholder_15(4276768UL, 4277803UL, var_14, var_13, 0UL);
            var_17 = (uint32_t)var_16.field_0;
            *var_12 = var_17;
            local_sp_3 = var_15;
            local_sp_4_be = var_15;
            switch_state_var = 0;
            switch (var_17) {
              case 119U:
                {
                    var_18 = *(uint64_t *)4298072UL;
                    var_19 = local_sp_4 + (-16L);
                    *(uint64_t *)var_19 = 4206531UL;
                    var_20 = indirect_placeholder_14(4276667UL, 18446744073709551615UL, var_18, 0UL, 0UL, 4277769UL);
                    *var_9 = var_20;
                    local_sp_4_be = var_19;
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
                break;
              case 4294967295U:
                {
                    var_21 = var_16.field_1;
                    var_22 = *(uint32_t *)4297464UL;
                    var_23 = *var_4;
                    r9_1 = var_21;
                    if ((int)(var_23 - var_22) <= (int)1U) {
                        if ((int)var_22 < (int)var_23) {
                            var_24 = (uint64_t *)(var_0 + (-40L));
                            *var_24 = 4277825UL;
                            _pre_phi151 = var_24;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_25 = *(uint64_t *)(*var_6 + ((uint64_t)var_22 << 3UL));
                        var_26 = (uint64_t *)(var_0 + (-40L));
                        *var_26 = var_25;
                        _pre_phi151 = var_26;
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_56 = *(uint64_t *)(*var_6 + ((uint64_t)var_22 << 3UL));
                    *(uint64_t *)(local_sp_4 + (-16L)) = 4206720UL;
                    var_57 = indirect_placeholder_12(var_56);
                    var_58 = var_57.field_0;
                    var_59 = var_57.field_1;
                    var_60 = var_57.field_2;
                    *(uint64_t *)(local_sp_4 + (-24L)) = 4206748UL;
                    indirect_placeholder_11(0UL, var_58, 4277808UL, 0UL, 0UL, var_59, var_60);
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4206758UL;
                    indirect_placeholder_8(var_3, 1UL);
                    abort();
                }
                break;
              default:
                {
                    if ((int)var_17 <= (int)119U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_17 == 105U) {
                        *var_8 = (unsigned char)'\x01';
                    } else {
                        if ((int)var_17 <= (int)105U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if (var_17 != 100U) {
                            if ((int)var_17 <= (int)100U) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            switch_state_var = 0;
                            switch (var_17) {
                              case 4294967166U:
                                {
                                    *(uint64_t *)(local_sp_4 + (-16L)) = 4206553UL;
                                    indirect_placeholder_8(var_3, 0UL);
                                    abort();
                                }
                                break;
                              case 4294967165U:
                                {
                                    var_61 = *(uint64_t *)4297312UL;
                                    *(uint64_t *)(local_sp_4 + (-16L)) = 4206605UL;
                                    indirect_placeholder_13(0UL, var_61, 4276504UL, 4277454UL, 0UL, 4277787UL);
                                    var_62 = local_sp_4 + (-24L);
                                    *(uint64_t *)var_62 = 4206615UL;
                                    indirect_placeholder_1();
                                    local_sp_3 = var_62;
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                              default:
                                {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        *var_7 = (unsigned char)'\x01';
                    }
                    local_sp_4 = local_sp_4_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4206625UL;
            indirect_placeholder_8(var_3, 1UL);
            abort();
        }
        break;
      case 1U:
        {
            var_27 = *_pre_phi151;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4206826UL;
            indirect_placeholder_1();
            if ((uint64_t)(uint32_t)var_27 == 0UL) {
                var_36 = local_sp_4 + (-24L);
                *(uint64_t *)var_36 = 4206845UL;
                indirect_placeholder_8(0UL, 0UL);
                var_37 = *(uint64_t *)4296264UL;
                var_38 = (uint64_t *)(var_0 + (-32L));
                *var_38 = var_37;
                _pre_phi153 = var_38;
                local_sp_2 = var_36;
            } else {
                var_28 = *_pre_phi151;
                var_29 = local_sp_4 + (-24L);
                *(uint64_t *)var_29 = 4206875UL;
                indirect_placeholder_1();
                var_30 = (uint64_t *)(var_0 + (-32L));
                *var_30 = var_28;
                _pre_phi153 = var_30;
                local_sp_2 = var_29;
                if (var_28 == 0UL) {
                    var_31 = *_pre_phi151;
                    *(uint64_t *)(local_sp_4 + (-32L)) = 4206908UL;
                    var_32 = indirect_placeholder_6(var_31, 0UL, 3UL);
                    *(uint64_t *)(local_sp_4 + (-40L)) = 4206916UL;
                    indirect_placeholder_1();
                    var_33 = (uint64_t)*(uint32_t *)var_32;
                    var_34 = local_sp_4 + (-48L);
                    *(uint64_t *)var_34 = 4206943UL;
                    var_35 = indirect_placeholder_10(0UL, var_32, 4277830UL, 1UL, var_33, var_21, 0UL);
                    local_sp_2 = var_34;
                    r9_1 = var_35.field_1;
                    r8_1 = var_35.field_2;
                }
            }
            var_39 = *_pre_phi153;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4206960UL;
            indirect_placeholder_8(var_39, 2UL);
            r9_0 = r9_1;
            r8_0 = r8_1;
            if (*var_7 == '\x00') {
                var_43 = *(uint64_t *)4296256UL;
                var_44 = *var_9;
                var_45 = *_pre_phi153;
                var_46 = local_sp_2 + (-16L);
                *(uint64_t *)var_46 = 4207020UL;
                indirect_placeholder_9(var_44, var_45, var_43);
                local_sp_1 = var_46;
                rcx_0 = var_43;
            } else {
                var_40 = (uint64_t)*var_8;
                var_41 = *(uint64_t *)4296256UL;
                var_42 = local_sp_2 + (-16L);
                *(uint64_t *)var_42 = 4206992UL;
                indirect_placeholder_8(var_40, var_41);
                local_sp_1 = var_42;
                rcx_0 = var_41;
            }
            *(uint64_t *)(local_sp_1 + (-8L)) = 4207032UL;
            var_47 = indirect_placeholder_7();
            if ((uint64_t)((uint32_t)var_47 + 1U) != 0UL) {
                var_48 = *_pre_phi151;
                var_49 = local_sp_1 + (-16L);
                *(uint64_t *)var_49 = 4207054UL;
                indirect_placeholder_1();
                local_sp_0 = var_49;
                if ((uint64_t)(uint32_t)var_48 == 0UL) {
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4207063UL;
                    indirect_placeholder_1();
                    var_50 = (uint64_t)*(uint32_t *)var_48;
                    var_51 = local_sp_1 + (-32L);
                    *(uint64_t *)var_51 = 4207087UL;
                    var_52 = indirect_placeholder_5(0UL, rcx_0, 4277833UL, 1UL, var_50, r9_1, r8_1);
                    local_sp_0 = var_51;
                    r9_0 = var_52.field_1;
                    r8_0 = var_52.field_2;
                }
                var_53 = *_pre_phi151;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4207109UL;
                var_54 = indirect_placeholder_6(var_53, 0UL, 3UL);
                *(uint64_t *)(local_sp_0 + (-16L)) = 4207117UL;
                indirect_placeholder_1();
                var_55 = (uint64_t)*(uint32_t *)var_54;
                *(uint64_t *)(local_sp_0 + (-24L)) = 4207144UL;
                indirect_placeholder_4(0UL, var_54, 4277830UL, 1UL, var_55, r9_0, r8_0);
            }
            return;
        }
        break;
    }
}
