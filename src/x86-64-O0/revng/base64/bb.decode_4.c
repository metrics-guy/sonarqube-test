typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
uint64_t bb_decode_4(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t **var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t rax_0;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char var_48;
    uint64_t var_49;
    uint64_t var_50;
    unsigned char var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t local_sp_0;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t *var_41;
    uint64_t var_42;
    unsigned char var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t **var_17;
    uint64_t local_sp_1;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    unsigned char var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    unsigned char **var_11;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = var_0 + (-40L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-48L));
    *var_5 = rsi;
    var_6 = var_0 + (-56L);
    *(uint64_t *)var_6 = rdx;
    var_7 = var_0 + (-64L);
    *(uint64_t *)var_7 = rcx;
    var_8 = (uint64_t **)var_6;
    var_9 = **var_8;
    var_10 = (uint64_t *)(var_0 + (-24L));
    *var_10 = var_9;
    rax_0 = 0UL;
    if (*var_5 > 1UL) {
        return rax_0;
    }
    var_11 = (unsigned char **)var_3;
    var_12 = (uint64_t)(uint32_t)(uint64_t)**var_11;
    *(uint64_t *)(var_0 + (-72L)) = 4208470UL;
    var_13 = indirect_placeholder_16(var_12);
    var_14 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_4 + 1UL);
    var_15 = var_0 + (-80L);
    *(uint64_t *)var_15 = 4208498UL;
    var_16 = indirect_placeholder_16(var_14);
    local_sp_1 = var_15;
    if (~((uint64_t)(unsigned char)var_13 != 1UL & (uint64_t)(unsigned char)var_16 != 1UL)) {
        return;
    }
    var_17 = (uint64_t **)var_7;
    rax_0 = 1UL;
    if (**var_17 == 0UL) {
        var_18 = (uint64_t)(uint32_t)(uint64_t)**var_11;
        *(uint64_t *)(var_0 + (-88L)) = 4208544UL;
        var_19 = indirect_placeholder_16(var_18);
        var_20 = (uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_19 + 4277952UL) << 2UL;
        var_21 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_4 + 1UL);
        var_22 = var_0 + (-96L);
        *(uint64_t *)var_22 = 4208585UL;
        var_23 = indirect_placeholder_16(var_21);
        var_24 = (uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_23 + 4277952UL) << 56UL) >> (long)60UL);
        var_25 = *var_10;
        *var_10 = (var_25 + 1UL);
        *(unsigned char *)var_25 = ((unsigned char)var_20 | (unsigned char)var_24);
        var_26 = *var_17;
        *var_26 = (*var_26 + (-1L));
        local_sp_1 = var_22;
    }
    var_27 = *var_5;
    if (var_27 == 2UL) {
        **var_8 = *var_10;
    } else {
        var_28 = *var_4;
        var_29 = *(unsigned char *)(var_28 + 2UL);
        if (var_29 == '=') {
            if (var_27 != 4UL) {
                **var_8 = *var_10;
                return rax_0;
            }
            if (*(unsigned char *)(var_28 + 3UL) != '=') {
                **var_8 = *var_10;
                return rax_0;
            }
        }
        var_30 = (uint64_t)(uint32_t)(uint64_t)var_29;
        var_31 = local_sp_1 + (-8L);
        *(uint64_t *)var_31 = 4208770UL;
        var_32 = indirect_placeholder_16(var_30);
        local_sp_0 = var_31;
        if ((uint64_t)(unsigned char)var_32 != 1UL) {
            **var_8 = *var_10;
            return rax_0;
        }
        if (**var_17 == 0UL) {
            var_33 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_4 + 1UL);
            *(uint64_t *)(local_sp_1 + (-16L)) = 4208831UL;
            var_34 = indirect_placeholder_16(var_33);
            var_35 = (uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_34 + 4277952UL) << 4UL;
            var_36 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_4 + 2UL);
            var_37 = local_sp_1 + (-24L);
            *(uint64_t *)var_37 = 4208875UL;
            var_38 = indirect_placeholder_16(var_36);
            var_39 = (uint64_t)((long)((uint64_t)*(unsigned char *)((uint64_t)(unsigned char)var_38 + 4277952UL) << 56UL) >> (long)58UL);
            var_40 = *var_10;
            *var_10 = (var_40 + 1UL);
            *(unsigned char *)var_40 = ((unsigned char)var_35 | (unsigned char)var_39);
            var_41 = *var_17;
            *var_41 = (*var_41 + (-1L));
            local_sp_0 = var_37;
        }
        var_42 = *var_5;
        if (var_42 != 3UL) {
            **var_8 = *var_10;
            return rax_0;
        }
        var_43 = *(unsigned char *)(*var_4 + 3UL);
        if (var_43 == '=') {
            if (var_42 == 4UL) {
                **var_8 = *var_10;
                return rax_0;
            }
        }
        var_44 = (uint64_t)(uint32_t)(uint64_t)var_43;
        *(uint64_t *)(local_sp_0 + (-8L)) = 4209024UL;
        var_45 = indirect_placeholder_16(var_44);
        if ((uint64_t)(unsigned char)var_45 != 1UL) {
            **var_8 = *var_10;
            return rax_0;
        }
        if (**var_17 == 0UL) {
            var_46 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_4 + 2UL);
            *(uint64_t *)(local_sp_0 + (-16L)) = 4209085UL;
            var_47 = indirect_placeholder_16(var_46);
            var_48 = *(unsigned char *)((uint64_t)(unsigned char)var_47 + 4277952UL) << '\x06';
            var_49 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_4 + 3UL);
            *(uint64_t *)(local_sp_0 + (-24L)) = 4209129UL;
            var_50 = indirect_placeholder_16(var_49);
            var_51 = *(unsigned char *)((uint64_t)(unsigned char)var_50 + 4277952UL);
            var_52 = *var_10;
            *var_10 = (var_52 + 1UL);
            *(unsigned char *)var_52 = (var_48 | var_51);
            var_53 = *var_17;
            *var_53 = (*var_53 + (-1L));
        }
        **var_8 = *var_10;
    }
}
