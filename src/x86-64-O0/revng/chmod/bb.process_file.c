typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_process_file_ret_type;
struct indirect_placeholder_69_ret_type;
struct indirect_placeholder_68_ret_type;
struct indirect_placeholder_71_ret_type;
struct indirect_placeholder_70_ret_type;
struct indirect_placeholder_74_ret_type;
struct indirect_placeholder_76_ret_type;
struct indirect_placeholder_75_ret_type;
struct indirect_placeholder_77_ret_type;
struct indirect_placeholder_73_ret_type;
struct indirect_placeholder_72_ret_type;
struct indirect_placeholder_79_ret_type;
struct indirect_placeholder_78_ret_type;
struct indirect_placeholder_84_ret_type;
struct indirect_placeholder_80_ret_type;
struct indirect_placeholder_85_ret_type;
struct indirect_placeholder_81_ret_type;
struct indirect_placeholder_86_ret_type;
struct indirect_placeholder_82_ret_type;
struct indirect_placeholder_87_ret_type;
struct indirect_placeholder_83_ret_type;
struct bb_process_file_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_69_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_68_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_71_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_70_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_74_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_76_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_75_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_77_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_73_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_72_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_79_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_78_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_84_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_80_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_85_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_81_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_86_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_82_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_87_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_83_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t init_r12(void);
extern void indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_69_ret_type indirect_placeholder_69(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_68_ret_type indirect_placeholder_68(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_71_ret_type indirect_placeholder_71(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_70_ret_type indirect_placeholder_70(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_74_ret_type indirect_placeholder_74(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_76_ret_type indirect_placeholder_76(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_75_ret_type indirect_placeholder_75(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_77_ret_type indirect_placeholder_77(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_73_ret_type indirect_placeholder_73(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_72_ret_type indirect_placeholder_72(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_79_ret_type indirect_placeholder_79(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_78_ret_type indirect_placeholder_78(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_84_ret_type indirect_placeholder_84(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_80_ret_type indirect_placeholder_80(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_85_ret_type indirect_placeholder_85(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_81_ret_type indirect_placeholder_81(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_86_ret_type indirect_placeholder_86(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_82_ret_type indirect_placeholder_82(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_87_ret_type indirect_placeholder_87(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_83_ret_type indirect_placeholder_83(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
struct bb_process_file_ret_type bb_process_file(uint64_t rdi, uint64_t rsi) {
    uint64_t var_131;
    uint64_t var_118;
    struct indirect_placeholder_68_ret_type var_146;
    uint64_t var_97;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    unsigned char *var_16;
    unsigned char *var_17;
    uint64_t var_18;
    uint16_t var_19;
    uint64_t local_sp_6;
    uint64_t r8_10;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    struct indirect_placeholder_69_ret_type var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t local_sp_11;
    uint64_t local_sp_7;
    uint64_t local_sp_0;
    uint64_t r9_6;
    uint64_t var_104;
    struct indirect_placeholder_71_ret_type var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    struct indirect_placeholder_70_ret_type var_111;
    uint64_t var_23;
    uint64_t r8_6;
    uint64_t r9_0;
    uint64_t r8_0;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t local_sp_1;
    uint64_t rcx_0;
    uint64_t r9_1;
    uint64_t r8_1;
    struct indirect_placeholder_74_ret_type var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t rax_1;
    uint64_t var_76;
    struct indirect_placeholder_76_ret_type var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    struct indirect_placeholder_75_ret_type var_82;
    struct indirect_placeholder_77_ret_type var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_73_ret_type var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    struct indirect_placeholder_72_ret_type var_75;
    struct indirect_placeholder_79_ret_type var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    struct indirect_placeholder_78_ret_type var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_2;
    uint64_t var_31;
    struct indirect_placeholder_84_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    struct indirect_placeholder_80_ret_type var_37;
    uint64_t r9_2;
    uint64_t r8_2;
    uint64_t local_sp_3;
    uint64_t var_38;
    struct indirect_placeholder_85_ret_type var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    struct indirect_placeholder_81_ret_type var_45;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t local_sp_4;
    uint64_t var_46;
    struct indirect_placeholder_86_ret_type var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    struct indirect_placeholder_82_ret_type var_53;
    uint64_t r9_4;
    uint64_t r8_4;
    uint64_t *var_54;
    struct bb_process_file_ret_type mrv2;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t local_sp_5;
    uint64_t var_55;
    struct indirect_placeholder_87_ret_type var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    struct indirect_placeholder_83_ret_type var_62;
    uint64_t r9_5;
    uint64_t r8_5;
    bool var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t var_90;
    uint32_t *var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint32_t *var_98;
    uint32_t var_99;
    uint64_t r9_7;
    uint64_t r8_7;
    uint64_t local_sp_10;
    uint64_t local_sp_8;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t local_sp_9;
    uint64_t r8_8;
    uint64_t r8_9;
    unsigned char storemerge;
    unsigned char *var_119;
    unsigned char var_120;
    uint32_t rax_0;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint32_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint32_t *var_132;
    uint32_t var_133;
    uint32_t var_134;
    uint32_t var_135;
    uint64_t r9_8;
    uint64_t r8_11;
    uint64_t r9_9;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t r8_12;
    struct bb_process_file_ret_type mrv;
    struct bb_process_file_ret_type mrv1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbx();
    var_5 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    *(uint64_t *)(var_0 + (-24L)) = var_4;
    var_6 = var_0 + (-136L);
    var_7 = (uint64_t *)(var_0 + (-128L));
    *var_7 = rdi;
    var_8 = (uint64_t *)var_6;
    *var_8 = rsi;
    var_9 = *(uint64_t *)(rsi + 56UL);
    var_10 = (uint64_t *)(var_0 + (-48L));
    *var_10 = var_9;
    var_11 = *(uint64_t *)(*var_8 + 48UL);
    var_12 = (uint64_t *)(var_0 + (-56L));
    *var_12 = var_11;
    var_13 = *var_8 + 120UL;
    var_14 = var_0 + (-64L);
    var_15 = (uint64_t *)var_14;
    *var_15 = var_13;
    var_16 = (unsigned char *)(var_0 + (-33L));
    *var_16 = (unsigned char)'\x01';
    var_17 = (unsigned char *)(var_0 + (-34L));
    *var_17 = (unsigned char)'\x00';
    var_18 = *var_8;
    var_19 = *(uint16_t *)(var_18 + 112UL);
    local_sp_6 = var_6;
    r9_6 = var_2;
    r8_6 = var_3;
    r8_0 = 0UL;
    rax_1 = 1UL;
    local_sp_2 = var_6;
    r9_2 = var_2;
    r8_2 = var_3;
    local_sp_3 = var_6;
    r9_3 = var_2;
    r8_3 = var_3;
    local_sp_4 = var_6;
    r9_4 = var_2;
    r8_4 = var_3;
    local_sp_5 = var_6;
    r9_5 = var_2;
    r8_5 = var_3;
    storemerge = (unsigned char)'\x00';
    rax_0 = 2U;
    r9_9 = var_2;
    r8_12 = var_3;
    if (var_19 <= (unsigned short)13U) {
        var_65 = (*var_16 == '\x00');
        local_sp_7 = local_sp_6;
        r9_0 = r9_6;
        rax_1 = 0UL;
        r9_7 = r9_6;
        r8_7 = r8_6;
        if (!var_65) {
            var_66 = *(uint64_t *)4322304UL;
            if (var_66 != 0UL & *(uint64_t *)(*var_15 + 8UL) != **(uint64_t **)4322304UL & **(uint64_t **)var_14 != *(uint64_t *)(var_66 + 8UL)) {
                var_67 = *var_10;
                *(uint64_t *)(local_sp_6 + (-8L)) = 4205845UL;
                indirect_placeholder_2();
                if ((uint64_t)(uint32_t)var_67 == 0UL) {
                    var_76 = *var_10;
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4205866UL;
                    var_77 = indirect_placeholder_76(4UL, var_76);
                    var_78 = var_77.field_0;
                    var_79 = var_77.field_1;
                    var_80 = var_77.field_2;
                    var_81 = local_sp_6 + (-24L);
                    *(uint64_t *)var_81 = 4205894UL;
                    var_82 = indirect_placeholder_75(0UL, var_78, 4298056UL, 0UL, 0UL, var_79, var_80);
                    local_sp_1 = var_81;
                    rcx_0 = var_82.field_0;
                    r9_1 = var_82.field_1;
                    r8_1 = var_82.field_2;
                } else {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4205916UL;
                    var_68 = indirect_placeholder_77(4298050UL, 1UL, 4UL);
                    var_69 = var_68.field_0;
                    var_70 = *var_10;
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4205941UL;
                    var_71 = indirect_placeholder_73(var_70, 0UL, 4UL);
                    var_72 = var_71.field_0;
                    var_73 = var_71.field_1;
                    var_74 = local_sp_6 + (-32L);
                    *(uint64_t *)var_74 = 4205972UL;
                    var_75 = indirect_placeholder_72(0UL, var_72, 4298104UL, 0UL, 0UL, var_73, var_69);
                    local_sp_1 = var_74;
                    rcx_0 = var_75.field_0;
                    r9_1 = var_75.field_1;
                    r8_1 = var_75.field_2;
                }
                *(uint64_t *)(local_sp_1 + (-8L)) = 4205997UL;
                var_83 = indirect_placeholder_74(0UL, rcx_0, 4298168UL, 0UL, 0UL, r9_1, r8_1);
                var_84 = var_83.field_1;
                var_85 = var_83.field_2;
                var_86 = *var_8;
                var_87 = *var_7;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4206021UL;
                indirect_placeholder_5(4UL, var_87, var_86);
                var_88 = *var_7;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4206033UL;
                var_89 = indirect_placeholder_5(var_88, var_84, var_85);
                *(uint64_t *)(var_0 + (-72L)) = var_89;
                r9_9 = var_84;
                r8_12 = var_85;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = r9_9;
                mrv2 = mrv1;
                mrv2.field_2 = r8_12;
                return mrv2;
            }
        }
        var_90 = *(uint32_t *)(*var_15 + 24UL);
        var_91 = (uint32_t *)(var_0 + (-28L));
        *var_91 = var_90;
        var_92 = *(uint64_t *)4322288UL;
        var_93 = (uint64_t)*(uint32_t *)4322296UL;
        var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
        var_95 = (uint64_t)var_90;
        var_96 = local_sp_6 + (-8L);
        *(uint64_t *)var_96 = 4206115UL;
        var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
        var_98 = (uint32_t *)(var_0 + (-32L));
        var_99 = (uint32_t)var_97;
        *var_98 = var_99;
        local_sp_7 = var_96;
        r8_7 = 0UL;
        if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
            var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
            var_101 = (uint64_t)var_99;
            var_102 = local_sp_6 + (-16L);
            *(uint64_t *)var_102 = 4206157UL;
            var_103 = indirect_placeholder_8(var_100);
            local_sp_0 = var_102;
            local_sp_7 = var_102;
            if ((uint64_t)(uint32_t)var_103 == 0UL) {
                *var_17 = (unsigned char)'\x01';
            } else {
                if (*(unsigned char *)4322301UL != '\x01') {
                    var_104 = *var_10;
                    *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                    var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                    var_106 = var_105.field_0;
                    var_107 = var_105.field_1;
                    var_108 = var_105.field_2;
                    *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                    indirect_placeholder_2();
                    var_109 = (uint64_t)*(uint32_t *)var_106;
                    var_110 = local_sp_6 + (-40L);
                    *(uint64_t *)var_110 = 4206233UL;
                    var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                    local_sp_0 = var_110;
                    r9_0 = var_111.field_1;
                    r8_0 = var_111.field_2;
                }
                *var_16 = (unsigned char)'\x00';
                local_sp_7 = local_sp_0;
                r9_7 = r9_0;
                r8_7 = r8_0;
            }
        }
        r8_10 = r8_7;
        local_sp_10 = local_sp_7;
        local_sp_8 = local_sp_7;
        r8_8 = r8_7;
        r9_8 = r9_7;
        if (*(uint32_t *)4321984UL != 2U) {
            if (*var_17 == '\x00') {
                local_sp_9 = local_sp_8;
                r8_9 = r8_8;
            } else {
                var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                var_115 = *var_10;
                var_116 = *var_12;
                var_117 = local_sp_7 + (-8L);
                *(uint64_t *)var_117 = 4206289UL;
                var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                local_sp_8 = var_117;
                local_sp_9 = var_117;
                r8_8 = var_113;
                r8_9 = var_113;
                storemerge = (unsigned char)'\x01';
                if ((uint64_t)(unsigned char)var_118 == 0UL) {
                    local_sp_9 = local_sp_8;
                    r8_9 = r8_8;
                }
            }
            var_119 = (unsigned char *)(var_0 + (-73L));
            var_120 = storemerge & '\x01';
            *var_119 = var_120;
            local_sp_10 = local_sp_9;
            r8_10 = r8_9;
            if (var_120 == '\x00') {
                if (*(uint32_t *)4321984UL != 0U) {
                    rax_0 = 0U;
                    if (*var_16 != '\x01' & *var_17 == '\x01') {
                        rax_0 = (var_120 == '\x01') ? 1U : 3U;
                    }
                    *(uint32_t *)(var_0 + (-80L)) = rax_0;
                    var_121 = (uint64_t)rax_0;
                    var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                    var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                    var_124 = *var_10;
                    var_125 = local_sp_9 + (-8L);
                    *(uint64_t *)var_125 = 4206411UL;
                    indirect_placeholder(var_121, var_122, var_124, var_123);
                    local_sp_10 = var_125;
                }
            } else {
                rax_0 = 0U;
                if (*var_16 != '\x01' & *var_17 == '\x01') {
                    rax_0 = (var_120 == '\x01') ? 1U : 3U;
                }
                *(uint32_t *)(var_0 + (-80L)) = rax_0;
                var_121 = (uint64_t)rax_0;
                var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                var_124 = *var_10;
                var_125 = local_sp_9 + (-8L);
                *(uint64_t *)var_125 = 4206411UL;
                indirect_placeholder(var_121, var_122, var_124, var_123);
                local_sp_10 = var_125;
            }
        }
        local_sp_11 = local_sp_10;
        r8_11 = r8_10;
        var_126 = *(uint64_t *)4322288UL;
        var_127 = *(uint32_t *)(var_0 + (-28L));
        var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
        var_129 = (uint64_t)var_127;
        var_130 = local_sp_10 + (-8L);
        *(uint64_t *)var_130 = 4206486UL;
        var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
        var_132 = (uint32_t *)(var_0 + (-84L));
        var_133 = (uint32_t)var_131;
        *var_132 = var_133;
        var_134 = var_133 ^ (-1);
        var_135 = *(uint32_t *)(var_0 + (-32L));
        local_sp_11 = var_130;
        r8_11 = 0UL;
        if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
            var_136 = var_0 + (-96L);
            var_137 = (uint64_t)var_135;
            *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
            indirect_placeholder_14(var_137, var_136);
            var_138 = var_0 + (-108L);
            var_139 = (uint64_t)*var_132;
            *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
            indirect_placeholder_14(var_139, var_138);
            *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
            *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
            var_140 = var_0 + (-107L);
            var_141 = var_0 + (-95L);
            var_142 = *var_10;
            *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
            var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
            var_144 = var_143.field_0;
            var_145 = local_sp_10 + (-40L);
            *(uint64_t *)var_145 = 4206618UL;
            var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
            var_147 = var_146.field_1;
            var_148 = var_146.field_2;
            *var_16 = (unsigned char)'\x00';
            local_sp_11 = var_145;
            r9_8 = var_147;
            r8_11 = var_148;
        }
        r9_9 = r9_8;
        r8_12 = r8_11;
        if (*(unsigned char *)4322300UL == '\x01') {
            var_149 = *var_8;
            var_150 = *var_7;
            *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
            indirect_placeholder_5(4UL, var_150, var_149);
        }
        var_151 = (uint64_t)*var_16;
        rax_1 = var_151;
        mrv.field_0 = rax_1;
        mrv1 = mrv;
        mrv1.field_1 = r9_9;
        mrv2 = mrv1;
        mrv2.field_2 = r8_12;
        return mrv2;
    }
    switch (*(uint64_t *)(((uint64_t)var_19 << 3UL) + 4298288UL)) {
      case 4205284UL:
        {
            mrv.field_0 = rax_1;
            mrv1 = mrv;
            mrv1.field_1 = r9_9;
            mrv2 = mrv1;
            mrv2.field_2 = r8_12;
            return mrv2;
        }
        break;
      case 4205294UL:
        {
            if (*(uint64_t *)(var_18 + 88UL) != 0UL) {
                var_54 = (uint64_t *)(var_18 + 32UL);
                if (*var_54 != 0UL) {
                    *var_54 = 1UL;
                    var_63 = *var_8;
                    var_64 = *var_7;
                    *(uint64_t *)(var_0 + (-144L)) = 4205356UL;
                    indirect_placeholder_5(1UL, var_64, var_63);
                    mrv.field_0 = rax_1;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_9;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_12;
                    return mrv2;
                }
            }
            if (*(unsigned char *)4322301UL == '\x01') {
                var_55 = *var_10;
                *(uint64_t *)(var_0 + (-144L)) = 4205397UL;
                var_56 = indirect_placeholder_87(4UL, var_55);
                var_57 = var_56.field_0;
                var_58 = var_56.field_1;
                var_59 = var_56.field_2;
                var_60 = (uint64_t)*(uint32_t *)(*var_8 + 64UL);
                var_61 = var_0 + (-152L);
                *(uint64_t *)var_61 = 4205432UL;
                var_62 = indirect_placeholder_83(0UL, var_57, 4297779UL, 0UL, var_60, var_58, var_59);
                local_sp_5 = var_61;
                r9_5 = var_62.field_1;
                r8_5 = var_62.field_2;
            }
            *var_16 = (unsigned char)'\x00';
            local_sp_6 = local_sp_5;
            r9_6 = r9_5;
            r8_6 = r8_5;
            var_65 = (*var_16 == '\x00');
            local_sp_7 = local_sp_6;
            r9_0 = r9_6;
            rax_1 = 0UL;
            r9_7 = r9_6;
            r8_7 = r8_6;
            if (!var_65) {
                var_66 = *(uint64_t *)4322304UL;
                if (var_66 != 0UL & *(uint64_t *)(*var_15 + 8UL) != **(uint64_t **)4322304UL & **(uint64_t **)var_14 != *(uint64_t *)(var_66 + 8UL)) {
                    var_67 = *var_10;
                    *(uint64_t *)(local_sp_6 + (-8L)) = 4205845UL;
                    indirect_placeholder_2();
                    if ((uint64_t)(uint32_t)var_67 == 0UL) {
                        var_76 = *var_10;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4205866UL;
                        var_77 = indirect_placeholder_76(4UL, var_76);
                        var_78 = var_77.field_0;
                        var_79 = var_77.field_1;
                        var_80 = var_77.field_2;
                        var_81 = local_sp_6 + (-24L);
                        *(uint64_t *)var_81 = 4205894UL;
                        var_82 = indirect_placeholder_75(0UL, var_78, 4298056UL, 0UL, 0UL, var_79, var_80);
                        local_sp_1 = var_81;
                        rcx_0 = var_82.field_0;
                        r9_1 = var_82.field_1;
                        r8_1 = var_82.field_2;
                    } else {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4205916UL;
                        var_68 = indirect_placeholder_77(4298050UL, 1UL, 4UL);
                        var_69 = var_68.field_0;
                        var_70 = *var_10;
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4205941UL;
                        var_71 = indirect_placeholder_73(var_70, 0UL, 4UL);
                        var_72 = var_71.field_0;
                        var_73 = var_71.field_1;
                        var_74 = local_sp_6 + (-32L);
                        *(uint64_t *)var_74 = 4205972UL;
                        var_75 = indirect_placeholder_72(0UL, var_72, 4298104UL, 0UL, 0UL, var_73, var_69);
                        local_sp_1 = var_74;
                        rcx_0 = var_75.field_0;
                        r9_1 = var_75.field_1;
                        r8_1 = var_75.field_2;
                    }
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4205997UL;
                    var_83 = indirect_placeholder_74(0UL, rcx_0, 4298168UL, 0UL, 0UL, r9_1, r8_1);
                    var_84 = var_83.field_1;
                    var_85 = var_83.field_2;
                    var_86 = *var_8;
                    var_87 = *var_7;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4206021UL;
                    indirect_placeholder_5(4UL, var_87, var_86);
                    var_88 = *var_7;
                    *(uint64_t *)(local_sp_1 + (-24L)) = 4206033UL;
                    var_89 = indirect_placeholder_5(var_88, var_84, var_85);
                    *(uint64_t *)(var_0 + (-72L)) = var_89;
                    r9_9 = var_84;
                    r8_12 = var_85;
                    mrv.field_0 = rax_1;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_9;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_12;
                    return mrv2;
                }
            }
            var_90 = *(uint32_t *)(*var_15 + 24UL);
            var_91 = (uint32_t *)(var_0 + (-28L));
            *var_91 = var_90;
            var_92 = *(uint64_t *)4322288UL;
            var_93 = (uint64_t)*(uint32_t *)4322296UL;
            var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
            var_95 = (uint64_t)var_90;
            var_96 = local_sp_6 + (-8L);
            *(uint64_t *)var_96 = 4206115UL;
            var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
            var_98 = (uint32_t *)(var_0 + (-32L));
            var_99 = (uint32_t)var_97;
            *var_98 = var_99;
            local_sp_7 = var_96;
            r8_7 = 0UL;
            if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                var_101 = (uint64_t)var_99;
                var_102 = local_sp_6 + (-16L);
                *(uint64_t *)var_102 = 4206157UL;
                var_103 = indirect_placeholder_8(var_100);
                local_sp_0 = var_102;
                local_sp_7 = var_102;
                if ((uint64_t)(uint32_t)var_103 == 0UL) {
                    *var_17 = (unsigned char)'\x01';
                } else {
                    if (*(unsigned char *)4322301UL != '\x01') {
                        var_104 = *var_10;
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                        var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                        var_106 = var_105.field_0;
                        var_107 = var_105.field_1;
                        var_108 = var_105.field_2;
                        *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                        indirect_placeholder_2();
                        var_109 = (uint64_t)*(uint32_t *)var_106;
                        var_110 = local_sp_6 + (-40L);
                        *(uint64_t *)var_110 = 4206233UL;
                        var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                        local_sp_0 = var_110;
                        r9_0 = var_111.field_1;
                        r8_0 = var_111.field_2;
                    }
                    *var_16 = (unsigned char)'\x00';
                    local_sp_7 = local_sp_0;
                    r9_7 = r9_0;
                    r8_7 = r8_0;
                }
            }
            r8_10 = r8_7;
            local_sp_10 = local_sp_7;
            local_sp_8 = local_sp_7;
            r8_8 = r8_7;
            r9_8 = r9_7;
            if (*(uint32_t *)4321984UL != 2U) {
                if (*var_17 == '\x00') {
                    local_sp_9 = local_sp_8;
                    r8_9 = r8_8;
                } else {
                    var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                    var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                    var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                    var_115 = *var_10;
                    var_116 = *var_12;
                    var_117 = local_sp_7 + (-8L);
                    *(uint64_t *)var_117 = 4206289UL;
                    var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                    local_sp_8 = var_117;
                    local_sp_9 = var_117;
                    r8_8 = var_113;
                    r8_9 = var_113;
                    storemerge = (unsigned char)'\x01';
                    if ((uint64_t)(unsigned char)var_118 == 0UL) {
                        local_sp_9 = local_sp_8;
                        r8_9 = r8_8;
                    }
                }
                var_119 = (unsigned char *)(var_0 + (-73L));
                var_120 = storemerge & '\x01';
                *var_119 = var_120;
                local_sp_10 = local_sp_9;
                r8_10 = r8_9;
                if (var_120 == '\x00') {
                    if (*(uint32_t *)4321984UL != 0U) {
                        rax_0 = 0U;
                        if (*var_16 != '\x01' & *var_17 == '\x01') {
                            rax_0 = (var_120 == '\x01') ? 1U : 3U;
                        }
                        *(uint32_t *)(var_0 + (-80L)) = rax_0;
                        var_121 = (uint64_t)rax_0;
                        var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                        var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                        var_124 = *var_10;
                        var_125 = local_sp_9 + (-8L);
                        *(uint64_t *)var_125 = 4206411UL;
                        indirect_placeholder(var_121, var_122, var_124, var_123);
                        local_sp_10 = var_125;
                    }
                } else {
                    rax_0 = 0U;
                    if (*var_16 != '\x01' & *var_17 == '\x01') {
                        rax_0 = (var_120 == '\x01') ? 1U : 3U;
                    }
                    *(uint32_t *)(var_0 + (-80L)) = rax_0;
                    var_121 = (uint64_t)rax_0;
                    var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                    var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                    var_124 = *var_10;
                    var_125 = local_sp_9 + (-8L);
                    *(uint64_t *)var_125 = 4206411UL;
                    indirect_placeholder(var_121, var_122, var_124, var_123);
                    local_sp_10 = var_125;
                }
            }
            local_sp_11 = local_sp_10;
            r8_11 = r8_10;
            var_126 = *(uint64_t *)4322288UL;
            var_127 = *(uint32_t *)(var_0 + (-28L));
            var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
            var_129 = (uint64_t)var_127;
            var_130 = local_sp_10 + (-8L);
            *(uint64_t *)var_130 = 4206486UL;
            var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
            var_132 = (uint32_t *)(var_0 + (-84L));
            var_133 = (uint32_t)var_131;
            *var_132 = var_133;
            var_134 = var_133 ^ (-1);
            var_135 = *(uint32_t *)(var_0 + (-32L));
            local_sp_11 = var_130;
            r8_11 = 0UL;
            if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                var_136 = var_0 + (-96L);
                var_137 = (uint64_t)var_135;
                *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                indirect_placeholder_14(var_137, var_136);
                var_138 = var_0 + (-108L);
                var_139 = (uint64_t)*var_132;
                *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                indirect_placeholder_14(var_139, var_138);
                *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                var_140 = var_0 + (-107L);
                var_141 = var_0 + (-95L);
                var_142 = *var_10;
                *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                var_144 = var_143.field_0;
                var_145 = local_sp_10 + (-40L);
                *(uint64_t *)var_145 = 4206618UL;
                var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                var_147 = var_146.field_1;
                var_148 = var_146.field_2;
                *var_16 = (unsigned char)'\x00';
                local_sp_11 = var_145;
                r9_8 = var_147;
                r8_11 = var_148;
            }
            r9_9 = r9_8;
            r8_12 = r8_11;
            if (*(unsigned char *)4322300UL != '\x01') {
                var_149 = *var_8;
                var_150 = *var_7;
                *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                indirect_placeholder_5(4UL, var_150, var_149);
            }
            var_151 = (uint64_t)*var_16;
            rax_1 = var_151;
        }
        break;
      case 4205661UL:
        {
            var_20 = *var_7;
            var_21 = var_0 + (-144L);
            *(uint64_t *)var_21 = 4205680UL;
            var_22 = indirect_placeholder_3(var_20, var_18);
            local_sp_6 = var_21;
            rax_1 = 0UL;
            if ((uint64_t)(unsigned char)var_22 == 0UL) {
                var_23 = *var_10;
                *(uint64_t *)(var_0 + (-152L)) = 4205706UL;
                var_24 = indirect_placeholder_79(var_23, 0UL, 3UL);
                var_25 = var_24.field_0;
                var_26 = var_24.field_1;
                var_27 = var_24.field_2;
                *(uint64_t *)(var_0 + (-160L)) = 4205734UL;
                var_28 = indirect_placeholder_78(0UL, var_25, 4297864UL, 0UL, 0UL, var_26, var_27);
                var_29 = var_28.field_1;
                var_30 = var_28.field_2;
                r9_9 = var_29;
                r8_12 = var_30;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = r9_9;
                mrv2 = mrv1;
                mrv2.field_2 = r8_12;
                return mrv2;
            }
            var_65 = (*var_16 == '\x00');
            local_sp_7 = local_sp_6;
            r9_0 = r9_6;
            rax_1 = 0UL;
            r9_7 = r9_6;
            r8_7 = r8_6;
            if (var_65) {
                var_66 = *(uint64_t *)4322304UL;
                if (var_66 != 0UL) {
                    var_90 = *(uint32_t *)(*var_15 + 24UL);
                    var_91 = (uint32_t *)(var_0 + (-28L));
                    *var_91 = var_90;
                    var_92 = *(uint64_t *)4322288UL;
                    var_93 = (uint64_t)*(uint32_t *)4322296UL;
                    var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
                    var_95 = (uint64_t)var_90;
                    var_96 = local_sp_6 + (-8L);
                    *(uint64_t *)var_96 = 4206115UL;
                    var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
                    var_98 = (uint32_t *)(var_0 + (-32L));
                    var_99 = (uint32_t)var_97;
                    *var_98 = var_99;
                    local_sp_7 = var_96;
                    r8_7 = 0UL;
                    if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                        var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                        var_101 = (uint64_t)var_99;
                        var_102 = local_sp_6 + (-16L);
                        *(uint64_t *)var_102 = 4206157UL;
                        var_103 = indirect_placeholder_8(var_100);
                        local_sp_0 = var_102;
                        local_sp_7 = var_102;
                        if ((uint64_t)(uint32_t)var_103 == 0UL) {
                            *var_17 = (unsigned char)'\x01';
                        } else {
                            if (*(unsigned char *)4322301UL == '\x01') {
                                var_104 = *var_10;
                                *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                                var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                                var_106 = var_105.field_0;
                                var_107 = var_105.field_1;
                                var_108 = var_105.field_2;
                                *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                                indirect_placeholder_2();
                                var_109 = (uint64_t)*(uint32_t *)var_106;
                                var_110 = local_sp_6 + (-40L);
                                *(uint64_t *)var_110 = 4206233UL;
                                var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                                local_sp_0 = var_110;
                                r9_0 = var_111.field_1;
                                r8_0 = var_111.field_2;
                            }
                            *var_16 = (unsigned char)'\x00';
                            local_sp_7 = local_sp_0;
                            r9_7 = r9_0;
                            r8_7 = r8_0;
                        }
                    }
                    r8_10 = r8_7;
                    local_sp_10 = local_sp_7;
                    local_sp_8 = local_sp_7;
                    r8_8 = r8_7;
                    r9_8 = r9_7;
                    if (*(uint32_t *)4321984UL != 2U) {
                        if (*var_17 == '\x00') {
                            local_sp_9 = local_sp_8;
                            r8_9 = r8_8;
                        } else {
                            var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                            var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                            var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                            var_115 = *var_10;
                            var_116 = *var_12;
                            var_117 = local_sp_7 + (-8L);
                            *(uint64_t *)var_117 = 4206289UL;
                            var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                            local_sp_8 = var_117;
                            local_sp_9 = var_117;
                            r8_8 = var_113;
                            r8_9 = var_113;
                            storemerge = (unsigned char)'\x01';
                            if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                local_sp_9 = local_sp_8;
                                r8_9 = r8_8;
                            }
                        }
                        var_119 = (unsigned char *)(var_0 + (-73L));
                        var_120 = storemerge & '\x01';
                        *var_119 = var_120;
                        local_sp_10 = local_sp_9;
                        r8_10 = r8_9;
                        if (var_120 == '\x00') {
                            if (*(uint32_t *)4321984UL != 0U) {
                                rax_0 = 0U;
                                if (*var_16 != '\x01' & *var_17 == '\x01') {
                                    rax_0 = (var_120 == '\x01') ? 1U : 3U;
                                }
                                *(uint32_t *)(var_0 + (-80L)) = rax_0;
                                var_121 = (uint64_t)rax_0;
                                var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                var_124 = *var_10;
                                var_125 = local_sp_9 + (-8L);
                                *(uint64_t *)var_125 = 4206411UL;
                                indirect_placeholder(var_121, var_122, var_124, var_123);
                                local_sp_10 = var_125;
                            }
                        } else {
                            rax_0 = 0U;
                            if (*var_16 != '\x01' & *var_17 == '\x01') {
                                rax_0 = (var_120 == '\x01') ? 1U : 3U;
                            }
                            *(uint32_t *)(var_0 + (-80L)) = rax_0;
                            var_121 = (uint64_t)rax_0;
                            var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                            var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                            var_124 = *var_10;
                            var_125 = local_sp_9 + (-8L);
                            *(uint64_t *)var_125 = 4206411UL;
                            indirect_placeholder(var_121, var_122, var_124, var_123);
                            local_sp_10 = var_125;
                        }
                    }
                    local_sp_11 = local_sp_10;
                    r8_11 = r8_10;
                    var_126 = *(uint64_t *)4322288UL;
                    var_127 = *(uint32_t *)(var_0 + (-28L));
                    var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
                    var_129 = (uint64_t)var_127;
                    var_130 = local_sp_10 + (-8L);
                    *(uint64_t *)var_130 = 4206486UL;
                    var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
                    var_132 = (uint32_t *)(var_0 + (-84L));
                    var_133 = (uint32_t)var_131;
                    *var_132 = var_133;
                    var_134 = var_133 ^ (-1);
                    var_135 = *(uint32_t *)(var_0 + (-32L));
                    local_sp_11 = var_130;
                    r8_11 = 0UL;
                    if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                        var_136 = var_0 + (-96L);
                        var_137 = (uint64_t)var_135;
                        *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                        indirect_placeholder_14(var_137, var_136);
                        var_138 = var_0 + (-108L);
                        var_139 = (uint64_t)*var_132;
                        *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                        indirect_placeholder_14(var_139, var_138);
                        *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                        *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                        var_140 = var_0 + (-107L);
                        var_141 = var_0 + (-95L);
                        var_142 = *var_10;
                        *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                        var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                        var_144 = var_143.field_0;
                        var_145 = local_sp_10 + (-40L);
                        *(uint64_t *)var_145 = 4206618UL;
                        var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                        var_147 = var_146.field_1;
                        var_148 = var_146.field_2;
                        *var_16 = (unsigned char)'\x00';
                        local_sp_11 = var_145;
                        r9_8 = var_147;
                        r8_11 = var_148;
                    }
                    r9_9 = r9_8;
                    r8_12 = r8_11;
                    if (*(unsigned char *)4322300UL == '\x01') {
                        var_149 = *var_8;
                        var_150 = *var_7;
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                        indirect_placeholder_5(4UL, var_150, var_149);
                    }
                    var_151 = (uint64_t)*var_16;
                    rax_1 = var_151;
                    mrv.field_0 = rax_1;
                    mrv1 = mrv;
                    mrv1.field_1 = r9_9;
                    mrv2 = mrv1;
                    mrv2.field_2 = r8_12;
                    return mrv2;
                }
                if (*(uint64_t *)(*var_15 + 8UL) != **(uint64_t **)4322304UL) {
                    if (**(uint64_t **)var_14 == *(uint64_t *)(var_66 + 8UL)) {
                        var_67 = *var_10;
                        *(uint64_t *)(local_sp_6 + (-8L)) = 4205845UL;
                        indirect_placeholder_2();
                        if ((uint64_t)(uint32_t)var_67 == 0UL) {
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4205916UL;
                            var_68 = indirect_placeholder_77(4298050UL, 1UL, 4UL);
                            var_69 = var_68.field_0;
                            var_70 = *var_10;
                            *(uint64_t *)(local_sp_6 + (-24L)) = 4205941UL;
                            var_71 = indirect_placeholder_73(var_70, 0UL, 4UL);
                            var_72 = var_71.field_0;
                            var_73 = var_71.field_1;
                            var_74 = local_sp_6 + (-32L);
                            *(uint64_t *)var_74 = 4205972UL;
                            var_75 = indirect_placeholder_72(0UL, var_72, 4298104UL, 0UL, 0UL, var_73, var_69);
                            local_sp_1 = var_74;
                            rcx_0 = var_75.field_0;
                            r9_1 = var_75.field_1;
                            r8_1 = var_75.field_2;
                        } else {
                            var_76 = *var_10;
                            *(uint64_t *)(local_sp_6 + (-16L)) = 4205866UL;
                            var_77 = indirect_placeholder_76(4UL, var_76);
                            var_78 = var_77.field_0;
                            var_79 = var_77.field_1;
                            var_80 = var_77.field_2;
                            var_81 = local_sp_6 + (-24L);
                            *(uint64_t *)var_81 = 4205894UL;
                            var_82 = indirect_placeholder_75(0UL, var_78, 4298056UL, 0UL, 0UL, var_79, var_80);
                            local_sp_1 = var_81;
                            rcx_0 = var_82.field_0;
                            r9_1 = var_82.field_1;
                            r8_1 = var_82.field_2;
                        }
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4205997UL;
                        var_83 = indirect_placeholder_74(0UL, rcx_0, 4298168UL, 0UL, 0UL, r9_1, r8_1);
                        var_84 = var_83.field_1;
                        var_85 = var_83.field_2;
                        var_86 = *var_8;
                        var_87 = *var_7;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206021UL;
                        indirect_placeholder_5(4UL, var_87, var_86);
                        var_88 = *var_7;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4206033UL;
                        var_89 = indirect_placeholder_5(var_88, var_84, var_85);
                        *(uint64_t *)(var_0 + (-72L)) = var_89;
                        r9_9 = var_84;
                        r8_12 = var_85;
                        mrv.field_0 = rax_1;
                        mrv1 = mrv;
                        mrv1.field_1 = r9_9;
                        mrv2 = mrv1;
                        mrv2.field_2 = r8_12;
                        return mrv2;
                    }
                }
                var_90 = *(uint32_t *)(*var_15 + 24UL);
                var_91 = (uint32_t *)(var_0 + (-28L));
                *var_91 = var_90;
                var_92 = *(uint64_t *)4322288UL;
                var_93 = (uint64_t)*(uint32_t *)4322296UL;
                var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
                var_95 = (uint64_t)var_90;
                var_96 = local_sp_6 + (-8L);
                *(uint64_t *)var_96 = 4206115UL;
                var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
                var_98 = (uint32_t *)(var_0 + (-32L));
                var_99 = (uint32_t)var_97;
                *var_98 = var_99;
                local_sp_7 = var_96;
                r8_7 = 0UL;
                if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                    var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                    var_101 = (uint64_t)var_99;
                    var_102 = local_sp_6 + (-16L);
                    *(uint64_t *)var_102 = 4206157UL;
                    var_103 = indirect_placeholder_8(var_100);
                    local_sp_0 = var_102;
                    local_sp_7 = var_102;
                    if ((uint64_t)(uint32_t)var_103 == 0UL) {
                        *var_17 = (unsigned char)'\x01';
                    } else {
                        if (*(unsigned char *)4322301UL == '\x01') {
                            var_104 = *var_10;
                            *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                            var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                            var_106 = var_105.field_0;
                            var_107 = var_105.field_1;
                            var_108 = var_105.field_2;
                            *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                            indirect_placeholder_2();
                            var_109 = (uint64_t)*(uint32_t *)var_106;
                            var_110 = local_sp_6 + (-40L);
                            *(uint64_t *)var_110 = 4206233UL;
                            var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                            local_sp_0 = var_110;
                            r9_0 = var_111.field_1;
                            r8_0 = var_111.field_2;
                        }
                        *var_16 = (unsigned char)'\x00';
                        local_sp_7 = local_sp_0;
                        r9_7 = r9_0;
                        r8_7 = r8_0;
                    }
                }
                r8_10 = r8_7;
                local_sp_10 = local_sp_7;
                local_sp_8 = local_sp_7;
                r8_8 = r8_7;
                r9_8 = r9_7;
                if (*(uint32_t *)4321984UL != 2U) {
                    if (*var_17 == '\x00') {
                        local_sp_9 = local_sp_8;
                        r8_9 = r8_8;
                    } else {
                        var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                        var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                        var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                        var_115 = *var_10;
                        var_116 = *var_12;
                        var_117 = local_sp_7 + (-8L);
                        *(uint64_t *)var_117 = 4206289UL;
                        var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                        local_sp_8 = var_117;
                        local_sp_9 = var_117;
                        r8_8 = var_113;
                        r8_9 = var_113;
                        storemerge = (unsigned char)'\x01';
                        if ((uint64_t)(unsigned char)var_118 == 0UL) {
                            local_sp_9 = local_sp_8;
                            r8_9 = r8_8;
                        }
                    }
                    var_119 = (unsigned char *)(var_0 + (-73L));
                    var_120 = storemerge & '\x01';
                    *var_119 = var_120;
                    local_sp_10 = local_sp_9;
                    r8_10 = r8_9;
                    if (var_120 == '\x00') {
                        if (*(uint32_t *)4321984UL != 0U) {
                            rax_0 = 0U;
                            if (*var_16 != '\x01' & *var_17 == '\x01') {
                                rax_0 = (var_120 == '\x01') ? 1U : 3U;
                            }
                            *(uint32_t *)(var_0 + (-80L)) = rax_0;
                            var_121 = (uint64_t)rax_0;
                            var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                            var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                            var_124 = *var_10;
                            var_125 = local_sp_9 + (-8L);
                            *(uint64_t *)var_125 = 4206411UL;
                            indirect_placeholder(var_121, var_122, var_124, var_123);
                            local_sp_10 = var_125;
                        }
                    } else {
                        rax_0 = 0U;
                        if (*var_16 != '\x01' & *var_17 == '\x01') {
                            rax_0 = (var_120 == '\x01') ? 1U : 3U;
                        }
                        *(uint32_t *)(var_0 + (-80L)) = rax_0;
                        var_121 = (uint64_t)rax_0;
                        var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                        var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                        var_124 = *var_10;
                        var_125 = local_sp_9 + (-8L);
                        *(uint64_t *)var_125 = 4206411UL;
                        indirect_placeholder(var_121, var_122, var_124, var_123);
                        local_sp_10 = var_125;
                    }
                }
                local_sp_11 = local_sp_10;
                r8_11 = r8_10;
                var_126 = *(uint64_t *)4322288UL;
                var_127 = *(uint32_t *)(var_0 + (-28L));
                var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
                var_129 = (uint64_t)var_127;
                var_130 = local_sp_10 + (-8L);
                *(uint64_t *)var_130 = 4206486UL;
                var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
                var_132 = (uint32_t *)(var_0 + (-84L));
                var_133 = (uint32_t)var_131;
                *var_132 = var_133;
                var_134 = var_133 ^ (-1);
                var_135 = *(uint32_t *)(var_0 + (-32L));
                local_sp_11 = var_130;
                r8_11 = 0UL;
                if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                    var_136 = var_0 + (-96L);
                    var_137 = (uint64_t)var_135;
                    *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                    indirect_placeholder_14(var_137, var_136);
                    var_138 = var_0 + (-108L);
                    var_139 = (uint64_t)*var_132;
                    *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                    indirect_placeholder_14(var_139, var_138);
                    *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                    *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                    var_140 = var_0 + (-107L);
                    var_141 = var_0 + (-95L);
                    var_142 = *var_10;
                    *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                    var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                    var_144 = var_143.field_0;
                    var_145 = local_sp_10 + (-40L);
                    *(uint64_t *)var_145 = 4206618UL;
                    var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                    var_147 = var_146.field_1;
                    var_148 = var_146.field_2;
                    *var_16 = (unsigned char)'\x00';
                    local_sp_11 = var_145;
                    r9_8 = var_147;
                    r8_11 = var_148;
                }
                r9_9 = r9_8;
                r8_12 = r8_11;
                if (*(unsigned char *)4322300UL == '\x01') {
                    var_149 = *var_8;
                    var_150 = *var_7;
                    *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                    indirect_placeholder_5(4UL, var_150, var_149);
                }
                var_151 = (uint64_t)*var_16;
                rax_1 = var_151;
                mrv.field_0 = rax_1;
                mrv1 = mrv;
                mrv1.field_1 = r9_9;
                mrv2 = mrv1;
                mrv2.field_2 = r8_12;
                return mrv2;
            }
            var_90 = *(uint32_t *)(*var_15 + 24UL);
            var_91 = (uint32_t *)(var_0 + (-28L));
            *var_91 = var_90;
            var_92 = *(uint64_t *)4322288UL;
            var_93 = (uint64_t)*(uint32_t *)4322296UL;
            var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
            var_95 = (uint64_t)var_90;
            var_96 = local_sp_6 + (-8L);
            *(uint64_t *)var_96 = 4206115UL;
            var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
            var_98 = (uint32_t *)(var_0 + (-32L));
            var_99 = (uint32_t)var_97;
            *var_98 = var_99;
            local_sp_7 = var_96;
            r8_7 = 0UL;
            if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                var_101 = (uint64_t)var_99;
                var_102 = local_sp_6 + (-16L);
                *(uint64_t *)var_102 = 4206157UL;
                var_103 = indirect_placeholder_8(var_100);
                local_sp_0 = var_102;
                local_sp_7 = var_102;
                if ((uint64_t)(uint32_t)var_103 == 0UL) {
                    *var_17 = (unsigned char)'\x01';
                } else {
                    if (*(unsigned char *)4322301UL == '\x01') {
                        var_104 = *var_10;
                        *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                        var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                        var_106 = var_105.field_0;
                        var_107 = var_105.field_1;
                        var_108 = var_105.field_2;
                        *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                        indirect_placeholder_2();
                        var_109 = (uint64_t)*(uint32_t *)var_106;
                        var_110 = local_sp_6 + (-40L);
                        *(uint64_t *)var_110 = 4206233UL;
                        var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                        local_sp_0 = var_110;
                        r9_0 = var_111.field_1;
                        r8_0 = var_111.field_2;
                    }
                    *var_16 = (unsigned char)'\x00';
                    local_sp_7 = local_sp_0;
                    r9_7 = r9_0;
                    r8_7 = r8_0;
                }
            }
            r8_10 = r8_7;
            local_sp_10 = local_sp_7;
            local_sp_8 = local_sp_7;
            r8_8 = r8_7;
            r9_8 = r9_7;
            if (*(uint32_t *)4321984UL != 2U) {
                if (*var_17 == '\x00') {
                    local_sp_9 = local_sp_8;
                    r8_9 = r8_8;
                } else {
                    var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                    var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                    var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                    var_115 = *var_10;
                    var_116 = *var_12;
                    var_117 = local_sp_7 + (-8L);
                    *(uint64_t *)var_117 = 4206289UL;
                    var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                    local_sp_8 = var_117;
                    local_sp_9 = var_117;
                    r8_8 = var_113;
                    r8_9 = var_113;
                    storemerge = (unsigned char)'\x01';
                    if ((uint64_t)(unsigned char)var_118 == 0UL) {
                        local_sp_9 = local_sp_8;
                        r8_9 = r8_8;
                    }
                }
                var_119 = (unsigned char *)(var_0 + (-73L));
                var_120 = storemerge & '\x01';
                *var_119 = var_120;
                local_sp_10 = local_sp_9;
                r8_10 = r8_9;
                if (var_120 == '\x00') {
                    if (*(uint32_t *)4321984UL != 0U) {
                        rax_0 = 0U;
                        if (*var_16 != '\x01' & *var_17 == '\x01') {
                            rax_0 = (var_120 == '\x01') ? 1U : 3U;
                        }
                        *(uint32_t *)(var_0 + (-80L)) = rax_0;
                        var_121 = (uint64_t)rax_0;
                        var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                        var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                        var_124 = *var_10;
                        var_125 = local_sp_9 + (-8L);
                        *(uint64_t *)var_125 = 4206411UL;
                        indirect_placeholder(var_121, var_122, var_124, var_123);
                        local_sp_10 = var_125;
                    }
                } else {
                    rax_0 = 0U;
                    if (*var_16 != '\x01' & *var_17 == '\x01') {
                        rax_0 = (var_120 == '\x01') ? 1U : 3U;
                    }
                    *(uint32_t *)(var_0 + (-80L)) = rax_0;
                    var_121 = (uint64_t)rax_0;
                    var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                    var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                    var_124 = *var_10;
                    var_125 = local_sp_9 + (-8L);
                    *(uint64_t *)var_125 = 4206411UL;
                    indirect_placeholder(var_121, var_122, var_124, var_123);
                    local_sp_10 = var_125;
                }
            }
            local_sp_11 = local_sp_10;
            r8_11 = r8_10;
            var_126 = *(uint64_t *)4322288UL;
            var_127 = *(uint32_t *)(var_0 + (-28L));
            var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
            var_129 = (uint64_t)var_127;
            var_130 = local_sp_10 + (-8L);
            *(uint64_t *)var_130 = 4206486UL;
            var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
            var_132 = (uint32_t *)(var_0 + (-84L));
            var_133 = (uint32_t)var_131;
            *var_132 = var_133;
            var_134 = var_133 ^ (-1);
            var_135 = *(uint32_t *)(var_0 + (-32L));
            local_sp_11 = var_130;
            r8_11 = 0UL;
            if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                var_136 = var_0 + (-96L);
                var_137 = (uint64_t)var_135;
                *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                indirect_placeholder_14(var_137, var_136);
                var_138 = var_0 + (-108L);
                var_139 = (uint64_t)*var_132;
                *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                indirect_placeholder_14(var_139, var_138);
                *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                var_140 = var_0 + (-107L);
                var_141 = var_0 + (-95L);
                var_142 = *var_10;
                *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                var_144 = var_143.field_0;
                var_145 = local_sp_10 + (-40L);
                *(uint64_t *)var_145 = 4206618UL;
                var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                var_147 = var_146.field_1;
                var_148 = var_146.field_2;
                *var_16 = (unsigned char)'\x00';
                local_sp_11 = var_145;
                r9_8 = var_147;
                r8_11 = var_148;
            }
            r9_9 = r9_8;
            r8_12 = r8_11;
            if (*(unsigned char *)4322300UL == '\x01') {
                var_149 = *var_8;
                var_150 = *var_7;
                *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                indirect_placeholder_5(4UL, var_150, var_149);
            }
            var_151 = (uint64_t)*var_16;
            rax_1 = var_151;
        }
        break;
      case 4205744UL:
      case 4205596UL:
      case 4205521UL:
      case 4205441UL:
        {
            switch (*(uint64_t *)(((uint64_t)var_19 << 3UL) + 4298288UL)) {
              case 4205744UL:
                {
                    var_65 = (*var_16 == '\x00');
                    local_sp_7 = local_sp_6;
                    r9_0 = r9_6;
                    rax_1 = 0UL;
                    r9_7 = r9_6;
                    r8_7 = r8_6;
                    if (var_65) {
                        var_66 = *(uint64_t *)4322304UL;
                        if (var_66 != 0UL) {
                            var_90 = *(uint32_t *)(*var_15 + 24UL);
                            var_91 = (uint32_t *)(var_0 + (-28L));
                            *var_91 = var_90;
                            var_92 = *(uint64_t *)4322288UL;
                            var_93 = (uint64_t)*(uint32_t *)4322296UL;
                            var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
                            var_95 = (uint64_t)var_90;
                            var_96 = local_sp_6 + (-8L);
                            *(uint64_t *)var_96 = 4206115UL;
                            var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
                            var_98 = (uint32_t *)(var_0 + (-32L));
                            var_99 = (uint32_t)var_97;
                            *var_98 = var_99;
                            local_sp_7 = var_96;
                            r8_7 = 0UL;
                            if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                                var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                                var_101 = (uint64_t)var_99;
                                var_102 = local_sp_6 + (-16L);
                                *(uint64_t *)var_102 = 4206157UL;
                                var_103 = indirect_placeholder_8(var_100);
                                local_sp_0 = var_102;
                                local_sp_7 = var_102;
                                if ((uint64_t)(uint32_t)var_103 == 0UL) {
                                    *var_17 = (unsigned char)'\x01';
                                } else {
                                    if (*(unsigned char *)4322301UL == '\x01') {
                                        var_104 = *var_10;
                                        *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                                        var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                                        var_106 = var_105.field_0;
                                        var_107 = var_105.field_1;
                                        var_108 = var_105.field_2;
                                        *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                                        indirect_placeholder_2();
                                        var_109 = (uint64_t)*(uint32_t *)var_106;
                                        var_110 = local_sp_6 + (-40L);
                                        *(uint64_t *)var_110 = 4206233UL;
                                        var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                                        local_sp_0 = var_110;
                                        r9_0 = var_111.field_1;
                                        r8_0 = var_111.field_2;
                                    }
                                    *var_16 = (unsigned char)'\x00';
                                    local_sp_7 = local_sp_0;
                                    r9_7 = r9_0;
                                    r8_7 = r8_0;
                                }
                            }
                            r8_10 = r8_7;
                            local_sp_10 = local_sp_7;
                            local_sp_8 = local_sp_7;
                            r8_8 = r8_7;
                            r9_8 = r9_7;
                            if (*(uint32_t *)4321984UL != 2U) {
                                if (*var_17 == '\x00') {
                                    local_sp_9 = local_sp_8;
                                    r8_9 = r8_8;
                                } else {
                                    var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                                    var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                    var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                    var_115 = *var_10;
                                    var_116 = *var_12;
                                    var_117 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_117 = 4206289UL;
                                    var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                                    local_sp_8 = var_117;
                                    local_sp_9 = var_117;
                                    r8_8 = var_113;
                                    r8_9 = var_113;
                                    storemerge = (unsigned char)'\x01';
                                    if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                        local_sp_9 = local_sp_8;
                                        r8_9 = r8_8;
                                    }
                                }
                                var_119 = (unsigned char *)(var_0 + (-73L));
                                var_120 = storemerge & '\x01';
                                *var_119 = var_120;
                                local_sp_10 = local_sp_9;
                                r8_10 = r8_9;
                                if (var_120 == '\x00') {
                                    if (*(uint32_t *)4321984UL != 0U) {
                                        rax_0 = 0U;
                                        if (*var_16 != '\x01' & *var_17 == '\x01') {
                                            rax_0 = (var_120 == '\x01') ? 1U : 3U;
                                        }
                                        *(uint32_t *)(var_0 + (-80L)) = rax_0;
                                        var_121 = (uint64_t)rax_0;
                                        var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                        var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                        var_124 = *var_10;
                                        var_125 = local_sp_9 + (-8L);
                                        *(uint64_t *)var_125 = 4206411UL;
                                        indirect_placeholder(var_121, var_122, var_124, var_123);
                                        local_sp_10 = var_125;
                                    }
                                } else {
                                    rax_0 = 0U;
                                    if (*var_16 != '\x01' & *var_17 == '\x01') {
                                        rax_0 = (var_120 == '\x01') ? 1U : 3U;
                                    }
                                    *(uint32_t *)(var_0 + (-80L)) = rax_0;
                                    var_121 = (uint64_t)rax_0;
                                    var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                    var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                    var_124 = *var_10;
                                    var_125 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_125 = 4206411UL;
                                    indirect_placeholder(var_121, var_122, var_124, var_123);
                                    local_sp_10 = var_125;
                                }
                            }
                            local_sp_11 = local_sp_10;
                            r8_11 = r8_10;
                            var_126 = *(uint64_t *)4322288UL;
                            var_127 = *(uint32_t *)(var_0 + (-28L));
                            var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
                            var_129 = (uint64_t)var_127;
                            var_130 = local_sp_10 + (-8L);
                            *(uint64_t *)var_130 = 4206486UL;
                            var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
                            var_132 = (uint32_t *)(var_0 + (-84L));
                            var_133 = (uint32_t)var_131;
                            *var_132 = var_133;
                            var_134 = var_133 ^ (-1);
                            var_135 = *(uint32_t *)(var_0 + (-32L));
                            local_sp_11 = var_130;
                            r8_11 = 0UL;
                            if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                                var_136 = var_0 + (-96L);
                                var_137 = (uint64_t)var_135;
                                *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                                indirect_placeholder_14(var_137, var_136);
                                var_138 = var_0 + (-108L);
                                var_139 = (uint64_t)*var_132;
                                *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                                indirect_placeholder_14(var_139, var_138);
                                *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                                *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                                var_140 = var_0 + (-107L);
                                var_141 = var_0 + (-95L);
                                var_142 = *var_10;
                                *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                                var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                                var_144 = var_143.field_0;
                                var_145 = local_sp_10 + (-40L);
                                *(uint64_t *)var_145 = 4206618UL;
                                var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                                var_147 = var_146.field_1;
                                var_148 = var_146.field_2;
                                *var_16 = (unsigned char)'\x00';
                                local_sp_11 = var_145;
                                r9_8 = var_147;
                                r8_11 = var_148;
                            }
                            r9_9 = r9_8;
                            r8_12 = r8_11;
                            if (*(unsigned char *)4322300UL == '\x01') {
                                var_149 = *var_8;
                                var_150 = *var_7;
                                *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                                indirect_placeholder_5(4UL, var_150, var_149);
                            }
                            var_151 = (uint64_t)*var_16;
                            rax_1 = var_151;
                            mrv.field_0 = rax_1;
                            mrv1 = mrv;
                            mrv1.field_1 = r9_9;
                            mrv2 = mrv1;
                            mrv2.field_2 = r8_12;
                            return mrv2;
                        }
                        if (*(uint64_t *)(*var_15 + 8UL) != **(uint64_t **)4322304UL) {
                            if (**(uint64_t **)var_14 == *(uint64_t *)(var_66 + 8UL)) {
                                var_67 = *var_10;
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4205845UL;
                                indirect_placeholder_2();
                                if ((uint64_t)(uint32_t)var_67 == 0UL) {
                                    *(uint64_t *)(local_sp_6 + (-16L)) = 4205916UL;
                                    var_68 = indirect_placeholder_77(4298050UL, 1UL, 4UL);
                                    var_69 = var_68.field_0;
                                    var_70 = *var_10;
                                    *(uint64_t *)(local_sp_6 + (-24L)) = 4205941UL;
                                    var_71 = indirect_placeholder_73(var_70, 0UL, 4UL);
                                    var_72 = var_71.field_0;
                                    var_73 = var_71.field_1;
                                    var_74 = local_sp_6 + (-32L);
                                    *(uint64_t *)var_74 = 4205972UL;
                                    var_75 = indirect_placeholder_72(0UL, var_72, 4298104UL, 0UL, 0UL, var_73, var_69);
                                    local_sp_1 = var_74;
                                    rcx_0 = var_75.field_0;
                                    r9_1 = var_75.field_1;
                                    r8_1 = var_75.field_2;
                                } else {
                                    var_76 = *var_10;
                                    *(uint64_t *)(local_sp_6 + (-16L)) = 4205866UL;
                                    var_77 = indirect_placeholder_76(4UL, var_76);
                                    var_78 = var_77.field_0;
                                    var_79 = var_77.field_1;
                                    var_80 = var_77.field_2;
                                    var_81 = local_sp_6 + (-24L);
                                    *(uint64_t *)var_81 = 4205894UL;
                                    var_82 = indirect_placeholder_75(0UL, var_78, 4298056UL, 0UL, 0UL, var_79, var_80);
                                    local_sp_1 = var_81;
                                    rcx_0 = var_82.field_0;
                                    r9_1 = var_82.field_1;
                                    r8_1 = var_82.field_2;
                                }
                                *(uint64_t *)(local_sp_1 + (-8L)) = 4205997UL;
                                var_83 = indirect_placeholder_74(0UL, rcx_0, 4298168UL, 0UL, 0UL, r9_1, r8_1);
                                var_84 = var_83.field_1;
                                var_85 = var_83.field_2;
                                var_86 = *var_8;
                                var_87 = *var_7;
                                *(uint64_t *)(local_sp_1 + (-16L)) = 4206021UL;
                                indirect_placeholder_5(4UL, var_87, var_86);
                                var_88 = *var_7;
                                *(uint64_t *)(local_sp_1 + (-24L)) = 4206033UL;
                                var_89 = indirect_placeholder_5(var_88, var_84, var_85);
                                *(uint64_t *)(var_0 + (-72L)) = var_89;
                                r9_9 = var_84;
                                r8_12 = var_85;
                                mrv.field_0 = rax_1;
                                mrv1 = mrv;
                                mrv1.field_1 = r9_9;
                                mrv2 = mrv1;
                                mrv2.field_2 = r8_12;
                                return mrv2;
                            }
                        }
                        var_90 = *(uint32_t *)(*var_15 + 24UL);
                        var_91 = (uint32_t *)(var_0 + (-28L));
                        *var_91 = var_90;
                        var_92 = *(uint64_t *)4322288UL;
                        var_93 = (uint64_t)*(uint32_t *)4322296UL;
                        var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
                        var_95 = (uint64_t)var_90;
                        var_96 = local_sp_6 + (-8L);
                        *(uint64_t *)var_96 = 4206115UL;
                        var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
                        var_98 = (uint32_t *)(var_0 + (-32L));
                        var_99 = (uint32_t)var_97;
                        *var_98 = var_99;
                        local_sp_7 = var_96;
                        r8_7 = 0UL;
                        if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                            var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                            var_101 = (uint64_t)var_99;
                            var_102 = local_sp_6 + (-16L);
                            *(uint64_t *)var_102 = 4206157UL;
                            var_103 = indirect_placeholder_8(var_100);
                            local_sp_0 = var_102;
                            local_sp_7 = var_102;
                            if ((uint64_t)(uint32_t)var_103 == 0UL) {
                                *var_17 = (unsigned char)'\x01';
                            } else {
                                if (*(unsigned char *)4322301UL == '\x01') {
                                    var_104 = *var_10;
                                    *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                                    var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                                    var_106 = var_105.field_0;
                                    var_107 = var_105.field_1;
                                    var_108 = var_105.field_2;
                                    *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                                    indirect_placeholder_2();
                                    var_109 = (uint64_t)*(uint32_t *)var_106;
                                    var_110 = local_sp_6 + (-40L);
                                    *(uint64_t *)var_110 = 4206233UL;
                                    var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                                    local_sp_0 = var_110;
                                    r9_0 = var_111.field_1;
                                    r8_0 = var_111.field_2;
                                }
                                *var_16 = (unsigned char)'\x00';
                                local_sp_7 = local_sp_0;
                                r9_7 = r9_0;
                                r8_7 = r8_0;
                            }
                        }
                        r8_10 = r8_7;
                        local_sp_10 = local_sp_7;
                        local_sp_8 = local_sp_7;
                        r8_8 = r8_7;
                        r9_8 = r9_7;
                        if (*(uint32_t *)4321984UL != 2U) {
                            if (*var_17 == '\x00') {
                                local_sp_9 = local_sp_8;
                                r8_9 = r8_8;
                            } else {
                                var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                                var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                var_115 = *var_10;
                                var_116 = *var_12;
                                var_117 = local_sp_7 + (-8L);
                                *(uint64_t *)var_117 = 4206289UL;
                                var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                                local_sp_8 = var_117;
                                local_sp_9 = var_117;
                                r8_8 = var_113;
                                r8_9 = var_113;
                                storemerge = (unsigned char)'\x01';
                                if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                    local_sp_9 = local_sp_8;
                                    r8_9 = r8_8;
                                }
                            }
                            var_119 = (unsigned char *)(var_0 + (-73L));
                            var_120 = storemerge & '\x01';
                            *var_119 = var_120;
                            local_sp_10 = local_sp_9;
                            r8_10 = r8_9;
                            if (var_120 == '\x00') {
                                if (*(uint32_t *)4321984UL != 0U) {
                                    rax_0 = 0U;
                                    if (*var_16 != '\x01' & *var_17 == '\x01') {
                                        rax_0 = (var_120 == '\x01') ? 1U : 3U;
                                    }
                                    *(uint32_t *)(var_0 + (-80L)) = rax_0;
                                    var_121 = (uint64_t)rax_0;
                                    var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                    var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                    var_124 = *var_10;
                                    var_125 = local_sp_9 + (-8L);
                                    *(uint64_t *)var_125 = 4206411UL;
                                    indirect_placeholder(var_121, var_122, var_124, var_123);
                                    local_sp_10 = var_125;
                                }
                            } else {
                                rax_0 = 0U;
                                if (*var_16 != '\x01' & *var_17 == '\x01') {
                                    rax_0 = (var_120 == '\x01') ? 1U : 3U;
                                }
                                *(uint32_t *)(var_0 + (-80L)) = rax_0;
                                var_121 = (uint64_t)rax_0;
                                var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                var_124 = *var_10;
                                var_125 = local_sp_9 + (-8L);
                                *(uint64_t *)var_125 = 4206411UL;
                                indirect_placeholder(var_121, var_122, var_124, var_123);
                                local_sp_10 = var_125;
                            }
                        }
                        local_sp_11 = local_sp_10;
                        r8_11 = r8_10;
                        var_126 = *(uint64_t *)4322288UL;
                        var_127 = *(uint32_t *)(var_0 + (-28L));
                        var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
                        var_129 = (uint64_t)var_127;
                        var_130 = local_sp_10 + (-8L);
                        *(uint64_t *)var_130 = 4206486UL;
                        var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
                        var_132 = (uint32_t *)(var_0 + (-84L));
                        var_133 = (uint32_t)var_131;
                        *var_132 = var_133;
                        var_134 = var_133 ^ (-1);
                        var_135 = *(uint32_t *)(var_0 + (-32L));
                        local_sp_11 = var_130;
                        r8_11 = 0UL;
                        if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                            var_136 = var_0 + (-96L);
                            var_137 = (uint64_t)var_135;
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                            indirect_placeholder_14(var_137, var_136);
                            var_138 = var_0 + (-108L);
                            var_139 = (uint64_t)*var_132;
                            *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                            indirect_placeholder_14(var_139, var_138);
                            *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                            *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                            var_140 = var_0 + (-107L);
                            var_141 = var_0 + (-95L);
                            var_142 = *var_10;
                            *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                            var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                            var_144 = var_143.field_0;
                            var_145 = local_sp_10 + (-40L);
                            *(uint64_t *)var_145 = 4206618UL;
                            var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                            var_147 = var_146.field_1;
                            var_148 = var_146.field_2;
                            *var_16 = (unsigned char)'\x00';
                            local_sp_11 = var_145;
                            r9_8 = var_147;
                            r8_11 = var_148;
                        }
                        r9_9 = r9_8;
                        r8_12 = r8_11;
                        if (*(unsigned char *)4322300UL == '\x01') {
                            var_149 = *var_8;
                            var_150 = *var_7;
                            *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                            indirect_placeholder_5(4UL, var_150, var_149);
                        }
                        var_151 = (uint64_t)*var_16;
                        rax_1 = var_151;
                        mrv.field_0 = rax_1;
                        mrv1 = mrv;
                        mrv1.field_1 = r9_9;
                        mrv2 = mrv1;
                        mrv2.field_2 = r8_12;
                        return mrv2;
                    }
                    var_90 = *(uint32_t *)(*var_15 + 24UL);
                    var_91 = (uint32_t *)(var_0 + (-28L));
                    *var_91 = var_90;
                    var_92 = *(uint64_t *)4322288UL;
                    var_93 = (uint64_t)*(uint32_t *)4322296UL;
                    var_94 = ((uint32_t)((uint16_t)var_90 & (unsigned short)61440U) == 16384U);
                    var_95 = (uint64_t)var_90;
                    var_96 = local_sp_6 + (-8L);
                    *(uint64_t *)var_96 = 4206115UL;
                    var_97 = indirect_placeholder_20(var_92, var_93, var_95, var_94, 0UL);
                    var_98 = (uint32_t *)(var_0 + (-32L));
                    var_99 = (uint32_t)var_97;
                    *var_98 = var_99;
                    local_sp_7 = var_96;
                    r8_7 = 0UL;
                    if (!var_65 & (uint32_t)((uint16_t)*var_91 & (unsigned short)61440U) != 40960U) {
                        var_100 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                        var_101 = (uint64_t)var_99;
                        var_102 = local_sp_6 + (-16L);
                        *(uint64_t *)var_102 = 4206157UL;
                        var_103 = indirect_placeholder_8(var_100);
                        local_sp_0 = var_102;
                        local_sp_7 = var_102;
                        if ((uint64_t)(uint32_t)var_103 == 0UL) {
                            *var_17 = (unsigned char)'\x01';
                        } else {
                            if (*(unsigned char *)4322301UL == '\x01') {
                                var_104 = *var_10;
                                *(uint64_t *)(local_sp_6 + (-24L)) = 4206198UL;
                                var_105 = indirect_placeholder_71(var_101, 4UL, var_104);
                                var_106 = var_105.field_0;
                                var_107 = var_105.field_1;
                                var_108 = var_105.field_2;
                                *(uint64_t *)(local_sp_6 + (-32L)) = 4206206UL;
                                indirect_placeholder_2();
                                var_109 = (uint64_t)*(uint32_t *)var_106;
                                var_110 = local_sp_6 + (-40L);
                                *(uint64_t *)var_110 = 4206233UL;
                                var_111 = indirect_placeholder_70(0UL, var_106, 4298217UL, 0UL, var_109, var_107, var_108);
                                local_sp_0 = var_110;
                                r9_0 = var_111.field_1;
                                r8_0 = var_111.field_2;
                            }
                            *var_16 = (unsigned char)'\x00';
                            local_sp_7 = local_sp_0;
                            r9_7 = r9_0;
                            r8_7 = r8_0;
                        }
                    }
                    r8_10 = r8_7;
                    local_sp_10 = local_sp_7;
                    local_sp_8 = local_sp_7;
                    r8_8 = r8_7;
                    r9_8 = r9_7;
                    if (*(uint32_t *)4321984UL != 2U) {
                        if (*var_17 == '\x00') {
                            local_sp_9 = local_sp_8;
                            r8_9 = r8_8;
                        } else {
                            var_112 = (uint64_t)*(uint32_t *)(*var_7 + 44UL);
                            var_113 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                            var_114 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                            var_115 = *var_10;
                            var_116 = *var_12;
                            var_117 = local_sp_7 + (-8L);
                            *(uint64_t *)var_117 = 4206289UL;
                            var_118 = indirect_placeholder_20(var_114, var_115, var_112, var_116, var_113);
                            local_sp_8 = var_117;
                            local_sp_9 = var_117;
                            r8_8 = var_113;
                            r8_9 = var_113;
                            storemerge = (unsigned char)'\x01';
                            if ((uint64_t)(unsigned char)var_118 == 0UL) {
                                local_sp_9 = local_sp_8;
                                r8_9 = r8_8;
                            }
                        }
                        var_119 = (unsigned char *)(var_0 + (-73L));
                        var_120 = storemerge & '\x01';
                        *var_119 = var_120;
                        local_sp_10 = local_sp_9;
                        r8_10 = r8_9;
                        if (var_120 == '\x00') {
                            if (*(uint32_t *)4321984UL != 0U) {
                                rax_0 = 0U;
                                if (*var_16 != '\x01' & *var_17 == '\x01') {
                                    rax_0 = (var_120 == '\x01') ? 1U : 3U;
                                }
                                *(uint32_t *)(var_0 + (-80L)) = rax_0;
                                var_121 = (uint64_t)rax_0;
                                var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                                var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                                var_124 = *var_10;
                                var_125 = local_sp_9 + (-8L);
                                *(uint64_t *)var_125 = 4206411UL;
                                indirect_placeholder(var_121, var_122, var_124, var_123);
                                local_sp_10 = var_125;
                            }
                        } else {
                            rax_0 = 0U;
                            if (*var_16 != '\x01' & *var_17 == '\x01') {
                                rax_0 = (var_120 == '\x01') ? 1U : 3U;
                            }
                            *(uint32_t *)(var_0 + (-80L)) = rax_0;
                            var_121 = (uint64_t)rax_0;
                            var_122 = (uint64_t)*(uint32_t *)(var_0 + (-32L));
                            var_123 = (uint64_t)*(uint32_t *)(var_0 + (-28L));
                            var_124 = *var_10;
                            var_125 = local_sp_9 + (-8L);
                            *(uint64_t *)var_125 = 4206411UL;
                            indirect_placeholder(var_121, var_122, var_124, var_123);
                            local_sp_10 = var_125;
                        }
                    }
                    local_sp_11 = local_sp_10;
                    r8_11 = r8_10;
                    var_126 = *(uint64_t *)4322288UL;
                    var_127 = *(uint32_t *)(var_0 + (-28L));
                    var_128 = ((uint32_t)((uint16_t)var_127 & (unsigned short)61440U) == 16384U);
                    var_129 = (uint64_t)var_127;
                    var_130 = local_sp_10 + (-8L);
                    *(uint64_t *)var_130 = 4206486UL;
                    var_131 = indirect_placeholder_20(var_126, 0UL, var_129, var_128, 0UL);
                    var_132 = (uint32_t *)(var_0 + (-84L));
                    var_133 = (uint32_t)var_131;
                    *var_132 = var_133;
                    var_134 = var_133 ^ (-1);
                    var_135 = *(uint32_t *)(var_0 + (-32L));
                    local_sp_11 = var_130;
                    r8_11 = 0UL;
                    if (*var_17 != '\x00' & *(unsigned char *)4322302UL != '\x00' & (var_135 & var_134) == 0U) {
                        var_136 = var_0 + (-96L);
                        var_137 = (uint64_t)var_135;
                        *(uint64_t *)(local_sp_10 + (-16L)) = 4206518UL;
                        indirect_placeholder_14(var_137, var_136);
                        var_138 = var_0 + (-108L);
                        var_139 = (uint64_t)*var_132;
                        *(uint64_t *)(local_sp_10 + (-24L)) = 4206535UL;
                        indirect_placeholder_14(var_139, var_138);
                        *(unsigned char *)(var_0 + (-98L)) = (unsigned char)'\x00';
                        *(unsigned char *)(var_0 + (-86L)) = (unsigned char)'\x00';
                        var_140 = var_0 + (-107L);
                        var_141 = var_0 + (-95L);
                        var_142 = *var_10;
                        *(uint64_t *)(local_sp_10 + (-32L)) = 4206584UL;
                        var_143 = indirect_placeholder_69(var_142, 0UL, 3UL);
                        var_144 = var_143.field_0;
                        var_145 = local_sp_10 + (-40L);
                        *(uint64_t *)var_145 = 4206618UL;
                        var_146 = indirect_placeholder_68(0UL, var_144, 4298248UL, 0UL, 0UL, var_140, var_141);
                        var_147 = var_146.field_1;
                        var_148 = var_146.field_2;
                        *var_16 = (unsigned char)'\x00';
                        local_sp_11 = var_145;
                        r9_8 = var_147;
                        r8_11 = var_148;
                    }
                    r9_9 = r9_8;
                    r8_12 = r8_11;
                    if (*(unsigned char *)4322300UL == '\x01') {
                        var_149 = *var_8;
                        var_150 = *var_7;
                        *(uint64_t *)(local_sp_11 + (-8L)) = 4206660UL;
                        indirect_placeholder_5(4UL, var_150, var_149);
                    }
                    var_151 = (uint64_t)*var_16;
                    rax_1 = var_151;
                }
                break;
              case 4205441UL:
                {
                    if (*(unsigned char *)4322301UL != '\x01') {
                        var_46 = *var_10;
                        *(uint64_t *)(var_0 + (-144L)) = 4205477UL;
                        var_47 = indirect_placeholder_86(var_46, 0UL, 3UL);
                        var_48 = var_47.field_0;
                        var_49 = var_47.field_1;
                        var_50 = var_47.field_2;
                        var_51 = (uint64_t)*(uint32_t *)(*var_8 + 64UL);
                        var_52 = var_0 + (-152L);
                        *(uint64_t *)var_52 = 4205512UL;
                        var_53 = indirect_placeholder_82(0UL, var_48, 4297796UL, 0UL, var_51, var_49, var_50);
                        local_sp_4 = var_52;
                        r9_4 = var_53.field_1;
                        r8_4 = var_53.field_2;
                    }
                    *var_16 = (unsigned char)'\x00';
                    local_sp_6 = local_sp_4;
                    r9_6 = r9_4;
                    r8_6 = r8_4;
                }
                break;
              case 4205596UL:
                {
                    if (*(unsigned char *)4322301UL != '\x01') {
                        var_31 = *var_10;
                        *(uint64_t *)(var_0 + (-144L)) = 4205627UL;
                        var_32 = indirect_placeholder_84(4UL, var_31);
                        var_33 = var_32.field_0;
                        var_34 = var_32.field_1;
                        var_35 = var_32.field_2;
                        var_36 = var_0 + (-152L);
                        *(uint64_t *)var_36 = 4205655UL;
                        var_37 = indirect_placeholder_80(0UL, var_33, 4297824UL, 0UL, 0UL, var_34, var_35);
                        local_sp_2 = var_36;
                        r9_2 = var_37.field_1;
                        r8_2 = var_37.field_2;
                    }
                    *var_16 = (unsigned char)'\x00';
                    local_sp_6 = local_sp_2;
                    r9_6 = r9_2;
                    r8_6 = r8_2;
                }
                break;
              case 4205521UL:
                {
                    if (*(unsigned char *)4322301UL != '\x01') {
                        var_38 = *var_10;
                        *(uint64_t *)(var_0 + (-144L)) = 4205552UL;
                        var_39 = indirect_placeholder_85(4UL, var_38);
                        var_40 = var_39.field_0;
                        var_41 = var_39.field_1;
                        var_42 = var_39.field_2;
                        var_43 = (uint64_t)*(uint32_t *)(*var_8 + 64UL);
                        var_44 = var_0 + (-152L);
                        *(uint64_t *)var_44 = 4205587UL;
                        var_45 = indirect_placeholder_81(0UL, var_40, 4297799UL, 0UL, var_43, var_41, var_42);
                        local_sp_3 = var_44;
                        r9_3 = var_45.field_1;
                        r8_3 = var_45.field_2;
                    }
                    *var_16 = (unsigned char)'\x00';
                    local_sp_6 = local_sp_3;
                    r9_6 = r9_3;
                    r8_6 = r8_3;
                }
                break;
            }
        }
        break;
      default:
        {
            abort();
        }
        break;
    }
}
