typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_mode_compile(uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_10;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    unsigned char var_5;
    uint64_t rax_1;
    uint64_t local_sp_0;
    unsigned char var_40;
    uint32_t var_41;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t var_42;
    unsigned char var_43;
    uint32_t var_44;
    uint32_t var_45;
    uint32_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    unsigned char var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint32_t var_56;
    uint32_t rax_0_in;
    uint32_t var_57;
    bool var_58;
    uint32_t var_59;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t storemerge21;
    uint32_t var_11;
    uint32_t var_12;
    uint32_t var_13;
    unsigned char var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint32_t storemerge;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    unsigned char **var_26;
    uint64_t var_27;
    uint64_t var_28;
    struct indirect_placeholder_16_ret_type var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint32_t *var_32;
    unsigned char *var_33;
    uint32_t *var_34;
    unsigned char *var_35;
    uint32_t *var_36;
    uint32_t *var_37;
    uint64_t var_38;
    uint64_t *var_39;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-112L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-16L));
    *var_4 = 0UL;
    var_5 = **(unsigned char **)var_2;
    var_45 = 0U;
    rax_1 = 0UL;
    storemerge21 = 0UL;
    if (!(((signed char)var_5 <= '/') || ((signed char)var_5 > '7'))) {
        var_6 = (uint32_t *)(var_0 + (-28L));
        *var_6 = 0U;
        var_7 = *var_3;
        var_8 = var_0 + (-24L);
        var_9 = (uint64_t *)var_8;
        *var_9 = var_7;
        var_10 = var_7;
        var_11 = *var_6;
        var_12 = var_11 << 3U;
        *var_9 = (var_10 + 1UL);
        var_13 = (var_12 + (uint32_t)*(unsigned char *)var_10) + (-48);
        *var_6 = var_13;
        var_11 = var_13;
        while (var_13 <= 4095U)
            {
                var_14 = **(unsigned char **)var_8;
                if (((signed char)var_14 <= '/') || ((signed char)var_14 > '7')) {
                    var_10 = *var_9;
                    var_12 = var_11 << 3U;
                    *var_9 = (var_10 + 1UL);
                    var_13 = (var_12 + (uint32_t)*(unsigned char *)var_10) + (-48);
                    *var_6 = var_13;
                    var_11 = var_13;
                    continue;
                }
                if (var_14 == '\x00') {
                    break;
                }
                var_15 = (uint64_t)var_13;
                *(uint64_t *)(var_0 + (-128L)) = 4209639UL;
                var_16 = indirect_placeholder_8(var_15);
                var_17 = (uint32_t *)(var_0 + (-64L));
                var_18 = (uint32_t)var_16;
                *var_17 = var_18;
                storemerge = ((long)(*var_9 - *var_3) > (long)4UL) ? 4095U : ((uint32_t)((uint16_t)var_18 & (unsigned short)3072U) | 1023U);
                *(uint32_t *)(var_0 + (-68L)) = storemerge;
                var_19 = (uint64_t)storemerge;
                var_20 = (uint64_t)*var_17;
                *(uint64_t *)(var_0 + (-136L)) = 4209694UL;
                var_21 = indirect_placeholder_3(var_20, var_19);
                rax_1 = var_21;
                break;
            }
        return rax_1;
    }
    var_22 = (uint64_t *)(var_0 + (-40L));
    *var_22 = 1UL;
    var_23 = *var_3;
    var_24 = var_0 + (-24L);
    var_25 = (uint64_t *)var_24;
    *var_25 = var_23;
    while (1U)
        {
            switch_state_var = 1;
            break;
        }
    var_27 = *var_22;
    var_28 = var_0 + (-128L);
    *(uint64_t *)var_28 = 4209801UL;
    var_29 = indirect_placeholder_16(var_27, 16UL, r9, r8);
    var_30 = var_29.field_0;
    var_31 = (uint64_t *)(var_0 + (-80L));
    *var_31 = var_30;
    *var_25 = *var_3;
    var_32 = (uint32_t *)(var_0 + (-44L));
    var_33 = (unsigned char *)(var_0 + (-81L));
    var_34 = (uint32_t *)(var_0 + (-52L));
    var_35 = (unsigned char *)(var_0 + (-53L));
    var_36 = (uint32_t *)(var_0 + (-60L));
    var_37 = (uint32_t *)(var_0 + (-48L));
    var_38 = var_0 + (-96L);
    var_39 = (uint64_t *)var_38;
    local_sp_0 = var_28;
    while (1U)
        {
            *var_32 = 0U;
            local_sp_1 = local_sp_0;
            local_sp_3 = local_sp_0;
            while (1U)
                {
                    var_40 = **var_26;
                    var_41 = (uint32_t)(uint64_t)var_40;
                    if ((uint64_t)(var_41 + (-117)) == 0UL) {
                        *var_32 = (*var_32 | 2496U);
                    } else {
                        if ((signed char)var_40 <= 'u') {
                            loop_state_var = 0U;
                            break;
                        }
                        if ((uint64_t)(var_41 + (-111)) == 0UL) {
                            *var_32 = (*var_32 | 519U);
                        } else {
                            if ((signed char)var_40 <= 'o') {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(var_41 + (-103)) == 0UL) {
                                *var_32 = (*var_32 | 1080U);
                            } else {
                                if ((signed char)var_40 <= 'g') {
                                    loop_state_var = 0U;
                                    break;
                                }
                                if ((uint64_t)(var_41 + (-97)) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_32 = (*var_32 | 4095U);
                            }
                        }
                    }
                    *var_25 = (*var_25 + 1UL);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    if ((signed char)var_40 <= 'a') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if ((uint64_t)(var_41 + (-61)) != 0UL) {
                        if ((signed char)var_40 <= '=') {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if ((uint64_t)(var_41 + (-43)) != 0UL & (uint64_t)(var_41 + (-45)) != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    while (1U)
                        {
                            var_42 = *var_25;
                            *var_25 = (var_42 + 1UL);
                            *var_33 = *(unsigned char *)var_42;
                            *var_34 = 0U;
                            *var_35 = (unsigned char)'\x03';
                            var_43 = **var_26;
                            var_44 = (uint32_t)(uint64_t)var_43;
                            local_sp_2 = local_sp_1;
                            local_sp_3 = local_sp_1;
                            if ((uint64_t)(var_44 + (-117)) == 0UL) {
                                *var_37 = 448U;
                                *var_25 = (*var_25 + 1UL);
                            } else {
                                if ((signed char)var_43 <= 'u') {
                                    *var_37 = 0U;
                                    *var_35 = (unsigned char)'\x01';
                                    var_53 = (uint64_t)((uint32_t)(uint64_t)**var_26 + (-88));
                                    if (var_53 > 32UL) {
                                        var_54 = *(uint64_t *)((var_53 << 3UL) + 4300368UL);
                                        function_dispatcher((unsigned char *)(0UL));
                                        return var_54;
                                    }
                                }
                                if ((uint64_t)(var_44 + (-111)) == 0UL) {
                                    *var_37 = 7U;
                                    *var_25 = (*var_25 + 1UL);
                                } else {
                                    if ((signed char)var_43 <= 'o') {
                                        *var_37 = 0U;
                                        *var_35 = (unsigned char)'\x01';
                                        var_53 = (uint64_t)((uint32_t)(uint64_t)**var_26 + (-88));
                                        if (var_53 > 32UL) {
                                            var_54 = *(uint64_t *)((var_53 << 3UL) + 4300368UL);
                                            function_dispatcher((unsigned char *)(0UL));
                                            return var_54;
                                        }
                                    }
                                    if ((signed char)var_43 > '7') {
                                        if ((uint64_t)(var_44 + (-103)) != 0UL) {
                                            *var_37 = 0U;
                                            *var_35 = (unsigned char)'\x01';
                                            var_53 = (uint64_t)((uint32_t)(uint64_t)**var_26 + (-88));
                                            if (var_53 > 32UL) {
                                                var_54 = *(uint64_t *)((var_53 << 3UL) + 4300368UL);
                                                function_dispatcher((unsigned char *)(0UL));
                                                return var_54;
                                            }
                                        }
                                        *var_37 = 56U;
                                        *var_25 = (*var_25 + 1UL);
                                    } else {
                                        if ((signed char)var_43 >= '0') {
                                            *var_37 = 0U;
                                            *var_35 = (unsigned char)'\x01';
                                            var_53 = (uint64_t)((uint32_t)(uint64_t)**var_26 + (-88));
                                            if (var_53 > 32UL) {
                                                var_54 = *(uint64_t *)((var_53 << 3UL) + 4300368UL);
                                                function_dispatcher((unsigned char *)(0UL));
                                                return var_54;
                                            }
                                        }
                                        *var_36 = 0U;
                                        while (1U)
                                            {
                                                var_46 = var_45 << 3U;
                                                var_47 = *var_25;
                                                *var_25 = (var_47 + 1UL);
                                                var_48 = (var_46 + (uint32_t)*(unsigned char *)var_47) + (-48);
                                                *var_36 = var_48;
                                                var_45 = var_48;
                                                if (var_48 <= 4095U) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                var_49 = **var_26;
                                                if (((signed char)var_49 <= '/') || ((signed char)var_49 > '7')) {
                                                    continue;
                                                }
                                                loop_state_var = 1U;
                                                break;
                                            }
                                        switch_state_var = 0;
                                        switch (loop_state_var) {
                                          case 1U:
                                            {
                                                if (*var_32 != 0U) {
                                                    loop_state_var = 1U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                *var_34 = 4095U;
                                                *var_32 = 4095U;
                                                var_50 = (uint64_t)*var_36;
                                                var_51 = local_sp_1 + (-8L);
                                                *(uint64_t *)var_51 = 4210201UL;
                                                var_52 = indirect_placeholder_8(var_50);
                                                *var_37 = (uint32_t)var_52;
                                                *var_35 = (unsigned char)'\x01';
                                                local_sp_2 = var_51;
                                            }
                                            break;
                                          case 0U:
                                            {
                                                loop_state_var = 1U;
                                                switch_state_var = 1;
                                                break;
                                            }
                                            break;
                                        }
                                        if (switch_state_var)
                                            break;
                                    }
                                }
                            }
                        }
                    switch_state_var = 0;
                    switch (loop_state_var) {
                      case 2U:
                        {
                            *var_25 = (*var_25 + 1UL);
                            continue;
                        }
                        break;
                      case 0U:
                        {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 1U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    *(uint64_t *)(local_sp_3 + (-8L)) = 4210585UL;
    indirect_placeholder_2();
}
