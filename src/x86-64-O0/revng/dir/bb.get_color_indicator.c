typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
uint64_t bb_get_color_indicator(uint64_t rdi, uint64_t rsi) {
    uint32_t *_pre_phi112;
    uint32_t *var_15;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char var_7;
    uint64_t local_sp_1;
    uint64_t var_57;
    uint64_t local_sp_0;
    uint64_t local_sp_9;
    uint64_t *var_52;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint32_t *_pre_phi114;
    uint64_t local_sp_2;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint32_t spec_select;
    uint32_t *var_8;
    uint64_t var_9;
    uint64_t _pre;
    uint64_t var_10;
    uint64_t storemerge4_in_in_in;
    unsigned char var_11;
    uint32_t *var_12;
    uint64_t local_sp_3;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_16;
    uint64_t local_sp_8;
    uint32_t var_17;
    uint32_t *var_18;
    uint32_t *var_19;
    uint32_t *var_37;
    uint16_t var_38;
    uint16_t _pre_phi116;
    uint64_t var_39;
    uint64_t var_40;
    uint16_t _pre115;
    uint64_t local_sp_4;
    uint64_t local_sp_5;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t *var_25;
    uint32_t var_26;
    bool var_27;
    uint32_t var_28;
    uint32_t _pre_phi118;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t _pre109;
    uint32_t _pre117;
    uint32_t var_31;
    uint64_t local_sp_6;
    uint32_t var_34;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t _pre110;
    uint64_t local_sp_7;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t *var_24;
    uint32_t *var_23;
    uint32_t *var_22;
    uint32_t *var_21;
    uint32_t *var_20;
    uint32_t *var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t storemerge5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    var_4 = var_0 + (-64L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (unsigned char *)(var_0 + (-68L));
    var_7 = (unsigned char)rsi;
    *var_6 = var_7;
    local_sp_3 = var_3;
    if (var_7 == '\x00') {
        *(uint64_t *)(var_0 + (-32L)) = *(uint64_t *)(*var_5 + 8UL);
        *(uint32_t *)(var_0 + (-36L)) = *(uint32_t *)(*var_5 + 172UL);
        spec_select = (*(unsigned char *)(*var_5 + 185UL) == '\x00') ? 4294967295U : 0U;
        var_8 = (uint32_t *)(var_0 + (-40L));
        *var_8 = spec_select;
        _pre_phi112 = var_8;
    } else {
        *(uint64_t *)(var_0 + (-32L)) = **(uint64_t **)var_4;
        if (*(unsigned char *)4429720UL == '\x00') {
            _pre = *var_5;
            var_10 = _pre;
            storemerge4_in_in_in = var_10 + 48UL;
        } else {
            var_9 = *var_5;
            var_10 = var_9;
            if (*(unsigned char *)(var_9 + 185UL) == '\x00') {
                storemerge4_in_in_in = var_10 + 48UL;
            } else {
                storemerge4_in_in_in = var_9 + 172UL;
            }
        }
        *(uint32_t *)(var_0 + (-36L)) = *(uint32_t *)storemerge4_in_in_in;
        var_11 = *(unsigned char *)(*var_5 + 185UL);
        var_12 = (uint32_t *)(var_0 + (-40L));
        *var_12 = (uint32_t)var_11;
        _pre_phi112 = var_12;
    }
    if (*_pre_phi112 != 4294967295U) {
        var_13 = var_0 + (-80L);
        *(uint64_t *)var_13 = 4240949UL;
        var_14 = indirect_placeholder_13(12UL);
        local_sp_3 = var_13;
        local_sp_8 = var_13;
        if ((uint64_t)(unsigned char)var_14 != 0UL) {
            var_15 = (uint32_t *)(var_0 + (-12L));
            *var_15 = 12U;
            _pre_phi114 = var_15;
            var_50 = var_0 + (-24L);
            var_51 = (uint64_t *)var_50;
            *var_51 = 0UL;
            local_sp_9 = local_sp_8;
            if (*_pre_phi114 != 5U) {
                var_52 = (uint64_t *)(var_0 + (-32L));
                var_53 = *var_52;
                var_54 = local_sp_8 + (-8L);
                *(uint64_t *)var_54 = 4241543UL;
                indirect_placeholder_2();
                var_55 = (uint64_t *)(var_0 + (-48L));
                *var_55 = var_53;
                *var_52 = (*var_52 + var_53);
                var_56 = *(uint64_t *)4429848UL;
                *var_51 = var_56;
                var_57 = var_56;
                local_sp_0 = var_54;
                local_sp_1 = local_sp_0;
                local_sp_9 = local_sp_0;
                while (var_57 != 0UL)
                    {
                        var_58 = **(uint64_t **)var_50;
                        var_59 = helper_cc_compute_c_wrapper(*var_55 - var_58, var_58, var_2, 17U);
                        if (var_59 != 0UL) {
                            var_60 = *(uint64_t *)(*var_51 + 8UL);
                            var_61 = local_sp_0 + (-8L);
                            *(uint64_t *)var_61 = 4241627UL;
                            indirect_placeholder_2();
                            local_sp_1 = var_61;
                            local_sp_9 = var_61;
                            if ((uint64_t)(uint32_t)var_60 == 0UL) {
                                break;
                            }
                        }
                        var_62 = *(uint64_t *)(*var_51 + 32UL);
                        *var_51 = var_62;
                        var_57 = var_62;
                        local_sp_0 = local_sp_1;
                        local_sp_1 = local_sp_0;
                        local_sp_9 = local_sp_0;
                    }
            }
            if (*_pre_phi114 != 7U & *_pre_phi112 != 0U) {
                if (*(unsigned char *)4429720UL == '\x00') {
                    *_pre_phi114 = 13U;
                } else {
                    *(uint64_t *)(local_sp_9 + (-8L)) = 4241686UL;
                    var_63 = indirect_placeholder_13(13UL);
                    if ((uint64_t)(unsigned char)var_63 == 0UL) {
                        *_pre_phi114 = 13U;
                    }
                }
            }
            var_64 = *var_51;
            if (var_64 == 0UL) {
                storemerge5 = ((uint64_t)*_pre_phi114 << 4UL) + 4428864UL;
            } else {
                storemerge5 = var_64 + 16UL;
            }
            *(uint64_t *)(var_0 + (-56L)) = storemerge5;
            return (*(uint64_t *)(storemerge5 + 8UL) == 0UL) ? 0UL : storemerge5;
        }
    }
    var_16 = *var_5;
    local_sp_4 = local_sp_3;
    local_sp_6 = local_sp_3;
    local_sp_8 = local_sp_3;
    if (*(unsigned char *)(var_16 + 184UL) != '\x01') {
        var_17 = *(uint32_t *)(((uint64_t)*(uint32_t *)(var_16 + 168UL) << 2UL) + 4429280UL);
        var_18 = (uint32_t *)(var_0 + (-12L));
        *var_18 = var_17;
        _pre_phi114 = var_18;
        var_50 = var_0 + (-24L);
        var_51 = (uint64_t *)var_50;
        *var_51 = 0UL;
        local_sp_9 = local_sp_8;
        if (*_pre_phi114 != 5U) {
            var_52 = (uint64_t *)(var_0 + (-32L));
            var_53 = *var_52;
            var_54 = local_sp_8 + (-8L);
            *(uint64_t *)var_54 = 4241543UL;
            indirect_placeholder_2();
            var_55 = (uint64_t *)(var_0 + (-48L));
            *var_55 = var_53;
            *var_52 = (*var_52 + var_53);
            var_56 = *(uint64_t *)4429848UL;
            *var_51 = var_56;
            var_57 = var_56;
            local_sp_0 = var_54;
            local_sp_1 = local_sp_0;
            local_sp_9 = local_sp_0;
            while (var_57 != 0UL)
                {
                    var_58 = **(uint64_t **)var_50;
                    var_59 = helper_cc_compute_c_wrapper(*var_55 - var_58, var_58, var_2, 17U);
                    if (var_59 != 0UL) {
                        var_60 = *(uint64_t *)(*var_51 + 8UL);
                        var_61 = local_sp_0 + (-8L);
                        *(uint64_t *)var_61 = 4241627UL;
                        indirect_placeholder_2();
                        local_sp_1 = var_61;
                        local_sp_9 = var_61;
                        if ((uint64_t)(uint32_t)var_60 == 0UL) {
                            break;
                        }
                    }
                    var_62 = *(uint64_t *)(*var_51 + 32UL);
                    *var_51 = var_62;
                    var_57 = var_62;
                    local_sp_0 = local_sp_1;
                    local_sp_1 = local_sp_0;
                    local_sp_9 = local_sp_0;
                }
        }
        if (*_pre_phi114 != 7U & *_pre_phi112 != 0U) {
            if (*(unsigned char *)4429720UL == '\x00') {
                *_pre_phi114 = 13U;
            } else {
                *(uint64_t *)(local_sp_9 + (-8L)) = 4241686UL;
                var_63 = indirect_placeholder_13(13UL);
                if ((uint64_t)(unsigned char)var_63 == 0UL) {
                    *_pre_phi114 = 13U;
                }
            }
        }
        var_64 = *var_51;
        if (var_64 == 0UL) {
            storemerge5 = ((uint64_t)*_pre_phi114 << 4UL) + 4428864UL;
        } else {
            storemerge5 = var_64 + 16UL;
        }
        *(uint64_t *)(var_0 + (-56L)) = storemerge5;
        return (*(uint64_t *)(storemerge5 + 8UL) == 0UL) ? 0UL : storemerge5;
    }
    var_19 = (uint32_t *)(var_0 + (-36L));
    switch (((uint32_t)((uint16_t)*var_19 & (unsigned short)61440U) - 4096U) >> 12U) {
      case 9U:
        {
            var_24 = (uint32_t *)(var_0 + (-12L));
            *var_24 = 7U;
            _pre_phi114 = var_24;
        }
        break;
      case 0U:
        {
            var_23 = (uint32_t *)(var_0 + (-12L));
            *var_23 = 8U;
            _pre_phi114 = var_23;
        }
        break;
      case 11U:
        {
            var_22 = (uint32_t *)(var_0 + (-12L));
            *var_22 = 9U;
            _pre_phi114 = var_22;
        }
        break;
      case 5U:
        {
            var_21 = (uint32_t *)(var_0 + (-12L));
            *var_21 = 10U;
            _pre_phi114 = var_21;
        }
        break;
      case 1U:
        {
            var_20 = (uint32_t *)(var_0 + (-12L));
            *var_20 = 11U;
            _pre_phi114 = var_20;
        }
        break;
      case 7U:
        {
            var_37 = (uint32_t *)(var_0 + (-12L));
            *var_37 = 5U;
            var_38 = (uint16_t)*var_19;
            _pre_phi116 = var_38;
            _pre_phi114 = var_37;
            if ((uint32_t)(var_38 & (unsigned short)2048U) == 0U) {
                var_39 = local_sp_3 + (-8L);
                *(uint64_t *)var_39 = 4241058UL;
                var_40 = indirect_placeholder_13(16UL);
                local_sp_4 = var_39;
                local_sp_8 = var_39;
                if ((uint64_t)(unsigned char)var_40 == 0UL) {
                    *var_37 = 16U;
                } else {
                    _pre115 = (uint16_t)*var_19;
                    _pre_phi116 = _pre115;
                    local_sp_5 = local_sp_4;
                    if ((uint32_t)(_pre_phi116 & (unsigned short)1024U) == 0U) {
                        var_41 = local_sp_4 + (-8L);
                        *(uint64_t *)var_41 = 4241096UL;
                        var_42 = indirect_placeholder_13(17UL);
                        local_sp_5 = var_41;
                        local_sp_8 = var_41;
                        if ((uint64_t)(unsigned char)var_42 == 0UL) {
                            *var_37 = 17U;
                        } else {
                            var_43 = local_sp_5 + (-8L);
                            *(uint64_t *)var_43 = 4241122UL;
                            var_44 = indirect_placeholder_13(21UL);
                            local_sp_2 = var_43;
                            local_sp_8 = var_43;
                            if ((uint64_t)(unsigned char)var_44 == 0UL) {
                                if (*(unsigned char *)(*var_5 + 192UL) == '\x00') {
                                    *var_37 = 21U;
                                } else {
                                    if ((*var_19 & 73U) == 0U) {
                                        var_45 = local_sp_5 + (-16L);
                                        *(uint64_t *)var_45 = 4241173UL;
                                        var_46 = indirect_placeholder_13(14UL);
                                        local_sp_2 = var_45;
                                        local_sp_8 = var_45;
                                        if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                            *var_37 = 14U;
                                        } else {
                                            local_sp_8 = local_sp_2;
                                            var_47 = local_sp_2 + (-8L);
                                            *(uint64_t *)var_47 = 4241217UL;
                                            var_48 = indirect_placeholder_13(22UL);
                                            local_sp_8 = var_47;
                                            if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                                *var_37 = 22U;
                                            }
                                        }
                                    } else {
                                        local_sp_8 = local_sp_2;
                                        var_47 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_47 = 4241217UL;
                                        var_48 = indirect_placeholder_13(22UL);
                                        local_sp_8 = var_47;
                                        if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                            *var_37 = 22U;
                                        }
                                    }
                                }
                            } else {
                                if ((*var_19 & 73U) == 0U) {
                                    var_45 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_45 = 4241173UL;
                                    var_46 = indirect_placeholder_13(14UL);
                                    local_sp_2 = var_45;
                                    local_sp_8 = var_45;
                                    if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                        *var_37 = 14U;
                                    } else {
                                        local_sp_8 = local_sp_2;
                                        var_47 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_47 = 4241217UL;
                                        var_48 = indirect_placeholder_13(22UL);
                                        local_sp_8 = var_47;
                                        if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                            *var_37 = 22U;
                                        }
                                    }
                                } else {
                                    local_sp_8 = local_sp_2;
                                    var_47 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_47 = 4241217UL;
                                    var_48 = indirect_placeholder_13(22UL);
                                    local_sp_8 = var_47;
                                    if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                        *var_37 = 22U;
                                    }
                                }
                            }
                        }
                    } else {
                        var_43 = local_sp_5 + (-8L);
                        *(uint64_t *)var_43 = 4241122UL;
                        var_44 = indirect_placeholder_13(21UL);
                        local_sp_2 = var_43;
                        local_sp_8 = var_43;
                        if ((uint64_t)(unsigned char)var_44 == 0UL) {
                            if (*(unsigned char *)(*var_5 + 192UL) == '\x00') {
                                *var_37 = 21U;
                            } else {
                                if ((*var_19 & 73U) == 0U) {
                                    var_45 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_45 = 4241173UL;
                                    var_46 = indirect_placeholder_13(14UL);
                                    local_sp_2 = var_45;
                                    local_sp_8 = var_45;
                                    if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                        *var_37 = 14U;
                                    } else {
                                        local_sp_8 = local_sp_2;
                                        var_47 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_47 = 4241217UL;
                                        var_48 = indirect_placeholder_13(22UL);
                                        local_sp_8 = var_47;
                                        if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                            *var_37 = 22U;
                                        }
                                    }
                                } else {
                                    local_sp_8 = local_sp_2;
                                    var_47 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_47 = 4241217UL;
                                    var_48 = indirect_placeholder_13(22UL);
                                    local_sp_8 = var_47;
                                    if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                        *var_37 = 22U;
                                    }
                                }
                            }
                        } else {
                            if ((*var_19 & 73U) == 0U) {
                                var_45 = local_sp_5 + (-16L);
                                *(uint64_t *)var_45 = 4241173UL;
                                var_46 = indirect_placeholder_13(14UL);
                                local_sp_2 = var_45;
                                local_sp_8 = var_45;
                                if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                    *var_37 = 14U;
                                } else {
                                    local_sp_8 = local_sp_2;
                                    var_47 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_47 = 4241217UL;
                                    var_48 = indirect_placeholder_13(22UL);
                                    local_sp_8 = var_47;
                                    if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                        *var_37 = 22U;
                                    }
                                }
                            } else {
                                local_sp_8 = local_sp_2;
                                var_47 = local_sp_2 + (-8L);
                                *(uint64_t *)var_47 = 4241217UL;
                                var_48 = indirect_placeholder_13(22UL);
                                local_sp_8 = var_47;
                                if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                    *var_37 = 22U;
                                }
                            }
                        }
                    }
                }
            } else {
                local_sp_5 = local_sp_4;
                if ((uint32_t)(_pre_phi116 & (unsigned short)1024U) == 0U) {
                    var_41 = local_sp_4 + (-8L);
                    *(uint64_t *)var_41 = 4241096UL;
                    var_42 = indirect_placeholder_13(17UL);
                    local_sp_5 = var_41;
                    local_sp_8 = var_41;
                    if ((uint64_t)(unsigned char)var_42 == 0UL) {
                        *var_37 = 17U;
                    } else {
                        var_43 = local_sp_5 + (-8L);
                        *(uint64_t *)var_43 = 4241122UL;
                        var_44 = indirect_placeholder_13(21UL);
                        local_sp_2 = var_43;
                        local_sp_8 = var_43;
                        if ((uint64_t)(unsigned char)var_44 == 0UL) {
                            if (*(unsigned char *)(*var_5 + 192UL) == '\x00') {
                                *var_37 = 21U;
                            } else {
                                if ((*var_19 & 73U) == 0U) {
                                    var_45 = local_sp_5 + (-16L);
                                    *(uint64_t *)var_45 = 4241173UL;
                                    var_46 = indirect_placeholder_13(14UL);
                                    local_sp_2 = var_45;
                                    local_sp_8 = var_45;
                                    if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                        *var_37 = 14U;
                                    } else {
                                        local_sp_8 = local_sp_2;
                                        var_47 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_47 = 4241217UL;
                                        var_48 = indirect_placeholder_13(22UL);
                                        local_sp_8 = var_47;
                                        if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                            *var_37 = 22U;
                                        }
                                    }
                                } else {
                                    local_sp_8 = local_sp_2;
                                    var_47 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_47 = 4241217UL;
                                    var_48 = indirect_placeholder_13(22UL);
                                    local_sp_8 = var_47;
                                    if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                        *var_37 = 22U;
                                    }
                                }
                            }
                        } else {
                            if ((*var_19 & 73U) == 0U) {
                                var_45 = local_sp_5 + (-16L);
                                *(uint64_t *)var_45 = 4241173UL;
                                var_46 = indirect_placeholder_13(14UL);
                                local_sp_2 = var_45;
                                local_sp_8 = var_45;
                                if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                    *var_37 = 14U;
                                } else {
                                    local_sp_8 = local_sp_2;
                                    var_47 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_47 = 4241217UL;
                                    var_48 = indirect_placeholder_13(22UL);
                                    local_sp_8 = var_47;
                                    if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                        *var_37 = 22U;
                                    }
                                }
                            } else {
                                local_sp_8 = local_sp_2;
                                var_47 = local_sp_2 + (-8L);
                                *(uint64_t *)var_47 = 4241217UL;
                                var_48 = indirect_placeholder_13(22UL);
                                local_sp_8 = var_47;
                                if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                    *var_37 = 22U;
                                }
                            }
                        }
                    }
                } else {
                    var_43 = local_sp_5 + (-8L);
                    *(uint64_t *)var_43 = 4241122UL;
                    var_44 = indirect_placeholder_13(21UL);
                    local_sp_2 = var_43;
                    local_sp_8 = var_43;
                    if ((uint64_t)(unsigned char)var_44 == 0UL) {
                        if (*(unsigned char *)(*var_5 + 192UL) == '\x00') {
                            *var_37 = 21U;
                        } else {
                            if ((*var_19 & 73U) == 0U) {
                                var_45 = local_sp_5 + (-16L);
                                *(uint64_t *)var_45 = 4241173UL;
                                var_46 = indirect_placeholder_13(14UL);
                                local_sp_2 = var_45;
                                local_sp_8 = var_45;
                                if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                    *var_37 = 14U;
                                } else {
                                    local_sp_8 = local_sp_2;
                                    var_47 = local_sp_2 + (-8L);
                                    *(uint64_t *)var_47 = 4241217UL;
                                    var_48 = indirect_placeholder_13(22UL);
                                    local_sp_8 = var_47;
                                    if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                        *var_37 = 22U;
                                    }
                                }
                            } else {
                                local_sp_8 = local_sp_2;
                                var_47 = local_sp_2 + (-8L);
                                *(uint64_t *)var_47 = 4241217UL;
                                var_48 = indirect_placeholder_13(22UL);
                                local_sp_8 = var_47;
                                if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                    *var_37 = 22U;
                                }
                            }
                        }
                    } else {
                        if ((*var_19 & 73U) == 0U) {
                            var_45 = local_sp_5 + (-16L);
                            *(uint64_t *)var_45 = 4241173UL;
                            var_46 = indirect_placeholder_13(14UL);
                            local_sp_2 = var_45;
                            local_sp_8 = var_45;
                            if ((uint64_t)(unsigned char)var_46 == 0UL) {
                                *var_37 = 14U;
                            } else {
                                local_sp_8 = local_sp_2;
                                var_47 = local_sp_2 + (-8L);
                                *(uint64_t *)var_47 = 4241217UL;
                                var_48 = indirect_placeholder_13(22UL);
                                local_sp_8 = var_47;
                                if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                    *var_37 = 22U;
                                }
                            }
                        } else {
                            local_sp_8 = local_sp_2;
                            var_47 = local_sp_2 + (-8L);
                            *(uint64_t *)var_47 = 4241217UL;
                            var_48 = indirect_placeholder_13(22UL);
                            local_sp_8 = var_47;
                            if (*(uint64_t *)(*var_5 + 40UL) <= 1UL & (uint64_t)(unsigned char)var_48 == 0UL) {
                                *var_37 = 22U;
                            }
                        }
                    }
                }
            }
        }
        break;
      case 3U:
        {
            var_25 = (uint32_t *)(var_0 + (-12L));
            *var_25 = 6U;
            var_26 = *var_19;
            var_27 = ((uint32_t)((uint16_t)var_26 & (unsigned short)512U) == 0U);
            var_28 = var_26 & 2U;
            _pre_phi118 = var_28;
            var_31 = var_26;
            _pre_phi114 = var_25;
            if (var_27 || (var_28 == 0U)) {
                var_29 = local_sp_3 + (-8L);
                *(uint64_t *)var_29 = 4241295UL;
                var_30 = indirect_placeholder_13(20UL);
                local_sp_6 = var_29;
                local_sp_8 = var_29;
                if ((uint64_t)(unsigned char)var_30 == 0UL) {
                    *var_25 = 20U;
                } else {
                    _pre109 = *var_19;
                    _pre117 = _pre109 & 2U;
                    _pre_phi118 = _pre117;
                    var_31 = _pre109;
                    var_34 = var_31;
                    local_sp_7 = local_sp_6;
                    if (_pre_phi118 == 0U) {
                        var_32 = local_sp_6 + (-8L);
                        *(uint64_t *)var_32 = 4241331UL;
                        var_33 = indirect_placeholder_13(19UL);
                        local_sp_7 = var_32;
                        local_sp_8 = var_32;
                        if ((uint64_t)(unsigned char)var_33 == 0UL) {
                            *var_25 = 19U;
                        } else {
                            _pre110 = *var_19;
                            var_34 = _pre110;
                            local_sp_8 = local_sp_7;
                            var_35 = local_sp_7 + (-8L);
                            *(uint64_t *)var_35 = 4241373UL;
                            var_36 = indirect_placeholder_13(18UL);
                            local_sp_8 = var_35;
                            if ((uint32_t)((uint16_t)var_34 & (unsigned short)512U) != 0U & (uint64_t)(unsigned char)var_36 == 0UL) {
                                *var_25 = 18U;
                            }
                        }
                    } else {
                        local_sp_8 = local_sp_7;
                        var_35 = local_sp_7 + (-8L);
                        *(uint64_t *)var_35 = 4241373UL;
                        var_36 = indirect_placeholder_13(18UL);
                        local_sp_8 = var_35;
                        if ((uint32_t)((uint16_t)var_34 & (unsigned short)512U) != 0U & (uint64_t)(unsigned char)var_36 == 0UL) {
                            *var_25 = 18U;
                        }
                    }
                }
            } else {
                var_34 = var_31;
                local_sp_7 = local_sp_6;
                if (_pre_phi118 == 0U) {
                    var_32 = local_sp_6 + (-8L);
                    *(uint64_t *)var_32 = 4241331UL;
                    var_33 = indirect_placeholder_13(19UL);
                    local_sp_7 = var_32;
                    local_sp_8 = var_32;
                    if ((uint64_t)(unsigned char)var_33 == 0UL) {
                        *var_25 = 19U;
                    } else {
                        _pre110 = *var_19;
                        var_34 = _pre110;
                        local_sp_8 = local_sp_7;
                        var_35 = local_sp_7 + (-8L);
                        *(uint64_t *)var_35 = 4241373UL;
                        var_36 = indirect_placeholder_13(18UL);
                        local_sp_8 = var_35;
                        if ((uint32_t)((uint16_t)var_34 & (unsigned short)512U) != 0U & (uint64_t)(unsigned char)var_36 == 0UL) {
                            *var_25 = 18U;
                        }
                    }
                } else {
                    local_sp_8 = local_sp_7;
                    var_35 = local_sp_7 + (-8L);
                    *(uint64_t *)var_35 = 4241373UL;
                    var_36 = indirect_placeholder_13(18UL);
                    local_sp_8 = var_35;
                    if ((uint32_t)((uint16_t)var_34 & (unsigned short)512U) != 0U & (uint64_t)(unsigned char)var_36 == 0UL) {
                        *var_25 = 18U;
                    }
                }
            }
        }
        break;
      default:
        {
            var_49 = (uint32_t *)(var_0 + (-12L));
            *var_49 = 13U;
            _pre_phi114 = var_49;
        }
        break;
    }
}
