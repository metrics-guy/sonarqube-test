typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_verrevcmp(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi) {
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint32_t *var_12;
    uint64_t local_sp_4;
    uint32_t var_54;
    uint64_t rax_2;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_1;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t local_sp_8;
    uint64_t local_sp_0;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_31;
    uint64_t rax_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_2;
    uint64_t var_35;
    uint64_t rax_1;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t local_sp_3;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t local_sp_5;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_21;
    uint64_t local_sp_6;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_7_ph;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_55;
    uint64_t var_56;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    var_4 = (uint64_t *)(var_0 + (-48L));
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-56L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-64L));
    *var_6 = rdx;
    var_7 = (uint64_t *)var_3;
    *var_7 = rcx;
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-24L));
    *var_9 = 0UL;
    var_10 = (uint32_t *)(var_0 + (-28L));
    var_11 = (uint32_t *)(var_0 + (-32L));
    var_12 = (uint32_t *)(var_0 + (-36L));
    rax_0 = 0UL;
    rax_1 = 0UL;
    local_sp_4 = var_3;
    rax_2 = 0UL;
    while (1U)
        {
            var_13 = *var_8;
            var_14 = *var_5;
            var_15 = helper_cc_compute_c_wrapper(var_13 - var_14, var_14, var_2, 17U);
            local_sp_5 = local_sp_4;
            if (var_15 != 0UL) {
                var_16 = *var_9;
                var_17 = *var_7;
                var_18 = helper_cc_compute_c_wrapper(var_16 - var_17, var_17, var_2, 17U);
                if (var_18 == 0UL) {
                    break;
                }
            }
            *var_10 = 0U;
            rax_2 = 1UL;
            while (1U)
                {
                    var_19 = *var_8;
                    var_20 = *var_5;
                    var_21 = helper_cc_compute_c_wrapper(var_19 - var_20, var_20, var_2, 17U);
                    local_sp_6 = local_sp_5;
                    if (var_21 == 0UL) {
                        var_22 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_8 + *var_4);
                        var_23 = local_sp_5 + (-8L);
                        *(uint64_t *)var_23 = 4251790UL;
                        var_24 = indirect_placeholder_13(var_22);
                        local_sp_1 = var_23;
                        local_sp_6 = var_23;
                        if ((uint64_t)(unsigned char)var_24 != 1UL) {
                            var_25 = *var_9;
                            var_26 = *var_7;
                            var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_2, 17U);
                            local_sp_7_ph = local_sp_6;
                            if (var_27 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_28 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_9 + *var_6);
                            var_29 = local_sp_6 + (-8L);
                            *(uint64_t *)var_29 = 4251835UL;
                            var_30 = indirect_placeholder_13(var_28);
                            local_sp_1 = var_29;
                            local_sp_7_ph = var_29;
                            if ((uint64_t)(unsigned char)var_30 != 1UL) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                    }
                    var_25 = *var_9;
                    var_26 = *var_7;
                    var_27 = helper_cc_compute_c_wrapper(var_25 - var_26, var_26, var_2, 17U);
                    local_sp_7_ph = local_sp_6;
                    if (var_27 == 0UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_28 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_9 + *var_6);
                    var_29 = local_sp_6 + (-8L);
                    *(uint64_t *)var_29 = 4251835UL;
                    var_30 = indirect_placeholder_13(var_28);
                    local_sp_1 = var_29;
                    local_sp_7_ph = var_29;
                    if ((uint64_t)(unsigned char)var_30 != 1UL) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_31 = *var_8;
                    local_sp_2 = local_sp_1;
                    if (var_31 == *var_5) {
                        var_32 = (uint64_t)*(unsigned char *)(var_31 + *var_4);
                        var_33 = local_sp_1 + (-8L);
                        *(uint64_t *)var_33 = 4251673UL;
                        var_34 = indirect_placeholder_13(var_32);
                        rax_0 = var_34;
                        local_sp_2 = var_33;
                    }
                    *var_11 = (uint32_t)rax_0;
                    var_35 = *var_9;
                    local_sp_3 = local_sp_2;
                    if (var_35 == *var_7) {
                        var_36 = (uint64_t)*(unsigned char *)(var_35 + *var_6);
                        var_37 = local_sp_2 + (-8L);
                        *(uint64_t *)var_37 = 4251717UL;
                        var_38 = indirect_placeholder_13(var_36);
                        rax_1 = var_38;
                        local_sp_3 = var_37;
                    }
                    var_39 = (uint32_t)rax_1;
                    *var_12 = var_39;
                    var_40 = (uint64_t)(*var_11 - var_39);
                    rax_2 = var_40;
                    local_sp_5 = local_sp_3;
                    if (var_40 != 0UL) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_8 = (*var_8 + 1UL);
                    *var_9 = (*var_9 + 1UL);
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    var_41 = *var_8;
                    local_sp_8 = local_sp_7_ph;
                    while (*(unsigned char *)(var_41 + *var_4) != '0')
                        {
                            var_56 = var_41 + 1UL;
                            *var_8 = var_56;
                            var_41 = var_56;
                        }
                    var_42 = *var_9;
                    while (*(unsigned char *)(var_42 + *var_6) != '0')
                        {
                            var_55 = var_42 + 1UL;
                            *var_9 = var_55;
                            var_42 = var_55;
                        }
                    var_43 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_8 + *var_4);
                    var_44 = local_sp_8 + (-8L);
                    *(uint64_t *)var_44 = 4251979UL;
                    var_45 = indirect_placeholder_13(var_43);
                    local_sp_0 = var_44;
                    while ((uint64_t)(unsigned char)var_45 != 0UL)
                        {
                            var_46 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_9 + *var_6);
                            var_47 = local_sp_8 + (-16L);
                            *(uint64_t *)var_47 = 4252007UL;
                            var_48 = indirect_placeholder_13(var_46);
                            local_sp_0 = var_47;
                            local_sp_8 = var_47;
                            if ((uint64_t)(unsigned char)var_48 == 0UL) {
                                break;
                            }
                            if (*var_10 == 0U) {
                                *var_10 = ((uint32_t)*(unsigned char *)(*var_8 + *var_4) - (uint32_t)*(unsigned char *)(*var_9 + *var_6));
                            }
                            *var_8 = (*var_8 + 1UL);
                            *var_9 = (*var_9 + 1UL);
                            var_43 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_8 + *var_4);
                            var_44 = local_sp_8 + (-8L);
                            *(uint64_t *)var_44 = 4251979UL;
                            var_45 = indirect_placeholder_13(var_43);
                            local_sp_0 = var_44;
                        }
                    var_49 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_8 + *var_4);
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4252035UL;
                    var_50 = indirect_placeholder_13(var_49);
                    if ((uint64_t)(unsigned char)var_50 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    var_51 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_9 + *var_6);
                    var_52 = local_sp_0 + (-16L);
                    *(uint64_t *)var_52 = 4252070UL;
                    var_53 = indirect_placeholder_13(var_51);
                    local_sp_4 = var_52;
                    rax_2 = 4294967295UL;
                    if ((uint64_t)(unsigned char)var_53 == 0UL) {
                        switch_state_var = 1;
                        break;
                    }
                    var_54 = *var_10;
                    if (var_54 == 0U) {
                        continue;
                    }
                    rax_2 = (uint64_t)var_54;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return rax_2;
}
