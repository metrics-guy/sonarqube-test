typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_116_ret_type;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_118_ret_type;
struct indirect_placeholder_119_ret_type;
struct indirect_placeholder_120_ret_type;
struct indirect_placeholder_121_ret_type;
struct indirect_placeholder_122_ret_type;
struct indirect_placeholder_123_ret_type;
struct indirect_placeholder_116_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_118_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_119_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_120_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_121_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_122_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_123_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern uint64_t indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_12(uint64_t param_0);
extern uint64_t init_r12(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_116_ret_type indirect_placeholder_116(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_118_ret_type indirect_placeholder_118(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_119_ret_type indirect_placeholder_119(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_120_ret_type indirect_placeholder_120(uint64_t param_0);
extern struct indirect_placeholder_121_ret_type indirect_placeholder_121(uint64_t param_0);
extern struct indirect_placeholder_122_ret_type indirect_placeholder_122(uint64_t param_0);
extern struct indirect_placeholder_123_ret_type indirect_placeholder_123(uint64_t param_0);
void bb_print_long_format(uint64_t rdi) {
    uint64_t var_29;
    uint64_t rax_1;
    uint64_t *var_36;
    uint32_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t *var_40;
    uint64_t var_35;
    uint64_t var_89;
    struct indirect_placeholder_118_ret_type var_115;
    struct indirect_placeholder_116_ret_type var_132;
    struct indirect_placeholder_117_ret_type var_125;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_155;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    unsigned char storemerge6;
    unsigned char *var_133;
    unsigned char var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t local_sp_4;
    uint64_t var_117;
    uint64_t local_sp_2;
    uint64_t var_118;
    uint64_t *var_119;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_107;
    uint64_t *var_108;
    uint64_t var_109;
    uint64_t *var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_116;
    uint64_t local_sp_13;
    uint32_t var_96;
    uint64_t local_sp_9;
    uint64_t var_97;
    uint64_t var_98;
    unsigned char var_99;
    uint64_t var_156;
    uint32_t var_157;
    uint64_t local_sp_3;
    uint64_t var_74;
    uint64_t *var_100;
    unsigned char **var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    struct indirect_placeholder_119_ret_type var_106;
    uint64_t var_140;
    uint64_t local_sp_5;
    uint64_t storemerge7;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t local_sp_6;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t *var_149;
    uint64_t var_150;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t local_sp_7;
    uint64_t local_sp_8;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t local_sp_10;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t local_sp_12;
    uint64_t var_54;
    uint64_t local_sp_18;
    uint64_t local_sp_11;
    unsigned char var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t rax_0;
    uint64_t *var_90;
    uint32_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint32_t *var_94;
    uint32_t var_95;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_55;
    uint64_t local_sp_16;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned char var_45;
    uint64_t local_sp_17;
    uint64_t var_46;
    uint32_t var_47;
    uint64_t local_sp_14;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t local_sp_15;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t var_41;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_48;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_19;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    struct indirect_placeholder_121_ret_type var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    struct indirect_placeholder_122_ret_type var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    struct indirect_placeholder_123_ret_type var_20;
    uint64_t var_21;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = init_r12();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    *(uint64_t *)(var_0 + (-24L)) = var_2;
    var_4 = (uint64_t *)(var_0 + (-2400L));
    *var_4 = rdi;
    storemerge6 = (unsigned char)'\x00';
    storemerge7 = 4383182UL;
    rax_0 = 4383182UL;
    rax_1 = 4383182UL;
    if (*(unsigned char *)(rdi + 184UL) == '\x00') {
        *(unsigned char *)(var_0 + (-100L)) = *(unsigned char *)((uint64_t)*(uint32_t *)(rdi + 168UL) + 4375216UL);
        var_7 = var_0 + (-2416L);
        *(uint64_t *)var_7 = 4234529UL;
        indirect_placeholder_2();
        *(unsigned char *)(var_0 + (-89L)) = (unsigned char)'\x00';
        local_sp_19 = var_7;
    } else {
        var_5 = rdi + 24UL;
        var_6 = var_0 + (-2416L);
        *(uint64_t *)var_6 = 4234476UL;
        indirect_placeholder_12(var_5);
        local_sp_19 = var_6;
    }
    if (*(unsigned char *)4429761UL != '\x01') {
        *(unsigned char *)(var_0 + (-90L)) = (unsigned char)'\x00';
        var_8 = *(uint32_t *)4429808UL;
        if ((uint64_t)(var_8 + (-2)) == 0UL) {
            var_18 = *var_4 + 24UL;
            var_19 = local_sp_19 + (-8L);
            *(uint64_t *)var_19 = 4234713UL;
            var_20 = indirect_placeholder_123(var_18);
            var_21 = var_20.field_1;
            *(uint64_t *)(var_0 + (-1944L)) = var_20.field_0;
            *(uint64_t *)(var_0 + (-1936L)) = var_21;
            local_sp_14 = var_19;
        } else {
            if (var_8 > 2U) {
                var_17 = local_sp_19 + (-8L);
                *(uint64_t *)var_17 = 4234734UL;
                indirect_placeholder_2();
                local_sp_14 = var_17;
            } else {
                if (var_8 == 0U) {
                    var_13 = *var_4 + 24UL;
                    var_14 = local_sp_19 + (-8L);
                    *(uint64_t *)var_14 = 4234678UL;
                    var_15 = indirect_placeholder_122(var_13);
                    var_16 = var_15.field_1;
                    *(uint64_t *)(var_0 + (-1944L)) = var_15.field_0;
                    *(uint64_t *)(var_0 + (-1936L)) = var_16;
                    local_sp_14 = var_14;
                } else {
                    if ((uint64_t)(var_8 + (-1)) == 0UL) {
                        var_9 = *var_4 + 24UL;
                        var_10 = local_sp_19 + (-8L);
                        *(uint64_t *)var_10 = 4234643UL;
                        var_11 = indirect_placeholder_121(var_9);
                        var_12 = var_11.field_1;
                        *(uint64_t *)(var_0 + (-1944L)) = var_11.field_0;
                        *(uint64_t *)(var_0 + (-1936L)) = var_12;
                        local_sp_14 = var_10;
                    } else {
                        var_17 = local_sp_19 + (-8L);
                        *(uint64_t *)var_17 = 4234734UL;
                        indirect_placeholder_2();
                        local_sp_14 = var_17;
                    }
                }
            }
        }
        var_22 = var_0 + (-1928L);
        var_23 = var_0 + (-40L);
        var_24 = (uint64_t *)var_23;
        *var_24 = var_22;
        local_sp_15 = local_sp_14;
        if (*(unsigned char *)4429865UL == '\x00') {
            var_25 = *var_4;
            var_26 = var_0 + (-2040L);
            *(uint64_t *)(local_sp_14 + (-8L)) = 4234783UL;
            indirect_placeholder_4(var_25, var_26, 21UL);
            *(uint64_t *)(local_sp_14 + (-16L)) = 4234814UL;
            indirect_placeholder_2();
            var_27 = *var_24;
            var_28 = local_sp_14 + (-24L);
            *(uint64_t *)var_28 = 4234826UL;
            indirect_placeholder_2();
            *var_24 = (*var_24 + var_27);
            local_sp_15 = var_28;
        }
        local_sp_16 = local_sp_15;
        local_sp_17 = local_sp_15;
        if (*(unsigned char *)4429819UL != '\x00') {
            var_29 = *var_4;
            if (*(unsigned char *)(var_29 + 184UL) == '\x01') {
                var_30 = *(uint64_t *)4429824UL;
                var_31 = (uint64_t)*(uint32_t *)4429820UL;
                var_32 = *(uint64_t *)(var_29 + 88UL);
                var_33 = var_0 + (-2392L);
                var_34 = local_sp_15 + (-8L);
                *(uint64_t *)var_34 = 4234923UL;
                var_35 = indirect_placeholder(512UL, var_31, var_32, var_33, var_30);
                local_sp_16 = var_34;
                rax_1 = var_35;
            }
            var_36 = (uint64_t *)(var_0 + (-48L));
            *var_36 = rax_1;
            var_37 = *(uint32_t *)4429768UL;
            var_38 = local_sp_16 + (-8L);
            *(uint64_t *)var_38 = 4234950UL;
            var_39 = indirect_placeholder_5(rax_1, 0UL);
            var_40 = (uint32_t *)(var_0 + (-52L));
            var_41 = var_37 - (uint32_t)var_39;
            *var_40 = var_41;
            var_42 = var_41;
            local_sp_17 = var_38;
            while ((int)var_42 <= (int)0U)
                {
                    var_46 = *var_24;
                    *var_24 = (var_46 + 1UL);
                    *(unsigned char *)var_46 = (unsigned char)' ';
                    var_47 = *var_40 + (-1);
                    *var_40 = var_47;
                    var_42 = var_47;
                }
            var_43 = *var_36;
            *var_36 = (var_43 + 1UL);
            var_44 = *var_24;
            *var_24 = (var_44 + 1UL);
            var_45 = *(unsigned char *)var_43;
            *(unsigned char *)var_44 = var_45;
            do {
                var_43 = *var_36;
                *var_36 = (var_43 + 1UL);
                var_44 = *var_24;
                *var_24 = (var_44 + 1UL);
                var_45 = *(unsigned char *)var_43;
                *(unsigned char *)var_44 = var_45;
            } while (var_45 != '\x00');
            *(unsigned char *)(*var_24 + (-1L)) = (unsigned char)' ';
        }
        var_48 = *var_4;
        local_sp_18 = local_sp_17;
        if (*(unsigned char *)(var_48 + 184UL) == '\x01') {
            var_49 = *(uint64_t *)(var_48 + 40UL);
            var_50 = var_0 + (-2072L);
            var_51 = local_sp_17 + (-8L);
            *(uint64_t *)var_51 = 4235091UL;
            indirect_placeholder_5(var_49, var_50);
            local_sp_18 = var_51;
        }
        *(uint64_t *)(local_sp_18 + (-8L)) = 4235129UL;
        indirect_placeholder_2();
        var_52 = *var_24;
        var_53 = local_sp_18 + (-16L);
        *(uint64_t *)var_53 = 4235141UL;
        indirect_placeholder_2();
        *var_24 = (*var_24 + var_52);
        local_sp_11 = var_53;
        if (*(unsigned char *)4429836UL == '\x00') {
            var_54 = local_sp_18 + (-24L);
            *(uint64_t *)var_54 = 4235186UL;
            indirect_placeholder_2();
            *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + 2UL);
            local_sp_11 = var_54;
        }
        local_sp_12 = local_sp_11;
        if (*(unsigned char *)4428834UL == '\x00') {
            if (*(unsigned char *)4428835UL == '\x00') {
                if (*(unsigned char *)4429817UL == '\x00') {
                    if (*(unsigned char *)4429760UL != '\x00') {
                        var_55 = local_sp_11 + (-8L);
                        *(uint64_t *)var_55 = 4235277UL;
                        indirect_placeholder_2();
                        *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + (*var_24 - var_22));
                        local_sp_7 = var_55;
                        if (*(unsigned char *)4428834UL == '\x00') {
                            var_56 = *var_4;
                            var_57 = (uint64_t)*(unsigned char *)(var_56 + 184UL);
                            var_58 = (uint64_t)*(uint32_t *)4429780UL;
                            var_59 = (uint64_t)*(uint32_t *)(var_56 + 52UL);
                            var_60 = local_sp_11 + (-16L);
                            *(uint64_t *)var_60 = 4235367UL;
                            indirect_placeholder_16(var_57, var_59, var_58);
                            local_sp_7 = var_60;
                        }
                        local_sp_8 = local_sp_7;
                        if (*(unsigned char *)4428835UL == '\x00') {
                            var_61 = *var_4;
                            var_62 = (uint64_t)*(unsigned char *)(var_61 + 184UL);
                            var_63 = (uint64_t)*(uint32_t *)4429784UL;
                            var_64 = (uint64_t)*(uint32_t *)(var_61 + 56UL);
                            var_65 = local_sp_7 + (-8L);
                            *(uint64_t *)var_65 = 4235420UL;
                            indirect_placeholder_16(var_62, var_64, var_63);
                            local_sp_8 = var_65;
                        }
                        local_sp_9 = local_sp_8;
                        if (*(unsigned char *)4429817UL == '\x00') {
                            var_66 = *var_4;
                            var_67 = (uint64_t)*(unsigned char *)(var_66 + 184UL);
                            var_68 = (uint64_t)*(uint32_t *)4429788UL;
                            var_69 = (uint64_t)*(uint32_t *)(var_66 + 52UL);
                            var_70 = local_sp_8 + (-8L);
                            *(uint64_t *)var_70 = 4235473UL;
                            indirect_placeholder_16(var_67, var_69, var_68);
                            local_sp_9 = var_70;
                        }
                        local_sp_10 = local_sp_9;
                        if (*(unsigned char *)4429760UL != '\x00') {
                            var_71 = (uint64_t)*(uint32_t *)4429776UL;
                            var_72 = *(uint64_t *)(*var_4 + 176UL);
                            var_73 = local_sp_9 + (-8L);
                            *(uint64_t *)var_73 = 4235517UL;
                            indirect_placeholder_16(var_71, var_72, 0UL);
                            local_sp_10 = var_73;
                        }
                        *var_24 = var_22;
                        local_sp_12 = local_sp_10;
                    }
                } else {
                    var_55 = local_sp_11 + (-8L);
                    *(uint64_t *)var_55 = 4235277UL;
                    indirect_placeholder_2();
                    *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + (*var_24 - var_22));
                    local_sp_7 = var_55;
                    if (*(unsigned char *)4428834UL == '\x00') {
                        var_56 = *var_4;
                        var_57 = (uint64_t)*(unsigned char *)(var_56 + 184UL);
                        var_58 = (uint64_t)*(uint32_t *)4429780UL;
                        var_59 = (uint64_t)*(uint32_t *)(var_56 + 52UL);
                        var_60 = local_sp_11 + (-16L);
                        *(uint64_t *)var_60 = 4235367UL;
                        indirect_placeholder_16(var_57, var_59, var_58);
                        local_sp_7 = var_60;
                    }
                    local_sp_8 = local_sp_7;
                    if (*(unsigned char *)4428835UL == '\x00') {
                        var_61 = *var_4;
                        var_62 = (uint64_t)*(unsigned char *)(var_61 + 184UL);
                        var_63 = (uint64_t)*(uint32_t *)4429784UL;
                        var_64 = (uint64_t)*(uint32_t *)(var_61 + 56UL);
                        var_65 = local_sp_7 + (-8L);
                        *(uint64_t *)var_65 = 4235420UL;
                        indirect_placeholder_16(var_62, var_64, var_63);
                        local_sp_8 = var_65;
                    }
                    local_sp_9 = local_sp_8;
                    if (*(unsigned char *)4429817UL == '\x00') {
                        var_66 = *var_4;
                        var_67 = (uint64_t)*(unsigned char *)(var_66 + 184UL);
                        var_68 = (uint64_t)*(uint32_t *)4429788UL;
                        var_69 = (uint64_t)*(uint32_t *)(var_66 + 52UL);
                        var_70 = local_sp_8 + (-8L);
                        *(uint64_t *)var_70 = 4235473UL;
                        indirect_placeholder_16(var_67, var_69, var_68);
                        local_sp_9 = var_70;
                    }
                    local_sp_10 = local_sp_9;
                    if (*(unsigned char *)4429760UL == '\x00') {
                        var_71 = (uint64_t)*(uint32_t *)4429776UL;
                        var_72 = *(uint64_t *)(*var_4 + 176UL);
                        var_73 = local_sp_9 + (-8L);
                        *(uint64_t *)var_73 = 4235517UL;
                        indirect_placeholder_16(var_71, var_72, 0UL);
                        local_sp_10 = var_73;
                    }
                    *var_24 = var_22;
                    local_sp_12 = local_sp_10;
                }
            } else {
                var_55 = local_sp_11 + (-8L);
                *(uint64_t *)var_55 = 4235277UL;
                indirect_placeholder_2();
                *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + (*var_24 - var_22));
                local_sp_7 = var_55;
                if (*(unsigned char *)4428834UL == '\x00') {
                    var_56 = *var_4;
                    var_57 = (uint64_t)*(unsigned char *)(var_56 + 184UL);
                    var_58 = (uint64_t)*(uint32_t *)4429780UL;
                    var_59 = (uint64_t)*(uint32_t *)(var_56 + 52UL);
                    var_60 = local_sp_11 + (-16L);
                    *(uint64_t *)var_60 = 4235367UL;
                    indirect_placeholder_16(var_57, var_59, var_58);
                    local_sp_7 = var_60;
                }
                local_sp_8 = local_sp_7;
                if (*(unsigned char *)4428835UL == '\x00') {
                    var_61 = *var_4;
                    var_62 = (uint64_t)*(unsigned char *)(var_61 + 184UL);
                    var_63 = (uint64_t)*(uint32_t *)4429784UL;
                    var_64 = (uint64_t)*(uint32_t *)(var_61 + 56UL);
                    var_65 = local_sp_7 + (-8L);
                    *(uint64_t *)var_65 = 4235420UL;
                    indirect_placeholder_16(var_62, var_64, var_63);
                    local_sp_8 = var_65;
                }
                local_sp_9 = local_sp_8;
                if (*(unsigned char *)4429817UL == '\x00') {
                    var_66 = *var_4;
                    var_67 = (uint64_t)*(unsigned char *)(var_66 + 184UL);
                    var_68 = (uint64_t)*(uint32_t *)4429788UL;
                    var_69 = (uint64_t)*(uint32_t *)(var_66 + 52UL);
                    var_70 = local_sp_8 + (-8L);
                    *(uint64_t *)var_70 = 4235473UL;
                    indirect_placeholder_16(var_67, var_69, var_68);
                    local_sp_9 = var_70;
                }
                local_sp_10 = local_sp_9;
                if (*(unsigned char *)4429760UL == '\x00') {
                    var_71 = (uint64_t)*(uint32_t *)4429776UL;
                    var_72 = *(uint64_t *)(*var_4 + 176UL);
                    var_73 = local_sp_9 + (-8L);
                    *(uint64_t *)var_73 = 4235517UL;
                    indirect_placeholder_16(var_71, var_72, 0UL);
                    local_sp_10 = var_73;
                }
                *var_24 = var_22;
                local_sp_12 = local_sp_10;
            }
        } else {
            var_55 = local_sp_11 + (-8L);
            *(uint64_t *)var_55 = 4235277UL;
            indirect_placeholder_2();
            *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + (*var_24 - var_22));
            local_sp_7 = var_55;
            if (*(unsigned char *)4428834UL == '\x00') {
                var_56 = *var_4;
                var_57 = (uint64_t)*(unsigned char *)(var_56 + 184UL);
                var_58 = (uint64_t)*(uint32_t *)4429780UL;
                var_59 = (uint64_t)*(uint32_t *)(var_56 + 52UL);
                var_60 = local_sp_11 + (-16L);
                *(uint64_t *)var_60 = 4235367UL;
                indirect_placeholder_16(var_57, var_59, var_58);
                local_sp_7 = var_60;
            }
            local_sp_8 = local_sp_7;
            if (*(unsigned char *)4428835UL == '\x00') {
                var_61 = *var_4;
                var_62 = (uint64_t)*(unsigned char *)(var_61 + 184UL);
                var_63 = (uint64_t)*(uint32_t *)4429784UL;
                var_64 = (uint64_t)*(uint32_t *)(var_61 + 56UL);
                var_65 = local_sp_7 + (-8L);
                *(uint64_t *)var_65 = 4235420UL;
                indirect_placeholder_16(var_62, var_64, var_63);
                local_sp_8 = var_65;
            }
            local_sp_9 = local_sp_8;
            if (*(unsigned char *)4429817UL == '\x00') {
                var_66 = *var_4;
                var_67 = (uint64_t)*(unsigned char *)(var_66 + 184UL);
                var_68 = (uint64_t)*(uint32_t *)4429788UL;
                var_69 = (uint64_t)*(uint32_t *)(var_66 + 52UL);
                var_70 = local_sp_8 + (-8L);
                *(uint64_t *)var_70 = 4235473UL;
                indirect_placeholder_16(var_67, var_69, var_68);
                local_sp_9 = var_70;
            }
            local_sp_10 = local_sp_9;
            if (*(unsigned char *)4429760UL == '\x00') {
                var_71 = (uint64_t)*(uint32_t *)4429776UL;
                var_72 = *(uint64_t *)(*var_4 + 176UL);
                var_73 = local_sp_9 + (-8L);
                *(uint64_t *)var_73 = 4235517UL;
                indirect_placeholder_16(var_71, var_72, 0UL);
                local_sp_10 = var_73;
            }
            *var_24 = var_22;
            local_sp_12 = local_sp_10;
        }
        var_74 = *var_4;
        var_75 = *(unsigned char *)(var_74 + 184UL);
        local_sp_13 = local_sp_12;
        if (var_75 == '\x00') {
            *(uint32_t *)(var_0 + (-72L)) = (*(uint32_t *)4429800UL - ((*(uint32_t *)4429792UL + 2U) + *(uint32_t *)4429796UL));
            var_76 = *(uint64_t *)(*var_4 + 64UL);
            var_77 = (uint64_t)(unsigned char)var_76 | (uint64_t)((uint32_t)(var_76 >> 12UL) & (-256));
            var_78 = var_0 + (-2136L);
            *(uint64_t *)(local_sp_12 + (-8L)) = 4235681UL;
            indirect_placeholder_5(var_77, var_78);
            var_79 = *(uint64_t *)(*var_4 + 64UL);
            var_80 = (uint64_t)((uint16_t)(var_79 >> 8UL) & (unsigned short)4095U) | (uint64_t)((uint32_t)(var_79 >> 32UL) & (-4096));
            var_81 = var_0 + (-2104L);
            *(uint64_t *)(local_sp_12 + (-16L)) = 4235755UL;
            indirect_placeholder_5(var_80, var_81);
            var_82 = local_sp_12 + (-24L);
            *(uint64_t *)var_82 = 4235809UL;
            indirect_placeholder_2();
            *var_24 = (*var_24 + ((uint64_t)*(uint32_t *)4429800UL + 1UL));
            local_sp_3 = var_82;
        } else {
            if (var_75 == '\x01') {
                var_83 = *(uint64_t *)4428840UL;
                var_84 = (uint64_t)*(uint32_t *)4429832UL;
                var_85 = *(uint64_t *)(var_74 + 72UL);
                *(uint64_t *)(local_sp_12 + (-8L)) = 4235890UL;
                var_86 = indirect_placeholder_13(var_85);
                var_87 = var_0 + (-2392L);
                var_88 = local_sp_12 + (-16L);
                *(uint64_t *)var_88 = 4235918UL;
                var_89 = indirect_placeholder(1UL, var_84, var_86, var_87, var_83);
                local_sp_13 = var_88;
                rax_0 = var_89;
            }
            var_90 = (uint64_t *)(var_0 + (-64L));
            *var_90 = rax_0;
            var_91 = *(uint32_t *)4429800UL;
            var_92 = local_sp_13 + (-8L);
            *(uint64_t *)var_92 = 4235945UL;
            var_93 = indirect_placeholder_5(rax_0, 0UL);
            var_94 = (uint32_t *)(var_0 + (-68L));
            var_95 = var_91 - (uint32_t)var_93;
            *var_94 = var_95;
            var_96 = var_95;
            local_sp_3 = var_92;
            while ((int)var_96 <= (int)0U)
                {
                    var_156 = *var_24;
                    *var_24 = (var_156 + 1UL);
                    *(unsigned char *)var_156 = (unsigned char)' ';
                    var_157 = *var_94 + (-1);
                    *var_94 = var_157;
                    var_96 = var_157;
                }
            var_97 = *var_90;
            *var_90 = (var_97 + 1UL);
            var_98 = *var_24;
            *var_24 = (var_98 + 1UL);
            var_99 = *(unsigned char *)var_97;
            *(unsigned char *)var_98 = var_99;
            do {
                var_97 = *var_90;
                *var_90 = (var_97 + 1UL);
                var_98 = *var_24;
                *var_24 = (var_98 + 1UL);
                var_99 = *(unsigned char *)var_97;
                *(unsigned char *)var_98 = var_99;
            } while (var_99 != '\x00');
            *(unsigned char *)(*var_24 + (-1L)) = (unsigned char)' ';
        }
        var_100 = (uint64_t *)(var_0 + (-32L));
        *var_100 = 0UL;
        var_101 = (unsigned char **)var_23;
        **var_101 = (unsigned char)'\x01';
        local_sp_4 = local_sp_3;
        var_102 = *(uint64_t *)4429944UL;
        var_103 = var_0 + (-2008L);
        var_104 = var_0 + (-1944L);
        var_105 = local_sp_3 + (-8L);
        *(uint64_t *)var_105 = 4236098UL;
        var_106 = indirect_placeholder_119(var_103, var_102, var_104);
        local_sp_4 = var_105;
        if (*(unsigned char *)(*var_4 + 184UL) != '\x00' & var_106.field_0 != 0UL) {
            var_107 = var_106.field_1;
            var_108 = (uint64_t *)var_104;
            var_109 = *var_108;
            var_110 = (uint64_t *)(var_0 + (-1936L));
            var_111 = *var_110;
            var_112 = *(uint64_t *)4429744UL;
            var_113 = *(uint64_t *)4429752UL;
            var_114 = local_sp_3 + (-16L);
            *(uint64_t *)var_114 = 4236146UL;
            var_115 = indirect_placeholder_118(var_111, var_109, var_112, var_113, var_107);
            var_116 = var_115.field_1;
            local_sp_2 = var_114;
            if ((int)(uint32_t)var_115.field_0 > (int)4294967295U) {
                var_117 = local_sp_3 + (-24L);
                *(uint64_t *)var_117 = 4236160UL;
                indirect_placeholder_12(4429744UL);
                local_sp_2 = var_117;
            }
            var_118 = *(uint64_t *)4429744UL + (-15778476L);
            var_119 = (uint64_t *)(var_0 + (-2152L));
            *var_119 = var_118;
            var_120 = *(uint64_t *)4429752UL;
            *(uint64_t *)(var_0 + (-2144L)) = var_120;
            var_121 = *var_108;
            var_122 = *var_110;
            var_123 = *var_119;
            var_124 = local_sp_2 + (-8L);
            *(uint64_t *)var_124 = 4236233UL;
            var_125 = indirect_placeholder_117(var_122, var_121, var_123, var_120, var_116);
            local_sp_0 = var_124;
            if ((int)(uint32_t)var_125.field_0 > (int)4294967295U) {
                local_sp_1 = local_sp_0;
            } else {
                var_126 = var_125.field_1;
                var_127 = *(uint64_t *)4429744UL;
                var_128 = *(uint64_t *)4429752UL;
                var_129 = *var_108;
                var_130 = *var_110;
                var_131 = local_sp_2 + (-16L);
                *(uint64_t *)var_131 = 4236276UL;
                var_132 = indirect_placeholder_116(var_128, var_127, var_129, var_130, var_126);
                local_sp_0 = var_131;
                local_sp_1 = var_131;
                storemerge6 = (unsigned char)'\x01';
                if ((int)(uint32_t)var_132.field_0 > (int)4294967295U) {
                    local_sp_1 = local_sp_0;
                }
            }
            var_133 = (unsigned char *)(var_0 + (-73L));
            var_134 = storemerge6 & '\x01';
            *var_133 = var_134;
            var_135 = (uint64_t)(uint32_t)*var_110;
            var_136 = *(uint64_t *)4429944UL;
            var_137 = (uint64_t)var_134;
            var_138 = local_sp_1 + (-8L);
            *(uint64_t *)var_138 = 4236349UL;
            var_139 = indirect_placeholder_4(var_137, var_136, var_135);
            *var_100 = var_139;
            local_sp_4 = var_138;
        }
        var_140 = *var_100;
        local_sp_5 = local_sp_4;
        local_sp_6 = local_sp_4;
        if (var_140 == 0UL) {
            var_145 = *var_24 + var_140;
            *var_24 = (var_145 + 1UL);
            *(unsigned char *)var_145 = (unsigned char)' ';
            **var_101 = (unsigned char)'\x00';
        } else {
            if (**var_101 == '\x00') {
                var_145 = *var_24 + var_140;
                *var_24 = (var_145 + 1UL);
                *(unsigned char *)var_145 = (unsigned char)' ';
                **var_101 = (unsigned char)'\x00';
            } else {
                if (*(unsigned char *)(*var_4 + 184UL) == '\x01') {
                    var_141 = local_sp_4 + (-8L);
                    *(uint64_t *)var_141 = 4236456UL;
                    var_142 = indirect_placeholder_1();
                    local_sp_5 = var_141;
                    storemerge7 = var_142;
                }
                *(uint64_t *)(local_sp_5 + (-8L)) = 4236464UL;
                indirect_placeholder_120(storemerge7);
                *(uint64_t *)(local_sp_5 + (-16L)) = 4236491UL;
                indirect_placeholder_2();
                var_143 = *var_24;
                var_144 = local_sp_5 + (-24L);
                *(uint64_t *)var_144 = 4236503UL;
                indirect_placeholder_2();
                *var_24 = (*var_24 + var_143);
                local_sp_6 = var_144;
            }
        }
        *(uint64_t *)(local_sp_6 + (-8L)) = 4236532UL;
        indirect_placeholder_2();
        *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + (*var_24 - var_22));
        var_146 = *var_24 - var_22;
        var_147 = *var_4;
        *(uint64_t *)(local_sp_6 + (-16L)) = 4236617UL;
        var_148 = indirect_placeholder_3(var_146, 4430176UL, var_147, 0UL);
        var_149 = (uint64_t *)(var_0 + (-88L));
        *var_149 = var_148;
        var_150 = *var_4;
        if (*(uint32_t *)(var_150 + 168UL) == 6U) {
            *(uint64_t *)(local_sp_6 + (-24L)) = 4236693UL;
            indirect_placeholder_2();
            *(uint64_t *)4430144UL = (*(uint64_t *)4430144UL + 4UL);
            var_153 = (*var_149 + (*var_24 - var_22)) + 4UL;
            var_154 = *var_4;
            *(uint64_t *)(local_sp_6 + (-32L)) = 4236770UL;
            indirect_placeholder_3(var_153, 0UL, var_154, 1UL);
            if (*(uint64_t *)(var_150 + 8UL) != 0UL & *(uint32_t *)4429840UL == 0U) {
                var_155 = (uint64_t)*(uint32_t *)(*var_4 + 172UL);
                *(uint64_t *)(local_sp_6 + (-40L)) = 4236810UL;
                indirect_placeholder_5(1UL, var_155);
            }
        } else {
            if (*(uint32_t *)4429840UL == 0U) {
                var_151 = (uint64_t)*(uint32_t *)(var_150 + 48UL);
                var_152 = (uint64_t)*(unsigned char *)(var_150 + 184UL);
                *(uint64_t *)(local_sp_6 + (-24L)) = 4236871UL;
                indirect_placeholder_5(var_152, var_151);
            }
        }
        return;
    }
    switch (*(uint32_t *)(*var_4 + 188UL)) {
      case 1U:
        {
            *(unsigned char *)(var_0 + (-90L)) = (unsigned char)'.';
        }
        break;
      case 2U:
        {
            *(unsigned char *)(var_0 + (-90L)) = (unsigned char)'+';
        }
        break;
      default:
        {
            break;
        }
        break;
    }
}
