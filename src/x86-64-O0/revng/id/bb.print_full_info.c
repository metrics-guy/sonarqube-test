typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_print_full_info(uint64_t rdi) {
    uint64_t var_44;
    uint64_t local_sp_3;
    uint64_t var_20;
    uint32_t storemerge;
    uint64_t var_31;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t local_sp_7;
    uint64_t var_49;
    uint32_t var_45;
    uint64_t local_sp_0;
    uint32_t var_50;
    uint32_t var_43;
    bool var_32;
    uint64_t *var_33;
    struct indirect_placeholder_34_ret_type var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_1;
    uint64_t var_40;
    uint32_t *var_41;
    uint64_t *var_42;
    uint64_t local_sp_2;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_19;
    uint64_t local_sp_6;
    uint64_t var_16;
    uint64_t local_sp_5;
    uint64_t var_13;
    uint64_t local_sp_4;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t *var_21;
    uint32_t *_pre_phi99;
    uint32_t var_22;
    uint32_t *var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint32_t *var_29;
    uint32_t var_30;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t *var_12;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r9();
    var_3 = init_r8();
    var_4 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_4;
    var_5 = (uint64_t *)(var_0 + (-80L));
    *var_5 = rdi;
    *(uint64_t *)(var_0 + (-96L)) = 4207041UL;
    indirect_placeholder_3(4297716UL);
    *(uint64_t *)(var_0 + (-104L)) = 4207059UL;
    indirect_placeholder();
    var_6 = (uint64_t)*(uint32_t *)4297716UL;
    var_7 = var_0 + (-112L);
    *(uint64_t *)var_7 = 4207072UL;
    indirect_placeholder();
    var_8 = (uint64_t *)(var_0 + (-32L));
    *var_8 = var_6;
    var_43 = 0U;
    storemerge = 4294967295U;
    local_sp_7 = var_7;
    if (var_6 == 0UL) {
        var_9 = var_0 + (-120L);
        *(uint64_t *)var_9 = 4207108UL;
        indirect_placeholder();
        local_sp_7 = var_9;
    }
    *(uint64_t *)(local_sp_7 + (-8L)) = 4207118UL;
    indirect_placeholder_3(4297724UL);
    *(uint64_t *)(local_sp_7 + (-16L)) = 4207136UL;
    indirect_placeholder();
    var_10 = (uint64_t)*(uint32_t *)4297724UL;
    var_11 = local_sp_7 + (-24L);
    *(uint64_t *)var_11 = 4207149UL;
    indirect_placeholder();
    var_12 = (uint64_t *)(var_0 + (-48L));
    *var_12 = var_10;
    local_sp_4 = var_11;
    if (var_10 == 0UL) {
        var_13 = local_sp_7 + (-32L);
        *(uint64_t *)var_13 = 4207185UL;
        indirect_placeholder();
        local_sp_4 = var_13;
    }
    local_sp_5 = local_sp_4;
    *(uint64_t *)(local_sp_4 + (-8L)) = 4207211UL;
    indirect_placeholder_3(4297720UL);
    *(uint64_t *)(local_sp_4 + (-16L)) = 4207229UL;
    indirect_placeholder();
    var_14 = (uint64_t)*(uint32_t *)4297720UL;
    var_15 = local_sp_4 + (-24L);
    *(uint64_t *)var_15 = 4207242UL;
    indirect_placeholder();
    *var_8 = var_14;
    local_sp_5 = var_15;
    if ((uint64_t)(*(uint32_t *)4297720UL - *(uint32_t *)4297716UL) != 0UL & var_14 == 0UL) {
        var_16 = local_sp_4 + (-32L);
        *(uint64_t *)var_16 = 4207278UL;
        indirect_placeholder();
        local_sp_5 = var_16;
    }
    local_sp_6 = local_sp_5;
    *(uint64_t *)(local_sp_5 + (-8L)) = 4207304UL;
    indirect_placeholder_3(4297728UL);
    *(uint64_t *)(local_sp_5 + (-16L)) = 4207322UL;
    indirect_placeholder();
    var_17 = (uint64_t)*(uint32_t *)4297728UL;
    var_18 = local_sp_5 + (-24L);
    *(uint64_t *)var_18 = 4207335UL;
    indirect_placeholder();
    *var_12 = var_17;
    local_sp_6 = var_18;
    if ((uint64_t)(*(uint32_t *)4297728UL - *(uint32_t *)4297724UL) != 0UL & var_17 == 0UL) {
        var_19 = local_sp_5 + (-32L);
        *(uint64_t *)var_19 = 4207371UL;
        indirect_placeholder();
        local_sp_6 = var_19;
    }
    if (*var_5 == 0UL) {
        var_22 = *(uint32_t *)4297728UL;
        var_23 = (uint32_t *)(var_0 + (-36L));
        *var_23 = var_22;
        _pre_phi99 = var_23;
    } else {
        var_20 = *var_8;
        if (var_20 != 0UL) {
            storemerge = *(uint32_t *)(var_20 + 20UL);
        }
        var_21 = (uint32_t *)(var_0 + (-36L));
        *var_21 = storemerge;
        _pre_phi99 = var_21;
    }
    var_24 = var_0 + (-64L);
    var_25 = (uint64_t)*_pre_phi99;
    var_26 = *var_5;
    var_27 = local_sp_6 + (-8L);
    *(uint64_t *)var_27 = 4207434UL;
    var_28 = indirect_placeholder_7(var_24, var_25, var_26);
    var_29 = (uint32_t *)(var_0 + (-52L));
    var_30 = (uint32_t)var_28;
    *var_29 = var_30;
    local_sp_1 = var_27;
    if ((int)var_30 > (int)4294967295U) {
        if ((int)var_30 > (int)0U) {
            var_40 = local_sp_6 + (-16L);
            *(uint64_t *)var_40 = 4207576UL;
            indirect_placeholder();
            local_sp_1 = var_40;
        }
        var_41 = (uint32_t *)(var_0 + (-40L));
        *var_41 = 0U;
        var_42 = (uint64_t *)var_24;
        local_sp_2 = local_sp_1;
        var_45 = var_43;
        local_sp_3 = local_sp_2;
        while ((long)((uint64_t)var_43 << 32UL) >= (long)((uint64_t)*var_29 << 32UL))
            {
                if ((int)var_43 > (int)0U) {
                    var_44 = local_sp_2 + (-8L);
                    *(uint64_t *)var_44 = 4207601UL;
                    indirect_placeholder();
                    var_45 = *var_41;
                    local_sp_3 = var_44;
                }
                var_46 = *var_42 + ((uint64_t)var_45 << 2UL);
                *(uint64_t *)(local_sp_3 + (-8L)) = 4207633UL;
                indirect_placeholder_3(var_46);
                *(uint64_t *)(local_sp_3 + (-16L)) = 4207644UL;
                indirect_placeholder();
                var_47 = (uint64_t)*(uint32_t *)(*var_42 + ((uint64_t)*var_41 << 2UL));
                var_48 = local_sp_3 + (-24L);
                *(uint64_t *)var_48 = 4207670UL;
                indirect_placeholder();
                *var_12 = var_47;
                local_sp_0 = var_48;
                if (var_47 == 0UL) {
                    var_49 = local_sp_3 + (-32L);
                    *(uint64_t *)var_49 = 4207706UL;
                    indirect_placeholder();
                    local_sp_0 = var_49;
                }
                var_50 = *var_41 + 1U;
                *var_41 = var_50;
                var_43 = var_50;
                local_sp_2 = local_sp_0;
                var_45 = var_43;
                local_sp_3 = local_sp_2;
            }
        *(uint64_t *)(local_sp_2 + (-8L)) = 4207734UL;
        indirect_placeholder();
        if (*(uint64_t *)4297736UL == 0UL) {
            *(uint64_t *)(local_sp_2 + (-16L)) = 4207771UL;
            indirect_placeholder();
        }
    } else {
        var_31 = *var_5;
        var_32 = (var_31 == 0UL);
        var_33 = (uint64_t *)(local_sp_6 + (-16L));
        if (var_32) {
            *var_33 = 4207504UL;
            indirect_placeholder();
            var_39 = (uint64_t)*(uint32_t *)var_28;
            *(uint64_t *)(local_sp_6 + (-24L)) = 4207528UL;
            indirect_placeholder_32(0UL, var_25, 4278880UL, var_39, 0UL, var_2, var_3);
        } else {
            *var_33 = 4207462UL;
            var_34 = indirect_placeholder_34(var_31);
            var_35 = var_34.field_0;
            var_36 = var_34.field_1;
            var_37 = var_34.field_2;
            *(uint64_t *)(local_sp_6 + (-24L)) = 4207470UL;
            indirect_placeholder();
            var_38 = (uint64_t)*(uint32_t *)var_35;
            *(uint64_t *)(local_sp_6 + (-32L)) = 4207497UL;
            indirect_placeholder_33(0UL, var_35, 4278840UL, var_38, 0UL, var_36, var_37);
        }
        *(unsigned char *)4297408UL = (unsigned char)'\x00';
    }
    return;
}
