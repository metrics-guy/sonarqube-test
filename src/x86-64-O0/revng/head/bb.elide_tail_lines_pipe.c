typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_12_ret_type;
struct indirect_placeholder_13_ret_type;
struct indirect_placeholder_14_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_12_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_13_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_14_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_2(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_12_ret_type indirect_placeholder_12(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_13_ret_type indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_14_ret_type indirect_placeholder_14(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
typedef _Bool bool;
uint64_t bb_elide_tail_lines_pipe(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    unsigned char *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t local_sp_7_ph;
    uint64_t *var_55;
    uint64_t var_56;
    uint64_t var_48;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t r8_1_ph;
    uint64_t local_sp_4_ph;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t local_sp_0;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t *var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t local_sp_1;
    uint64_t var_49;
    uint64_t r8_1_ph_be;
    uint64_t var_57;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_92;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t local_sp_2;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_65;
    uint64_t local_sp_4;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    struct indirect_placeholder_13_ret_type var_71;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t *var_84;
    uint64_t local_sp_7;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    struct indirect_placeholder_15_ret_type var_90;
    uint64_t var_91;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t *var_78;
    uint64_t var_73;
    uint64_t local_sp_7_ph_be;
    uint64_t r9_1_ph_be;
    uint64_t r9_1_ph;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_16_ret_type var_98;
    uint64_t var_72;
    uint64_t local_sp_6;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_26;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_rbx();
    var_4 = init_r9();
    var_5 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_3;
    var_6 = (uint64_t *)(var_0 + (-128L));
    *var_6 = rdi;
    var_7 = (uint32_t *)(var_0 + (-132L));
    *var_7 = (uint32_t)rsi;
    var_8 = (uint64_t *)(var_0 + (-144L));
    *var_8 = rdx;
    var_9 = (uint64_t *)(var_0 + (-152L));
    *var_9 = rcx;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = rcx;
    var_11 = (uint64_t *)(var_0 + (-64L));
    *var_11 = 0UL;
    var_12 = (unsigned char *)(var_0 + (-65L));
    *var_12 = (unsigned char)'\x01';
    *(uint64_t *)(var_0 + (-160L)) = 4207442UL;
    var_13 = indirect_placeholder_5(1048UL);
    var_14 = (uint64_t *)(var_0 + (-48L));
    *var_14 = var_13;
    var_15 = (uint64_t *)(var_0 + (-40L));
    *var_15 = var_13;
    *(uint64_t *)(var_13 + 1032UL) = 0UL;
    var_16 = *var_15;
    *(uint64_t *)(var_16 + 1024UL) = *(uint64_t *)(var_16 + 1032UL);
    *(uint64_t *)(*var_15 + 1040UL) = 0UL;
    var_17 = var_0 + (-168L);
    *(uint64_t *)var_17 = 4207516UL;
    var_18 = indirect_placeholder_5(1048UL);
    var_19 = (uint64_t *)(var_0 + (-56L));
    *var_19 = var_18;
    var_20 = (uint64_t *)(var_0 + (-104L));
    var_21 = (uint64_t *)(var_0 + (-112L));
    var_22 = (uint64_t *)(var_0 + (-80L));
    var_57 = 0UL;
    local_sp_7_ph = var_17;
    r9_1_ph = var_4;
    r8_1_ph = var_5;
    while (1U)
        {
            r9_1_ph_be = r9_1_ph;
            r8_1_ph_be = r8_1_ph;
            local_sp_7 = local_sp_7_ph;
            while (1U)
                {
                    var_26 = local_sp_7 + (-16L);
                    *(uint64_t *)var_26 = 4208101UL;
                    indirect_placeholder();
                    local_sp_0 = var_26;
                    if (*var_20 != 18446744073709551615UL) {
                        var_27 = *var_14;
                        var_28 = *(uint64_t *)(var_27 + 1024UL);
                        if (var_28 != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        if ((uint64_t)(*(unsigned char *)((var_28 + (-1L)) + var_27) - *(unsigned char *)4301714UL) != 0UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_29 = (uint64_t *)(var_27 + 1032UL);
                        *var_29 = (*var_29 + 1UL);
                        *var_11 = (*var_11 + 1UL);
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_61 = *var_6;
                    *(uint64_t *)(local_sp_7 + (-24L)) = 4208125UL;
                    var_62 = indirect_placeholder_1(var_61, 4UL);
                    *(uint64_t *)(local_sp_7 + (-32L)) = 4208133UL;
                    indirect_placeholder();
                    var_63 = (uint64_t)*(uint32_t *)var_62;
                    var_64 = local_sp_7 + (-40L);
                    *(uint64_t *)var_64 = 4208160UL;
                    indirect_placeholder_2(0UL, var_62, 4282326UL, var_63, 0UL, r9_1_ph, r8_1_ph);
                    *var_12 = (unsigned char)'\x00';
                    local_sp_4_ph = var_64;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_30 = *var_15;
                    *var_19 = var_30;
                    var_31 = var_30;
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3U:
              case 2U:
                {
                    switch (loop_state_var) {
                      case 2U:
                        {
                            *var_10 = (*var_10 + var_25);
                            var_95 = *var_19;
                            var_96 = *var_20;
                            var_97 = local_sp_7 + (-16L);
                            *(uint64_t *)var_97 = 4207605UL;
                            var_98 = indirect_placeholder_16(var_96, var_95);
                            local_sp_7_ph_be = var_97;
                            r9_1_ph_be = var_98.field_4;
                            r8_1_ph_be = var_98.field_5;
                        }
                        break;
                      case 3U:
                        {
                            *(uint64_t *)(var_81 + 1040UL) = var_79;
                            *var_14 = *(uint64_t *)(*var_14 + 1040UL);
                            var_85 = *var_11 - *(uint64_t *)(*var_15 + 1032UL);
                            var_86 = helper_cc_compute_c_wrapper(*var_8 - var_85, var_85, var_2, 17U);
                            if (var_86 == 0UL) {
                                var_93 = local_sp_6 + (-16L);
                                *(uint64_t *)var_93 = 4208080UL;
                                var_94 = indirect_placeholder_5(1048UL);
                                *var_19 = var_94;
                                local_sp_7_ph_be = var_93;
                            } else {
                                *var_10 = (*var_10 + *(uint64_t *)(*var_15 + 1024UL));
                                var_87 = *var_15;
                                var_88 = *(uint64_t *)(var_87 + 1024UL);
                                var_89 = local_sp_6 + (-16L);
                                *(uint64_t *)var_89 = 4208027UL;
                                var_90 = indirect_placeholder_15(var_88, var_87);
                                var_91 = var_90.field_4;
                                var_92 = var_90.field_5;
                                *var_19 = *var_15;
                                *var_11 = (*var_11 - *(uint64_t *)(*var_15 + 1032UL));
                                *var_15 = *(uint64_t *)(*var_15 + 1040UL);
                                local_sp_7_ph_be = var_89;
                                r9_1_ph_be = var_91;
                                r8_1_ph_be = var_92;
                            }
                        }
                        break;
                    }
                    local_sp_7_ph = local_sp_7_ph_be;
                    r9_1_ph = r9_1_ph_be;
                    r8_1_ph = r8_1_ph_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            var_65 = *var_15;
            local_sp_4 = local_sp_4_ph;
            while (var_65 != 0UL)
                {
                    *var_19 = *(uint64_t *)(var_65 + 1040UL);
                    var_66 = local_sp_4 + (-8L);
                    *(uint64_t *)var_66 = 4208596UL;
                    indirect_placeholder();
                    var_67 = *var_19;
                    *var_15 = var_67;
                    var_65 = var_67;
                    local_sp_4 = var_66;
                }
            var_68 = *var_10;
            var_69 = *var_6;
            var_70 = (uint64_t)*var_7;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4208647UL;
            var_71 = indirect_placeholder_13(var_69, 0UL, var_68, var_70);
            if ((long)*var_9 >= (long)0UL & (long)var_71.field_0 > (long)18446744073709551615UL) {
                *var_12 = (unsigned char)'\x00';
            }
            return (uint64_t)*var_12;
        }
        break;
      case 1U:
        {
            var_32 = *var_11 - *(uint64_t *)(var_31 + 1032UL);
            var_33 = helper_cc_compute_c_wrapper(*var_8 - var_32, var_32, var_2, 17U);
            local_sp_1 = local_sp_0;
            local_sp_4_ph = local_sp_0;
            while (var_33 != 0UL)
                {
                    *var_10 = (*var_10 + *(uint64_t *)(*var_19 + 1024UL));
                    var_34 = *var_19;
                    var_35 = *(uint64_t *)(var_34 + 1024UL);
                    var_36 = local_sp_0 + (-8L);
                    *(uint64_t *)var_36 = 4208301UL;
                    indirect_placeholder_14(var_35, var_34);
                    *var_11 = (*var_11 - *(uint64_t *)(*var_19 + 1032UL));
                    var_37 = *(uint64_t *)(*var_19 + 1040UL);
                    *var_19 = var_37;
                    var_31 = var_37;
                    local_sp_0 = var_36;
                    var_32 = *var_11 - *(uint64_t *)(var_31 + 1032UL);
                    var_33 = helper_cc_compute_c_wrapper(*var_8 - var_32, var_32, var_2, 17U);
                    local_sp_1 = local_sp_0;
                    local_sp_4_ph = local_sp_0;
                }
            var_38 = *var_8;
            var_39 = *var_11;
            var_40 = helper_cc_compute_c_wrapper(var_38 - var_39, var_39, var_2, 17U);
            if (var_40 != 0UL) {
                var_41 = *var_11 - *var_8;
                var_42 = (uint64_t *)(var_0 + (-88L));
                *var_42 = var_41;
                var_43 = *var_19;
                var_44 = *(uint64_t *)(var_43 + 1024UL) + var_43;
                var_45 = (uint64_t *)(var_0 + (-120L));
                *var_45 = var_44;
                var_46 = *var_19;
                var_47 = (uint64_t *)(var_0 + (-96L));
                *var_47 = var_46;
                var_48 = *var_42;
                while (1U)
                    {
                        local_sp_2 = local_sp_1;
                        if (var_48 != 0UL) {
                            var_57 = *var_47;
                            break;
                        }
                        var_49 = *var_45;
                        var_50 = *var_47;
                        var_51 = var_49 - var_50;
                        var_52 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)4301714UL;
                        var_53 = local_sp_1 + (-8L);
                        *(uint64_t *)var_53 = 4208503UL;
                        var_54 = indirect_placeholder_3(var_51, var_52, var_50);
                        *var_47 = var_54;
                        local_sp_1 = var_53;
                        local_sp_2 = var_53;
                        if (var_54 != 0UL) {
                            break;
                        }
                        *var_47 = (var_54 + 1UL);
                        var_55 = (uint64_t *)(*var_19 + 1032UL);
                        *var_55 = (*var_55 + 1UL);
                        var_56 = *var_42 + (-1L);
                        *var_42 = var_56;
                        var_48 = var_56;
                        continue;
                    }
                *var_10 = (*var_10 + (var_57 - *var_19));
                var_58 = *var_19;
                var_59 = *var_47 - var_58;
                var_60 = local_sp_2 + (-8L);
                *(uint64_t *)var_60 = 4208564UL;
                indirect_placeholder_12(var_59, var_58);
                local_sp_4_ph = var_60;
            }
        }
        break;
    }
}
