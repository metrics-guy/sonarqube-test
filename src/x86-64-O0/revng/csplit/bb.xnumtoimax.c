typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0);
uint64_t bb_xnumtoimax(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9) {
    uint64_t var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint32_t *var_15;
    uint32_t var_16;
    uint64_t local_sp_1;
    uint64_t var_33;
    uint64_t local_sp_0;
    uint64_t storemerge;
    uint32_t var_34;
    uint64_t spec_select;
    uint64_t var_35;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_25;
    bool var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_18;
    uint64_t var_17;
    uint64_t var_26;
    struct indirect_placeholder_35_ret_type var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-52L));
    *var_4 = (uint32_t)rsi;
    var_5 = (uint64_t *)(var_0 + (-64L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-72L));
    *var_6 = rcx;
    var_7 = (uint64_t *)(var_0 + (-80L));
    *var_7 = r8;
    var_8 = (uint64_t *)(var_0 + (-88L));
    *var_8 = r9;
    var_9 = *var_7;
    var_10 = var_0 + (-40L);
    var_11 = (uint64_t)*var_4;
    var_12 = *var_3;
    var_13 = var_0 + (-96L);
    *(uint64_t *)var_13 = 4223475UL;
    var_14 = indirect_placeholder_11(var_10, var_11, 0UL, var_12, var_9);
    var_15 = (uint32_t *)(var_0 + (-28L));
    var_16 = (uint32_t)var_14;
    *var_15 = var_16;
    storemerge = 0UL;
    local_sp_1 = var_13;
    switch (var_16) {
      case 1U:
        {
            var_18 = var_0 + (-104L);
            *(uint64_t *)var_18 = 4223585UL;
            indirect_placeholder_2();
            *(uint32_t *)var_14 = 75U;
            local_sp_1 = var_18;
        }
        break;
      case 3U:
        {
            var_17 = var_0 + (-104L);
            *(uint64_t *)var_17 = 4223604UL;
            indirect_placeholder_2();
            *(uint32_t *)var_14 = 0U;
            local_sp_1 = var_17;
        }
        break;
      case 0U:
        {
            var_19 = (uint64_t *)var_10;
            var_20 = *var_19;
            if ((long)*var_5 > (long)var_20) {
                if ((long)*var_6 >= (long)var_20) {
                    *var_15 = 1U;
                    var_21 = *var_19;
                    if ((long)var_21 > (long)1073741823UL) {
                        var_25 = var_0 + (-104L);
                        *(uint64_t *)var_25 = 4223528UL;
                        indirect_placeholder_2();
                        *(uint32_t *)var_21 = 75U;
                        local_sp_1 = var_25;
                    } else {
                        var_22 = ((long)var_21 < (long)18446744072635809792UL);
                        var_23 = var_0 + (-104L);
                        var_24 = (uint64_t *)var_23;
                        local_sp_1 = var_23;
                        if (var_22) {
                            *var_24 = 4223553UL;
                            indirect_placeholder_2();
                            *(uint32_t *)var_21 = 75U;
                        } else {
                            *var_24 = 4223566UL;
                            indirect_placeholder_2();
                            *(uint32_t *)var_21 = 34U;
                        }
                    }
                }
            } else {
                *var_15 = 1U;
                var_21 = *var_19;
                if ((long)var_21 > (long)1073741823UL) {
                    var_25 = var_0 + (-104L);
                    *(uint64_t *)var_25 = 4223528UL;
                    indirect_placeholder_2();
                    *(uint32_t *)var_21 = 75U;
                    local_sp_1 = var_25;
                } else {
                    var_22 = ((long)var_21 < (long)18446744072635809792UL);
                    var_23 = var_0 + (-104L);
                    var_24 = (uint64_t *)var_23;
                    local_sp_1 = var_23;
                    if (var_22) {
                        *var_24 = 4223566UL;
                        indirect_placeholder_2();
                        *(uint32_t *)var_21 = 34U;
                    } else {
                        *var_24 = 4223553UL;
                        indirect_placeholder_2();
                        *(uint32_t *)var_21 = 75U;
                    }
                }
            }
        }
        break;
      default:
        {
            if (*var_15 != 0U) {
                var_26 = *var_3;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4223628UL;
                var_27 = indirect_placeholder_35(var_26);
                var_28 = var_27.field_0;
                var_29 = var_27.field_2;
                var_30 = local_sp_1 + (-16L);
                *(uint64_t *)var_30 = 4223636UL;
                indirect_placeholder_2();
                var_31 = *(uint32_t *)var_28;
                var_32 = (uint64_t)var_31;
                local_sp_0 = var_30;
                if ((uint64_t)(var_31 + (-22)) != 0UL) {
                    var_33 = local_sp_1 + (-24L);
                    *(uint64_t *)var_33 = 4223648UL;
                    indirect_placeholder_2();
                    local_sp_0 = var_33;
                    storemerge = (uint64_t)*(uint32_t *)var_32;
                }
                var_34 = *(uint32_t *)(var_0 | 8UL);
                spec_select = (var_34 == 0U) ? 1UL : (uint64_t)var_34;
                var_35 = *var_8;
                *(uint64_t *)(local_sp_0 + (-8L)) = 4223700UL;
                indirect_placeholder_34(0UL, var_35, 4376943UL, storemerge, spec_select, var_28, var_29);
            }
            return *(uint64_t *)var_10;
        }
        break;
    }
}
