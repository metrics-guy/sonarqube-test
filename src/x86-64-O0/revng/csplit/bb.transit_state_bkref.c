typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_transit_state_bkref_ret_type;
struct indirect_placeholder_189_ret_type;
struct indirect_placeholder_190_ret_type;
struct indirect_placeholder_191_ret_type;
struct indirect_placeholder_192_ret_type;
struct bb_transit_state_bkref_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_189_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_190_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_191_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_192_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_189_ret_type indirect_placeholder_189(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_190_ret_type indirect_placeholder_190(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_191_ret_type indirect_placeholder_191(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_192_ret_type indirect_placeholder_192(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
struct bb_transit_state_bkref_ret_type bb_transit_state_bkref(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_190_ret_type var_72;
    struct indirect_placeholder_192_ret_type var_86;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    uint32_t *var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint32_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t r8_0;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    struct indirect_placeholder_189_ret_type var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint32_t var_108;
    uint64_t local_sp_2;
    uint32_t var_109;
    uint32_t var_80;
    uint64_t r9_2;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint32_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r10_0;
    uint64_t var_96;
    uint64_t var_97;
    struct indirect_placeholder_191_ret_type var_98;
    uint32_t var_99;
    uint64_t r9_4;
    uint64_t r10_4;
    uint64_t storemerge13;
    uint64_t storemerge;
    uint64_t var_64;
    uint64_t var_81;
    uint64_t local_sp_4;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t var_68;
    uint64_t var_50;
    uint64_t var_49;
    uint64_t r8_2;
    uint64_t local_sp_1;
    uint64_t r10_5;
    uint64_t r8_1;
    uint64_t r10_2;
    uint64_t r9_1;
    uint64_t r10_1;
    uint64_t local_sp_5;
    uint64_t var_52;
    uint64_t var_53;
    bool var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_110;
    uint64_t r8_5;
    uint64_t local_sp_3;
    uint64_t r9_5;
    uint64_t r8_3;
    uint64_t r9_3;
    uint64_t r10_3;
    uint64_t var_30;
    uint64_t r8_4;
    struct bb_transit_state_bkref_ret_type mrv;
    struct bb_transit_state_bkref_ret_type mrv1;
    struct bb_transit_state_bkref_ret_type mrv2;
    struct bb_transit_state_bkref_ret_type mrv3;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint16_t var_40;
    bool var_41;
    bool var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    uint64_t var_51;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r8();
    var_3 = init_r9();
    var_4 = init_r10();
    var_5 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_6 = var_0 + (-184L);
    var_7 = (uint64_t *)(var_0 + (-176L));
    *var_7 = rdi;
    var_8 = (uint64_t *)var_6;
    *var_8 = rsi;
    var_9 = *(uint64_t *)(*var_7 + 152UL);
    var_10 = var_0 + (-48L);
    var_11 = (uint64_t *)var_10;
    *var_11 = var_9;
    var_12 = *(uint64_t *)(*var_7 + 72UL);
    var_13 = (uint64_t *)(var_0 + (-56L));
    *var_13 = var_12;
    var_14 = (uint64_t *)(var_0 + (-32L));
    *var_14 = 0UL;
    var_15 = (uint64_t *)(var_0 + (-64L));
    var_16 = (uint64_t *)(var_0 + (-72L));
    var_17 = (uint32_t *)(var_0 + (-76L));
    var_18 = (uint64_t *)(var_0 + (-40L));
    var_19 = var_0 + (-132L);
    var_20 = (uint32_t *)var_19;
    var_21 = var_0 + (-88L);
    var_22 = (uint64_t *)var_21;
    var_23 = (uint64_t *)(var_0 + (-96L));
    var_24 = (uint64_t *)(var_0 + (-104L));
    var_25 = (uint64_t *)(var_0 + (-112L));
    var_26 = (uint64_t *)(var_0 + (-120L));
    var_27 = (uint64_t *)(var_0 + (-128L));
    var_28 = var_0 + (-168L);
    var_29 = 0UL;
    var_109 = 0U;
    storemerge = 0UL;
    local_sp_3 = var_6;
    r8_3 = var_2;
    r9_3 = var_3;
    r10_3 = var_4;
    while (1U)
        {
            var_30 = *var_8;
            r9_4 = r9_3;
            r10_4 = r10_3;
            local_sp_4 = local_sp_3;
            r10_5 = r10_3;
            r8_1 = r8_3;
            r9_1 = r9_3;
            r10_1 = r10_3;
            local_sp_5 = local_sp_3;
            r8_5 = r8_3;
            r9_5 = r9_3;
            r8_4 = r8_3;
            if ((long)var_29 >= (long)*(uint64_t *)(var_30 + 8UL)) {
                *var_20 = 0U;
                break;
            }
            var_31 = *(uint64_t *)(*(uint64_t *)(var_30 + 16UL) + (var_29 << 3UL));
            *var_15 = var_31;
            var_32 = **(uint64_t **)var_10 + (var_31 << 4UL);
            *var_16 = var_32;
            var_33 = var_32 + 8UL;
            if (*(unsigned char *)var_33 == '\x04') {
                var_51 = *var_14 + 1UL;
                *var_14 = var_51;
                var_29 = var_51;
                local_sp_3 = local_sp_5;
                r8_3 = r8_5;
                r9_3 = r9_5;
                r10_3 = r10_5;
                continue;
            }
            if ((*(uint32_t *)var_33 & 261888U) != 0U) {
                var_34 = *var_7;
                var_35 = (uint64_t)*(uint32_t *)(var_34 + 160UL);
                var_36 = *var_13;
                var_37 = local_sp_3 + (-8L);
                *(uint64_t *)var_37 = 4302376UL;
                var_38 = indirect_placeholder_9(var_35, var_36, var_34);
                var_39 = (uint32_t)var_38;
                *var_17 = var_39;
                var_40 = (uint16_t)*(uint32_t *)(*var_16 + 8UL);
                var_41 = ((uint32_t)(var_40 & (unsigned short)1024U) != 0U);
                var_42 = ((var_39 & 1U) == 0U);
                local_sp_4 = var_37;
                local_sp_5 = var_37;
                if (!(var_41 && var_42)) {
                    var_51 = *var_14 + 1UL;
                    *var_14 = var_51;
                    var_29 = var_51;
                    local_sp_3 = local_sp_5;
                    r8_3 = r8_5;
                    r9_3 = r9_5;
                    r10_3 = r10_5;
                    continue;
                }
                if ((uint32_t)(var_40 & (unsigned short)2048U) != 0U) {
                    if (!var_42) {
                        var_51 = *var_14 + 1UL;
                        *var_14 = var_51;
                        var_29 = var_51;
                        local_sp_3 = local_sp_5;
                        r8_3 = r8_5;
                        r9_3 = r9_5;
                        r10_3 = r10_5;
                        continue;
                    }
                }
                if ((uint32_t)(var_40 & (unsigned short)8192U) != 0U) {
                    if ((var_39 & 2U) != 0U) {
                        var_51 = *var_14 + 1UL;
                        *var_14 = var_51;
                        var_29 = var_51;
                        local_sp_3 = local_sp_5;
                        r8_3 = r8_5;
                        r9_3 = r9_5;
                        r10_3 = r10_5;
                        continue;
                    }
                }
                if ((short)var_40 <= (short)(unsigned short)65535U & (var_39 & 8U) != 0U) {
                    var_51 = *var_14 + 1UL;
                    *var_14 = var_51;
                    var_29 = var_51;
                    local_sp_3 = local_sp_5;
                    r8_3 = r8_5;
                    r9_3 = r9_5;
                    r10_3 = r10_5;
                    continue;
                }
            }
            *var_18 = *(uint64_t *)(*var_7 + 200UL);
            var_43 = *var_13;
            var_44 = *var_15;
            var_45 = *var_7;
            var_46 = local_sp_4 + (-8L);
            *(uint64_t *)var_46 = 4302577UL;
            var_47 = indirect_placeholder_9(var_43, var_44, var_45);
            var_48 = (uint32_t)var_47;
            *var_20 = var_48;
            local_sp_1 = var_46;
            var_109 = var_48;
            if (var_48 == 0U) {
                break;
            }
            var_49 = *var_18;
            while (1U)
                {
                    var_50 = *var_7;
                    local_sp_2 = local_sp_1;
                    r9_2 = r9_1;
                    r9_4 = r9_1;
                    r10_4 = r10_1;
                    r8_2 = r8_1;
                    r10_5 = r10_1;
                    r10_2 = r10_1;
                    local_sp_5 = local_sp_1;
                    r8_5 = r8_1;
                    r9_5 = r9_1;
                    r8_4 = r8_1;
                    if ((long)var_49 >= (long)*(uint64_t *)(var_50 + 200UL)) {
                        loop_state_var = 0U;
                        break;
                    }
                    var_52 = (var_49 * 40UL) + *(uint64_t *)(var_50 + 216UL);
                    *var_22 = var_52;
                    if (*var_15 == **(uint64_t **)var_21) {
                        var_110 = *var_18 + 1UL;
                        *var_18 = var_110;
                        var_49 = var_110;
                        local_sp_1 = local_sp_2;
                        r8_1 = r8_2;
                        r9_1 = r9_2;
                        r10_1 = r10_2;
                        continue;
                    }
                    if (*var_13 != *(uint64_t *)(var_52 + 8UL)) {
                        var_53 = *(uint64_t *)(var_52 + 24UL) - *(uint64_t *)(var_52 + 16UL);
                        *var_23 = var_53;
                        var_54 = (var_53 == 0UL);
                        var_55 = *var_11;
                        var_56 = *(uint64_t *)(var_55 + 48UL);
                        if (var_54) {
                            storemerge13 = (**(uint64_t **)(((*var_15 * 24UL) + *(uint64_t *)(var_55 + 40UL)) + 16UL) * 24UL) + var_56;
                        } else {
                            storemerge13 = (*(uint64_t *)(*(uint64_t *)(var_55 + 24UL) + (*var_15 << 3UL)) * 24UL) + var_56;
                        }
                        *var_24 = storemerge13;
                        var_57 = *var_22;
                        var_58 = (*(uint64_t *)(var_57 + 24UL) + *var_13) - *(uint64_t *)(var_57 + 16UL);
                        *var_25 = var_58;
                        var_59 = *var_7;
                        var_60 = (uint64_t)*(uint32_t *)(var_59 + 160UL);
                        var_61 = var_58 + (-1L);
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4302901UL;
                        var_62 = indirect_placeholder_9(var_60, var_61, var_59);
                        *var_17 = (uint32_t)var_62;
                        *var_26 = *(uint64_t *)(*(uint64_t *)(*var_7 + 184UL) + (*var_25 << 3UL));
                        var_63 = *(uint64_t *)(*(uint64_t *)(*var_7 + 184UL) + (*var_13 << 3UL));
                        if (var_63 == 0UL) {
                            storemerge = *(uint64_t *)(var_63 + 16UL);
                        }
                        *var_27 = storemerge;
                        var_64 = *var_26;
                        if (var_64 == 0UL) {
                            var_81 = (*var_25 << 3UL) + *(uint64_t *)(*var_7 + 184UL);
                            var_82 = (uint64_t)*var_17;
                            var_83 = *var_24;
                            var_84 = *var_11;
                            var_85 = local_sp_1 + (-16L);
                            *(uint64_t *)var_85 = 4303068UL;
                            var_86 = indirect_placeholder_192(var_82, var_83, var_84, var_19, var_81);
                            var_87 = var_86.field_0;
                            var_88 = var_86.field_1;
                            var_89 = var_86.field_2;
                            var_90 = var_86.field_3;
                            *(uint64_t *)var_86.field_4 = var_87;
                            var_91 = *var_7;
                            var_92 = *(uint64_t *)(var_91 + 184UL);
                            r8_0 = var_88;
                            var_94 = var_92;
                            var_95 = var_91;
                            local_sp_0 = var_85;
                            r9_0 = var_89;
                            r10_0 = var_90;
                            r9_4 = var_89;
                            r10_4 = var_90;
                            r8_4 = var_88;
                            var_93 = *var_20;
                            var_109 = var_93;
                            if (*(uint64_t *)(var_92 + (*var_25 << 3UL)) != 0UL & var_93 != 0U) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_65 = *(uint64_t *)(var_64 + 80UL);
                        var_66 = *var_24;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4303172UL;
                        var_67 = indirect_placeholder_9(var_66, var_65, var_28);
                        var_68 = (uint32_t)var_67;
                        *var_20 = var_68;
                        if (var_68 != 0U) {
                            *(uint64_t *)(local_sp_1 + (-24L)) = 4303206UL;
                            indirect_placeholder_2();
                            var_109 = *var_20;
                            loop_state_var = 1U;
                            break;
                        }
                        var_69 = (*var_25 << 3UL) + *(uint64_t *)(*var_7 + 184UL);
                        var_70 = (uint64_t)*var_17;
                        var_71 = *var_11;
                        *(uint64_t *)(local_sp_1 + (-24L)) = 4303263UL;
                        var_72 = indirect_placeholder_190(var_70, var_28, var_71, var_19, var_69);
                        var_73 = var_72.field_0;
                        var_74 = var_72.field_1;
                        var_75 = var_72.field_2;
                        var_76 = var_72.field_3;
                        *(uint64_t *)var_72.field_4 = var_73;
                        var_77 = local_sp_1 + (-32L);
                        *(uint64_t *)var_77 = 4303281UL;
                        indirect_placeholder_2();
                        var_78 = *var_7;
                        var_79 = *(uint64_t *)(var_78 + 184UL);
                        r8_0 = var_74;
                        var_94 = var_79;
                        var_95 = var_78;
                        local_sp_0 = var_77;
                        r9_0 = var_75;
                        r10_0 = var_76;
                        r9_4 = var_75;
                        r10_4 = var_76;
                        r8_4 = var_74;
                        if (*(uint64_t *)(var_79 + (*var_25 << 3UL)) != 0UL) {
                            var_80 = *var_20;
                            var_109 = var_80;
                            if (var_80 == 0U) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        local_sp_2 = local_sp_0;
                        r8_2 = r8_0;
                        r9_2 = r9_0;
                        r10_2 = r10_0;
                        var_96 = *var_13;
                        if (*var_23 != 0UL & (long)*var_27 >= (long)*(uint64_t *)(*(uint64_t *)(var_94 + (var_96 << 3UL)) + 16UL)) {
                            var_97 = *var_24;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4303414UL;
                            var_98 = indirect_placeholder_191(var_96, var_97, var_95);
                            var_99 = (uint32_t)var_98.field_0;
                            *var_20 = var_99;
                            var_109 = var_99;
                            if (var_99 != 0U) {
                                r8_4 = var_98.field_1;
                                r9_4 = var_98.field_2;
                                r10_4 = var_98.field_3;
                                loop_state_var = 1U;
                                break;
                            }
                            var_100 = *var_24;
                            var_101 = *var_7;
                            var_102 = local_sp_0 + (-16L);
                            *(uint64_t *)var_102 = 4303455UL;
                            var_103 = indirect_placeholder_189(var_100, var_101);
                            var_104 = var_103.field_0;
                            var_105 = var_103.field_1;
                            var_106 = var_103.field_2;
                            var_107 = var_103.field_3;
                            var_108 = (uint32_t)var_104;
                            *var_20 = var_108;
                            local_sp_2 = var_102;
                            var_109 = var_108;
                            r9_2 = var_106;
                            r9_4 = var_106;
                            r10_4 = var_107;
                            r8_2 = var_105;
                            r10_2 = var_107;
                            r8_4 = var_105;
                            if (var_108 != 0U) {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 0U:
                {
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    mrv.field_0 = (uint64_t)var_109;
    mrv1 = mrv;
    mrv1.field_1 = r8_4;
    mrv2 = mrv1;
    mrv2.field_2 = r9_4;
    mrv3 = mrv2;
    mrv3.field_3 = r10_4;
    return mrv3;
}
