typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_117_ret_type;
struct indirect_placeholder_117_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_117_ret_type indirect_placeholder_117(uint64_t param_0);
uint64_t bb_build_wcs_upper_buffer(uint64_t rdi) {
    uint64_t local_sp_5;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_128;
    uint64_t *_pre_phi362;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t *var_130;
    uint64_t var_131;
    uint64_t var_133;
    uint64_t *var_132;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t storemerge;
    uint64_t var_127;
    uint64_t var_129;
    uint64_t local_sp_0;
    uint64_t *var_138;
    uint64_t *_pre_phi358;
    uint64_t *var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_153;
    uint64_t var_136;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_108;
    uint64_t var_104;
    uint64_t var_105;
    struct indirect_placeholder_117_ret_type var_106;
    uint64_t var_107;
    uint64_t rax_0;
    uint64_t local_sp_1;
    uint64_t *var_109;
    uint64_t var_49;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    bool var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t *var_120;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t *var_100;
    uint64_t var_137;
    uint32_t *var_95;
    uint32_t var_96;
    uint32_t *var_97;
    uint64_t var_79;
    uint64_t var_94;
    uint64_t var_43;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t *var_82;
    unsigned char var_83;
    uint32_t *var_84;
    uint32_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint32_t var_89;
    uint32_t var_88;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_2;
    uint64_t var_155;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t var_158;
    uint64_t var_159;
    uint64_t local_sp_3_be;
    uint64_t var_26;
    uint64_t var_160;
    uint64_t var_154;
    uint64_t _pre342;
    uint64_t _pre343;
    uint64_t _pre_phi354;
    uint64_t local_sp_3;
    uint32_t var_50;
    bool var_51;
    uint64_t var_52;
    uint64_t *var_53;
    uint64_t var_44;
    uint64_t var_48;
    unsigned char var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint32_t *var_21;
    uint32_t *var_22;
    uint32_t *var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint64_t var_27;
    uint64_t **var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_38;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_39;
    uint64_t local_sp_4;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t _pre_phi352;
    uint64_t *var_57;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t local_sp_6;
    uint64_t var_56;
    uint64_t var_58;
    uint64_t *var_59;
    uint64_t var_60;
    uint32_t *var_61;
    uint64_t *var_62;
    uint32_t *var_63;
    uint64_t *_pre_phi350;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint32_t var_69;
    uint32_t var_70;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t *var_78;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = var_0 + (-232L);
    var_5 = var_0 + (-224L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = *(uint64_t *)(rdi + 48UL);
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = var_7;
    var_9 = *var_6;
    var_10 = *(uint64_t *)(var_9 + 64UL);
    var_11 = *(uint64_t *)(var_9 + 88UL);
    var_12 = ((long)var_10 > (long)var_11) ? var_11 : var_10;
    var_13 = (uint64_t *)(var_0 + (-32L));
    *var_13 = var_12;
    var_14 = *var_6;
    local_sp_5 = var_4;
    var_121 = 1UL;
    var_140 = 0UL;
    rax_0 = 0UL;
    var_110 = 0UL;
    var_26 = var_12;
    local_sp_3 = var_4;
    var_64 = 0U;
    if (*(unsigned char *)(var_14 + 138UL) == '\x00') {
        if (*(uint64_t *)(var_14 + 120UL) == 0UL) {
            if (*(unsigned char *)(var_14 + 140UL) == '\x00') {
                var_15 = var_0 + (-80L);
                var_16 = (uint64_t *)var_15;
                var_17 = var_0 + (-128L);
                var_18 = (uint64_t *)var_17;
                var_19 = var_0 + (-204L);
                var_20 = (uint64_t *)(var_0 + (-88L));
                var_21 = (uint32_t *)(var_0 + (-92L));
                var_22 = (uint32_t *)var_19;
                var_23 = (uint32_t *)(var_0 + (-96L));
                var_24 = var_0 + (-200L);
                var_25 = (uint64_t *)(var_0 + (-104L));
                _pre_phi354 = var_17;
                _pre_phi352 = var_15;
                while (1U)
                    {
                        var_27 = *var_8;
                        var_49 = var_27;
                        var_38 = var_27;
                        var_39 = var_26;
                        local_sp_4 = local_sp_3;
                        if ((long)var_27 >= (long)var_26) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_28 = (uint64_t **)var_5;
                        var_29 = **var_28;
                        var_30 = *var_6;
                        if ((signed char)*(unsigned char *)((var_27 + *(uint64_t *)(var_30 + 40UL)) + var_29) >= '\x00') {
                            var_31 = (uint64_t)((uint32_t)var_30 + 32U);
                            var_32 = local_sp_3 + (-8L);
                            *(uint64_t *)var_32 = 4236442UL;
                            indirect_placeholder_2();
                            local_sp_4 = var_32;
                            if (var_31 != 0UL) {
                                var_33 = *(unsigned char *)((*var_8 + *(uint64_t *)(*var_6 + 40UL)) + **var_28);
                                var_34 = local_sp_3 + (-16L);
                                *(uint64_t *)var_34 = 4236490UL;
                                indirect_placeholder_2();
                                *(unsigned char *)(*var_8 + *(uint64_t *)(*var_6 + 8UL)) = var_33;
                                var_35 = *var_6;
                                var_36 = *(uint64_t *)(var_35 + 8UL);
                                var_37 = *var_8;
                                *(uint32_t *)((var_37 << 2UL) + *(uint64_t *)(var_35 + 16UL)) = (uint32_t)*(unsigned char *)(var_37 + var_36);
                                *var_8 = (*var_8 + 1UL);
                                local_sp_3_be = var_34;
                                var_26 = *var_13;
                                local_sp_3 = local_sp_3_be;
                                continue;
                            }
                            var_38 = *var_8;
                            var_39 = *var_13;
                        }
                        *var_16 = (var_39 - var_38);
                        *var_18 = *(uint64_t *)(*var_6 + 32UL);
                        var_40 = *var_16;
                        var_41 = **var_28 + (*var_8 + *(uint64_t *)(*var_6 + 40UL));
                        var_42 = local_sp_4 + (-8L);
                        *(uint64_t *)var_42 = 4236663UL;
                        var_43 = indirect_placeholder_9(var_40, var_41, var_19);
                        *var_20 = var_43;
                        local_sp_3_be = var_42;
                        if (var_43 < 18446744073709551614UL) {
                            switch_state_var = 0;
                            switch (var_43) {
                              case 18446744073709551615UL:
                              case 0UL:
                                {
                                    var_45 = *(unsigned char *)((*var_8 + *(uint64_t *)(*var_6 + 40UL)) + **var_28);
                                    *var_21 = (uint32_t)var_45;
                                    *(unsigned char *)(*var_8 + *(uint64_t *)(*var_6 + 8UL)) = var_45;
                                    var_46 = *(uint64_t *)(*var_6 + 16UL);
                                    var_47 = *var_8;
                                    *var_8 = (var_47 + 1UL);
                                    *(uint32_t *)((var_47 << 2UL) + var_46) = *var_21;
                                    if (*var_20 == 18446744073709551615UL) {
                                        *(uint64_t *)(*var_6 + 32UL) = *var_18;
                                    }
                                    var_26 = *var_13;
                                    local_sp_3 = local_sp_3_be;
                                    continue;
                                }
                                break;
                              case 18446744073709551614UL:
                                {
                                    var_44 = *var_6;
                                    var_48 = var_44;
                                    if ((long)*(uint64_t *)(var_44 + 64UL) < (long)*(uint64_t *)(var_44 + 88UL)) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_45 = *(unsigned char *)((*var_8 + *(uint64_t *)(*var_6 + 40UL)) + **var_28);
                                    *var_21 = (uint32_t)var_45;
                                    *(unsigned char *)(*var_8 + *(uint64_t *)(*var_6 + 8UL)) = var_45;
                                    var_46 = *(uint64_t *)(*var_6 + 16UL);
                                    var_47 = *var_8;
                                    *var_8 = (var_47 + 1UL);
                                    *(uint32_t *)((var_47 << 2UL) + var_46) = *var_21;
                                    if (*var_20 == 18446744073709551615UL) {
                                        *(uint64_t *)(*var_6 + 32UL) = *var_18;
                                    }
                                    var_26 = *var_13;
                                    local_sp_3 = local_sp_3_be;
                                    continue;
                                }
                                break;
                              default:
                                {
                                    var_48 = *var_6;
                                    loop_state_var = 2U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        var_50 = *var_22;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4236700UL;
                        indirect_placeholder_2();
                        *var_23 = var_50;
                        var_51 = ((uint64_t)(var_50 - *var_22) == 0UL);
                        var_52 = local_sp_4 + (-24L);
                        var_53 = (uint64_t *)var_52;
                        local_sp_2 = var_52;
                        local_sp_6 = var_52;
                        if (var_51) {
                            *var_53 = 4236886UL;
                            indirect_placeholder_2();
                        } else {
                            *var_53 = 4236738UL;
                            indirect_placeholder_2();
                            *var_25 = var_24;
                            if (*var_20 != var_24) {
                                *(uint64_t *)(var_0 + (-16L)) = *var_8;
                                _pre342 = *var_13;
                                _pre343 = *var_8;
                                var_54 = _pre343;
                                var_55 = _pre342;
                                loop_state_var = 0U;
                                break;
                            }
                            var_154 = local_sp_4 + (-32L);
                            *(uint64_t *)var_154 = 4236802UL;
                            indirect_placeholder_2();
                            local_sp_2 = var_154;
                        }
                        var_155 = *(uint64_t *)(*var_6 + 16UL);
                        var_156 = *var_8;
                        *var_8 = (var_156 + 1UL);
                        *(uint32_t *)((var_156 << 2UL) + var_155) = *var_23;
                        var_157 = (*var_20 + *var_8) + (-1L);
                        *var_16 = var_157;
                        var_158 = var_157;
                        local_sp_3_be = local_sp_2;
                        var_159 = *var_8;
                        while ((long)var_159 >= (long)var_158)
                            {
                                var_160 = *(uint64_t *)(*var_6 + 16UL);
                                *var_8 = (var_159 + 1UL);
                                *(uint32_t *)((var_159 << 2UL) + var_160) = 4294967295U;
                                var_158 = *var_16;
                                var_159 = *var_8;
                            }
                        var_26 = *var_13;
                        local_sp_3 = local_sp_3_be;
                        continue;
                    }
                switch (loop_state_var) {
                  case 2U:
                  case 1U:
                    {
                        switch (loop_state_var) {
                          case 2U:
                            {
                                *(uint64_t *)(var_48 + 32UL) = *var_18;
                                var_49 = *var_8;
                            }
                            break;
                          case 1U:
                            {
                                *(uint64_t *)(*var_6 + 48UL) = var_49;
                                *(uint64_t *)(*var_6 + 56UL) = *var_8;
                                return rax_0;
                            }
                            break;
                        }
                    }
                    break;
                  case 0U:
                    {
                        break;
                    }
                    break;
                }
            } else {
                *(uint64_t *)(var_0 + (-16L)) = *(uint64_t *)(var_14 + 56UL);
                var_151 = *var_8;
                var_152 = *var_13;
                var_54 = var_151;
                var_55 = var_152;
                local_sp_6 = local_sp_5;
                if ((long)var_151 >= (long)var_152) {
                    *(uint64_t *)(*var_6 + 48UL) = *var_8;
                    *(uint64_t *)(*var_6 + 56UL) = *(uint64_t *)(var_0 + (-16L));
                    return rax_0;
                }
                _pre_phi354 = var_0 + (-128L);
                _pre_phi352 = var_0 + (-80L);
                while (1U)
                    {
                        var_56 = var_55 - var_54;
                        var_57 = (uint64_t *)_pre_phi352;
                        *var_57 = var_56;
                        var_58 = *(uint64_t *)(*var_6 + 32UL);
                        var_59 = (uint64_t *)_pre_phi354;
                        *var_59 = var_58;
                        var_60 = *var_6;
                        rax_0 = 12UL;
                        if (*(uint64_t *)(var_60 + 120UL) == 0UL) {
                            var_71 = (*(uint64_t *)(var_0 + (-16L)) + *(uint64_t *)(var_60 + 40UL)) + **(uint64_t **)var_5;
                            var_72 = (uint64_t *)(var_0 + (-40L));
                            *var_72 = var_71;
                            _pre_phi350 = var_72;
                        } else {
                            var_61 = (uint32_t *)(var_0 + (-44L));
                            *var_61 = 0U;
                            var_62 = (uint64_t *)(var_0 + (-16L));
                            var_63 = (uint32_t *)(var_0 + (-108L));
                            var_65 = *var_6;
                            while ((int)*(uint32_t *)(var_65 + 144UL) <= (int)var_64)
                                {
                                    var_66 = (uint64_t)var_64;
                                    if ((long)*var_57 > (long)var_66) {
                                        break;
                                    }
                                    var_69 = (uint32_t)*(unsigned char *)(((*(uint64_t *)(var_65 + 40UL) + *var_62) + var_66) + **(uint64_t **)var_5);
                                    *var_63 = var_69;
                                    *(unsigned char *)((var_3 + (uint64_t)*var_61) + (-192L)) = *(unsigned char *)(*(uint64_t *)(*var_6 + 120UL) + (uint64_t)var_69);
                                    var_70 = *var_61 + 1U;
                                    *var_61 = var_70;
                                    var_64 = var_70;
                                    var_65 = *var_6;
                                }
                            var_67 = var_0 + (-200L);
                            var_68 = (uint64_t *)(var_0 + (-40L));
                            *var_68 = var_67;
                            _pre_phi350 = var_68;
                        }
                        var_73 = *var_57;
                        var_74 = *_pre_phi350;
                        var_75 = var_0 + (-208L);
                        var_76 = local_sp_6 + (-8L);
                        *(uint64_t *)var_76 = 4237542UL;
                        var_77 = indirect_placeholder_9(var_73, var_74, var_75);
                        var_78 = (uint64_t *)(var_0 + (-88L));
                        *var_78 = var_77;
                        local_sp_5 = var_76;
                        if (var_77 < 18446744073709551614UL) {
                            switch_state_var = 0;
                            switch (var_77) {
                              case 18446744073709551615UL:
                              case 0UL:
                                {
                                    var_80 = **(uint64_t **)var_5;
                                    var_81 = *(uint64_t *)(*var_6 + 40UL);
                                    var_82 = (uint64_t *)(var_0 + (-16L));
                                    var_83 = *(unsigned char *)((*var_82 + var_81) + var_80);
                                    var_84 = (uint32_t *)(var_0 + (-68L));
                                    var_85 = (uint32_t)var_83;
                                    *var_84 = var_85;
                                    var_86 = *var_6;
                                    var_87 = *(uint64_t *)(var_86 + 120UL);
                                    var_89 = var_85;
                                    var_90 = var_86;
                                    if (var_87 == 0UL) {
                                        var_88 = (uint32_t)*(unsigned char *)(var_87 + (uint64_t)var_85);
                                        *var_84 = var_88;
                                        var_89 = var_88;
                                        var_90 = *var_6;
                                    }
                                    *(unsigned char *)(*var_8 + *(uint64_t *)(var_90 + 8UL)) = (unsigned char)var_89;
                                    var_91 = *var_6;
                                    if (*(unsigned char *)(var_91 + 140UL) == '\x00') {
                                        *(uint64_t *)((*var_8 << 3UL) + *(uint64_t *)(var_91 + 24UL)) = *var_82;
                                    }
                                    *var_82 = (*var_82 + 1UL);
                                    var_92 = *(uint64_t *)(*var_6 + 16UL);
                                    var_93 = *var_8;
                                    *var_8 = (var_93 + 1UL);
                                    *(uint32_t *)((var_93 << 2UL) + var_92) = *var_84;
                                    if (*var_78 == 18446744073709551615UL) {
                                        *(uint64_t *)(*var_6 + 32UL) = *var_59;
                                    }
                                    var_151 = *var_8;
                                    var_152 = *var_13;
                                    var_54 = var_151;
                                    var_55 = var_152;
                                    local_sp_6 = local_sp_5;
                                    if ((long)var_151 >= (long)var_152) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    _pre_phi354 = var_0 + (-128L);
                                    _pre_phi352 = var_0 + (-80L);
                                    continue;
                                }
                                break;
                              case 18446744073709551614UL:
                                {
                                    var_79 = *var_6;
                                    var_94 = var_79;
                                    if ((long)*(uint64_t *)(var_79 + 64UL) < (long)*(uint64_t *)(var_79 + 88UL)) {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    var_80 = **(uint64_t **)var_5;
                                    var_81 = *(uint64_t *)(*var_6 + 40UL);
                                    var_82 = (uint64_t *)(var_0 + (-16L));
                                    var_83 = *(unsigned char *)((*var_82 + var_81) + var_80);
                                    var_84 = (uint32_t *)(var_0 + (-68L));
                                    var_85 = (uint32_t)var_83;
                                    *var_84 = var_85;
                                    var_86 = *var_6;
                                    var_87 = *(uint64_t *)(var_86 + 120UL);
                                    var_89 = var_85;
                                    var_90 = var_86;
                                    if (var_87 == 0UL) {
                                        var_88 = (uint32_t)*(unsigned char *)(var_87 + (uint64_t)var_85);
                                        *var_84 = var_88;
                                        var_89 = var_88;
                                        var_90 = *var_6;
                                    }
                                    *(unsigned char *)(*var_8 + *(uint64_t *)(var_90 + 8UL)) = (unsigned char)var_89;
                                    var_91 = *var_6;
                                    if (*(unsigned char *)(var_91 + 140UL) == '\x00') {
                                        *(uint64_t *)((*var_8 << 3UL) + *(uint64_t *)(var_91 + 24UL)) = *var_82;
                                    }
                                    *var_82 = (*var_82 + 1UL);
                                    var_92 = *(uint64_t *)(*var_6 + 16UL);
                                    var_93 = *var_8;
                                    *var_8 = (var_93 + 1UL);
                                    *(uint32_t *)((var_93 << 2UL) + var_92) = *var_84;
                                    if (*var_78 == 18446744073709551615UL) {
                                        *(uint64_t *)(*var_6 + 32UL) = *var_59;
                                    }
                                    var_151 = *var_8;
                                    var_152 = *var_13;
                                    var_54 = var_151;
                                    var_55 = var_152;
                                    local_sp_6 = local_sp_5;
                                    if ((long)var_151 < (long)var_152) {
                                        loop_state_var = 1U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    _pre_phi354 = var_0 + (-128L);
                                    _pre_phi352 = var_0 + (-80L);
                                    continue;
                                }
                                break;
                              default:
                                {
                                    var_94 = *var_6;
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                break;
                            }
                            if (switch_state_var)
                                break;
                        }
                        var_95 = (uint32_t *)var_75;
                        var_96 = *var_95;
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4237579UL;
                        indirect_placeholder_2();
                        var_97 = (uint32_t *)(var_0 + (-112L));
                        *var_97 = var_96;
                        if ((uint64_t)(var_96 - *var_95) == 0UL) {
                            var_137 = local_sp_6 + (-24L);
                            *(uint64_t *)var_137 = 4238375UL;
                            indirect_placeholder_2();
                            local_sp_0 = var_137;
                        } else {
                            var_98 = var_0 + (-200L);
                            var_99 = local_sp_6 + (-24L);
                            *(uint64_t *)var_99 = 4237621UL;
                            indirect_placeholder_2();
                            var_100 = (uint64_t *)(var_0 + (-120L));
                            *var_100 = var_98;
                            local_sp_1 = var_99;
                            if (*var_78 != var_98) {
                                var_101 = *var_8 + var_98;
                                var_102 = *var_6;
                                var_103 = *(uint64_t *)(var_102 + 64UL);
                                var_108 = var_102;
                                if (var_101 <= var_103) {
                                    *(uint64_t *)(var_102 + 32UL) = *var_59;
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*(uint64_t *)(var_102 + 24UL) != 0UL) {
                                    var_104 = var_103 << 3UL;
                                    var_105 = local_sp_6 + (-32L);
                                    *(uint64_t *)var_105 = 4237787UL;
                                    var_106 = indirect_placeholder_117(var_104);
                                    *(uint64_t *)(*var_6 + 24UL) = var_106.field_0;
                                    var_107 = *var_6;
                                    var_108 = var_107;
                                    local_sp_1 = var_105;
                                    if (*(uint64_t *)(var_107 + 24UL) != 0UL) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                }
                                if (*(unsigned char *)(var_108 + 140UL) == '\x00') {
                                    _pre_phi362 = (uint64_t *)(var_0 + (-56L));
                                } else {
                                    var_109 = (uint64_t *)(var_0 + (-56L));
                                    *var_109 = 0UL;
                                    _pre_phi362 = var_109;
                                    var_111 = *var_8;
                                    var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
                                    var_113 = (var_112 == 0UL);
                                    var_114 = *var_6;
                                    while (!var_113)
                                        {
                                            var_115 = *(uint64_t *)(var_114 + 24UL);
                                            var_116 = *var_109;
                                            *(uint64_t *)((var_116 << 3UL) + var_115) = var_116;
                                            var_117 = *var_109 + 1UL;
                                            *var_109 = var_117;
                                            var_110 = var_117;
                                            var_111 = *var_8;
                                            var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
                                            var_113 = (var_112 == 0UL);
                                            var_114 = *var_6;
                                        }
                                    *(unsigned char *)(var_114 + 140UL) = (unsigned char)'\x01';
                                }
                                var_118 = local_sp_1 + (-8L);
                                *(uint64_t *)var_118 = 4237951UL;
                                indirect_placeholder_2();
                                *(uint32_t *)((*var_8 << 2UL) + *(uint64_t *)(*var_6 + 16UL)) = *var_97;
                                var_119 = (*var_8 << 3UL) + *(uint64_t *)(*var_6 + 24UL);
                                var_120 = (uint64_t *)(var_0 + (-16L));
                                *(uint64_t *)var_119 = *var_120;
                                *_pre_phi362 = 1UL;
                                local_sp_5 = var_118;
                                var_122 = *var_100;
                                var_123 = helper_cc_compute_c_wrapper(var_121 - var_122, var_122, var_2, 17U);
                                while (var_123 != 0UL)
                                    {
                                        var_124 = *_pre_phi362;
                                        var_125 = *var_78;
                                        var_126 = helper_cc_compute_c_wrapper(var_124 - var_125, var_125, var_2, 17U);
                                        if (var_126 == 0UL) {
                                            var_128 = *_pre_phi362;
                                            storemerge = *var_78 + (-1L);
                                        } else {
                                            var_127 = *_pre_phi362;
                                            var_128 = var_127;
                                            storemerge = var_127;
                                        }
                                        *(uint64_t *)(*(uint64_t *)(*var_6 + 24UL) + ((var_128 + *var_8) << 3UL)) = (*var_120 + storemerge);
                                        *(uint32_t *)(*(uint64_t *)(*var_6 + 16UL) + ((*_pre_phi362 + *var_8) << 2UL)) = 4294967295U;
                                        var_129 = *_pre_phi362 + 1UL;
                                        *_pre_phi362 = var_129;
                                        var_121 = var_129;
                                        var_122 = *var_100;
                                        var_123 = helper_cc_compute_c_wrapper(var_121 - var_122, var_122, var_2, 17U);
                                    }
                                var_130 = (uint64_t *)(*var_6 + 88UL);
                                *var_130 = ((*var_100 - *var_78) + *var_130);
                                var_131 = *var_6;
                                var_133 = var_131;
                                if ((long)*var_120 < (long)*(uint64_t *)(var_131 + 96UL)) {
                                    var_132 = (uint64_t *)(var_131 + 104UL);
                                    *var_132 = ((*var_100 - *var_78) + *var_132);
                                    var_133 = *var_6;
                                }
                                var_134 = *(uint64_t *)(var_133 + 64UL);
                                var_135 = *(uint64_t *)(var_133 + 88UL);
                                *var_13 = (((long)var_134 > (long)var_135) ? var_135 : var_134);
                                *var_8 = (*var_100 + *var_8);
                                *var_120 = (*var_78 + *var_120);
                                var_151 = *var_8;
                                var_152 = *var_13;
                                var_54 = var_151;
                                var_55 = var_152;
                                local_sp_6 = local_sp_5;
                                if ((long)var_151 >= (long)var_152) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                _pre_phi354 = var_0 + (-128L);
                                _pre_phi352 = var_0 + (-80L);
                                continue;
                            }
                            var_136 = local_sp_6 + (-32L);
                            *(uint64_t *)var_136 = 4237685UL;
                            indirect_placeholder_2();
                            local_sp_0 = var_136;
                        }
                        local_sp_5 = local_sp_0;
                        if (*(unsigned char *)(*var_6 + 140UL) != '\x00') {
                            var_138 = (uint64_t *)(var_0 + (-64L));
                            *var_138 = 0UL;
                            var_139 = (uint64_t *)(var_0 + (-16L));
                            _pre_phi358 = var_139;
                            var_141 = *var_78;
                            var_142 = helper_cc_compute_c_wrapper(var_140 - var_141, var_141, var_2, 17U);
                            while (var_142 != 0UL)
                                {
                                    var_143 = *var_139;
                                    var_144 = *var_138;
                                    *(uint64_t *)(*(uint64_t *)(*var_6 + 24UL) + ((var_144 + *var_8) << 3UL)) = (var_144 + var_143);
                                    var_145 = *var_138 + 1UL;
                                    *var_138 = var_145;
                                    var_140 = var_145;
                                    var_141 = *var_78;
                                    var_142 = helper_cc_compute_c_wrapper(var_140 - var_141, var_141, var_2, 17U);
                                }
                        }
                        _pre_phi358 = (uint64_t *)(var_0 + (-16L));
                        *_pre_phi358 = (*var_78 + *_pre_phi358);
                        var_146 = *(uint64_t *)(*var_6 + 16UL);
                        var_147 = *var_8;
                        *var_8 = (var_147 + 1UL);
                        *(uint32_t *)((var_147 << 2UL) + var_146) = *var_97;
                        var_148 = (*var_78 + *var_8) + (-1L);
                        *var_57 = var_148;
                        var_149 = var_148;
                        var_150 = *var_8;
                        while ((long)var_150 >= (long)var_149)
                            {
                                var_153 = *(uint64_t *)(*var_6 + 16UL);
                                *var_8 = (var_150 + 1UL);
                                *(uint32_t *)((var_150 << 2UL) + var_153) = 4294967295U;
                                var_149 = *var_57;
                                var_150 = *var_8;
                            }
                        var_151 = *var_8;
                        var_152 = *var_13;
                        var_54 = var_151;
                        var_55 = var_152;
                        local_sp_6 = local_sp_5;
                        if ((long)var_151 >= (long)var_152) {
                            loop_state_var = 1U;
                            break;
                        }
                        _pre_phi354 = var_0 + (-128L);
                        _pre_phi352 = var_0 + (-80L);
                        continue;
                    }
                switch (loop_state_var) {
                  case 2U:
                    {
                        return rax_0;
                    }
                    break;
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 0U:
                            {
                                *(uint64_t *)(var_94 + 32UL) = *var_59;
                            }
                            break;
                          case 1U:
                            {
                                *(uint64_t *)(*var_6 + 48UL) = *var_8;
                                *(uint64_t *)(*var_6 + 56UL) = *(uint64_t *)(var_0 + (-16L));
                                return rax_0;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        } else {
            *(uint64_t *)(var_0 + (-16L)) = *(uint64_t *)(var_14 + 56UL);
            var_151 = *var_8;
            var_152 = *var_13;
            var_54 = var_151;
            var_55 = var_152;
            local_sp_6 = local_sp_5;
            if ((long)var_151 >= (long)var_152) {
                *(uint64_t *)(*var_6 + 48UL) = *var_8;
                *(uint64_t *)(*var_6 + 56UL) = *(uint64_t *)(var_0 + (-16L));
                return rax_0;
            }
            _pre_phi354 = var_0 + (-128L);
            _pre_phi352 = var_0 + (-80L);
        }
    } else {
        *(uint64_t *)(var_0 + (-16L)) = *(uint64_t *)(var_14 + 56UL);
        var_151 = *var_8;
        var_152 = *var_13;
        var_54 = var_151;
        var_55 = var_152;
        local_sp_6 = local_sp_5;
        if ((long)var_151 < (long)var_152) {
            *(uint64_t *)(*var_6 + 48UL) = *var_8;
            *(uint64_t *)(*var_6 + 56UL) = *(uint64_t *)(var_0 + (-16L));
            return rax_0;
        }
        _pre_phi354 = var_0 + (-128L);
        _pre_phi352 = var_0 + (-80L);
        while (1U)
            {
                var_56 = var_55 - var_54;
                var_57 = (uint64_t *)_pre_phi352;
                *var_57 = var_56;
                var_58 = *(uint64_t *)(*var_6 + 32UL);
                var_59 = (uint64_t *)_pre_phi354;
                *var_59 = var_58;
                var_60 = *var_6;
                rax_0 = 12UL;
                if (*(uint64_t *)(var_60 + 120UL) == 0UL) {
                    var_71 = (*(uint64_t *)(var_0 + (-16L)) + *(uint64_t *)(var_60 + 40UL)) + **(uint64_t **)var_5;
                    var_72 = (uint64_t *)(var_0 + (-40L));
                    *var_72 = var_71;
                    _pre_phi350 = var_72;
                } else {
                    var_61 = (uint32_t *)(var_0 + (-44L));
                    *var_61 = 0U;
                    var_62 = (uint64_t *)(var_0 + (-16L));
                    var_63 = (uint32_t *)(var_0 + (-108L));
                    var_65 = *var_6;
                    while ((int)*(uint32_t *)(var_65 + 144UL) <= (int)var_64)
                        {
                            var_66 = (uint64_t)var_64;
                            if ((long)*var_57 > (long)var_66) {
                                break;
                            }
                            var_69 = (uint32_t)*(unsigned char *)(((*(uint64_t *)(var_65 + 40UL) + *var_62) + var_66) + **(uint64_t **)var_5);
                            *var_63 = var_69;
                            *(unsigned char *)((var_3 + (uint64_t)*var_61) + (-192L)) = *(unsigned char *)(*(uint64_t *)(*var_6 + 120UL) + (uint64_t)var_69);
                            var_70 = *var_61 + 1U;
                            *var_61 = var_70;
                            var_64 = var_70;
                            var_65 = *var_6;
                        }
                    var_67 = var_0 + (-200L);
                    var_68 = (uint64_t *)(var_0 + (-40L));
                    *var_68 = var_67;
                    _pre_phi350 = var_68;
                }
                var_73 = *var_57;
                var_74 = *_pre_phi350;
                var_75 = var_0 + (-208L);
                var_76 = local_sp_6 + (-8L);
                *(uint64_t *)var_76 = 4237542UL;
                var_77 = indirect_placeholder_9(var_73, var_74, var_75);
                var_78 = (uint64_t *)(var_0 + (-88L));
                *var_78 = var_77;
                local_sp_5 = var_76;
                if (var_77 < 18446744073709551614UL) {
                    switch_state_var = 0;
                    switch (var_77) {
                      case 18446744073709551615UL:
                      case 0UL:
                        {
                            var_80 = **(uint64_t **)var_5;
                            var_81 = *(uint64_t *)(*var_6 + 40UL);
                            var_82 = (uint64_t *)(var_0 + (-16L));
                            var_83 = *(unsigned char *)((*var_82 + var_81) + var_80);
                            var_84 = (uint32_t *)(var_0 + (-68L));
                            var_85 = (uint32_t)var_83;
                            *var_84 = var_85;
                            var_86 = *var_6;
                            var_87 = *(uint64_t *)(var_86 + 120UL);
                            var_89 = var_85;
                            var_90 = var_86;
                            if (var_87 == 0UL) {
                                var_88 = (uint32_t)*(unsigned char *)(var_87 + (uint64_t)var_85);
                                *var_84 = var_88;
                                var_89 = var_88;
                                var_90 = *var_6;
                            }
                            *(unsigned char *)(*var_8 + *(uint64_t *)(var_90 + 8UL)) = (unsigned char)var_89;
                            var_91 = *var_6;
                            if (*(unsigned char *)(var_91 + 140UL) == '\x00') {
                                *(uint64_t *)((*var_8 << 3UL) + *(uint64_t *)(var_91 + 24UL)) = *var_82;
                            }
                            *var_82 = (*var_82 + 1UL);
                            var_92 = *(uint64_t *)(*var_6 + 16UL);
                            var_93 = *var_8;
                            *var_8 = (var_93 + 1UL);
                            *(uint32_t *)((var_93 << 2UL) + var_92) = *var_84;
                            if (*var_78 == 18446744073709551615UL) {
                                *(uint64_t *)(*var_6 + 32UL) = *var_59;
                            }
                            var_151 = *var_8;
                            var_152 = *var_13;
                            var_54 = var_151;
                            var_55 = var_152;
                            local_sp_6 = local_sp_5;
                            if ((long)var_151 >= (long)var_152) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            _pre_phi354 = var_0 + (-128L);
                            _pre_phi352 = var_0 + (-80L);
                            continue;
                        }
                        break;
                      case 18446744073709551614UL:
                        {
                            var_79 = *var_6;
                            var_94 = var_79;
                            if ((long)*(uint64_t *)(var_79 + 64UL) < (long)*(uint64_t *)(var_79 + 88UL)) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_80 = **(uint64_t **)var_5;
                            var_81 = *(uint64_t *)(*var_6 + 40UL);
                            var_82 = (uint64_t *)(var_0 + (-16L));
                            var_83 = *(unsigned char *)((*var_82 + var_81) + var_80);
                            var_84 = (uint32_t *)(var_0 + (-68L));
                            var_85 = (uint32_t)var_83;
                            *var_84 = var_85;
                            var_86 = *var_6;
                            var_87 = *(uint64_t *)(var_86 + 120UL);
                            var_89 = var_85;
                            var_90 = var_86;
                            if (var_87 == 0UL) {
                                var_88 = (uint32_t)*(unsigned char *)(var_87 + (uint64_t)var_85);
                                *var_84 = var_88;
                                var_89 = var_88;
                                var_90 = *var_6;
                            }
                            *(unsigned char *)(*var_8 + *(uint64_t *)(var_90 + 8UL)) = (unsigned char)var_89;
                            var_91 = *var_6;
                            if (*(unsigned char *)(var_91 + 140UL) == '\x00') {
                                *(uint64_t *)((*var_8 << 3UL) + *(uint64_t *)(var_91 + 24UL)) = *var_82;
                            }
                            *var_82 = (*var_82 + 1UL);
                            var_92 = *(uint64_t *)(*var_6 + 16UL);
                            var_93 = *var_8;
                            *var_8 = (var_93 + 1UL);
                            *(uint32_t *)((var_93 << 2UL) + var_92) = *var_84;
                            if (*var_78 == 18446744073709551615UL) {
                                *(uint64_t *)(*var_6 + 32UL) = *var_59;
                            }
                            var_151 = *var_8;
                            var_152 = *var_13;
                            var_54 = var_151;
                            var_55 = var_152;
                            local_sp_6 = local_sp_5;
                            if ((long)var_151 < (long)var_152) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            _pre_phi354 = var_0 + (-128L);
                            _pre_phi352 = var_0 + (-80L);
                            continue;
                        }
                        break;
                      default:
                        {
                            var_94 = *var_6;
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
                var_95 = (uint32_t *)var_75;
                var_96 = *var_95;
                *(uint64_t *)(local_sp_6 + (-16L)) = 4237579UL;
                indirect_placeholder_2();
                var_97 = (uint32_t *)(var_0 + (-112L));
                *var_97 = var_96;
                if ((uint64_t)(var_96 - *var_95) == 0UL) {
                    var_137 = local_sp_6 + (-24L);
                    *(uint64_t *)var_137 = 4238375UL;
                    indirect_placeholder_2();
                    local_sp_0 = var_137;
                } else {
                    var_98 = var_0 + (-200L);
                    var_99 = local_sp_6 + (-24L);
                    *(uint64_t *)var_99 = 4237621UL;
                    indirect_placeholder_2();
                    var_100 = (uint64_t *)(var_0 + (-120L));
                    *var_100 = var_98;
                    local_sp_1 = var_99;
                    if (*var_78 != var_98) {
                        var_101 = *var_8 + var_98;
                        var_102 = *var_6;
                        var_103 = *(uint64_t *)(var_102 + 64UL);
                        var_108 = var_102;
                        if (var_101 <= var_103) {
                            *(uint64_t *)(var_102 + 32UL) = *var_59;
                            loop_state_var = 1U;
                            break;
                        }
                        if (*(uint64_t *)(var_102 + 24UL) != 0UL) {
                            var_104 = var_103 << 3UL;
                            var_105 = local_sp_6 + (-32L);
                            *(uint64_t *)var_105 = 4237787UL;
                            var_106 = indirect_placeholder_117(var_104);
                            *(uint64_t *)(*var_6 + 24UL) = var_106.field_0;
                            var_107 = *var_6;
                            var_108 = var_107;
                            local_sp_1 = var_105;
                            if (*(uint64_t *)(var_107 + 24UL) != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                        }
                        if (*(unsigned char *)(var_108 + 140UL) == '\x00') {
                            _pre_phi362 = (uint64_t *)(var_0 + (-56L));
                        } else {
                            var_109 = (uint64_t *)(var_0 + (-56L));
                            *var_109 = 0UL;
                            _pre_phi362 = var_109;
                            var_111 = *var_8;
                            var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
                            var_113 = (var_112 == 0UL);
                            var_114 = *var_6;
                            while (!var_113)
                                {
                                    var_115 = *(uint64_t *)(var_114 + 24UL);
                                    var_116 = *var_109;
                                    *(uint64_t *)((var_116 << 3UL) + var_115) = var_116;
                                    var_117 = *var_109 + 1UL;
                                    *var_109 = var_117;
                                    var_110 = var_117;
                                    var_111 = *var_8;
                                    var_112 = helper_cc_compute_c_wrapper(var_110 - var_111, var_111, var_2, 17U);
                                    var_113 = (var_112 == 0UL);
                                    var_114 = *var_6;
                                }
                            *(unsigned char *)(var_114 + 140UL) = (unsigned char)'\x01';
                        }
                        var_118 = local_sp_1 + (-8L);
                        *(uint64_t *)var_118 = 4237951UL;
                        indirect_placeholder_2();
                        *(uint32_t *)((*var_8 << 2UL) + *(uint64_t *)(*var_6 + 16UL)) = *var_97;
                        var_119 = (*var_8 << 3UL) + *(uint64_t *)(*var_6 + 24UL);
                        var_120 = (uint64_t *)(var_0 + (-16L));
                        *(uint64_t *)var_119 = *var_120;
                        *_pre_phi362 = 1UL;
                        local_sp_5 = var_118;
                        var_122 = *var_100;
                        var_123 = helper_cc_compute_c_wrapper(var_121 - var_122, var_122, var_2, 17U);
                        while (var_123 != 0UL)
                            {
                                var_124 = *_pre_phi362;
                                var_125 = *var_78;
                                var_126 = helper_cc_compute_c_wrapper(var_124 - var_125, var_125, var_2, 17U);
                                if (var_126 == 0UL) {
                                    var_128 = *_pre_phi362;
                                    storemerge = *var_78 + (-1L);
                                } else {
                                    var_127 = *_pre_phi362;
                                    var_128 = var_127;
                                    storemerge = var_127;
                                }
                                *(uint64_t *)(*(uint64_t *)(*var_6 + 24UL) + ((var_128 + *var_8) << 3UL)) = (*var_120 + storemerge);
                                *(uint32_t *)(*(uint64_t *)(*var_6 + 16UL) + ((*_pre_phi362 + *var_8) << 2UL)) = 4294967295U;
                                var_129 = *_pre_phi362 + 1UL;
                                *_pre_phi362 = var_129;
                                var_121 = var_129;
                                var_122 = *var_100;
                                var_123 = helper_cc_compute_c_wrapper(var_121 - var_122, var_122, var_2, 17U);
                            }
                        var_130 = (uint64_t *)(*var_6 + 88UL);
                        *var_130 = ((*var_100 - *var_78) + *var_130);
                        var_131 = *var_6;
                        var_133 = var_131;
                        if ((long)*var_120 < (long)*(uint64_t *)(var_131 + 96UL)) {
                            var_132 = (uint64_t *)(var_131 + 104UL);
                            *var_132 = ((*var_100 - *var_78) + *var_132);
                            var_133 = *var_6;
                        }
                        var_134 = *(uint64_t *)(var_133 + 64UL);
                        var_135 = *(uint64_t *)(var_133 + 88UL);
                        *var_13 = (((long)var_134 > (long)var_135) ? var_135 : var_134);
                        *var_8 = (*var_100 + *var_8);
                        *var_120 = (*var_78 + *var_120);
                        var_151 = *var_8;
                        var_152 = *var_13;
                        var_54 = var_151;
                        var_55 = var_152;
                        local_sp_6 = local_sp_5;
                        if ((long)var_151 >= (long)var_152) {
                            loop_state_var = 1U;
                            break;
                        }
                        _pre_phi354 = var_0 + (-128L);
                        _pre_phi352 = var_0 + (-80L);
                        continue;
                    }
                    var_136 = local_sp_6 + (-32L);
                    *(uint64_t *)var_136 = 4237685UL;
                    indirect_placeholder_2();
                    local_sp_0 = var_136;
                }
                local_sp_5 = local_sp_0;
                if (*(unsigned char *)(*var_6 + 140UL) != '\x00') {
                    var_138 = (uint64_t *)(var_0 + (-64L));
                    *var_138 = 0UL;
                    var_139 = (uint64_t *)(var_0 + (-16L));
                    _pre_phi358 = var_139;
                    var_141 = *var_78;
                    var_142 = helper_cc_compute_c_wrapper(var_140 - var_141, var_141, var_2, 17U);
                    while (var_142 != 0UL)
                        {
                            var_143 = *var_139;
                            var_144 = *var_138;
                            *(uint64_t *)(*(uint64_t *)(*var_6 + 24UL) + ((var_144 + *var_8) << 3UL)) = (var_144 + var_143);
                            var_145 = *var_138 + 1UL;
                            *var_138 = var_145;
                            var_140 = var_145;
                            var_141 = *var_78;
                            var_142 = helper_cc_compute_c_wrapper(var_140 - var_141, var_141, var_2, 17U);
                        }
                }
                _pre_phi358 = (uint64_t *)(var_0 + (-16L));
                *_pre_phi358 = (*var_78 + *_pre_phi358);
                var_146 = *(uint64_t *)(*var_6 + 16UL);
                var_147 = *var_8;
                *var_8 = (var_147 + 1UL);
                *(uint32_t *)((var_147 << 2UL) + var_146) = *var_97;
                var_148 = (*var_78 + *var_8) + (-1L);
                *var_57 = var_148;
                var_149 = var_148;
                var_150 = *var_8;
                while ((long)var_150 >= (long)var_149)
                    {
                        var_153 = *(uint64_t *)(*var_6 + 16UL);
                        *var_8 = (var_150 + 1UL);
                        *(uint32_t *)((var_150 << 2UL) + var_153) = 4294967295U;
                        var_149 = *var_57;
                        var_150 = *var_8;
                    }
                var_151 = *var_8;
                var_152 = *var_13;
                var_54 = var_151;
                var_55 = var_152;
                local_sp_6 = local_sp_5;
                if ((long)var_151 >= (long)var_152) {
                    loop_state_var = 1U;
                    break;
                }
                _pre_phi354 = var_0 + (-128L);
                _pre_phi352 = var_0 + (-80L);
                continue;
            }
        switch (loop_state_var) {
          case 2U:
            {
                return rax_0;
            }
            break;
          case 1U:
          case 0U:
            {
                switch (loop_state_var) {
                  case 0U:
                    {
                        *(uint64_t *)(var_94 + 32UL) = *var_59;
                    }
                    break;
                  case 1U:
                    {
                        *(uint64_t *)(*var_6 + 48UL) = *var_8;
                        *(uint64_t *)(*var_6 + 56UL) = *(uint64_t *)(var_0 + (-16L));
                        return rax_0;
                    }
                    break;
                }
            }
            break;
        }
    }
}
