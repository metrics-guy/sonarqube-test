typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_67(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_parse_dup_op(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9) {
    uint64_t var_77;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    unsigned char var_18;
    uint64_t var_80;
    uint64_t var_70;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_68;
    uint64_t *var_69;
    uint64_t local_sp_0;
    uint64_t var_81;
    uint64_t var_85;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t rax_1;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t local_sp_2;
    uint64_t var_54;
    uint64_t var_45;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t local_sp_1;
    uint64_t var_46;
    uint64_t var_86;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_59;
    uint64_t var_63;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t local_sp_3;
    uint64_t *var_64;
    uint64_t spec_select;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_28;
    uint64_t var_34;
    uint64_t var_29;
    uint64_t rax_0;
    uint64_t local_sp_4;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_5;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t local_sp_6;
    bool var_90;
    uint32_t *var_91;
    uint64_t *var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t *var_27;
    uint64_t _pre183;
    uint64_t var_40;
    uint64_t var_41;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-136L);
    var_3 = (uint64_t *)(var_0 + (-96L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-104L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-112L));
    *var_5 = rdx;
    var_6 = var_0 + (-120L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rcx;
    var_8 = (uint64_t *)(var_0 + (-128L));
    *var_8 = r8;
    *(uint64_t *)var_2 = r9;
    var_9 = (uint64_t *)(var_0 + (-16L));
    *var_9 = 0UL;
    var_10 = (uint64_t *)(var_0 + (-24L));
    *var_10 = 0UL;
    var_11 = *(uint64_t *)(*var_4 + 72UL);
    var_12 = (uint64_t *)(var_0 + (-56L));
    *var_12 = var_11;
    var_13 = *var_7;
    var_14 = *(uint64_t *)(var_13 + 8UL);
    var_15 = *(uint64_t *)var_13;
    var_16 = (uint64_t *)(var_0 + (-88L));
    *var_16 = var_15;
    var_17 = (uint64_t *)(var_0 + (-80L));
    *var_17 = var_14;
    var_18 = *(unsigned char *)(*var_7 + 8UL);
    rax_1 = 0UL;
    var_45 = 2UL;
    var_34 = 18446744073709551614UL;
    rax_0 = 18446744073709551614UL;
    local_sp_6 = var_2;
    if (var_18 == '\x17') {
        *(uint64_t *)(var_0 + (-40L)) = (var_18 == '\x12');
        *(uint64_t *)(var_0 + (-48L)) = ((*(unsigned char *)(*var_7 + 8UL) == '\x13') ? 1UL : 18446744073709551615UL);
        _pre183 = *var_7;
        var_37 = _pre183;
        var_38 = *var_8;
        var_39 = *var_4;
        var_40 = local_sp_6 + (-8L);
        *(uint64_t *)var_40 = 4272769UL;
        indirect_placeholder_67(var_38, var_39, var_37);
        var_41 = *var_3;
        local_sp_1 = var_40;
        local_sp_2 = var_40;
        if (var_41 == 0UL) {
            return rax_1;
        }
        var_42 = (uint64_t *)(var_0 + (-40L));
        var_43 = *var_42;
        if (var_43 != 0UL) {
            if (*(uint64_t *)(var_0 + (-48L)) != 0UL) {
                *(uint64_t *)(local_sp_6 + (-16L)) = 4272849UL;
                indirect_placeholder_9(0UL, 4281605UL, var_41);
                return rax_1;
            }
        }
        if ((long)var_43 > (long)0UL) {
            *var_10 = 0UL;
            var_59 = *var_3;
            var_63 = var_59;
            local_sp_3 = local_sp_2;
            if (*(unsigned char *)(var_59 + 48UL) == '\x11') {
                var_60 = *(uint64_t *)(var_59 + 40UL);
                *(uint64_t *)(var_0 + (-64L)) = var_60;
                var_61 = *var_3;
                var_62 = local_sp_2 + (-8L);
                *(uint64_t *)var_62 = 4273129UL;
                indirect_placeholder_9(var_60, 4281437UL, var_61);
                var_63 = *var_3;
                local_sp_3 = var_62;
            }
            var_64 = (uint64_t *)(var_0 + (-48L));
            spec_select = (*var_64 == 18446744073709551615UL) ? 11UL : 10UL;
            var_65 = *var_5;
            var_66 = local_sp_3 + (-8L);
            *(uint64_t *)var_66 = 4273171UL;
            var_67 = indirect_placeholder_10(spec_select, 0UL, var_63, var_65);
            *var_9 = var_67;
            local_sp_0 = var_66;
            if (var_67 != 0UL) {
                var_68 = *var_42 + 2UL;
                var_69 = (uint64_t *)(var_0 + (-32L));
                *var_69 = var_68;
                var_70 = var_68;
                while (1U)
                    {
                        if ((long)var_70 > (long)*var_64) {
                            var_71 = *var_5;
                            var_72 = *var_3;
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4273231UL;
                            var_73 = indirect_placeholder(var_71, var_72);
                            *var_3 = var_73;
                            var_74 = *var_9;
                            var_75 = *var_5;
                            *(uint64_t *)(local_sp_0 + (-16L)) = 4273260UL;
                            var_76 = indirect_placeholder_10(16UL, var_73, var_74, var_75);
                            *var_9 = var_76;
                            if (!((*var_3 == 0UL) || (var_76 == 0UL))) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_77 = *var_5;
                            var_78 = local_sp_0 + (-24L);
                            *(uint64_t *)var_78 = 4273326UL;
                            var_79 = indirect_placeholder_10(10UL, 0UL, var_76, var_77);
                            *var_9 = var_79;
                            local_sp_0 = var_78;
                            if (var_79 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_80 = *var_69 + 1UL;
                            *var_69 = var_80;
                            var_70 = var_80;
                            continue;
                        }
                        var_81 = *var_10;
                        if (var_81 == 0UL) {
                            var_85 = *var_9;
                            loop_state_var = 1U;
                            break;
                        }
                        var_82 = *var_9;
                        var_83 = *var_5;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4273397UL;
                        var_84 = indirect_placeholder_10(16UL, var_82, var_81, var_83);
                        *var_9 = var_84;
                        var_85 = var_84;
                        loop_state_var = 1U;
                        break;
                    }
                switch (loop_state_var) {
                  case 0U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                    {
                        rax_1 = var_85;
                        return rax_1;
                    }
                    break;
                }
            }
            **(uint32_t **)var_2 = 12U;
        } else {
            *var_9 = var_41;
            var_44 = (uint64_t *)(var_0 + (-32L));
            *var_44 = 2UL;
            while (1U)
                {
                    var_46 = *var_42;
                    if ((long)var_45 > (long)var_46) {
                        var_47 = *var_5;
                        var_48 = *var_3;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4272916UL;
                        var_49 = indirect_placeholder(var_47, var_48);
                        *var_3 = var_49;
                        var_50 = *var_9;
                        var_51 = *var_5;
                        var_52 = local_sp_1 + (-16L);
                        *(uint64_t *)var_52 = 4272945UL;
                        var_53 = indirect_placeholder_10(16UL, var_49, var_50, var_51);
                        *var_9 = var_53;
                        local_sp_1 = var_52;
                        if (!((*var_3 == 0UL) || (var_53 == 0UL))) {
                            loop_state_var = 2U;
                            break;
                        }
                        var_54 = *var_44 + 1UL;
                        *var_44 = var_54;
                        var_45 = var_54;
                        continue;
                    }
                    if (var_46 == *(uint64_t *)(var_0 + (-48L))) {
                        var_86 = *var_9;
                        rax_1 = var_86;
                        loop_state_var = 1U;
                        break;
                    }
                    var_55 = *var_5;
                    var_56 = *var_3;
                    var_57 = local_sp_1 + (-8L);
                    *(uint64_t *)var_57 = 4273042UL;
                    var_58 = indirect_placeholder(var_55, var_56);
                    *var_3 = var_58;
                    local_sp_2 = var_57;
                    if (var_58 != 0UL) {
                        loop_state_var = 2U;
                        break;
                    }
                    *var_10 = *var_9;
                    loop_state_var = 0U;
                    break;
                }
            switch (loop_state_var) {
              case 1U:
                {
                    return rax_1;
                }
                break;
              case 2U:
                {
                    **(uint32_t **)var_2 = 12U;
                }
                break;
              case 0U:
                {
                    var_59 = *var_3;
                    var_63 = var_59;
                    local_sp_3 = local_sp_2;
                    if (*(unsigned char *)(var_59 + 48UL) == '\x11') {
                        var_60 = *(uint64_t *)(var_59 + 40UL);
                        *(uint64_t *)(var_0 + (-64L)) = var_60;
                        var_61 = *var_3;
                        var_62 = local_sp_2 + (-8L);
                        *(uint64_t *)var_62 = 4273129UL;
                        indirect_placeholder_9(var_60, 4281437UL, var_61);
                        var_63 = *var_3;
                        local_sp_3 = var_62;
                    }
                    var_64 = (uint64_t *)(var_0 + (-48L));
                    spec_select = (*var_64 == 18446744073709551615UL) ? 11UL : 10UL;
                    var_65 = *var_5;
                    var_66 = local_sp_3 + (-8L);
                    *(uint64_t *)var_66 = 4273171UL;
                    var_67 = indirect_placeholder_10(spec_select, 0UL, var_63, var_65);
                    *var_9 = var_67;
                    local_sp_0 = var_66;
                    if (var_67 != 0UL) {
                        var_68 = *var_42 + 2UL;
                        var_69 = (uint64_t *)(var_0 + (-32L));
                        *var_69 = var_68;
                        var_70 = var_68;
                        while (1U)
                            {
                                if ((long)var_70 > (long)*var_64) {
                                    var_71 = *var_5;
                                    var_72 = *var_3;
                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4273231UL;
                                    var_73 = indirect_placeholder(var_71, var_72);
                                    *var_3 = var_73;
                                    var_74 = *var_9;
                                    var_75 = *var_5;
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4273260UL;
                                    var_76 = indirect_placeholder_10(16UL, var_73, var_74, var_75);
                                    *var_9 = var_76;
                                    if (!((*var_3 == 0UL) || (var_76 == 0UL))) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_77 = *var_5;
                                    var_78 = local_sp_0 + (-24L);
                                    *(uint64_t *)var_78 = 4273326UL;
                                    var_79 = indirect_placeholder_10(10UL, 0UL, var_76, var_77);
                                    *var_9 = var_79;
                                    local_sp_0 = var_78;
                                    if (var_79 != 0UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    var_80 = *var_69 + 1UL;
                                    *var_69 = var_80;
                                    var_70 = var_80;
                                    continue;
                                }
                                var_81 = *var_10;
                                if (var_81 == 0UL) {
                                    var_85 = *var_9;
                                    loop_state_var = 1U;
                                    break;
                                }
                                var_82 = *var_9;
                                var_83 = *var_5;
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4273397UL;
                                var_84 = indirect_placeholder_10(16UL, var_82, var_81, var_83);
                                *var_9 = var_84;
                                var_85 = var_84;
                                loop_state_var = 1U;
                                break;
                            }
                        switch (loop_state_var) {
                          case 1U:
                            {
                                rax_1 = var_85;
                                return rax_1;
                            }
                            break;
                          case 0U:
                            {
                                break;
                            }
                            break;
                        }
                    }
                    **(uint32_t **)var_2 = 12U;
                }
                break;
            }
        }
    } else {
        var_19 = var_0 + (-48L);
        var_20 = (uint64_t *)var_19;
        *var_20 = 0UL;
        var_21 = *var_8;
        var_22 = *var_7;
        var_23 = *var_4;
        var_24 = var_0 + (-144L);
        *(uint64_t *)var_24 = 4272234UL;
        var_25 = indirect_placeholder_9(var_21, var_22, var_23);
        var_26 = var_0 + (-40L);
        var_27 = (uint64_t *)var_26;
        *var_27 = var_25;
        var_28 = var_25;
        local_sp_4 = var_24;
        local_sp_5 = var_24;
        if (var_25 != 18446744073709551615UL) {
            var_28 = 0UL;
            if (*(unsigned char *)(*var_7 + 8UL) != '\x01') {
                **(uint32_t **)var_2 = 10U;
                return rax_1;
            }
            if (**(unsigned char **)var_6 != ',') {
                **(uint32_t **)var_2 = 10U;
                return rax_1;
            }
            *var_27 = 0UL;
        }
        if (var_28 == 18446744073709551614UL) {
            local_sp_6 = local_sp_5;
            if (var_34 != 18446744073709551614UL) {
                if ((uint64_t)((uint32_t)*var_8 & 2097152U) == 0UL) {
                    *(uint64_t *)(*var_4 + 72UL) = *var_12;
                    var_87 = *(uint64_t **)var_6;
                    var_88 = *var_17;
                    *var_87 = *var_16;
                    *(uint64_t *)((uint64_t)var_87 + 8UL) = var_88;
                    *(unsigned char *)(*var_7 + 8UL) = (unsigned char)'\x01';
                    var_89 = *var_3;
                    rax_1 = var_89;
                } else {
                    var_90 = (*(unsigned char *)(*var_7 + 8UL) == '\x02');
                    var_91 = *(uint32_t **)var_2;
                    if (var_90) {
                        *var_91 = 9U;
                    } else {
                        *var_91 = 10U;
                    }
                }
                return rax_1;
            }
            var_35 = *var_20;
            switch (var_35) {
              case 18446744073709551614UL:
                {
                    if ((uint64_t)((uint32_t)*var_8 & 2097152U) == 0UL) {
                        *(uint64_t *)(*var_4 + 72UL) = *var_12;
                        var_87 = *(uint64_t **)var_6;
                        var_88 = *var_17;
                        *var_87 = *var_16;
                        *(uint64_t *)((uint64_t)var_87 + 8UL) = var_88;
                        *(unsigned char *)(*var_7 + 8UL) = (unsigned char)'\x01';
                        var_89 = *var_3;
                        rax_1 = var_89;
                    } else {
                        var_90 = (*(unsigned char *)(*var_7 + 8UL) == '\x02');
                        var_91 = *(uint32_t **)var_2;
                        if (var_90) {
                            *var_91 = 9U;
                        } else {
                            *var_91 = 10U;
                        }
                    }
                }
                break;
              case 18446744073709551615UL:
                {
                    var_36 = *var_7;
                    var_37 = var_36;
                    if (*(unsigned char *)(var_36 + 8UL) != '\x18') {
                        **(uint32_t **)var_2 = 10U;
                        return rax_1;
                    }
                    if ((long)*(uint64_t *)((var_35 == 18446744073709551615UL) ? var_26 : var_19) <= (long)32767UL) {
                        **(uint32_t **)var_2 = 15U;
                        return rax_1;
                    }
                    var_38 = *var_8;
                    var_39 = *var_4;
                    var_40 = local_sp_6 + (-8L);
                    *(uint64_t *)var_40 = 4272769UL;
                    indirect_placeholder_67(var_38, var_39, var_37);
                    var_41 = *var_3;
                    local_sp_1 = var_40;
                    local_sp_2 = var_40;
                    if (var_41 != 0UL) {
                        var_42 = (uint64_t *)(var_0 + (-40L));
                        var_43 = *var_42;
                        if (var_43 != 0UL) {
                            if (*(uint64_t *)(var_0 + (-48L)) != 0UL) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4272849UL;
                                indirect_placeholder_9(0UL, 4281605UL, var_41);
                                return rax_1;
                            }
                        }
                        if ((long)var_43 > (long)0UL) {
                            *var_10 = 0UL;
                            var_59 = *var_3;
                            var_63 = var_59;
                            local_sp_3 = local_sp_2;
                            if (*(unsigned char *)(var_59 + 48UL) == '\x11') {
                                var_60 = *(uint64_t *)(var_59 + 40UL);
                                *(uint64_t *)(var_0 + (-64L)) = var_60;
                                var_61 = *var_3;
                                var_62 = local_sp_2 + (-8L);
                                *(uint64_t *)var_62 = 4273129UL;
                                indirect_placeholder_9(var_60, 4281437UL, var_61);
                                var_63 = *var_3;
                                local_sp_3 = var_62;
                            }
                            var_64 = (uint64_t *)(var_0 + (-48L));
                            spec_select = (*var_64 == 18446744073709551615UL) ? 11UL : 10UL;
                            var_65 = *var_5;
                            var_66 = local_sp_3 + (-8L);
                            *(uint64_t *)var_66 = 4273171UL;
                            var_67 = indirect_placeholder_10(spec_select, 0UL, var_63, var_65);
                            *var_9 = var_67;
                            local_sp_0 = var_66;
                            if (var_67 != 0UL) {
                                var_68 = *var_42 + 2UL;
                                var_69 = (uint64_t *)(var_0 + (-32L));
                                *var_69 = var_68;
                                var_70 = var_68;
                                while (1U)
                                    {
                                        if ((long)var_70 > (long)*var_64) {
                                            var_71 = *var_5;
                                            var_72 = *var_3;
                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4273231UL;
                                            var_73 = indirect_placeholder(var_71, var_72);
                                            *var_3 = var_73;
                                            var_74 = *var_9;
                                            var_75 = *var_5;
                                            *(uint64_t *)(local_sp_0 + (-16L)) = 4273260UL;
                                            var_76 = indirect_placeholder_10(16UL, var_73, var_74, var_75);
                                            *var_9 = var_76;
                                            if (!((*var_3 == 0UL) || (var_76 == 0UL))) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_77 = *var_5;
                                            var_78 = local_sp_0 + (-24L);
                                            *(uint64_t *)var_78 = 4273326UL;
                                            var_79 = indirect_placeholder_10(10UL, 0UL, var_76, var_77);
                                            *var_9 = var_79;
                                            local_sp_0 = var_78;
                                            if (var_79 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_80 = *var_69 + 1UL;
                                            *var_69 = var_80;
                                            var_70 = var_80;
                                            continue;
                                        }
                                        var_81 = *var_10;
                                        if (var_81 == 0UL) {
                                            var_85 = *var_9;
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_82 = *var_9;
                                        var_83 = *var_5;
                                        *(uint64_t *)(local_sp_0 + (-8L)) = 4273397UL;
                                        var_84 = indirect_placeholder_10(16UL, var_82, var_81, var_83);
                                        *var_9 = var_84;
                                        var_85 = var_84;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        break;
                                    }
                                    break;
                                  case 1U:
                                    {
                                        rax_1 = var_85;
                                        return rax_1;
                                    }
                                    break;
                                }
                            }
                            **(uint32_t **)var_2 = 12U;
                        } else {
                            *var_9 = var_41;
                            var_44 = (uint64_t *)(var_0 + (-32L));
                            *var_44 = 2UL;
                            while (1U)
                                {
                                    var_46 = *var_42;
                                    if ((long)var_45 > (long)var_46) {
                                        var_47 = *var_5;
                                        var_48 = *var_3;
                                        *(uint64_t *)(local_sp_1 + (-8L)) = 4272916UL;
                                        var_49 = indirect_placeholder(var_47, var_48);
                                        *var_3 = var_49;
                                        var_50 = *var_9;
                                        var_51 = *var_5;
                                        var_52 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_52 = 4272945UL;
                                        var_53 = indirect_placeholder_10(16UL, var_49, var_50, var_51);
                                        *var_9 = var_53;
                                        local_sp_1 = var_52;
                                        if (!((*var_3 == 0UL) || (var_53 == 0UL))) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        var_54 = *var_44 + 1UL;
                                        *var_44 = var_54;
                                        var_45 = var_54;
                                        continue;
                                    }
                                    if (var_46 == *(uint64_t *)(var_0 + (-48L))) {
                                        var_86 = *var_9;
                                        rax_1 = var_86;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_55 = *var_5;
                                    var_56 = *var_3;
                                    var_57 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_57 = 4273042UL;
                                    var_58 = indirect_placeholder(var_55, var_56);
                                    *var_3 = var_58;
                                    local_sp_2 = var_57;
                                    if (var_58 != 0UL) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    *var_10 = *var_9;
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    **(uint32_t **)var_2 = 12U;
                                }
                                break;
                              case 1U:
                                {
                                    return rax_1;
                                }
                                break;
                              case 0U:
                                {
                                    var_59 = *var_3;
                                    var_63 = var_59;
                                    local_sp_3 = local_sp_2;
                                    if (*(unsigned char *)(var_59 + 48UL) == '\x11') {
                                        var_60 = *(uint64_t *)(var_59 + 40UL);
                                        *(uint64_t *)(var_0 + (-64L)) = var_60;
                                        var_61 = *var_3;
                                        var_62 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_62 = 4273129UL;
                                        indirect_placeholder_9(var_60, 4281437UL, var_61);
                                        var_63 = *var_3;
                                        local_sp_3 = var_62;
                                    }
                                    var_64 = (uint64_t *)(var_0 + (-48L));
                                    spec_select = (*var_64 == 18446744073709551615UL) ? 11UL : 10UL;
                                    var_65 = *var_5;
                                    var_66 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_66 = 4273171UL;
                                    var_67 = indirect_placeholder_10(spec_select, 0UL, var_63, var_65);
                                    *var_9 = var_67;
                                    local_sp_0 = var_66;
                                    if (var_67 != 0UL) {
                                        var_68 = *var_42 + 2UL;
                                        var_69 = (uint64_t *)(var_0 + (-32L));
                                        *var_69 = var_68;
                                        var_70 = var_68;
                                        while (1U)
                                            {
                                                if ((long)var_70 > (long)*var_64) {
                                                    var_71 = *var_5;
                                                    var_72 = *var_3;
                                                    *(uint64_t *)(local_sp_0 + (-8L)) = 4273231UL;
                                                    var_73 = indirect_placeholder(var_71, var_72);
                                                    *var_3 = var_73;
                                                    var_74 = *var_9;
                                                    var_75 = *var_5;
                                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4273260UL;
                                                    var_76 = indirect_placeholder_10(16UL, var_73, var_74, var_75);
                                                    *var_9 = var_76;
                                                    if (!((*var_3 == 0UL) || (var_76 == 0UL))) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_77 = *var_5;
                                                    var_78 = local_sp_0 + (-24L);
                                                    *(uint64_t *)var_78 = 4273326UL;
                                                    var_79 = indirect_placeholder_10(10UL, 0UL, var_76, var_77);
                                                    *var_9 = var_79;
                                                    local_sp_0 = var_78;
                                                    if (var_79 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_80 = *var_69 + 1UL;
                                                    *var_69 = var_80;
                                                    var_70 = var_80;
                                                    continue;
                                                }
                                                var_81 = *var_10;
                                                if (var_81 == 0UL) {
                                                    var_85 = *var_9;
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                var_82 = *var_9;
                                                var_83 = *var_5;
                                                *(uint64_t *)(local_sp_0 + (-8L)) = 4273397UL;
                                                var_84 = indirect_placeholder_10(16UL, var_82, var_81, var_83);
                                                *var_9 = var_84;
                                                var_85 = var_84;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                        switch (loop_state_var) {
                                          case 1U:
                                            {
                                                rax_1 = var_85;
                                                return rax_1;
                                            }
                                            break;
                                          case 0U:
                                            {
                                                break;
                                            }
                                            break;
                                        }
                                    }
                                    **(uint32_t **)var_2 = 12U;
                                }
                                break;
                            }
                        }
                    }
                }
                break;
              default:
                {
                    if ((long)var_34 > (long)var_35) {
                        **(uint32_t **)var_2 = 10U;
                        return rax_1;
                    }
                    var_36 = *var_7;
                    var_37 = var_36;
                    if (*(unsigned char *)(var_36 + 8UL) == '\x18') {
                        **(uint32_t **)var_2 = 10U;
                        return rax_1;
                    }
                    if ((long)*(uint64_t *)((var_35 == 18446744073709551615UL) ? var_26 : var_19) > (long)32767UL) {
                        **(uint32_t **)var_2 = 15U;
                        return rax_1;
                    }
                    var_38 = *var_8;
                    var_39 = *var_4;
                    var_40 = local_sp_6 + (-8L);
                    *(uint64_t *)var_40 = 4272769UL;
                    indirect_placeholder_67(var_38, var_39, var_37);
                    var_41 = *var_3;
                    local_sp_1 = var_40;
                    local_sp_2 = var_40;
                    if (var_41 != 0UL) {
                        var_42 = (uint64_t *)(var_0 + (-40L));
                        var_43 = *var_42;
                        if (var_43 != 0UL) {
                            if (*(uint64_t *)(var_0 + (-48L)) == 0UL) {
                                *(uint64_t *)(local_sp_6 + (-16L)) = 4272849UL;
                                indirect_placeholder_9(0UL, 4281605UL, var_41);
                                return rax_1;
                            }
                        }
                        if ((long)var_43 > (long)0UL) {
                            *var_9 = var_41;
                            var_44 = (uint64_t *)(var_0 + (-32L));
                            *var_44 = 2UL;
                            while (1U)
                                {
                                    var_46 = *var_42;
                                    if ((long)var_45 > (long)var_46) {
                                        var_47 = *var_5;
                                        var_48 = *var_3;
                                        *(uint64_t *)(local_sp_1 + (-8L)) = 4272916UL;
                                        var_49 = indirect_placeholder(var_47, var_48);
                                        *var_3 = var_49;
                                        var_50 = *var_9;
                                        var_51 = *var_5;
                                        var_52 = local_sp_1 + (-16L);
                                        *(uint64_t *)var_52 = 4272945UL;
                                        var_53 = indirect_placeholder_10(16UL, var_49, var_50, var_51);
                                        *var_9 = var_53;
                                        local_sp_1 = var_52;
                                        if (!((*var_3 == 0UL) || (var_53 == 0UL))) {
                                            loop_state_var = 2U;
                                            break;
                                        }
                                        var_54 = *var_44 + 1UL;
                                        *var_44 = var_54;
                                        var_45 = var_54;
                                        continue;
                                    }
                                    if (var_46 == *(uint64_t *)(var_0 + (-48L))) {
                                        var_86 = *var_9;
                                        rax_1 = var_86;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                    var_55 = *var_5;
                                    var_56 = *var_3;
                                    var_57 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_57 = 4273042UL;
                                    var_58 = indirect_placeholder(var_55, var_56);
                                    *var_3 = var_58;
                                    local_sp_2 = var_57;
                                    if (var_58 != 0UL) {
                                        loop_state_var = 2U;
                                        break;
                                    }
                                    *var_10 = *var_9;
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 2U:
                                {
                                    **(uint32_t **)var_2 = 12U;
                                }
                                break;
                              case 1U:
                                {
                                    return rax_1;
                                }
                                break;
                              case 0U:
                                {
                                    var_59 = *var_3;
                                    var_63 = var_59;
                                    local_sp_3 = local_sp_2;
                                    if (*(unsigned char *)(var_59 + 48UL) == '\x11') {
                                        var_60 = *(uint64_t *)(var_59 + 40UL);
                                        *(uint64_t *)(var_0 + (-64L)) = var_60;
                                        var_61 = *var_3;
                                        var_62 = local_sp_2 + (-8L);
                                        *(uint64_t *)var_62 = 4273129UL;
                                        indirect_placeholder_9(var_60, 4281437UL, var_61);
                                        var_63 = *var_3;
                                        local_sp_3 = var_62;
                                    }
                                    var_64 = (uint64_t *)(var_0 + (-48L));
                                    spec_select = (*var_64 == 18446744073709551615UL) ? 11UL : 10UL;
                                    var_65 = *var_5;
                                    var_66 = local_sp_3 + (-8L);
                                    *(uint64_t *)var_66 = 4273171UL;
                                    var_67 = indirect_placeholder_10(spec_select, 0UL, var_63, var_65);
                                    *var_9 = var_67;
                                    local_sp_0 = var_66;
                                    if (var_67 == 0UL) {
                                        **(uint32_t **)var_2 = 12U;
                                        return rax_1;
                                    }
                                    var_68 = *var_42 + 2UL;
                                    var_69 = (uint64_t *)(var_0 + (-32L));
                                    *var_69 = var_68;
                                    var_70 = var_68;
                                    while (1U)
                                        {
                                            if ((long)var_70 > (long)*var_64) {
                                                var_71 = *var_5;
                                                var_72 = *var_3;
                                                *(uint64_t *)(local_sp_0 + (-8L)) = 4273231UL;
                                                var_73 = indirect_placeholder(var_71, var_72);
                                                *var_3 = var_73;
                                                var_74 = *var_9;
                                                var_75 = *var_5;
                                                *(uint64_t *)(local_sp_0 + (-16L)) = 4273260UL;
                                                var_76 = indirect_placeholder_10(16UL, var_73, var_74, var_75);
                                                *var_9 = var_76;
                                                if (!((*var_3 == 0UL) || (var_76 == 0UL))) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                var_77 = *var_5;
                                                var_78 = local_sp_0 + (-24L);
                                                *(uint64_t *)var_78 = 4273326UL;
                                                var_79 = indirect_placeholder_10(10UL, 0UL, var_76, var_77);
                                                *var_9 = var_79;
                                                local_sp_0 = var_78;
                                                if (var_79 != 0UL) {
                                                    loop_state_var = 0U;
                                                    break;
                                                }
                                                var_80 = *var_69 + 1UL;
                                                *var_69 = var_80;
                                                var_70 = var_80;
                                                continue;
                                            }
                                            var_81 = *var_10;
                                            if (var_81 == 0UL) {
                                                var_85 = *var_9;
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_82 = *var_9;
                                            var_83 = *var_5;
                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4273397UL;
                                            var_84 = indirect_placeholder_10(16UL, var_82, var_81, var_83);
                                            *var_9 = var_84;
                                            var_85 = var_84;
                                            loop_state_var = 1U;
                                            break;
                                        }
                                    switch (loop_state_var) {
                                      case 1U:
                                        {
                                            rax_1 = var_85;
                                            return rax_1;
                                        }
                                        break;
                                      case 0U:
                                        {
                                            break;
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        } else {
                            *var_10 = 0UL;
                            var_59 = *var_3;
                            var_63 = var_59;
                            local_sp_3 = local_sp_2;
                            if (*(unsigned char *)(var_59 + 48UL) == '\x11') {
                                var_60 = *(uint64_t *)(var_59 + 40UL);
                                *(uint64_t *)(var_0 + (-64L)) = var_60;
                                var_61 = *var_3;
                                var_62 = local_sp_2 + (-8L);
                                *(uint64_t *)var_62 = 4273129UL;
                                indirect_placeholder_9(var_60, 4281437UL, var_61);
                                var_63 = *var_3;
                                local_sp_3 = var_62;
                            }
                            var_64 = (uint64_t *)(var_0 + (-48L));
                            spec_select = (*var_64 == 18446744073709551615UL) ? 11UL : 10UL;
                            var_65 = *var_5;
                            var_66 = local_sp_3 + (-8L);
                            *(uint64_t *)var_66 = 4273171UL;
                            var_67 = indirect_placeholder_10(spec_select, 0UL, var_63, var_65);
                            *var_9 = var_67;
                            local_sp_0 = var_66;
                            if (var_67 != 0UL) {
                                var_68 = *var_42 + 2UL;
                                var_69 = (uint64_t *)(var_0 + (-32L));
                                *var_69 = var_68;
                                var_70 = var_68;
                                while (1U)
                                    {
                                        if ((long)var_70 > (long)*var_64) {
                                            var_71 = *var_5;
                                            var_72 = *var_3;
                                            *(uint64_t *)(local_sp_0 + (-8L)) = 4273231UL;
                                            var_73 = indirect_placeholder(var_71, var_72);
                                            *var_3 = var_73;
                                            var_74 = *var_9;
                                            var_75 = *var_5;
                                            *(uint64_t *)(local_sp_0 + (-16L)) = 4273260UL;
                                            var_76 = indirect_placeholder_10(16UL, var_73, var_74, var_75);
                                            *var_9 = var_76;
                                            if (!((*var_3 == 0UL) || (var_76 == 0UL))) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_77 = *var_5;
                                            var_78 = local_sp_0 + (-24L);
                                            *(uint64_t *)var_78 = 4273326UL;
                                            var_79 = indirect_placeholder_10(10UL, 0UL, var_76, var_77);
                                            *var_9 = var_79;
                                            local_sp_0 = var_78;
                                            if (var_79 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_80 = *var_69 + 1UL;
                                            *var_69 = var_80;
                                            var_70 = var_80;
                                            continue;
                                        }
                                        var_81 = *var_10;
                                        if (var_81 == 0UL) {
                                            var_85 = *var_9;
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        var_82 = *var_9;
                                        var_83 = *var_5;
                                        *(uint64_t *)(local_sp_0 + (-8L)) = 4273397UL;
                                        var_84 = indirect_placeholder_10(16UL, var_82, var_81, var_83);
                                        *var_9 = var_84;
                                        var_85 = var_84;
                                        loop_state_var = 1U;
                                        break;
                                    }
                                switch (loop_state_var) {
                                  case 0U:
                                    {
                                        break;
                                    }
                                    break;
                                  case 1U:
                                    {
                                        rax_1 = var_85;
                                        return rax_1;
                                    }
                                    break;
                                }
                            }
                            **(uint32_t **)var_2 = 12U;
                        }
                    }
                }
                break;
            }
        } else {
            var_29 = *var_7;
            switch (*(unsigned char *)(var_29 + 8UL)) {
              case '\x18':
                {
                    rax_0 = var_28;
                }
                break;
              case '\x01':
                {
                    if (**(unsigned char **)var_6 == ',') {
                        var_30 = *var_8;
                        var_31 = *var_4;
                        var_32 = var_0 + (-152L);
                        *(uint64_t *)var_32 = 4272372UL;
                        var_33 = indirect_placeholder_9(var_30, var_29, var_31);
                        rax_0 = var_33;
                        local_sp_4 = var_32;
                    }
                }
                break;
              default:
                {
                    *var_20 = rax_0;
                    var_34 = *var_27;
                    local_sp_5 = local_sp_4;
                }
                break;
            }
        }
    }
}
