typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct helper_divq_EAX_wrapper_ret_type;
struct type_5;
struct indirect_placeholder_155_ret_type;
struct indirect_placeholder_156_ret_type;
struct indirect_placeholder_157_ret_type;
struct indirect_placeholder_158_ret_type;
struct indirect_placeholder_159_ret_type;
struct indirect_placeholder_160_ret_type;
struct indirect_placeholder_161_ret_type;
struct indirect_placeholder_162_ret_type;
struct indirect_placeholder_163_ret_type;
struct indirect_placeholder_164_ret_type;
struct helper_divq_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint32_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint64_t field_9;
    uint32_t field_10;
    unsigned char field_11;
    uint32_t field_12;
};
struct type_5 {
};
struct indirect_placeholder_155_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_156_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_157_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_158_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_159_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_161_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_162_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_163_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_164_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_r10(void);
extern uint64_t init_rbx(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern struct helper_divq_EAX_wrapper_ret_type helper_divq_EAX_wrapper(struct type_5 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint32_t param_13, uint64_t param_14, uint32_t param_15, uint64_t param_16, uint64_t param_17, uint64_t param_18, uint32_t param_19);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_155_ret_type indirect_placeholder_155(uint64_t param_0);
extern struct indirect_placeholder_156_ret_type indirect_placeholder_156(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_157_ret_type indirect_placeholder_157(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_158_ret_type indirect_placeholder_158(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_159_ret_type indirect_placeholder_159(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_160_ret_type indirect_placeholder_160(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_161_ret_type indirect_placeholder_161(uint64_t param_0);
extern struct indirect_placeholder_162_ret_type indirect_placeholder_162(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_163_ret_type indirect_placeholder_163(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_164_ret_type indirect_placeholder_164(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_build_trtable(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_160_ret_type var_119;
    struct indirect_placeholder_157_ret_type var_131;
    struct indirect_placeholder_156_ret_type var_137;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint32_t var_6;
    uint64_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint32_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_158_ret_type var_80;
    uint64_t *var_14;
    uint64_t *var_15;
    unsigned char *var_16;
    unsigned char *var_17;
    unsigned char *var_18;
    uint64_t *var_19;
    struct indirect_placeholder_155_ret_type var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t rax_0;
    uint64_t local_sp_3;
    uint64_t var_110;
    uint64_t local_sp_0;
    uint64_t r9_0;
    uint64_t r10_0;
    uint64_t rbx_0;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_105;
    uint64_t *var_106;
    uint64_t var_109;
    uint64_t var_107;
    uint64_t var_99;
    uint64_t local_sp_1;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    bool var_103;
    uint64_t var_104;
    uint64_t local_sp_2;
    uint64_t var_108;
    uint32_t *var_72;
    uint64_t *var_73;
    uint64_t *var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_79;
    uint64_t var_77;
    uint64_t var_128;
    uint64_t r8_0;
    uint64_t var_78;
    uint32_t *var_83;
    uint64_t *var_84;
    uint64_t *var_85;
    uint64_t var_86;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_87;
    uint64_t var_96;
    uint64_t local_sp_8;
    uint64_t var_113;
    uint64_t local_sp_7;
    uint64_t var_114;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    bool var_93;
    uint64_t var_94;
    uint64_t var_145;
    uint64_t var_95;
    uint64_t _pre_phi;
    uint64_t var_134;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_141;
    uint64_t var_142;
    uint64_t var_143;
    bool var_127;
    uint64_t var_129;
    uint64_t var_130;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_144;
    uint64_t local_sp_5;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_54;
    uint64_t var_65;
    uint64_t rcx_0;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t r10_1;
    uint64_t local_sp_10;
    uint64_t rbx_1;
    uint64_t local_sp_6;
    bool var_66;
    uint64_t var_67;
    uint64_t *var_68;
    uint64_t var_81;
    uint64_t *var_82;
    struct indirect_placeholder_159_ret_type var_69;
    uint64_t var_70;
    uint64_t *var_71;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_150;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_153;
    uint32_t var_154;
    uint64_t var_155;
    uint64_t local_sp_12;
    uint64_t var_159;
    uint64_t local_sp_9;
    uint64_t var_160;
    uint64_t var_161;
    uint64_t var_47;
    uint64_t var_48;
    struct indirect_placeholder_162_ret_type var_164;
    struct helper_divq_EAX_wrapper_ret_type var_49;
    uint64_t var_50;
    uint64_t rbx_2;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t *var_60;
    uint64_t *var_61;
    uint64_t *var_62;
    uint64_t *var_63;
    uint64_t var_64;
    uint64_t var_51;
    struct indirect_placeholder_161_ret_type var_52;
    uint64_t var_53;
    uint64_t local_sp_11;
    uint64_t var_156;
    uint64_t var_157;
    uint64_t *var_158;
    uint64_t var_163;
    uint64_t var_162;
    uint64_t local_sp_13;
    uint64_t spec_select;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_163_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint32_t *var_45;
    uint32_t var_46;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    struct indirect_placeholder_164_ret_type var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t *var_36;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r8();
    var_3 = init_r9();
    var_4 = init_r10();
    var_5 = init_rbx();
    var_6 = init_state_0x8248();
    var_7 = init_state_0x9018();
    var_8 = init_state_0x9010();
    var_9 = init_state_0x8408();
    var_10 = init_state_0x8328();
    var_11 = init_state_0x82d8();
    var_12 = init_state_0x9080();
    var_13 = var_0 + (-8L);
    *(uint64_t *)var_13 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_5;
    var_14 = (uint64_t *)(var_0 + (-224L));
    *var_14 = rdi;
    var_15 = (uint64_t *)(var_0 + (-232L));
    *var_15 = rsi;
    var_16 = (unsigned char *)(var_0 + (-45L));
    *var_16 = (unsigned char)'\x00';
    var_17 = (unsigned char *)(var_0 + (-89L));
    *var_17 = (unsigned char)'\x00';
    var_18 = (unsigned char *)(var_0 + (-65L));
    *var_18 = (unsigned char)'\x00';
    var_19 = (uint64_t *)(var_0 + (-88L));
    *var_19 = 0UL;
    *(uint64_t *)(var_0 + (-240L)) = 4309832UL;
    var_20 = indirect_placeholder_155(14336UL);
    var_21 = var_20.field_0;
    var_22 = (uint64_t *)(var_0 + (-104L));
    *var_22 = var_21;
    rax_0 = 0UL;
    var_110 = 0UL;
    var_99 = 0UL;
    var_75 = 0UL;
    var_77 = 0UL;
    var_86 = 0UL;
    var_113 = 0UL;
    var_88 = 0UL;
    var_65 = 0UL;
    r8_1 = var_2;
    r9_1 = var_3;
    r10_1 = var_4;
    var_159 = 0UL;
    rbx_2 = var_5;
    if (var_21 == 0UL) {
        return rax_0;
    }
    *var_17 = (unsigned char)'\x01';
    var_23 = *var_22;
    var_24 = (uint64_t *)(var_0 + (-112L));
    *var_24 = var_23;
    var_25 = *var_22 + 6144UL;
    var_26 = (uint64_t *)(var_0 + (-120L));
    *var_26 = var_25;
    *(uint64_t *)(*var_15 + 88UL) = 0UL;
    var_27 = *var_15;
    *(uint64_t *)(var_27 + 96UL) = *(uint64_t *)(var_27 + 88UL);
    var_28 = *var_26;
    var_29 = *var_24;
    var_30 = *var_15;
    var_31 = *var_14;
    var_32 = var_0 + (-248L);
    *(uint64_t *)var_32 = 4309955UL;
    var_33 = indirect_placeholder_164(var_28, var_29, var_30, var_31);
    var_34 = var_33.field_0;
    var_35 = var_33.field_1;
    var_36 = (uint64_t *)(var_0 + (-128L));
    *var_36 = var_34;
    rcx_0 = var_35;
    var_163 = var_34;
    local_sp_13 = var_32;
    rax_0 = 1UL;
    if ((long)var_34 < (long)1UL) {
        if (*var_17 == '\x00') {
            var_162 = var_0 + (-256L);
            *(uint64_t *)var_162 = 4309993UL;
            indirect_placeholder_2();
            var_163 = *var_36;
            local_sp_13 = var_162;
        }
        if (var_163 == 0UL) {
            *(uint64_t *)(local_sp_13 + (-8L)) = 4310015UL;
            var_164 = indirect_placeholder_162(var_35, 256UL, 8UL, var_2, var_3, var_4, var_5);
            *(uint64_t *)(*var_15 + 88UL) = var_164.field_0;
            spec_select = (*(uint64_t *)(*var_15 + 88UL) == 0UL) ? 0UL : 1UL;
            rax_0 = spec_select;
        }
    } else {
        var_37 = var_34 + 1UL;
        var_38 = var_0 + (-184L);
        var_39 = var_0 + (-256L);
        *(uint64_t *)var_39 = 4310107UL;
        var_40 = indirect_placeholder_163(var_37, var_38);
        var_41 = var_40.field_0;
        var_42 = var_40.field_1;
        var_43 = var_40.field_2;
        var_44 = var_0 + (-156L);
        var_45 = (uint32_t *)var_44;
        var_46 = (uint32_t)var_41;
        *var_45 = var_46;
        local_sp_11 = var_39;
        if (var_46 != 0U) {
            var_47 = *var_36;
            if (var_47 <= 768614336404564053UL) {
                var_48 = var_47 * 24UL;
                if ((var_48 + 14336UL) > 4031UL) {
                    var_49 = helper_divq_EAX_wrapper((struct type_5 *)(0UL), 16UL, 4310242UL, var_48 + 23UL, var_13, var_35, 0UL, var_42, var_43, var_2, var_3, var_4, 16UL, var_6, var_7, var_8, var_9, var_10, var_11, var_12);
                    var_50 = var_39 - (var_49.field_1 << 4UL);
                    *var_19 = var_50;
                    var_54 = var_50;
                    rbx_2 = 16UL;
                    local_sp_10 = var_50;
                } else {
                    var_51 = var_0 + (-264L);
                    *(uint64_t *)var_51 = 4310301UL;
                    var_52 = indirect_placeholder_161(var_48);
                    var_53 = var_52.field_0;
                    *var_19 = var_53;
                    local_sp_10 = var_51;
                    local_sp_11 = var_51;
                    if (var_53 != 0UL) {
                        local_sp_12 = local_sp_11;
                        if (*var_18 == '\x00') {
                            var_156 = local_sp_11 + (-8L);
                            *(uint64_t *)var_156 = 4310364UL;
                            indirect_placeholder_2();
                            local_sp_12 = var_156;
                        }
                        var_157 = local_sp_12 + (-8L);
                        *(uint64_t *)var_157 = 4310379UL;
                        indirect_placeholder_2();
                        var_158 = (uint64_t *)(var_0 + (-32L));
                        *var_158 = 0UL;
                        local_sp_9 = var_157;
                        while ((long)var_159 >= (long)*var_36)
                            {
                                var_160 = local_sp_9 + (-8L);
                                *(uint64_t *)var_160 = 4310428UL;
                                indirect_placeholder_2();
                                var_161 = *var_158 + 1UL;
                                *var_158 = var_161;
                                var_159 = var_161;
                                local_sp_9 = var_160;
                            }
                        if (*var_17 != '\x00') {
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4310461UL;
                            indirect_placeholder_2();
                        }
                        return rax_0;
                    }
                    *var_18 = (unsigned char)'\x01';
                    var_54 = *var_19;
                }
                var_55 = var_54 + (*var_36 << 3UL);
                var_56 = (uint64_t *)(var_0 + (-136L));
                *var_56 = var_55;
                var_57 = var_55 + (*var_36 << 3UL);
                var_58 = (uint64_t *)(var_0 + (-144L));
                *var_58 = var_57;
                var_59 = local_sp_10 + (-8L);
                *(uint64_t *)var_59 = 4310539UL;
                indirect_placeholder_2();
                var_60 = (uint64_t *)(var_0 + (-32L));
                *var_60 = 0UL;
                var_61 = (uint64_t *)(var_0 + (-176L));
                var_62 = (uint64_t *)(var_0 + (-40L));
                var_63 = (uint64_t *)(var_0 + (-152L));
                var_64 = var_0 + (-216L);
                rbx_1 = rbx_2;
                local_sp_6 = var_59;
                while (1U)
                    {
                        local_sp_7 = local_sp_6;
                        if ((long)var_65 >= (long)*var_36) {
                            var_66 = (*var_16 == '\x00');
                            var_67 = local_sp_6 + (-8L);
                            var_68 = (uint64_t *)var_67;
                            local_sp_11 = var_67;
                            if (!var_66) {
                                *var_68 = 4311770UL;
                                var_69 = indirect_placeholder_159(rcx_0, 512UL, 8UL, r8_1, r9_1, r10_1, rbx_1);
                                *(uint64_t *)(*var_15 + 96UL) = var_69.field_0;
                                var_70 = *(uint64_t *)(*var_15 + 96UL);
                                var_71 = (uint64_t *)(var_0 + (-80L));
                                *var_71 = var_70;
                                if (var_70 != 0UL) {
                                    loop_state_var = 2U;
                                    break;
                                }
                                *var_60 = 0UL;
                                var_72 = (uint32_t *)(var_0 + (-44L));
                                var_73 = (uint64_t *)(var_0 + (-56L));
                                var_74 = (uint64_t *)(var_0 + (-64L));
                                loop_state_var = 1U;
                                break;
                            }
                            *var_68 = 4311424UL;
                            var_80 = indirect_placeholder_158(rcx_0, 256UL, 8UL, r8_1, r9_1, r10_1, rbx_1);
                            *(uint64_t *)(*var_15 + 88UL) = var_80.field_0;
                            var_81 = *(uint64_t *)(*var_15 + 88UL);
                            var_82 = (uint64_t *)(var_0 + (-80L));
                            *var_82 = var_81;
                            if (var_81 != 0UL) {
                                loop_state_var = 2U;
                                break;
                            }
                            *var_60 = 0UL;
                            var_83 = (uint32_t *)(var_0 + (-44L));
                            var_84 = (uint64_t *)(var_0 + (-56L));
                            var_85 = (uint64_t *)(var_0 + (-64L));
                            loop_state_var = 0U;
                            break;
                        }
                        *var_61 = 0UL;
                        *var_62 = 0UL;
                        while (1U)
                            {
                                var_114 = *var_60;
                                var_115 = *var_24 + (var_114 * 24UL);
                                local_sp_8 = local_sp_7;
                                if ((long)var_113 >= (long)*(uint64_t *)(var_115 + 8UL)) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_150 = *(uint64_t *)((*(uint64_t *)(*(uint64_t *)(var_115 + 16UL) + (var_113 << 3UL)) << 3UL) + *(uint64_t *)(*var_14 + 24UL));
                                *var_63 = var_150;
                                if (var_150 != 18446744073709551615UL) {
                                    var_151 = (var_150 * 24UL) + *(uint64_t *)(*var_14 + 48UL);
                                    var_152 = local_sp_7 + (-8L);
                                    *(uint64_t *)var_152 = 4310712UL;
                                    var_153 = indirect_placeholder(var_151, var_38);
                                    var_154 = (uint32_t)var_153;
                                    *var_45 = var_154;
                                    local_sp_8 = var_152;
                                    local_sp_11 = var_152;
                                    if (var_154 != 0U) {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                }
                                var_155 = *var_62 + 1UL;
                                *var_62 = var_155;
                                var_113 = var_155;
                                local_sp_7 = local_sp_8;
                                continue;
                            }
                        switch_state_var = 0;
                        switch (loop_state_var) {
                          case 0U:
                            {
                                var_116 = *var_19 + (var_114 << 3UL);
                                var_117 = *var_14;
                                var_118 = local_sp_7 + (-8L);
                                *(uint64_t *)var_118 = 4310841UL;
                                var_119 = indirect_placeholder_160(0UL, var_38, var_117, var_44, var_116);
                                var_120 = var_119.field_0;
                                var_121 = var_119.field_1;
                                var_122 = var_119.field_2;
                                var_123 = var_119.field_3;
                                var_124 = var_119.field_4;
                                *(uint64_t *)var_124 = var_120;
                                var_125 = *var_60 << 3UL;
                                var_126 = *(uint64_t *)(*var_19 + var_125);
                                r9_0 = var_122;
                                r10_0 = var_123;
                                rbx_0 = var_124;
                                r8_0 = var_121;
                                local_sp_5 = var_118;
                                local_sp_11 = var_118;
                                if (var_126 != 0UL) {
                                    if (*var_45 != 0U) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                var_127 = ((signed char)*(unsigned char *)(var_126 + 104UL) > '\xff');
                                var_128 = *var_56 + var_125;
                                if (!var_127) {
                                    var_129 = *var_14;
                                    var_130 = local_sp_7 + (-16L);
                                    *(uint64_t *)var_130 = 4310994UL;
                                    var_131 = indirect_placeholder_157(1UL, var_38, var_129, var_44, var_128);
                                    *(uint64_t *)var_131.field_4 = var_131.field_0;
                                    var_132 = *var_60 << 3UL;
                                    var_133 = *(uint64_t *)(*var_56 + var_132);
                                    _pre_phi = var_132;
                                    local_sp_11 = var_130;
                                    if (var_133 != 0UL) {
                                        if (*var_45 != 0U) {
                                            loop_state_var = 2U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                    }
                                    if (*(uint64_t *)(*var_19 + var_132) != var_133 & (int)*(uint32_t *)(*var_14 + 180UL) > (int)1U) {
                                        *var_16 = (unsigned char)'\x01';
                                        _pre_phi = *var_60 << 3UL;
                                    }
                                    var_134 = *var_58 + _pre_phi;
                                    var_135 = *var_14;
                                    var_136 = local_sp_7 + (-24L);
                                    *(uint64_t *)var_136 = 4311184UL;
                                    var_137 = indirect_placeholder_156(2UL, var_38, var_135, var_44, var_134);
                                    var_138 = var_137.field_0;
                                    var_139 = var_137.field_1;
                                    var_140 = var_137.field_2;
                                    var_141 = var_137.field_3;
                                    var_142 = var_137.field_4;
                                    *(uint64_t *)var_142 = var_138;
                                    var_143 = *var_60;
                                    r9_0 = var_140;
                                    r10_0 = var_141;
                                    rbx_0 = var_142;
                                    r8_0 = var_139;
                                    var_145 = var_143;
                                    local_sp_5 = var_136;
                                    local_sp_11 = var_136;
                                    if (*(uint64_t *)(*var_58 + (var_143 << 3UL)) != 0UL & *var_45 != 0U) {
                                        loop_state_var = 2U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                }
                                *(uint64_t *)var_128 = var_126;
                                var_144 = *var_60 << 3UL;
                                *(uint64_t *)(*var_58 + var_144) = *(uint64_t *)(*var_19 + var_144);
                                var_145 = *var_60;
                            }
                            break;
                          case 1U:
                            {
                                loop_state_var = 2U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch (loop_state_var) {
                  case 2U:
                    {
                        break;
                    }
                    break;
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 0U:
                            {
                                while ((long)var_86 <= (long)3UL)
                                    {
                                        *var_83 = ((uint32_t)var_86 << 6U);
                                        *var_84 = *(uint64_t *)(((*var_60 << 3UL) + var_13) + (-208L));
                                        *var_85 = 1UL;
                                        var_87 = *var_84;
                                        while (var_87 != 0UL)
                                            {
                                                if ((var_87 & 1UL) != 0UL) {
                                                    *var_62 = 0UL;
                                                    var_89 = (var_88 << 5UL) + *var_26;
                                                    var_90 = *var_60 << 3UL;
                                                    var_91 = *(uint64_t *)(var_90 + var_89);
                                                    var_92 = *var_85;
                                                    while ((var_91 & var_92) != 0UL)
                                                        {
                                                            var_95 = var_88 + 1UL;
                                                            *var_62 = var_95;
                                                            var_88 = var_95;
                                                            var_89 = (var_88 << 5UL) + *var_26;
                                                            var_90 = *var_60 << 3UL;
                                                            var_91 = *(uint64_t *)(var_90 + var_89);
                                                            var_92 = *var_85;
                                                        }
                                                    var_93 = ((*(uint64_t *)(((var_90 + 176UL) + *var_14) + 8UL) & var_92) == 0UL);
                                                    var_94 = var_88 << 3UL;
                                                    if (var_93) {
                                                        *(uint64_t *)(*var_82 + ((uint64_t)*var_83 << 3UL)) = *(uint64_t *)(*var_19 + var_94);
                                                    } else {
                                                        *(uint64_t *)(*var_82 + ((uint64_t)*var_83 << 3UL)) = *(uint64_t *)(*var_56 + var_94);
                                                    }
                                                }
                                                *var_85 = (*var_85 << 1UL);
                                                *var_84 = (*var_84 >> 1UL);
                                                *var_83 = (*var_83 + 1U);
                                                var_87 = *var_84;
                                            }
                                        var_96 = *var_60 + 1UL;
                                        *var_60 = var_96;
                                        var_86 = var_96;
                                    }
                            }
                            break;
                          case 1U:
                            {
                                while ((long)var_75 <= (long)3UL)
                                    {
                                        *var_72 = ((uint32_t)var_75 << 6U);
                                        *var_73 = *(uint64_t *)(((*var_60 << 3UL) + var_13) + (-208L));
                                        *var_74 = 1UL;
                                        var_76 = *var_73;
                                        while (var_76 != 0UL)
                                            {
                                                if ((var_76 & 1UL) != 0UL) {
                                                    *var_62 = 0UL;
                                                    while ((*(uint64_t *)((*var_60 << 3UL) + ((var_77 << 5UL) + *var_26)) & *var_74) != 0UL)
                                                        {
                                                            var_78 = var_77 + 1UL;
                                                            *var_62 = var_78;
                                                            var_77 = var_78;
                                                        }
                                                    *(uint64_t *)(*var_71 + ((uint64_t)*var_72 << 3UL)) = *(uint64_t *)(*var_19 + (var_77 << 3UL));
                                                    *(uint64_t *)(*var_71 + (((uint64_t)*var_72 << 3UL) + 2048UL)) = *(uint64_t *)(*var_56 + (*var_62 << 3UL));
                                                }
                                                *var_74 = (*var_74 << 1UL);
                                                *var_73 = (*var_73 >> 1UL);
                                                *var_72 = (*var_72 + 1U);
                                                var_76 = *var_73;
                                            }
                                        var_79 = *var_60 + 1UL;
                                        *var_60 = var_79;
                                        var_75 = var_79;
                                    }
                            }
                            break;
                        }
                        var_97 = var_67 + (-8L);
                        *(uint64_t *)var_97 = 4312092UL;
                        var_98 = indirect_placeholder(10UL, var_64);
                        local_sp_1 = var_97;
                        local_sp_2 = var_97;
                        if ((uint64_t)(unsigned char)var_98 != 0UL) {
                            *var_62 = 0UL;
                            local_sp_2 = local_sp_1;
                            while ((long)var_99 >= (long)*var_36)
                                {
                                    var_100 = *var_26 + (var_99 << 5UL);
                                    var_101 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_101 = 4312141UL;
                                    var_102 = indirect_placeholder(10UL, var_100);
                                    var_103 = ((uint64_t)(unsigned char)var_102 == 0UL);
                                    var_104 = *var_62;
                                    local_sp_1 = var_101;
                                    local_sp_2 = var_101;
                                    if (var_103) {
                                        var_107 = var_104 + 1UL;
                                        *var_62 = var_107;
                                        var_99 = var_107;
                                        local_sp_2 = local_sp_1;
                                        continue;
                                    }
                                    var_105 = *var_58 + (var_104 << 3UL);
                                    var_106 = (uint64_t *)(var_0 + (-80L));
                                    *(uint64_t *)(*var_106 + 80UL) = *(uint64_t *)var_105;
                                    if (*var_16 == '\x00') {
                                        break;
                                    }
                                    *(uint64_t *)(*var_106 + 2128UL) = *(uint64_t *)(*var_58 + (*var_62 << 3UL));
                                    break;
                                }
                        }
                        local_sp_3 = local_sp_2;
                        if (*var_18 == '\x00') {
                            var_108 = local_sp_2 + (-8L);
                            *(uint64_t *)var_108 = 4312268UL;
                            indirect_placeholder_2();
                            local_sp_3 = var_108;
                        }
                        var_109 = local_sp_3 + (-8L);
                        *(uint64_t *)var_109 = 4312283UL;
                        indirect_placeholder_2();
                        *var_60 = 0UL;
                        local_sp_0 = var_109;
                        while ((long)var_110 >= (long)*var_36)
                            {
                                var_111 = local_sp_0 + (-8L);
                                *(uint64_t *)var_111 = 4312332UL;
                                indirect_placeholder_2();
                                var_112 = *var_60 + 1UL;
                                *var_60 = var_112;
                                var_110 = var_112;
                                local_sp_0 = var_111;
                            }
                        if (*var_17 != '\x00') {
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4312365UL;
                            indirect_placeholder_2();
                        }
                        return rax_0;
                    }
                    break;
                }
            }
        }
        local_sp_12 = local_sp_11;
        if (*var_18 == '\x00') {
            var_156 = local_sp_11 + (-8L);
            *(uint64_t *)var_156 = 4310364UL;
            indirect_placeholder_2();
            local_sp_12 = var_156;
        }
        var_157 = local_sp_12 + (-8L);
        *(uint64_t *)var_157 = 4310379UL;
        indirect_placeholder_2();
        var_158 = (uint64_t *)(var_0 + (-32L));
        *var_158 = 0UL;
        local_sp_9 = var_157;
        while ((long)var_159 >= (long)*var_36)
            {
                var_160 = local_sp_9 + (-8L);
                *(uint64_t *)var_160 = 4310428UL;
                indirect_placeholder_2();
                var_161 = *var_158 + 1UL;
                *var_158 = var_161;
                var_159 = var_161;
                local_sp_9 = var_160;
            }
        if (*var_17 == '\x00') {
            *(uint64_t *)(local_sp_9 + (-8L)) = 4310461UL;
            indirect_placeholder_2();
        }
    }
}
