typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
uint64_t bb_peek_token_bracket(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t rax_0;
    unsigned char var_9;
    unsigned char *var_10;
    unsigned char **var_11;
    uint64_t var_12;
    uint64_t var_13;
    unsigned char var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned char var_18;
    uint64_t var_20;
    unsigned char var_22;
    unsigned char *var_23;
    unsigned char *_pre_phi70;
    unsigned char *var_21;
    uint32_t *var_24;
    unsigned char var_25;
    uint32_t var_26;
    uint64_t var_27;
    uint32_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-32L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-40L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-48L));
    *var_5 = rdx;
    var_6 = *var_4;
    var_7 = *(uint64_t *)(var_6 + 104UL);
    var_8 = *(uint64_t *)(var_6 + 72UL);
    rax_0 = 1UL;
    if ((long)var_7 <= (long)var_8) {
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x02';
        rax_0 = 0UL;
        return rax_0;
    }
    var_9 = *(unsigned char *)(var_8 + *(uint64_t *)(var_6 + 8UL));
    var_10 = (unsigned char *)(var_0 + (-17L));
    *var_10 = var_9;
    var_11 = (unsigned char **)var_2;
    **var_11 = var_9;
    var_12 = *var_4;
    if ((int)*(uint32_t *)(var_12 + 144UL) <= (int)1U) {
        var_13 = *(uint64_t *)(var_12 + 72UL);
        if (var_13 != *(uint64_t *)(var_12 + 48UL) & *(uint32_t *)((var_13 << 2UL) + *(uint64_t *)(var_12 + 16UL)) != 4294967295U) {
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
            return rax_0;
        }
    }
    var_14 = *var_10;
    if (var_14 != '\\') {
        var_15 = (uint64_t *)(var_12 + 72UL);
        var_16 = *var_15 + 1UL;
        if ((*var_5 & 1UL) != 0UL & (long)var_16 >= (long)*(uint64_t *)(var_12 + 88UL)) {
            *var_15 = var_16;
            var_17 = *var_4;
            var_18 = *(unsigned char *)(*(uint64_t *)(var_17 + 72UL) + *(uint64_t *)(var_17 + 8UL));
            *(unsigned char *)(var_0 + (-18L)) = var_18;
            **var_11 = var_18;
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
            return rax_0;
        }
    }
    if (var_14 == '[') {
        var_20 = *(uint64_t *)(var_12 + 72UL) + 1UL;
        if ((long)var_20 < (long)*(uint64_t *)(var_12 + 88UL)) {
            var_22 = *(unsigned char *)(var_20 + *(uint64_t *)(var_12 + 8UL));
            var_23 = (unsigned char *)(var_0 + (-9L));
            *var_23 = var_22;
            _pre_phi70 = var_23;
        } else {
            var_21 = (unsigned char *)(var_0 + (-9L));
            *var_21 = (unsigned char)'\x00';
            _pre_phi70 = var_21;
        }
        **var_11 = *_pre_phi70;
        var_24 = (uint32_t *)(var_0 + (-16L));
        *var_24 = 2U;
        var_25 = *_pre_phi70;
        var_26 = (uint32_t)(uint64_t)var_25;
        if ((uint64_t)(var_26 + (-61)) == 0UL) {
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x1c';
        } else {
            if (var_25 <= '=') {
                if ((uint64_t)(var_26 + (-46)) != 0UL) {
                    *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x1a';
                    var_27 = (uint64_t)*var_24;
                    rax_0 = var_27;
                    return rax_0;
                }
                if ((uint64_t)(var_26 + (-58)) != 0UL & (*var_5 & 4UL) != 0UL) {
                    *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x1e';
                    var_27 = (uint64_t)*var_24;
                    rax_0 = var_27;
                    return rax_0;
                }
            }
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
            **var_11 = *var_10;
            *var_24 = 1U;
        }
    } else {
        var_19 = (uint32_t)(uint64_t)var_14;
        if ((uint64_t)(var_19 + (-94)) == 0UL) {
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x19';
        } else {
            if (var_14 <= '^') {
                if ((uint64_t)(var_19 + (-45)) != 0UL) {
                    *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x16';
                    return rax_0;
                }
                if ((uint64_t)(var_19 + (-93)) != 0UL) {
                    *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x15';
                    return rax_0;
                }
            }
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
        }
    }
}
