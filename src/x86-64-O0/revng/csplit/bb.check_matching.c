typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_170_ret_type;
struct indirect_placeholder_171_ret_type;
struct indirect_placeholder_172_ret_type;
struct indirect_placeholder_173_ret_type;
struct indirect_placeholder_174_ret_type;
struct indirect_placeholder_175_ret_type;
struct indirect_placeholder_176_ret_type;
struct indirect_placeholder_170_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_171_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_172_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_173_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_174_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_175_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_176_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_170_ret_type indirect_placeholder_170(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_171_ret_type indirect_placeholder_171(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_172_ret_type indirect_placeholder_172(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_173_ret_type indirect_placeholder_173(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_174_ret_type indirect_placeholder_174(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_175_ret_type indirect_placeholder_175(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_176_ret_type indirect_placeholder_176(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_check_matching(uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9, uint64_t r10, uint64_t rbx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    unsigned char *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    bool var_12;
    unsigned char *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint32_t *var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint64_t var_25;
    uint64_t r84_2;
    uint64_t var_107;
    uint64_t local_sp_2;
    uint64_t r84_1;
    uint64_t local_sp_6_be;
    uint64_t local_sp_6;
    uint64_t rbx7_2;
    uint64_t var_93;
    struct indirect_placeholder_170_ret_type var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_84;
    uint64_t var_99;
    uint64_t local_sp_0;
    uint64_t r84_0;
    uint64_t r95_0;
    uint64_t r106_0;
    uint64_t var_100;
    uint64_t var_101;
    struct indirect_placeholder_171_ret_type var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t local_sp_1;
    uint64_t r95_1;
    uint64_t r106_1;
    uint64_t r84_4;
    uint64_t var_108;
    unsigned char var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_62;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_4;
    uint64_t rax_0;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_172_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint32_t var_61;
    uint64_t local_sp_3;
    bool var_33;
    bool var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    struct indirect_placeholder_173_ret_type var_41;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint32_t *var_31;
    uint32_t var_32;
    uint64_t r95_2;
    uint64_t r106_2;
    uint64_t rbx7_0;
    uint64_t *var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    struct indirect_placeholder_174_ret_type var_47;
    uint32_t var_48;
    uint64_t r84_3;
    uint64_t r95_3;
    uint64_t r106_3;
    uint64_t var_63;
    unsigned char var_64;
    uint64_t local_sp_6_ph;
    uint64_t local_sp_5;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    bool var_69;
    uint64_t var_70;
    uint64_t *var_71;
    uint64_t *var_72;
    uint64_t r95_4;
    uint64_t r106_4;
    uint64_t rbx7_1;
    uint64_t var_73;
    uint64_t rax_1;
    uint64_t *var_114;
    uint64_t var_115;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    struct indirect_placeholder_175_ret_type var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint32_t var_83;
    uint64_t local_sp_7;
    uint64_t _pre;
    uint64_t var_85;
    uint64_t var_86;
    struct indirect_placeholder_176_ret_type var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-128L));
    *var_2 = rdi;
    var_3 = var_0 + (-144L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdx;
    var_5 = (unsigned char *)(var_0 + (-132L));
    *var_5 = (unsigned char)rsi;
    var_6 = *(uint64_t *)(*var_2 + 152UL);
    var_7 = (uint64_t *)(var_0 + (-56L));
    *var_7 = var_6;
    var_8 = (uint64_t *)(var_0 + (-16L));
    *var_8 = 0UL;
    var_9 = (uint64_t *)(var_0 + (-24L));
    *var_9 = 18446744073709551615UL;
    var_10 = *(uint64_t *)(*var_2 + 72UL);
    var_11 = (uint64_t *)(var_0 + (-64L));
    *var_11 = var_10;
    var_12 = (*var_4 != 0UL);
    var_13 = (unsigned char *)(var_0 + (-33L));
    *var_13 = var_12;
    var_14 = *var_11;
    var_15 = (uint64_t *)(var_0 + (-48L));
    *var_15 = var_14;
    var_16 = var_0 + (-120L);
    var_17 = (uint32_t *)var_16;
    *var_17 = 0U;
    var_18 = (uint64_t *)(var_0 + (-88L));
    *var_18 = var_16;
    var_19 = *var_2;
    var_20 = (uint64_t *)(var_0 + (-96L));
    *var_20 = var_19;
    var_21 = *var_11;
    var_22 = (uint64_t *)(var_0 + (-104L));
    *var_22 = var_21;
    var_23 = *(uint64_t *)(*var_20 + 152UL);
    var_24 = (uint64_t *)(var_0 + (-112L));
    *var_24 = var_23;
    var_25 = *(uint64_t *)(var_23 + 72UL);
    r84_2 = r8;
    rax_0 = var_25;
    r95_2 = r9;
    r106_2 = r10;
    rbx7_0 = rbx;
    rax_1 = 18446744073709551614UL;
    if ((signed char)*(unsigned char *)(var_25 + 104UL) > '\xff') {
        local_sp_3 = var_0 + (-152L);
    } else {
        var_26 = *var_20;
        var_27 = (uint64_t)*(uint32_t *)(var_26 + 160UL);
        var_28 = *var_22 + (-1L);
        var_29 = var_0 + (-160L);
        *(uint64_t *)var_29 = 4288108UL;
        var_30 = indirect_placeholder_9(var_27, var_28, var_26);
        var_31 = (uint32_t *)(var_0 + (-116L));
        var_32 = (uint32_t)var_30;
        *var_31 = var_32;
        local_sp_3 = var_29;
        if ((var_32 & 1U) == 0U) {
            rax_0 = *(uint64_t *)(*var_24 + 80UL);
        } else {
            if (var_32 == 0U) {
                rax_0 = *(uint64_t *)(*var_24 + 72UL);
            } else {
                var_33 = ((var_32 & 4U) == 0U);
                var_34 = ((var_32 & 2U) == 0U);
                if (var_33 || var_34) {
                    rax_0 = *(uint64_t *)(*var_24 + 96UL);
                } else {
                    if (var_34) {
                        rax_0 = *(uint64_t *)(*var_24 + 88UL);
                    } else {
                        var_35 = *var_24;
                        var_36 = *(uint64_t *)(var_35 + 72UL);
                        rax_0 = var_36;
                        if (var_33) {
                            var_37 = *(uint64_t *)(var_36 + 80UL);
                            var_38 = (uint64_t)var_32;
                            var_39 = *var_18;
                            var_40 = var_0 + (-168L);
                            *(uint64_t *)var_40 = 4288238UL;
                            var_41 = indirect_placeholder_173(var_38, var_37, var_35, var_39);
                            r84_2 = var_41.field_1;
                            rax_0 = var_41.field_0;
                            local_sp_3 = var_40;
                            r95_2 = var_41.field_2;
                            r106_2 = var_41.field_3;
                            rbx7_0 = var_41.field_4;
                        }
                    }
                }
            }
        }
    }
    var_42 = (uint64_t *)(var_0 + (-32L));
    *var_42 = rax_0;
    local_sp_4 = local_sp_3;
    r84_3 = r84_2;
    r95_3 = r95_2;
    r106_3 = r106_2;
    rbx7_1 = rbx7_0;
    if (rax_0 == 0UL) {
        if (*var_17 == 12U) {
            *(uint64_t *)(local_sp_3 + (-8L)) = 4288311UL;
            indirect_placeholder_2();
        }
    } else {
        var_43 = *(uint64_t *)(*var_2 + 184UL);
        if (var_43 != 0UL) {
            *(uint64_t *)((*var_11 << 3UL) + var_43) = rax_0;
            if (*(uint64_t *)(*var_7 + 152UL) != 0UL) {
                *var_13 = (unsigned char)'\x00';
                var_44 = *var_42 + 8UL;
                var_45 = *var_2;
                var_46 = local_sp_3 + (-8L);
                *(uint64_t *)var_46 = 4288424UL;
                var_47 = indirect_placeholder_174(0UL, var_44, var_45);
                var_48 = (uint32_t)var_47.field_0;
                *var_17 = var_48;
                local_sp_4 = var_46;
                if (var_48 != 0U) {
                    var_49 = (uint64_t)var_48;
                    rax_1 = var_49;
                    return rax_1;
                }
                var_50 = var_47.field_3;
                var_51 = var_47.field_2;
                var_52 = var_47.field_1;
                var_53 = *var_42;
                r84_3 = var_52;
                r95_3 = var_51;
                r106_3 = var_50;
                var_54 = var_53 + 8UL;
                var_55 = *var_2;
                var_56 = local_sp_3 + (-16L);
                *(uint64_t *)var_56 = 4288491UL;
                var_57 = indirect_placeholder_172(var_54, var_55);
                var_58 = var_57.field_1;
                var_59 = var_57.field_2;
                var_60 = var_57.field_3;
                var_61 = (uint32_t)var_57.field_0;
                *var_17 = var_61;
                local_sp_4 = var_56;
                r84_3 = var_58;
                r95_3 = var_59;
                r106_3 = var_60;
                if ((*(unsigned char *)(var_53 + 104UL) & '@') != '\x00' & var_61 != 0U) {
                    var_62 = (uint64_t)var_61;
                    rax_1 = var_62;
                    return rax_1;
                }
            }
        }
        var_63 = *var_42;
        var_64 = *(unsigned char *)(var_63 + 104UL);
        r84_4 = r84_3;
        local_sp_6_ph = local_sp_4;
        local_sp_5 = local_sp_4;
        r95_4 = r95_3;
        r106_4 = r106_3;
        if ((var_64 & '\x10') != '\x00') {
            if ((signed char)var_64 > '\xff') {
                var_65 = *var_11;
                var_66 = *var_2;
                var_67 = local_sp_4 + (-8L);
                *(uint64_t *)var_67 = 4288584UL;
                var_68 = indirect_placeholder_9(var_65, var_63, var_66);
                local_sp_5 = var_67;
                local_sp_6_ph = var_67;
                if (var_68 != 0UL) {
                    var_69 = (*var_5 == '\x01');
                    var_70 = *var_11;
                    local_sp_6_ph = local_sp_5;
                    rax_1 = var_70;
                    if (!var_69) {
                        return rax_1;
                    }
                    *var_9 = var_70;
                    *var_8 = 1UL;
                }
            } else {
                var_69 = (*var_5 == '\x01');
                var_70 = *var_11;
                local_sp_6_ph = local_sp_5;
                rax_1 = var_70;
                if (var_69) {
                    return rax_1;
                }
                *var_9 = var_70;
                *var_8 = 1UL;
            }
        }
        var_71 = (uint64_t *)(var_0 + (-72L));
        var_72 = (uint64_t *)(var_0 + (-80L));
        local_sp_6 = local_sp_6_ph;
        while (1U)
            {
                var_73 = *var_2;
                local_sp_7 = local_sp_6;
                rbx7_2 = rbx7_1;
                if ((long)*(uint64_t *)(var_73 + 104UL) <= (long)*(uint64_t *)(var_73 + 72UL)) {
                    loop_state_var = 1U;
                    break;
                }
                *var_71 = *var_42;
                var_74 = *(uint64_t *)(*var_2 + 72UL) + 1UL;
                *var_72 = var_74;
                var_75 = *var_2;
                var_76 = *(uint64_t *)(var_75 + 64UL);
                var_84 = var_75;
                if ((long)var_74 < (long)var_76) {
                    if ((long)var_76 >= (long)*(uint64_t *)(var_75 + 88UL)) {
                        var_77 = *(uint64_t *)(var_75 + 48UL);
                        if ((long)var_74 >= (long)var_77) {
                            var_85 = *var_42;
                            var_86 = local_sp_7 + (-8L);
                            *(uint64_t *)var_86 = 4288856UL;
                            var_87 = indirect_placeholder_176(var_85, var_84, var_16);
                            var_88 = var_87.field_0;
                            var_89 = var_87.field_1;
                            var_90 = var_87.field_2;
                            var_91 = var_87.field_3;
                            *var_42 = var_88;
                            var_92 = *var_2;
                            var_99 = var_88;
                            local_sp_0 = var_86;
                            r84_0 = var_89;
                            r95_0 = var_90;
                            r106_0 = var_91;
                            rbx7_1 = rbx7_2;
                            if (*(uint64_t *)(var_92 + 184UL) == 0UL) {
                                var_93 = local_sp_7 + (-16L);
                                *(uint64_t *)var_93 = 4288899UL;
                                var_94 = indirect_placeholder_170(var_88, var_92, var_16);
                                var_95 = var_94.field_0;
                                var_96 = var_94.field_1;
                                var_97 = var_94.field_2;
                                var_98 = var_94.field_3;
                                *var_42 = var_95;
                                var_99 = var_95;
                                local_sp_0 = var_93;
                                r84_0 = var_96;
                                r95_0 = var_97;
                                r106_0 = var_98;
                            }
                            var_107 = var_99;
                            r84_1 = r84_0;
                            local_sp_1 = local_sp_0;
                            r95_1 = r95_0;
                            r106_1 = r106_0;
                            if (var_99 != 0UL) {
                                if (*var_17 != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_100 = *var_2;
                                if (*(uint64_t *)(var_100 + 184UL) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*var_8 != 0UL) {
                                    if (*var_5 != '\x01') {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                }
                                var_101 = local_sp_0 + (-8L);
                                *(uint64_t *)var_101 = 4288999UL;
                                var_102 = indirect_placeholder_171(var_100, var_16);
                                var_103 = var_102.field_0;
                                var_104 = var_102.field_1;
                                var_105 = var_102.field_2;
                                var_106 = var_102.field_3;
                                *var_42 = var_103;
                                var_107 = var_103;
                                r84_1 = var_104;
                                local_sp_1 = var_101;
                                r95_1 = var_105;
                                r106_1 = var_106;
                                if (var_103 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            local_sp_2 = local_sp_1;
                            local_sp_6_be = local_sp_1;
                            r84_4 = r84_1;
                            r95_4 = r95_1;
                            r106_4 = r106_1;
                            if (*var_13 != '\x00') {
                                if (*var_71 == var_107) {
                                    *var_15 = *var_72;
                                } else {
                                    *var_13 = (unsigned char)'\x00';
                                }
                            }
                            var_108 = *var_42;
                            var_109 = *(unsigned char *)(var_108 + 104UL);
                            if ((var_109 & '\x10') == '\x00') {
                                local_sp_6 = local_sp_6_be;
                                continue;
                            }
                            if ((signed char)var_109 <= '\xff') {
                                var_110 = *var_2;
                                var_111 = *(uint64_t *)(var_110 + 72UL);
                                var_112 = local_sp_1 + (-8L);
                                *(uint64_t *)var_112 = 4289104UL;
                                var_113 = indirect_placeholder_9(var_111, var_108, var_110);
                                local_sp_6_be = var_112;
                                local_sp_2 = var_112;
                                if (var_113 != 0UL) {
                                    local_sp_6 = local_sp_6_be;
                                    continue;
                                }
                            }
                            *var_9 = *(uint64_t *)(*var_2 + 72UL);
                            *var_8 = 1UL;
                            *var_4 = 0UL;
                            local_sp_6_be = local_sp_2;
                            if (*var_5 != '\x01') {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        if ((long)var_77 >= (long)*(uint64_t *)(var_75 + 88UL)) {
                            var_85 = *var_42;
                            var_86 = local_sp_7 + (-8L);
                            *(uint64_t *)var_86 = 4288856UL;
                            var_87 = indirect_placeholder_176(var_85, var_84, var_16);
                            var_88 = var_87.field_0;
                            var_89 = var_87.field_1;
                            var_90 = var_87.field_2;
                            var_91 = var_87.field_3;
                            *var_42 = var_88;
                            var_92 = *var_2;
                            var_99 = var_88;
                            local_sp_0 = var_86;
                            r84_0 = var_89;
                            r95_0 = var_90;
                            r106_0 = var_91;
                            rbx7_1 = rbx7_2;
                            if (*(uint64_t *)(var_92 + 184UL) == 0UL) {
                                var_93 = local_sp_7 + (-16L);
                                *(uint64_t *)var_93 = 4288899UL;
                                var_94 = indirect_placeholder_170(var_88, var_92, var_16);
                                var_95 = var_94.field_0;
                                var_96 = var_94.field_1;
                                var_97 = var_94.field_2;
                                var_98 = var_94.field_3;
                                *var_42 = var_95;
                                var_99 = var_95;
                                local_sp_0 = var_93;
                                r84_0 = var_96;
                                r95_0 = var_97;
                                r106_0 = var_98;
                            }
                            var_107 = var_99;
                            r84_1 = r84_0;
                            local_sp_1 = local_sp_0;
                            r95_1 = r95_0;
                            r106_1 = r106_0;
                            if (var_99 != 0UL) {
                                if (*var_17 != 0U) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_100 = *var_2;
                                if (*(uint64_t *)(var_100 + 184UL) != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                if (*var_8 != 0UL) {
                                    if (*var_5 != '\x01') {
                                        loop_state_var = 1U;
                                        break;
                                    }
                                }
                                var_101 = local_sp_0 + (-8L);
                                *(uint64_t *)var_101 = 4288999UL;
                                var_102 = indirect_placeholder_171(var_100, var_16);
                                var_103 = var_102.field_0;
                                var_104 = var_102.field_1;
                                var_105 = var_102.field_2;
                                var_106 = var_102.field_3;
                                *var_42 = var_103;
                                var_107 = var_103;
                                r84_1 = var_104;
                                local_sp_1 = var_101;
                                r95_1 = var_105;
                                r106_1 = var_106;
                                if (var_103 != 0UL) {
                                    loop_state_var = 1U;
                                    break;
                                }
                            }
                            local_sp_2 = local_sp_1;
                            local_sp_6_be = local_sp_1;
                            r84_4 = r84_1;
                            r95_4 = r95_1;
                            r106_4 = r106_1;
                            if (*var_13 != '\x00') {
                                if (*var_71 == var_107) {
                                    *var_15 = *var_72;
                                } else {
                                    *var_13 = (unsigned char)'\x00';
                                }
                            }
                            var_108 = *var_42;
                            var_109 = *(unsigned char *)(var_108 + 104UL);
                            if ((var_109 & '\x10') == '\x00') {
                                local_sp_6 = local_sp_6_be;
                                continue;
                            }
                            if ((signed char)var_109 <= '\xff') {
                                var_110 = *var_2;
                                var_111 = *(uint64_t *)(var_110 + 72UL);
                                var_112 = local_sp_1 + (-8L);
                                *(uint64_t *)var_112 = 4289104UL;
                                var_113 = indirect_placeholder_9(var_111, var_108, var_110);
                                local_sp_6_be = var_112;
                                local_sp_2 = var_112;
                                if (var_113 != 0UL) {
                                    local_sp_6 = local_sp_6_be;
                                    continue;
                                }
                            }
                            *var_9 = *(uint64_t *)(*var_2 + 72UL);
                            *var_8 = 1UL;
                            *var_4 = 0UL;
                            local_sp_6_be = local_sp_2;
                            if (*var_5 != '\x01') {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                    }
                }
                var_77 = *(uint64_t *)(var_75 + 48UL);
                if ((long)var_74 < (long)var_77) {
                    var_85 = *var_42;
                    var_86 = local_sp_7 + (-8L);
                    *(uint64_t *)var_86 = 4288856UL;
                    var_87 = indirect_placeholder_176(var_85, var_84, var_16);
                    var_88 = var_87.field_0;
                    var_89 = var_87.field_1;
                    var_90 = var_87.field_2;
                    var_91 = var_87.field_3;
                    *var_42 = var_88;
                    var_92 = *var_2;
                    var_99 = var_88;
                    local_sp_0 = var_86;
                    r84_0 = var_89;
                    r95_0 = var_90;
                    r106_0 = var_91;
                    rbx7_1 = rbx7_2;
                    if (*(uint64_t *)(var_92 + 184UL) == 0UL) {
                        var_93 = local_sp_7 + (-16L);
                        *(uint64_t *)var_93 = 4288899UL;
                        var_94 = indirect_placeholder_170(var_88, var_92, var_16);
                        var_95 = var_94.field_0;
                        var_96 = var_94.field_1;
                        var_97 = var_94.field_2;
                        var_98 = var_94.field_3;
                        *var_42 = var_95;
                        var_99 = var_95;
                        local_sp_0 = var_93;
                        r84_0 = var_96;
                        r95_0 = var_97;
                        r106_0 = var_98;
                    }
                    var_107 = var_99;
                    r84_1 = r84_0;
                    local_sp_1 = local_sp_0;
                    r95_1 = r95_0;
                    r106_1 = r106_0;
                    if (var_99 != 0UL) {
                        if (*var_17 == 0U) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_100 = *var_2;
                        if (*(uint64_t *)(var_100 + 184UL) == 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (*var_8 != 0UL) {
                            if (*var_5 == '\x01') {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_101 = local_sp_0 + (-8L);
                        *(uint64_t *)var_101 = 4288999UL;
                        var_102 = indirect_placeholder_171(var_100, var_16);
                        var_103 = var_102.field_0;
                        var_104 = var_102.field_1;
                        var_105 = var_102.field_2;
                        var_106 = var_102.field_3;
                        *var_42 = var_103;
                        var_107 = var_103;
                        r84_1 = var_104;
                        local_sp_1 = var_101;
                        r95_1 = var_105;
                        r106_1 = var_106;
                        if (var_103 == 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    local_sp_2 = local_sp_1;
                    local_sp_6_be = local_sp_1;
                    r84_4 = r84_1;
                    r95_4 = r95_1;
                    r106_4 = r106_1;
                    if (*var_13 != '\x00') {
                        if (*var_71 == var_107) {
                            *var_13 = (unsigned char)'\x00';
                        } else {
                            *var_15 = *var_72;
                        }
                    }
                    var_108 = *var_42;
                    var_109 = *(unsigned char *)(var_108 + 104UL);
                    if ((var_109 & '\x10') == '\x00') {
                        local_sp_6 = local_sp_6_be;
                        continue;
                    }
                    if ((signed char)var_109 <= '\xff') {
                        var_110 = *var_2;
                        var_111 = *(uint64_t *)(var_110 + 72UL);
                        var_112 = local_sp_1 + (-8L);
                        *(uint64_t *)var_112 = 4289104UL;
                        var_113 = indirect_placeholder_9(var_111, var_108, var_110);
                        local_sp_6_be = var_112;
                        local_sp_2 = var_112;
                        if (var_113 == 0UL) {
                            local_sp_6 = local_sp_6_be;
                            continue;
                        }
                    }
                    *var_9 = *(uint64_t *)(*var_2 + 72UL);
                    *var_8 = 1UL;
                    *var_4 = 0UL;
                    local_sp_6_be = local_sp_2;
                    if (*var_5 == '\x01') {
                        loop_state_var = 1U;
                        break;
                    }
                }
                if ((long)var_77 >= (long)*(uint64_t *)(var_75 + 88UL)) {
                    var_85 = *var_42;
                    var_86 = local_sp_7 + (-8L);
                    *(uint64_t *)var_86 = 4288856UL;
                    var_87 = indirect_placeholder_176(var_85, var_84, var_16);
                    var_88 = var_87.field_0;
                    var_89 = var_87.field_1;
                    var_90 = var_87.field_2;
                    var_91 = var_87.field_3;
                    *var_42 = var_88;
                    var_92 = *var_2;
                    var_99 = var_88;
                    local_sp_0 = var_86;
                    r84_0 = var_89;
                    r95_0 = var_90;
                    r106_0 = var_91;
                    rbx7_1 = rbx7_2;
                    if (*(uint64_t *)(var_92 + 184UL) == 0UL) {
                        var_93 = local_sp_7 + (-16L);
                        *(uint64_t *)var_93 = 4288899UL;
                        var_94 = indirect_placeholder_170(var_88, var_92, var_16);
                        var_95 = var_94.field_0;
                        var_96 = var_94.field_1;
                        var_97 = var_94.field_2;
                        var_98 = var_94.field_3;
                        *var_42 = var_95;
                        var_99 = var_95;
                        local_sp_0 = var_93;
                        r84_0 = var_96;
                        r95_0 = var_97;
                        r106_0 = var_98;
                    }
                    var_107 = var_99;
                    r84_1 = r84_0;
                    local_sp_1 = local_sp_0;
                    r95_1 = r95_0;
                    r106_1 = r106_0;
                    if (var_99 != 0UL) {
                        if (*var_17 == 0U) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_100 = *var_2;
                        if (*(uint64_t *)(var_100 + 184UL) == 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                        if (*var_8 != 0UL) {
                            if (*var_5 == '\x01') {
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_101 = local_sp_0 + (-8L);
                        *(uint64_t *)var_101 = 4288999UL;
                        var_102 = indirect_placeholder_171(var_100, var_16);
                        var_103 = var_102.field_0;
                        var_104 = var_102.field_1;
                        var_105 = var_102.field_2;
                        var_106 = var_102.field_3;
                        *var_42 = var_103;
                        var_107 = var_103;
                        r84_1 = var_104;
                        local_sp_1 = var_101;
                        r95_1 = var_105;
                        r106_1 = var_106;
                        if (var_103 == 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    local_sp_2 = local_sp_1;
                    local_sp_6_be = local_sp_1;
                    r84_4 = r84_1;
                    r95_4 = r95_1;
                    r106_4 = r106_1;
                    if (*var_13 != '\x00') {
                        if (*var_71 == var_107) {
                            *var_13 = (unsigned char)'\x00';
                        } else {
                            *var_15 = *var_72;
                        }
                    }
                    var_108 = *var_42;
                    var_109 = *(unsigned char *)(var_108 + 104UL);
                    if ((var_109 & '\x10') == '\x00') {
                        local_sp_6 = local_sp_6_be;
                        continue;
                    }
                    if ((signed char)var_109 <= '\xff') {
                        var_110 = *var_2;
                        var_111 = *(uint64_t *)(var_110 + 72UL);
                        var_112 = local_sp_1 + (-8L);
                        *(uint64_t *)var_112 = 4289104UL;
                        var_113 = indirect_placeholder_9(var_111, var_108, var_110);
                        local_sp_6_be = var_112;
                        local_sp_2 = var_112;
                        if (var_113 == 0UL) {
                            local_sp_6 = local_sp_6_be;
                            continue;
                        }
                    }
                    *var_9 = *(uint64_t *)(*var_2 + 72UL);
                    *var_8 = 1UL;
                    *var_4 = 0UL;
                    local_sp_6_be = local_sp_2;
                    if (*var_5 == '\x01') {
                        loop_state_var = 1U;
                        break;
                    }
                }
                var_78 = (uint64_t)((uint32_t)var_74 + 1U);
                var_79 = local_sp_6 + (-8L);
                *(uint64_t *)var_79 = 4288769UL;
                var_80 = indirect_placeholder_175(var_78, var_75, r84_4, r95_4, r106_4, rbx7_1);
                var_81 = var_80.field_0;
                var_82 = var_80.field_4;
                var_83 = (uint32_t)var_81;
                *var_17 = var_83;
                local_sp_7 = var_79;
                rbx7_2 = var_82;
                switch_state_var = 0;
                switch (var_83) {
                  case 0U:
                    {
                        _pre = *var_2;
                        var_84 = _pre;
                        var_85 = *var_42;
                        var_86 = local_sp_7 + (-8L);
                        *(uint64_t *)var_86 = 4288856UL;
                        var_87 = indirect_placeholder_176(var_85, var_84, var_16);
                        var_88 = var_87.field_0;
                        var_89 = var_87.field_1;
                        var_90 = var_87.field_2;
                        var_91 = var_87.field_3;
                        *var_42 = var_88;
                        var_92 = *var_2;
                        var_99 = var_88;
                        local_sp_0 = var_86;
                        r84_0 = var_89;
                        r95_0 = var_90;
                        r106_0 = var_91;
                        rbx7_1 = rbx7_2;
                        if (*(uint64_t *)(var_92 + 184UL) == 0UL) {
                            var_93 = local_sp_7 + (-16L);
                            *(uint64_t *)var_93 = 4288899UL;
                            var_94 = indirect_placeholder_170(var_88, var_92, var_16);
                            var_95 = var_94.field_0;
                            var_96 = var_94.field_1;
                            var_97 = var_94.field_2;
                            var_98 = var_94.field_3;
                            *var_42 = var_95;
                            var_99 = var_95;
                            local_sp_0 = var_93;
                            r84_0 = var_96;
                            r95_0 = var_97;
                            r106_0 = var_98;
                        }
                        var_107 = var_99;
                        r84_1 = r84_0;
                        local_sp_1 = local_sp_0;
                        r95_1 = r95_0;
                        r106_1 = r106_0;
                        if (var_99 != 0UL) {
                            if (*var_17 != 0U) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            var_100 = *var_2;
                            if (*(uint64_t *)(var_100 + 184UL) != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            if (*var_8 != 0UL) {
                                if (*var_5 != '\x01') {
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            var_101 = local_sp_0 + (-8L);
                            *(uint64_t *)var_101 = 4288999UL;
                            var_102 = indirect_placeholder_171(var_100, var_16);
                            var_103 = var_102.field_0;
                            var_104 = var_102.field_1;
                            var_105 = var_102.field_2;
                            var_106 = var_102.field_3;
                            *var_42 = var_103;
                            var_107 = var_103;
                            r84_1 = var_104;
                            local_sp_1 = var_101;
                            r95_1 = var_105;
                            r106_1 = var_106;
                            if (var_103 != 0UL) {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                        }
                        local_sp_2 = local_sp_1;
                        local_sp_6_be = local_sp_1;
                        r84_4 = r84_1;
                        r95_4 = r95_1;
                        r106_4 = r106_1;
                        if (*var_13 != '\x00') {
                            if (*var_71 == var_107) {
                                *var_15 = *var_72;
                            } else {
                                *var_13 = (unsigned char)'\x00';
                            }
                        }
                        var_108 = *var_42;
                        var_109 = *(unsigned char *)(var_108 + 104UL);
                        if ((var_109 & '\x10') == '\x00') {
                            local_sp_6 = local_sp_6_be;
                            continue;
                        }
                        if ((signed char)var_109 <= '\xff') {
                            var_110 = *var_2;
                            var_111 = *(uint64_t *)(var_110 + 72UL);
                            var_112 = local_sp_1 + (-8L);
                            *(uint64_t *)var_112 = 4289104UL;
                            var_113 = indirect_placeholder_9(var_111, var_108, var_110);
                            local_sp_6_be = var_112;
                            local_sp_2 = var_112;
                            if (var_113 != 0UL) {
                                local_sp_6 = local_sp_6_be;
                                continue;
                            }
                        }
                        *var_9 = *(uint64_t *)(*var_2 + 72UL);
                        *var_8 = 1UL;
                        *var_4 = 0UL;
                        local_sp_6_be = local_sp_2;
                        if (*var_5 != '\x01') {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    break;
                  case 12U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  default:
                    {
                        *(uint64_t *)(local_sp_6 + (-16L)) = 4288821UL;
                        indirect_placeholder_2();
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        if (*var_4 != 0UL) {
            var_114 = *(uint64_t **)var_3;
            *var_114 = (*var_114 + *var_15);
        }
        var_115 = *var_9;
        rax_1 = var_115;
    }
}
