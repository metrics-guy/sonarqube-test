typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
uint64_t bb_check_node_accept_bytes(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint32_t var_44;
    uint64_t var_45;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint32_t var_48;
    uint32_t var_52;
    uint32_t *var_66;
    uint64_t var_49;
    uint64_t var_36;
    uint32_t var_21;
    uint64_t var_34;
    uint32_t rax_1_shrunk;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t *var_25;
    uint32_t *var_26;
    uint64_t var_27;
    uint64_t local_sp_0;
    uint32_t rax_0;
    uint32_t *var_32;
    uint64_t *var_33;
    uint64_t *var_35;
    uint64_t local_sp_1;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint32_t var_42;
    uint32_t var_43;
    uint32_t var_46;
    uint64_t var_47;
    uint32_t var_51;
    uint32_t var_53;
    uint32_t var_54;
    uint32_t _v;
    uint64_t var_50;
    uint32_t *var_13;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_55;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint32_t *var_19;
    uint32_t var_20;
    unsigned char var_56;
    unsigned char *var_57;
    uint64_t var_58;
    uint64_t var_59;
    unsigned char var_60;
    unsigned char *var_61;
    unsigned char var_62;
    uint32_t spec_select;
    uint32_t *var_63;
    uint32_t *_pre_phi141;
    uint32_t *var_64;
    uint32_t *var_65;
    uint64_t *var_67;
    uint64_t var_68;
    uint32_t var_69;
    unsigned char var_70;
    uint64_t var_71;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-96L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-104L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-112L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-120L));
    *var_6 = rcx;
    var_7 = **(uint64_t **)var_2 + (*var_4 << 4UL);
    var_8 = var_0 + (-40L);
    var_9 = (uint64_t *)var_8;
    *var_9 = var_7;
    var_36 = 0UL;
    var_34 = 0UL;
    rax_1_shrunk = 0U;
    rax_0 = 0U;
    var_38 = 0UL;
    var_68 = 1UL;
    if (*(unsigned char *)(var_7 + 8UL) == '\a') {
        var_56 = *(unsigned char *)(*var_6 + *(uint64_t *)(*var_5 + 8UL));
        var_57 = (unsigned char *)(var_0 + (-73L));
        *var_57 = var_56;
        var_58 = *var_6 + 1UL;
        var_59 = *var_5;
        if (var_56 >= '\xc2' & (long)var_58 >= (long)*(uint64_t *)(var_59 + 88UL)) {
            var_60 = *(unsigned char *)(*(uint64_t *)(var_59 + 8UL) + var_58);
            var_61 = (unsigned char *)(var_0 + (-74L));
            *var_61 = var_60;
            var_62 = *var_57;
            if (var_62 > '\xdf') {
                if (var_62 > '\xef') {
                    if (var_62 <= '\xf7') {
                        var_64 = (uint32_t *)(var_0 + (-12L));
                        *var_64 = 4U;
                        _pre_phi141 = var_64;
                        if (*var_57 != '\xf0' & *var_61 > '\x8f') {
                            return (uint64_t)rax_1_shrunk;
                        }
                    }
                    if (var_62 <= '\xfb') {
                        var_65 = (uint32_t *)(var_0 + (-12L));
                        *var_65 = 5U;
                        _pre_phi141 = var_65;
                        if (*var_57 != '\xf8' & *var_61 > '\x87') {
                            return (uint64_t)rax_1_shrunk;
                        }
                    }
                    if (var_62 > '\xfd') {
                        return (uint64_t)rax_1_shrunk;
                    }
                    var_66 = (uint32_t *)(var_0 + (-12L));
                    *var_66 = 6U;
                    _pre_phi141 = var_66;
                    if (*var_57 != '\xfc' & *var_61 > '\x83') {
                        return (uint64_t)rax_1_shrunk;
                    }
                }
                var_63 = (uint32_t *)(var_0 + (-12L));
                *var_63 = 3U;
                _pre_phi141 = var_63;
                if (*var_57 != '\xe0') {
                    if (*var_61 > '\x9f') {
                        return (uint64_t)rax_1_shrunk;
                    }
                }
                if ((long)(*var_6 + (uint64_t)*_pre_phi141) <= (long)*(uint64_t *)(*var_5 + 88UL)) {
                    var_67 = (uint64_t *)(var_0 + (-24L));
                    *var_67 = 1UL;
                    var_69 = *_pre_phi141;
                    rax_1_shrunk = var_69;
                    while ((long)var_68 >= (long)(uint64_t)var_69)
                        {
                            var_70 = *(unsigned char *)(*(uint64_t *)(*var_5 + 8UL) + (var_68 + *var_6));
                            *var_61 = var_70;
                            rax_1_shrunk = 0U;
                            if (((signed char)var_70 > '\xff') || (var_70 > '\xbf')) {
                                break;
                            }
                            var_71 = *var_67 + 1UL;
                            *var_67 = var_71;
                            var_68 = var_71;
                            var_69 = *_pre_phi141;
                            rax_1_shrunk = var_69;
                        }
                }
            }
            spec_select = (((signed char)var_60 > '\xff') || (var_60 > '\xbf')) ? 0U : 2U;
            rax_1_shrunk = spec_select;
        }
    } else {
        var_10 = *var_6;
        var_11 = *var_5;
        *(uint64_t *)(var_0 + (-128L)) = 4315098UL;
        var_12 = indirect_placeholder(var_10, var_11);
        var_13 = (uint32_t *)(var_0 + (-12L));
        var_14 = (uint32_t)var_12;
        *var_13 = var_14;
        if (*(unsigned char *)(*var_9 + 8UL) == '\x05') {
            if ((int)var_14 <= (int)1U) {
                var_55 = *(uint64_t *)(*var_3 + 216UL);
                rax_1_shrunk = var_14;
                if ((var_55 & 64UL) != 0UL) {
                    if (*(unsigned char *)(*var_6 + *(uint64_t *)(*var_5 + 8UL)) == '\n') {
                        return (uint64_t)rax_1_shrunk;
                    }
                }
                if ((signed char)(unsigned char)var_55 > '\xff') {
                } else {
                    if (*(unsigned char *)(*var_6 + *(uint64_t *)(*var_5 + 8UL)) == '\x00') {
                    }
                }
            }
        } else {
            var_15 = *var_6;
            var_16 = *var_5;
            var_17 = var_0 + (-136L);
            *(uint64_t *)var_17 = 4315250UL;
            var_18 = indirect_placeholder(var_15, var_16);
            var_19 = (uint32_t *)(var_0 + (-44L));
            var_20 = (uint32_t)var_18;
            *var_19 = var_20;
            local_sp_0 = var_17;
            if ((int)var_20 <= (int)1U) {
                var_21 = *var_13;
                var_22 = var_21;
                if ((int)var_21 > (int)1U) {
                    return (uint64_t)rax_1_shrunk;
                }
            }
            var_22 = *var_13;
        }
    }
}
