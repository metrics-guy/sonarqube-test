typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_check_arrival_ret_type;
struct indirect_placeholder_198_ret_type;
struct indirect_placeholder_199_ret_type;
struct bb_check_arrival_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_198_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_199_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_r10(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_198_ret_type indirect_placeholder_198(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_199_ret_type indirect_placeholder_199(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
struct bb_check_arrival_ret_type bb_check_arrival(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9) {
    uint32_t var_130;
    uint64_t var_68;
    uint64_t var_112;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t local_sp_7;
    uint64_t r89_1;
    uint64_t var_134;
    uint64_t var_135;
    uint32_t rax_0_shrunk;
    uint64_t storemerge11;
    uint64_t var_85;
    uint32_t var_114;
    uint32_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint32_t var_113;
    uint64_t var_115;
    uint64_t r910_1;
    uint64_t r10_1;
    uint64_t rbx_1;
    uint32_t var_99;
    uint32_t var_91;
    uint32_t var_80;
    uint64_t local_sp_1;
    uint32_t var_70;
    uint32_t var_60;
    uint64_t local_sp_0;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t r89_0;
    uint64_t r910_0;
    uint64_t r10_0;
    uint64_t rbx_0;
    uint64_t *var_81;
    uint64_t *var_82;
    uint64_t var_83;
    uint32_t *var_84;
    uint64_t var_56;
    uint64_t local_sp_2;
    uint64_t var_86;
    uint64_t local_sp_3;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint32_t var_90;
    uint64_t var_92;
    uint64_t local_sp_4;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint32_t var_98;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint32_t var_105;
    uint64_t local_sp_5;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint32_t var_120;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    struct indirect_placeholder_198_ret_type var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_131;
    uint64_t storemerge10;
    uint64_t *var_132;
    uint64_t var_133;
    uint64_t _pre_phi;
    uint64_t local_sp_6;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint32_t var_69;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    struct indirect_placeholder_199_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_57;
    uint64_t var_58;
    uint32_t var_59;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint32_t var_53;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint32_t var_48;
    uint64_t var_49;
    uint64_t var_30;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t *var_26;
    uint64_t r89_2;
    uint64_t r910_2;
    uint64_t r10_2;
    uint64_t rbx_2;
    struct bb_check_arrival_ret_type mrv;
    struct bb_check_arrival_ret_type mrv1;
    struct bb_check_arrival_ret_type mrv2;
    struct bb_check_arrival_ret_type mrv3;
    struct bb_check_arrival_ret_type mrv4;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t **var_31;
    uint64_t *storemerge_in_pre_phi;
    uint64_t storemerge;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t *var_34;
    uint64_t var_35;
    uint64_t *var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t *var_41;
    uint64_t var_42;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_r10();
    var_3 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_4 = var_0 + (-200L);
    var_5 = (uint64_t *)(var_0 + (-160L));
    *var_5 = rdi;
    var_6 = var_0 + (-168L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_8 = (uint64_t *)(var_0 + (-176L));
    *var_8 = rdx;
    var_9 = (uint64_t *)(var_0 + (-184L));
    *var_9 = rcx;
    var_10 = (uint64_t *)(var_0 + (-192L));
    *var_10 = r8;
    var_11 = (uint64_t *)var_4;
    *var_11 = r9;
    var_12 = *(uint64_t *)(*var_5 + 152UL);
    var_13 = var_0 + (-40L);
    var_14 = (uint64_t *)var_13;
    *var_14 = var_12;
    var_15 = var_0 + (-116L);
    var_16 = (uint32_t *)var_15;
    *var_16 = 0U;
    var_17 = (uint64_t *)(var_0 + (-32L));
    *var_17 = 0UL;
    var_18 = *(uint64_t *)(**(uint64_t **)var_13 + (*var_8 << 4UL));
    var_19 = (uint64_t *)(var_0 + (-48L));
    *var_19 = var_18;
    var_20 = *var_11 + (uint64_t)*(uint32_t *)(*var_5 + 224UL);
    var_21 = *(uint64_t *)(*var_7 + 8UL);
    local_sp_7 = var_4;
    rax_0_shrunk = 12U;
    storemerge11 = 0UL;
    var_85 = 0UL;
    r89_0 = r8;
    r910_0 = r9;
    r10_0 = var_2;
    rbx_0 = var_3;
    r89_2 = r8;
    r910_2 = r9;
    r10_2 = var_2;
    rbx_2 = var_3;
    storemerge_in_pre_phi = var_9;
    if ((long)var_20 >= (long)var_21) {
        var_22 = (uint64_t *)(var_0 + (-56L));
        *var_22 = var_21;
        var_23 = (*var_11 + (uint64_t)*(uint32_t *)(*var_5 + 224UL)) + 1UL;
        *(uint64_t *)(var_0 + (-64L)) = var_23;
        var_24 = *var_22;
        if ((long)var_23 > (long)(9223372036854775807UL - var_24)) {
            mrv.field_0 = (uint64_t)rax_0_shrunk;
            mrv1 = mrv;
            mrv1.field_1 = r89_2;
            mrv2 = mrv1;
            mrv2.field_2 = r910_2;
            mrv3 = mrv2;
            mrv3.field_3 = r10_2;
            mrv4 = mrv3;
            mrv4.field_4 = rbx_2;
            return mrv4;
        }
        var_25 = var_23 + var_24;
        var_26 = (uint64_t *)(var_0 + (-72L));
        *var_26 = var_25;
        if (var_25 > 2305843009213693951UL) {
            mrv.field_0 = (uint64_t)rax_0_shrunk;
            mrv1 = mrv;
            mrv1.field_1 = r89_2;
            mrv2 = mrv1;
            mrv2.field_2 = r910_2;
            mrv3 = mrv2;
            mrv3.field_3 = r10_2;
            mrv4 = mrv3;
            mrv4.field_4 = rbx_2;
            return mrv4;
        }
        var_27 = var_25 << 3UL;
        var_28 = *(uint64_t *)(*var_7 + 16UL);
        *(uint64_t *)(var_0 + (-208L)) = 4305668UL;
        var_29 = indirect_placeholder(var_27, var_28);
        *(uint64_t *)(var_0 + (-80L)) = var_29;
        if (var_29 != 0UL) {
            mrv.field_0 = (uint64_t)rax_0_shrunk;
            mrv1 = mrv;
            mrv1.field_1 = r89_2;
            mrv2 = mrv1;
            mrv2.field_2 = r910_2;
            mrv3 = mrv2;
            mrv3.field_3 = r10_2;
            mrv4 = mrv3;
            mrv4.field_4 = rbx_2;
            return mrv4;
        }
        *(uint64_t *)(*var_7 + 16UL) = var_29;
        *(uint64_t *)(*var_7 + 8UL) = *var_26;
        var_30 = var_0 + (-216L);
        *(uint64_t *)var_30 = 4305783UL;
        indirect_placeholder_2();
        local_sp_7 = var_30;
    }
    var_31 = (uint64_t **)var_6;
    rax_0_shrunk = 1U;
    if (**var_31 == 0UL) {
        storemerge_in_pre_phi = (uint64_t *)*var_7;
    }
    storemerge = *storemerge_in_pre_phi;
    var_32 = (uint64_t *)(var_0 + (-16L));
    *var_32 = storemerge;
    var_33 = *(uint64_t *)(*var_5 + 184UL);
    var_34 = (uint64_t *)(var_0 + (-88L));
    *var_34 = var_33;
    var_35 = *(uint64_t *)(*var_5 + 72UL);
    var_36 = (uint64_t *)(var_0 + (-96L));
    *var_36 = var_35;
    *(uint64_t *)(*var_5 + 184UL) = *(uint64_t *)(*var_7 + 16UL);
    *(uint64_t *)(*var_5 + 72UL) = *var_32;
    var_37 = *var_5;
    var_38 = (uint64_t)*(uint32_t *)(var_37 + 160UL);
    var_39 = *var_32 + (-1L);
    *(uint64_t *)(local_sp_7 + (-8L)) = 4305933UL;
    var_40 = indirect_placeholder_9(var_38, var_39, var_37);
    var_41 = (uint32_t *)(var_0 + (-100L));
    *var_41 = (uint32_t)var_40;
    var_42 = *var_32;
    if (var_42 == *var_9) {
        var_50 = *var_8;
        var_51 = var_0 + (-152L);
        *(uint64_t *)(local_sp_7 + (-16L)) = 4305974UL;
        var_52 = indirect_placeholder(var_50, var_51);
        var_53 = (uint32_t)var_52;
        *var_16 = var_53;
        rax_0_shrunk = var_53;
        if (var_53 == 0U) {
            mrv.field_0 = (uint64_t)rax_0_shrunk;
            mrv1 = mrv;
            mrv1.field_1 = r89_2;
            mrv2 = mrv1;
            mrv2.field_2 = r910_2;
            mrv3 = mrv2;
            mrv3.field_3 = r10_2;
            mrv4 = mrv3;
            mrv4.field_4 = rbx_2;
            return mrv4;
        }
        var_54 = *var_19;
        var_55 = *var_14;
        var_56 = (uint64_t)*(uint32_t *)(var_0 | 8UL);
        var_57 = local_sp_7 + (-24L);
        *(uint64_t *)var_57 = 4306027UL;
        var_58 = indirect_placeholder_10(var_56, var_54, var_51, var_55);
        var_59 = (uint32_t)var_58;
        *var_16 = var_59;
        local_sp_0 = var_57;
        if (var_59 != 0U) {
            *(uint64_t *)(local_sp_7 + (-32L)) = 4306062UL;
            indirect_placeholder_2();
            var_60 = *var_16;
            rax_0_shrunk = var_60;
            mrv.field_0 = (uint64_t)rax_0_shrunk;
            mrv1 = mrv;
            mrv1.field_1 = r89_2;
            mrv2 = mrv1;
            mrv2.field_2 = r910_2;
            mrv3 = mrv2;
            mrv3.field_3 = r10_2;
            mrv4 = mrv3;
            mrv4.field_4 = rbx_2;
            return mrv4;
        }
    }
    var_43 = *(uint64_t *)(*(uint64_t *)(*var_5 + 184UL) + (var_42 << 3UL));
    *var_17 = var_43;
    if (var_43 == 0UL) {
        if ((*(unsigned char *)(var_43 + 104UL) & '@') != '\x00') {
            var_44 = var_43 + 8UL;
            var_45 = var_0 + (-152L);
            var_46 = local_sp_7 + (-16L);
            *(uint64_t *)var_46 = 4306150UL;
            var_47 = indirect_placeholder(var_44, var_45);
            var_48 = (uint32_t)var_47;
            *var_16 = var_48;
            local_sp_0 = var_46;
            rax_0_shrunk = var_48;
            if (var_48 == 0U) {
                mrv.field_0 = (uint64_t)rax_0_shrunk;
                mrv1 = mrv;
                mrv1.field_1 = r89_2;
                mrv2 = mrv1;
                mrv2.field_2 = r910_2;
                mrv3 = mrv2;
                mrv3.field_3 = r10_2;
                mrv4 = mrv3;
                mrv4.field_4 = rbx_2;
                return mrv4;
            }
        }
        var_49 = local_sp_7 + (-16L);
        *(uint64_t *)var_49 = 4306202UL;
        indirect_placeholder_2();
        local_sp_0 = var_49;
    } else {
        var_49 = local_sp_7 + (-16L);
        *(uint64_t *)var_49 = 4306202UL;
        indirect_placeholder_2();
        local_sp_0 = var_49;
        var_61 = *var_32;
        local_sp_1 = local_sp_0;
        local_sp_6 = local_sp_0;
        if (var_61 == *var_9) {
            var_62 = *var_17;
            if (var_62 != 0UL & (*(unsigned char *)(var_62 + 104UL) & '@') != '\x00') {
                if (*(uint64_t *)(var_0 + (-144L)) != 0UL) {
                    var_63 = *var_19;
                    var_64 = var_0 + (-152L);
                    var_65 = *var_5;
                    var_66 = (uint64_t)*(uint32_t *)(var_0 | 8UL);
                    var_67 = local_sp_0 + (-8L);
                    *(uint64_t *)var_67 = 4306291UL;
                    var_68 = indirect_placeholder_11(var_63, var_61, var_64, var_65, var_66);
                    var_69 = (uint32_t)var_68;
                    *var_16 = var_69;
                    _pre_phi = var_64;
                    local_sp_6 = var_67;
                    r89_2 = var_66;
                    if (var_69 != 0U) {
                        *(uint64_t *)(local_sp_0 + (-16L)) = 4306322UL;
                        indirect_placeholder_2();
                        var_70 = *var_16;
                        rax_0_shrunk = var_70;
                        mrv.field_0 = (uint64_t)rax_0_shrunk;
                        mrv1 = mrv;
                        mrv1.field_1 = r89_2;
                        mrv2 = mrv1;
                        mrv2.field_2 = r910_2;
                        mrv3 = mrv2;
                        mrv3.field_3 = r10_2;
                        mrv4 = mrv3;
                        mrv4.field_4 = rbx_2;
                        return mrv4;
                    }
                }
                _pre_phi = var_0 + (-152L);
            }
        } else {
            if (*(uint64_t *)(var_0 + (-144L)) == 0UL) {
                var_63 = *var_19;
                var_64 = var_0 + (-152L);
                var_65 = *var_5;
                var_66 = (uint64_t)*(uint32_t *)(var_0 | 8UL);
                var_67 = local_sp_0 + (-8L);
                *(uint64_t *)var_67 = 4306291UL;
                var_68 = indirect_placeholder_11(var_63, var_61, var_64, var_65, var_66);
                var_69 = (uint32_t)var_68;
                *var_16 = var_69;
                _pre_phi = var_64;
                local_sp_6 = var_67;
                r89_2 = var_66;
                if (var_69 == 0U) {
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4306322UL;
                    indirect_placeholder_2();
                    var_70 = *var_16;
                    rax_0_shrunk = var_70;
                    mrv.field_0 = (uint64_t)rax_0_shrunk;
                    mrv1 = mrv;
                    mrv1.field_1 = r89_2;
                    mrv2 = mrv1;
                    mrv2.field_2 = r910_2;
                    mrv3 = mrv2;
                    mrv3.field_3 = r10_2;
                    mrv4 = mrv3;
                    mrv4.field_4 = rbx_2;
                    return mrv4;
                }
            }
            _pre_phi = var_0 + (-152L);
            var_71 = (uint64_t)*var_41;
            var_72 = *var_14;
            var_73 = local_sp_6 + (-8L);
            *(uint64_t *)var_73 = 4306356UL;
            var_74 = indirect_placeholder_199(var_71, _pre_phi, var_72, var_15);
            var_75 = var_74.field_0;
            var_76 = var_74.field_1;
            var_77 = var_74.field_2;
            var_78 = var_74.field_3;
            var_79 = var_74.field_4;
            *var_17 = var_75;
            local_sp_1 = var_73;
            r89_0 = var_76;
            r910_0 = var_77;
            r10_0 = var_78;
            rbx_0 = var_79;
            r89_2 = var_76;
            r910_2 = var_77;
            r10_2 = var_78;
            rbx_2 = var_79;
            if (var_75 != 0UL) {
                if (*var_16 == 0U) {
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4306404UL;
                    indirect_placeholder_2();
                    var_80 = *var_16;
                    rax_0_shrunk = var_80;
                    mrv.field_0 = (uint64_t)rax_0_shrunk;
                    mrv1 = mrv;
                    mrv1.field_1 = r89_2;
                    mrv2 = mrv1;
                    mrv2.field_2 = r910_2;
                    mrv3 = mrv2;
                    mrv3.field_3 = r10_2;
                    mrv4 = mrv3;
                    mrv4.field_4 = rbx_2;
                    return mrv4;
                }
            }
            *(uint64_t *)((*var_32 << 3UL) + *(uint64_t *)(*var_5 + 184UL)) = var_75;
            var_81 = (uint64_t *)(var_0 + (-24L));
            *var_81 = 0UL;
            var_82 = (uint64_t *)(var_0 + (-144L));
            var_83 = var_0 + (-152L);
            var_84 = (uint32_t *)(var_0 | 8UL);
            r89_1 = r89_0;
            r910_1 = r910_0;
            r10_1 = r10_0;
            rbx_1 = rbx_0;
            local_sp_2 = local_sp_1;
            while (1U)
                {
                    local_sp_3 = local_sp_2;
                    r89_2 = r89_1;
                    r910_2 = r910_1;
                    r10_2 = r10_1;
                    rbx_2 = rbx_1;
                    if ((long)*var_32 >= (long)*var_11) {
                        loop_state_var = 1U;
                        break;
                    }
                    if ((long)var_85 <= (long)(uint64_t)*(uint32_t *)(*var_5 + 224UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    *var_82 = 0UL;
                    var_86 = *(uint64_t *)(*(uint64_t *)(*var_5 + 184UL) + ((*var_32 << 3UL) + 8UL));
                    if (var_86 != 0UL) {
                        var_87 = var_86 + 8UL;
                        var_88 = local_sp_2 + (-8L);
                        *(uint64_t *)var_88 = 4306559UL;
                        var_89 = indirect_placeholder(var_87, var_83);
                        var_90 = (uint32_t)var_89;
                        *var_16 = var_90;
                        local_sp_3 = var_88;
                        if (var_90 != 0U) {
                            *(uint64_t *)(local_sp_2 + (-16L)) = 4306590UL;
                            indirect_placeholder_2();
                            var_91 = *var_16;
                            rax_0_shrunk = var_91;
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_92 = *var_17;
                    local_sp_4 = local_sp_3;
                    if (var_92 != 0UL) {
                        var_93 = var_92 + 32UL;
                        var_94 = *var_32;
                        var_95 = *var_5;
                        var_96 = local_sp_3 + (-8L);
                        *(uint64_t *)var_96 = 4306645UL;
                        var_97 = indirect_placeholder_10(var_83, var_93, var_94, var_95);
                        var_98 = (uint32_t)var_97;
                        *var_16 = var_98;
                        local_sp_4 = var_96;
                        if (var_98 != 0U) {
                            *(uint64_t *)(local_sp_3 + (-16L)) = 4306676UL;
                            indirect_placeholder_2();
                            var_99 = *var_16;
                            rax_0_shrunk = var_99;
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_100 = *var_32 + 1UL;
                    *var_32 = var_100;
                    var_115 = var_100;
                    local_sp_5 = local_sp_4;
                    if (*var_82 != 0UL) {
                        var_101 = *var_19;
                        var_102 = *var_14;
                        var_103 = (uint64_t)*var_84;
                        *(uint64_t *)(local_sp_4 + (-8L)) = 4306731UL;
                        var_104 = indirect_placeholder_10(var_103, var_101, var_83, var_102);
                        var_105 = (uint32_t)var_104;
                        *var_16 = var_105;
                        if (var_105 != 0U) {
                            *(uint64_t *)(local_sp_4 + (-16L)) = 4306762UL;
                            indirect_placeholder_2();
                            var_106 = *var_16;
                            rax_0_shrunk = var_106;
                            loop_state_var = 0U;
                            break;
                        }
                        var_107 = *var_19;
                        var_108 = *var_32;
                        var_109 = *var_5;
                        var_110 = (uint64_t)*var_84;
                        var_111 = local_sp_4 + (-16L);
                        *(uint64_t *)var_111 = 4306804UL;
                        var_112 = indirect_placeholder_11(var_107, var_108, var_83, var_109, var_110);
                        var_113 = (uint32_t)var_112;
                        *var_16 = var_113;
                        local_sp_5 = var_111;
                        r89_2 = var_110;
                        if (var_113 != 0U) {
                            *(uint64_t *)(local_sp_4 + (-24L)) = 4306835UL;
                            indirect_placeholder_2();
                            var_114 = *var_16;
                            rax_0_shrunk = var_114;
                            loop_state_var = 0U;
                            break;
                        }
                        var_115 = *var_32;
                    }
                    var_116 = *var_5;
                    var_117 = (uint64_t)*(uint32_t *)(var_116 + 160UL);
                    var_118 = var_115 + (-1L);
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4306882UL;
                    var_119 = indirect_placeholder_9(var_117, var_118, var_116);
                    var_120 = (uint32_t)var_119;
                    *var_41 = var_120;
                    var_121 = (uint64_t)var_120;
                    var_122 = *var_14;
                    var_123 = local_sp_5 + (-16L);
                    *(uint64_t *)var_123 = 4306911UL;
                    var_124 = indirect_placeholder_198(var_121, var_83, var_122, var_15);
                    var_125 = var_124.field_0;
                    var_126 = var_124.field_1;
                    var_127 = var_124.field_2;
                    var_128 = var_124.field_3;
                    var_129 = var_124.field_4;
                    *var_17 = var_125;
                    r89_1 = var_126;
                    r910_1 = var_127;
                    r10_1 = var_128;
                    rbx_1 = var_129;
                    local_sp_2 = var_123;
                    r89_2 = var_126;
                    r910_2 = var_127;
                    r10_2 = var_128;
                    rbx_2 = var_129;
                    if (var_125 == 0UL) {
                        *(uint64_t *)((*var_32 << 3UL) + *(uint64_t *)(*var_5 + 184UL)) = var_125;
                        if (*var_17 == 0UL) {
                            storemerge11 = *var_81 + 1UL;
                        }
                        *var_81 = storemerge11;
                        var_85 = storemerge11;
                        continue;
                    }
                    if (*var_16 != 0U) {
                        *(uint64_t *)(local_sp_5 + (-24L)) = 4306959UL;
                        indirect_placeholder_2();
                        var_130 = *var_16;
                        rax_0_shrunk = var_130;
                        loop_state_var = 0U;
                        break;
                    }
                }
            switch (loop_state_var) {
              case 0U:
                {
                    mrv.field_0 = (uint64_t)rax_0_shrunk;
                    mrv1 = mrv;
                    mrv1.field_1 = r89_2;
                    mrv2 = mrv1;
                    mrv2.field_2 = r910_2;
                    mrv3 = mrv2;
                    mrv3.field_3 = r10_2;
                    mrv4 = mrv3;
                    mrv4.field_4 = rbx_2;
                    return mrv4;
                }
                break;
              case 1U:
                {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4307075UL;
                    indirect_placeholder_2();
                    var_131 = *(uint64_t *)(*(uint64_t *)(*var_5 + 184UL) + (*var_11 << 3UL));
                    storemerge10 = (var_131 == 0UL) ? 0UL : (var_131 + 8UL);
                    var_132 = (uint64_t *)(var_0 + (-112L));
                    *var_132 = storemerge10;
                    **var_31 = *var_32;
                    *(uint64_t *)(*var_5 + 184UL) = *var_34;
                    *(uint64_t *)(*var_5 + 72UL) = *var_36;
                    var_133 = *var_132;
                    if (var_133 == 0UL) {
                    } else {
                        var_134 = *var_10;
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4307233UL;
                        var_135 = indirect_placeholder(var_134, var_133);
                        rax_0_shrunk = 0U;
                        if (var_135 == 0UL) {
                        }
                    }
                }
                break;
            }
        }
    }
}
