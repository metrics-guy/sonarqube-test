typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern void abort(void);
extern void function_dispatcher(unsigned char *param_0);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_peek_token(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    unsigned char storemerge7;
    unsigned char *var_20;
    uint64_t local_sp_0;
    unsigned char var_26;
    unsigned char *var_27;
    unsigned char var_28;
    uint64_t var_29;
    uint64_t rax_0;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t *var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    unsigned char var_43;
    uint16_t var_44;
    uint16_t var_45;
    uint16_t var_46;
    unsigned char var_52;
    unsigned char *var_53;
    unsigned char storemerge;
    unsigned char *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_54;
    uint64_t var_55;
    uint32_t *var_56;
    uint32_t var_57;
    unsigned char var_51;
    unsigned char var_9;
    unsigned char *var_10;
    unsigned char **var_11;
    unsigned char *var_12;
    unsigned char *var_13;
    uint64_t var_14;
    uint64_t var_15;
    unsigned char *var_16;
    uint64_t var_47;
    unsigned char *var_48;
    unsigned char var_49;
    uint64_t var_50;
    uint64_t var_17;
    uint64_t var_21;
    uint64_t var_22;
    uint32_t *var_23;
    uint32_t var_24;
    uint64_t var_25;
    unsigned char var_18;
    uint64_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-64L));
    *var_5 = rdx;
    var_6 = *var_4;
    var_7 = *(uint64_t *)(var_6 + 104UL);
    var_8 = *(uint64_t *)(var_6 + 72UL);
    storemerge7 = (unsigned char)'@';
    var_26 = (unsigned char)'@';
    rax_0 = 1UL;
    var_52 = (unsigned char)'@';
    storemerge = (unsigned char)'@';
    if ((long)var_7 <= (long)var_8) {
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x02';
        rax_0 = 0UL;
        return rax_0;
    }
    var_9 = *(unsigned char *)(var_8 + *(uint64_t *)(var_6 + 8UL));
    var_10 = (unsigned char *)(var_0 + (-9L));
    *var_10 = var_9;
    var_11 = (unsigned char **)var_2;
    **var_11 = var_9;
    var_12 = (unsigned char *)(*var_3 + 10UL);
    *var_12 = (*var_12 & '\xbf');
    var_13 = (unsigned char *)(*var_3 + 10UL);
    *var_13 = (*var_13 & '\xdf');
    var_14 = *var_4;
    if ((int)*(uint32_t *)(var_14 + 144UL) <= (int)1U) {
        var_15 = *(uint64_t *)(var_14 + 72UL);
        if (var_15 != *(uint64_t *)(var_14 + 48UL) & *(uint32_t *)((var_15 << 2UL) + *(uint64_t *)(var_14 + 16UL)) != 4294967295U) {
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
            var_16 = (unsigned char *)(*var_3 + 10UL);
            *var_16 = (*var_16 | ' ');
            return rax_0;
        }
    }
    rax_0 = 2UL;
    if (*var_10 == '\\') {
        if ((long)(*(uint64_t *)(var_14 + 72UL) + 1UL) < (long)*(uint64_t *)(var_14 + 88UL)) {
            *(uint64_t *)(var_0 + (-80L)) = 4265780UL;
            var_47 = indirect_placeholder(1UL, var_14);
            var_48 = (unsigned char *)(var_0 + (-18L));
            var_49 = (unsigned char)var_47;
            *var_48 = var_49;
            **var_11 = var_49;
            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
            var_50 = *var_4;
            if ((int)*(uint32_t *)(var_50 + 144UL) > (int)1U) {
                var_54 = *(uint64_t *)(var_50 + 72UL) + 1UL;
                *(uint64_t *)(var_0 + (-88L)) = 4265843UL;
                var_55 = indirect_placeholder(var_54, var_50);
                var_56 = (uint32_t *)(var_0 + (-24L));
                var_57 = (uint32_t)var_55;
                *var_56 = var_57;
                *(uint64_t *)(var_0 + (-96L)) = 4265856UL;
                indirect_placeholder_2();
                if (var_57 == 0U) {
                } else {
                    storemerge = (unsigned char)'\x00';
                    if (*var_56 == 95U) {
                    }
                }
                var_58 = (unsigned char *)(*var_3 + 10UL);
                *var_58 = (storemerge | (*var_58 & '\xbf'));
            } else {
                var_51 = *var_48;
                *(uint64_t *)(var_0 + (-88L)) = 4265920UL;
                indirect_placeholder_2();
                if (var_51 == '\x00') {
                    var_52 = (*var_48 == '_') ? '@' : '\x00';
                }
                var_53 = (unsigned char *)(*var_3 + 10UL);
                *var_53 = (var_52 | (*var_53 & '\xbf'));
            }
            var_59 = (uint64_t)((uint32_t)(uint64_t)*var_48 + (-39));
            if (var_59 > 86UL) {
                var_60 = *(uint64_t *)((var_59 << 3UL) + 4380024UL);
                function_dispatcher((unsigned char *)(0UL));
                return var_60;
            }
        }
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'$';
    } else {
        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x01';
        var_17 = *var_4;
        if ((int)*(uint32_t *)(var_17 + 144UL) > (int)1U) {
            var_21 = *(uint64_t *)(var_17 + 72UL);
            *(uint64_t *)(var_0 + (-80L)) = 4266806UL;
            var_22 = indirect_placeholder(var_21, var_17);
            var_23 = (uint32_t *)(var_0 + (-16L));
            var_24 = (uint32_t)var_22;
            *var_23 = var_24;
            var_25 = var_0 + (-88L);
            *(uint64_t *)var_25 = 4266819UL;
            indirect_placeholder_2();
            local_sp_0 = var_25;
            if (var_24 == 0U) {
                var_26 = (*var_23 == 95U) ? '@' : '\x00';
            }
            var_27 = (unsigned char *)(*var_3 + 10UL);
            *var_27 = (var_26 | (*var_27 & '\xbf'));
        } else {
            var_18 = **var_11;
            var_19 = var_0 + (-80L);
            *(uint64_t *)var_19 = 4266889UL;
            indirect_placeholder_2();
            local_sp_0 = var_19;
            if (var_18 == '\x00') {
            } else {
                storemerge7 = (unsigned char)'\x00';
                if (**var_11 == '_') {
                }
            }
            var_20 = (unsigned char *)(*var_3 + 10UL);
            *var_20 = (storemerge7 | (*var_20 & '\xbf'));
        }
        var_28 = *var_10;
        var_29 = (uint64_t)var_28;
        if (var_28 > '?') {
            var_40 = (uint64_t)((uint32_t)var_29 + (-91));
            if (!((var_28 > '}') || (var_28 < '[')) & var_40 <= 34UL) {
                switch (*(uint64_t *)((var_40 << 3UL) + 4380720UL)) {
                  case 4267376UL:
                    {
                        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x14';
                    }
                    break;
                  case 4267645UL:
                    {
                        break;
                    }
                    break;
                  case 4267060UL:
                    {
                        var_46 = (uint16_t)*var_5;
                        if (((uint64_t)(var_46 & (unsigned short)1024U) != 0UL) || ((short)var_46 > (short)(unsigned short)65535U)) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\n';
                        }
                    }
                    break;
                  case 4267216UL:
                    {
                        var_45 = (uint16_t)*var_5;
                        if (((uint64_t)(var_45 & (unsigned short)512U) == 0UL) || ((uint64_t)(var_45 & (unsigned short)4096U) == 0UL)) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x17';
                        }
                    }
                    break;
                  case 4267265UL:
                    {
                        var_44 = (uint16_t)*var_5;
                        if (((uint64_t)(var_44 & (unsigned short)512U) == 0UL) || ((uint64_t)(var_44 & (unsigned short)4096U) == 0UL)) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x18';
                        }
                    }
                    break;
                  case 4267402UL:
                    {
                        if ((uint64_t)((uint32_t)*var_5 & 8388616U) != 0UL) {
                            var_41 = *var_4;
                            var_42 = *(uint64_t *)(var_41 + 72UL);
                            var_43 = *(unsigned char *)((var_42 + (-1L)) + *(uint64_t *)(var_41 + 8UL));
                            *(unsigned char *)(var_0 + (-17L)) = var_43;
                            if (var_42 != 0UL & ((uint64_t)((uint16_t)*var_5 & (unsigned short)2048U) != 0UL) && (var_43 == '\n')) {
                                return rax_0;
                            }
                        }
                        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\f';
                        **(uint32_t **)var_2 = 16U;
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
            }
        } else {
            var_30 = (uint64_t)((uint32_t)var_29 + (-10));
            if (var_28 >= '\n' & var_30 <= 53UL) {
                switch (*(uint64_t *)((var_30 << 3UL) + 4381000UL)) {
                  case 4267109UL:
                    {
                        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\v';
                    }
                    break;
                  case 4267645UL:
                    {
                        break;
                    }
                    break;
                  case 4267389UL:
                    {
                        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x05';
                    }
                    break;
                  case 4267169UL:
                    {
                        var_38 = *var_5;
                        if (((uint64_t)((uint16_t)var_38 & (unsigned short)1024U) == 0UL) && ((var_38 & 2UL) == 0UL)) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x13';
                        }
                    }
                    break;
                  case 4267345UL:
                    {
                        if ((uint64_t)((uint16_t)*var_5 & (unsigned short)8192U) == 0UL) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\t';
                        }
                    }
                    break;
                  case 4267314UL:
                    {
                        if ((uint64_t)((uint16_t)*var_5 & (unsigned short)8192U) == 0UL) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\b';
                        }
                    }
                    break;
                  case 4267029UL:
                    {
                        if ((uint64_t)((uint16_t)*var_5 & (unsigned short)2048U) == 0UL) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\n';
                        }
                    }
                    break;
                  case 4267122UL:
                    {
                        var_39 = *var_5;
                        if (((uint64_t)((uint16_t)var_39 & (unsigned short)1024U) == 0UL) && ((var_39 & 2UL) == 0UL)) {
                            *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\x12';
                        }
                    }
                    break;
                  case 4267509UL:
                    {
                        if ((*var_5 & 8UL) != 0UL) {
                            var_31 = *var_4;
                            var_32 = (uint64_t *)(var_31 + 72UL);
                            var_33 = *var_32 + 1UL;
                            *var_32 = var_33;
                            var_34 = *var_5;
                            var_35 = *var_4;
                            var_36 = var_0 + (-40L);
                            *(uint64_t *)(local_sp_0 + (-8L)) = 4267589UL;
                            indirect_placeholder_9(var_34, var_35, var_36);
                            var_37 = (uint64_t *)(*var_4 + 72UL);
                            *var_37 = (*var_37 + (-1L));
                            if (var_33 != *(uint64_t *)(var_31 + 88UL) & (*(unsigned char *)(var_0 + (-32L)) + '\xf7') < '\x02') {
                                return rax_0;
                            }
                        }
                        *(unsigned char *)(*var_3 + 8UL) = (unsigned char)'\f';
                        **(uint32_t **)var_2 = 32U;
                    }
                    break;
                  default:
                    {
                        abort();
                    }
                    break;
                }
            }
        }
    }
}
