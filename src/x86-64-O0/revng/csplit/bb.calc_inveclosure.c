typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
typedef _Bool bool;
uint64_t bb_calc_inveclosure(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_20;
    uint64_t var_11;
    uint64_t var_5;
    uint64_t local_sp_0;
    uint64_t *var_6;
    uint64_t *var_7;
    unsigned char *var_8;
    uint64_t var_9;
    uint64_t local_sp_1;
    uint64_t var_10;
    uint64_t storemerge;
    uint64_t local_sp_2;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    unsigned char var_19;
    uint64_t var_21;
    uint64_t var_22;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-56L);
    var_3 = (uint64_t *)(var_0 + (-48L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-24L));
    *var_4 = 0UL;
    var_11 = 0UL;
    var_5 = 0UL;
    local_sp_0 = var_2;
    var_9 = 0UL;
    storemerge = 0UL;
    local_sp_1 = local_sp_0;
    while (*(uint64_t *)(*var_3 + 16UL) <= var_5)
        {
            var_21 = local_sp_0 + (-8L);
            *(uint64_t *)var_21 = 4263968UL;
            indirect_placeholder_2();
            var_22 = *var_4 + 1UL;
            *var_4 = var_22;
            var_5 = var_22;
            local_sp_0 = var_21;
            local_sp_1 = local_sp_0;
        }
    var_6 = (uint64_t *)(var_0 + (-16L));
    *var_6 = 0UL;
    var_7 = (uint64_t *)(var_0 + (-32L));
    var_8 = (unsigned char *)(var_0 + (-33L));
    var_10 = *var_3;
    local_sp_2 = local_sp_1;
    while (*(uint64_t *)(var_10 + 16UL) <= var_9)
        {
            *var_7 = *(uint64_t *)(((var_9 * 24UL) + *(uint64_t *)(var_10 + 48UL)) + 16UL);
            *var_4 = 0UL;
            storemerge = 12UL;
            while (1U)
                {
                    var_12 = *var_3;
                    var_13 = *(uint64_t *)(var_12 + 48UL);
                    var_14 = *var_6;
                    local_sp_1 = local_sp_2;
                    if ((long)var_11 >= (long)*(uint64_t *)(((var_14 * 24UL) + var_13) + 8UL)) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_16 = *(uint64_t *)(var_12 + 56UL) + (*(uint64_t *)(*var_7 + (var_11 << 3UL)) * 24UL);
                    var_17 = local_sp_2 + (-8L);
                    *(uint64_t *)var_17 = 4264113UL;
                    var_18 = indirect_placeholder(var_14, var_16);
                    var_19 = (unsigned char)var_18;
                    *var_8 = var_19;
                    local_sp_2 = var_17;
                    if (var_19 != '\x01') {
                        loop_state_var = 0U;
                        break;
                    }
                    var_20 = *var_4 + 1UL;
                    *var_4 = var_20;
                    var_11 = var_20;
                    continue;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    var_15 = var_14 + 1UL;
                    *var_6 = var_15;
                    var_9 = var_15;
                    var_10 = *var_3;
                    local_sp_2 = local_sp_1;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    return storemerge;
}
