typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_127_ret_type;
struct indirect_placeholder_132_ret_type;
struct indirect_placeholder_131_ret_type;
struct indirect_placeholder_130_ret_type;
struct indirect_placeholder_128_ret_type;
struct indirect_placeholder_129_ret_type;
struct indirect_placeholder_127_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_132_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_131_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_130_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_128_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_129_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern struct indirect_placeholder_127_ret_type indirect_placeholder_127(uint64_t param_0);
extern struct indirect_placeholder_132_ret_type indirect_placeholder_132(uint64_t param_0);
extern struct indirect_placeholder_131_ret_type indirect_placeholder_131(uint64_t param_0);
extern struct indirect_placeholder_130_ret_type indirect_placeholder_130(uint64_t param_0);
extern struct indirect_placeholder_128_ret_type indirect_placeholder_128(uint64_t param_0);
extern struct indirect_placeholder_129_ret_type indirect_placeholder_129(uint64_t param_0);
uint64_t bb_analyze(uint64_t rdi) {
    uint64_t var_38;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t var_6;
    struct indirect_placeholder_127_ret_type var_7;
    uint64_t var_8;
    struct indirect_placeholder_132_ret_type var_9;
    uint64_t var_10;
    struct indirect_placeholder_131_ret_type var_11;
    uint64_t var_12;
    struct indirect_placeholder_130_ret_type var_13;
    uint64_t var_14;
    uint32_t rax_0_shrunk;
    uint64_t var_54;
    uint32_t var_55;
    uint32_t var_56;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_50;
    uint64_t var_49;
    uint64_t var_51;
    struct indirect_placeholder_128_ret_type var_52;
    uint64_t var_53;
    uint64_t var_44;
    uint64_t var_45;
    uint32_t var_46;
    uint64_t var_19;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_43;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_27;
    uint64_t var_26;
    uint64_t *var_18;
    bool var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t *var_32;
    uint32_t var_33;
    uint64_t var_15;
    uint64_t var_16;
    struct indirect_placeholder_129_ret_type var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-48L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = **(uint64_t **)var_2;
    var_5 = (uint64_t *)(var_0 + (-32L));
    *var_5 = var_4;
    var_6 = *(uint64_t *)(var_4 + 8UL) << 3UL;
    *(uint64_t *)(var_0 + (-64L)) = 4258713UL;
    var_7 = indirect_placeholder_127(var_6);
    *(uint64_t *)(*var_5 + 24UL) = var_7.field_0;
    var_8 = *(uint64_t *)(*var_5 + 8UL) << 3UL;
    *(uint64_t *)(var_0 + (-72L)) = 4258741UL;
    var_9 = indirect_placeholder_132(var_8);
    *(uint64_t *)(*var_5 + 32UL) = var_9.field_0;
    var_10 = *(uint64_t *)(*var_5 + 8UL) * 24UL;
    *(uint64_t *)(var_0 + (-80L)) = 4258778UL;
    var_11 = indirect_placeholder_131(var_10);
    *(uint64_t *)(*var_5 + 40UL) = var_11.field_0;
    var_12 = *(uint64_t *)(*var_5 + 8UL) * 24UL;
    *(uint64_t *)(var_0 + (-88L)) = 4258815UL;
    var_13 = indirect_placeholder_130(var_12);
    *(uint64_t *)(*var_5 + 48UL) = var_13.field_0;
    var_14 = *var_5;
    var_56 = 0U;
    var_24 = 0UL;
    var_19 = 0UL;
    rax_0_shrunk = 12U;
    if (*(uint64_t *)(var_14 + 24UL) == 0UL) {
        return (uint64_t)rax_0_shrunk;
    }
    if (~(*(uint64_t *)(var_14 + 32UL) != 0UL & *(uint64_t *)(var_14 + 40UL) != 0UL & *(uint64_t *)(var_14 + 48UL) != 0UL)) {
        return;
    }
    var_15 = *(uint64_t *)(*var_3 + 48UL) << 3UL;
    var_16 = var_0 + (-96L);
    *(uint64_t *)var_16 = 4258973UL;
    var_17 = indirect_placeholder_129(var_15);
    *(uint64_t *)(*var_5 + 224UL) = var_17.field_0;
    local_sp_0 = var_16;
    if (*(uint64_t *)(*var_5 + 224UL) != 0UL) {
        var_18 = (uint64_t *)(var_0 + (-24L));
        *var_18 = 0UL;
        var_20 = (*(uint64_t *)(*var_3 + 48UL) > var_19);
        var_21 = *var_5;
        while (!var_20)
            {
                *(uint64_t *)((var_19 << 3UL) + *(uint64_t *)(var_21 + 224UL)) = var_19;
                var_28 = *var_18 + 1UL;
                *var_18 = var_28;
                var_19 = var_28;
                var_20 = (*(uint64_t *)(*var_3 + 48UL) > var_19);
                var_21 = *var_5;
            }
        var_22 = *(uint64_t *)(var_21 + 104UL);
        var_23 = var_0 + (-104L);
        *(uint64_t *)var_23 = 4259090UL;
        indirect_placeholder_9(var_21, 4259980UL, var_22);
        *var_18 = 0UL;
        local_sp_0 = var_23;
        var_25 = *(uint64_t *)(*var_3 + 48UL);
        while (var_25 <= var_24)
            {
                if (var_24 == *(uint64_t *)(*(uint64_t *)(*var_5 + 224UL) + (var_24 << 3UL))) {
                    break;
                }
                var_27 = var_24 + 1UL;
                *var_18 = var_27;
                var_24 = var_27;
                var_25 = *(uint64_t *)(*var_3 + 48UL);
            }
        if (var_25 == var_24) {
            var_26 = var_0 + (-112L);
            *(uint64_t *)var_26 = 4259192UL;
            indirect_placeholder_2();
            *(uint64_t *)(*var_5 + 224UL) = 0UL;
            local_sp_0 = var_26;
        }
    }
    var_29 = *(uint64_t *)(*var_5 + 104UL);
    var_30 = *var_3;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4259232UL;
    var_31 = indirect_placeholder_9(var_30, 4260356UL, var_29);
    var_32 = (uint32_t *)(var_0 + (-12L));
    var_33 = (uint32_t)var_31;
    *var_32 = var_33;
    rax_0_shrunk = var_33;
    var_34 = *var_5;
    var_35 = *(uint64_t *)(var_34 + 104UL);
    *(uint64_t *)(local_sp_0 + (-16L)) = 4259283UL;
    var_36 = indirect_placeholder_9(var_34, 4261073UL, var_35);
    var_37 = (uint32_t)var_36;
    *var_32 = var_37;
    rax_0_shrunk = var_37;
    var_38 = *var_5;
    var_39 = *(uint64_t *)(var_38 + 104UL);
    *(uint64_t *)(local_sp_0 + (-24L)) = 4259334UL;
    indirect_placeholder_9(var_38, 4261311UL, var_39);
    var_40 = *var_5;
    var_41 = *(uint64_t *)(var_40 + 104UL);
    *(uint64_t *)(local_sp_0 + (-32L)) = 4259359UL;
    var_42 = indirect_placeholder_9(var_40, 4261484UL, var_41);
    var_43 = (uint32_t)var_42;
    *var_32 = var_43;
    rax_0_shrunk = var_43;
    var_44 = *var_5;
    *(uint64_t *)(local_sp_0 + (-40L)) = 4259397UL;
    var_45 = indirect_placeholder_8(var_44);
    var_46 = (uint32_t)var_45;
    *var_32 = var_46;
    rax_0_shrunk = var_46;
    if (var_33 != 0U & var_37 != 0U & var_43 != 0U & var_46 != 0U) {
        var_47 = *var_3;
        rax_0_shrunk = 12U;
        if ((*(unsigned char *)(var_47 + 56UL) & '\x10') == '\x00') {
            if (*(uint64_t *)(var_47 + 48UL) != 0UL) {
                var_49 = *var_5;
                var_50 = var_49;
                if (*(uint64_t *)(var_49 + 152UL) != 0UL) {
                    rax_0_shrunk = var_56;
                    return (uint64_t)rax_0_shrunk;
                }
            }
            var_48 = *var_5;
            var_50 = var_48;
            var_49 = *var_5;
            var_50 = var_49;
            if ((*(unsigned char *)(var_48 + 176UL) & '\x01') != '\x00' & *(uint64_t *)(var_49 + 152UL) != 0UL) {
                rax_0_shrunk = var_56;
                return (uint64_t)rax_0_shrunk;
            }
        }
        var_49 = *var_5;
        var_50 = var_49;
        if (*(uint64_t *)(var_49 + 152UL) != 0UL) {
            rax_0_shrunk = var_56;
            return (uint64_t)rax_0_shrunk;
        }
        var_51 = *(uint64_t *)(var_50 + 16UL) * 24UL;
        *(uint64_t *)(local_sp_0 + (-48L)) = 4259514UL;
        var_52 = indirect_placeholder_128(var_51);
        *(uint64_t *)(*var_5 + 56UL) = var_52.field_0;
        var_53 = *var_5;
        if (*(uint64_t *)(var_53 + 56UL) != 0UL) {
            *(uint64_t *)(local_sp_0 + (-56L)) = 4259563UL;
            var_54 = indirect_placeholder_8(var_53);
            var_55 = (uint32_t)var_54;
            *var_32 = var_55;
            var_56 = var_55;
            rax_0_shrunk = var_56;
        }
    }
}
