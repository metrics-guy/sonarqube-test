typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
uint64_t bb_re_string_context_at(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint32_t *var_18;
    uint64_t var_21;
    unsigned char var_9;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint32_t var_5;
    uint64_t var_6;
    bool var_7;
    uint64_t var_8;
    uint32_t rax_0_shrunk;
    uint32_t var_19;
    uint32_t var_20;
    uint64_t *var_15;
    uint64_t var_16;
    uint32_t var_17;
    uint32_t *var_10;
    uint32_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-32L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-40L));
    *var_3 = rsi;
    var_4 = (uint32_t *)(var_0 + (-44L));
    var_5 = (uint32_t)rdx;
    *var_4 = var_5;
    var_6 = *var_3;
    var_7 = ((long)var_6 > (long)18446744073709551615UL);
    var_8 = *var_2;
    var_16 = var_6;
    rax_0_shrunk = 1U;
    if (!var_7) {
        rax_0_shrunk = *(uint32_t *)(var_8 + 112UL);
        return (uint64_t)rax_0_shrunk;
    }
    if (var_6 == *(uint64_t *)(var_8 + 88UL)) {
        rax_0_shrunk = ((var_5 & 2U) == 0U) ? 10U : 8U;
    } else {
        if ((int)*(uint32_t *)(var_8 + 144UL) > (int)1U) {
            var_15 = (uint64_t *)(var_0 + (-16L));
            *var_15 = var_6;
            rax_0_shrunk = 0U;
            while (1U)
                {
                    var_17 = *(uint32_t *)(*(uint64_t *)(*var_2 + 16UL) + (var_16 << 2UL));
                    var_20 = var_17;
                    if (var_17 == 4294967295U) {
                        var_21 = var_16 + (-1L);
                        *var_15 = var_21;
                        var_16 = var_21;
                        if ((long)var_21 > (long)18446744073709551615UL) {
                            continue;
                        }
                        rax_0_shrunk = *(uint32_t *)(*var_2 + 112UL);
                        loop_state_var = 1U;
                        break;
                    }
                    var_18 = (uint32_t *)(var_0 + (-24L));
                    *var_18 = var_17;
                    if (*(unsigned char *)(*var_2 + 142UL) != '\x00') {
                        loop_state_var = 0U;
                        break;
                    }
                    *(uint64_t *)(var_0 + (-64L)) = 4244008UL;
                    indirect_placeholder_2();
                    if (var_17 != 0U) {
                        loop_state_var = 1U;
                        break;
                    }
                    var_19 = *var_18;
                    var_20 = var_19;
                    if (var_19 != 95U) {
                        loop_state_var = 1U;
                        break;
                    }
                    loop_state_var = 0U;
                    break;
                }
            if (var_20 == 10U) {
            } else {
                rax_0_shrunk = 2U;
                if (*(unsigned char *)(*var_2 + 141UL) == '\x00') {
                }
            }
        } else {
            var_9 = *(unsigned char *)(var_6 + *(uint64_t *)(var_8 + 8UL));
            var_10 = (uint32_t *)(var_0 + (-20L));
            var_11 = (uint32_t)var_9;
            *var_10 = var_11;
            var_12 = (uint64_t)var_11;
            var_13 = *(uint64_t *)(*var_2 + 128UL);
            *(uint64_t *)(var_0 + (-64L)) = 4244115UL;
            var_14 = indirect_placeholder(var_12, var_13);
            if ((uint64_t)(unsigned char)var_14 != 0UL) {
                rax_0_shrunk = 2U;
                if (*var_10 == 10U) {
                    rax_0_shrunk = 0U;
                } else {
                    if (*(unsigned char *)(*var_2 + 141UL) == '\x00') {
                        rax_0_shrunk = 0U;
                    }
                }
            }
        }
    }
}
