typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_sift_states_bkref(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint32_t var_74;
    uint64_t local_sp_1;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    unsigned char var_70;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t *var_19;
    uint32_t *var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint64_t *var_26;
    uint64_t *var_27;
    uint64_t *var_28;
    uint64_t *var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t *var_32;
    uint64_t *var_33;
    uint32_t *var_34;
    unsigned char *var_35;
    uint64_t *var_36;
    uint64_t local_sp_6;
    uint64_t var_37;
    uint64_t local_sp_2;
    uint64_t local_sp_3;
    uint32_t var_83;
    uint64_t var_39;
    uint32_t var_40;
    uint64_t var_41;
    uint64_t local_sp_4;
    uint64_t var_42;
    uint64_t var_43;
    bool var_44;
    uint64_t var_45;
    uint64_t storemerge_in_in;
    uint64_t storemerge;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_58;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t storemerge13;
    uint64_t var_75;
    uint64_t local_sp_0;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint32_t var_81;
    uint32_t var_82;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_38;
    uint64_t local_sp_5;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-192L));
    *var_3 = rdi;
    var_4 = var_0 + (-200L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-208L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-216L));
    *var_7 = rcx;
    var_8 = *(uint64_t *)(*var_3 + 152UL);
    var_9 = var_0 + (-64L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    var_11 = *var_6;
    var_12 = *var_3;
    var_13 = var_0 + (-224L);
    *(uint64_t *)var_13 = 4298471UL;
    var_14 = indirect_placeholder(var_11, var_12);
    var_15 = (uint64_t *)(var_0 + (-72L));
    *var_15 = var_14;
    var_37 = 0UL;
    local_sp_2 = var_13;
    var_82 = 12U;
    storemerge13 = 0UL;
    if (var_14 == 18446744073709551615UL) {
        return storemerge13;
    }
    var_16 = var_0 + (-184L);
    var_17 = (uint64_t *)var_16;
    *var_17 = 0UL;
    var_18 = (uint64_t *)(var_0 + (-40L));
    *var_18 = 0UL;
    var_19 = (uint64_t *)(var_0 + (-80L));
    var_20 = (uint32_t *)(var_0 + (-84L));
    var_21 = var_0 + (-56L);
    var_22 = (uint64_t *)var_21;
    var_23 = (uint64_t *)(var_0 + (-48L));
    var_24 = (uint64_t *)(var_0 + (-96L));
    var_25 = (uint64_t *)(var_0 + (-104L));
    var_26 = (uint64_t *)(var_0 + (-112L));
    var_27 = (uint64_t *)(var_0 + (-176L));
    var_28 = (uint64_t *)(var_0 + (-168L));
    var_29 = (uint64_t *)(var_0 + (-160L));
    var_30 = var_0 + (-152L);
    var_31 = (uint64_t *)var_30;
    var_32 = (uint64_t *)(var_0 + (-144L));
    var_33 = (uint64_t *)(var_0 + (-136L));
    var_34 = (uint32_t *)(var_0 + (-28L));
    var_35 = (unsigned char *)(var_0 + (-113L));
    var_36 = (uint64_t *)(var_0 + (-128L));
    while (1U)
        {
            var_38 = *var_7;
            local_sp_3 = local_sp_2;
            local_sp_4 = local_sp_2;
            local_sp_6 = local_sp_2;
            if ((long)var_37 >= (long)*(uint64_t *)(var_38 + 8UL)) {
                *var_34 = 0U;
                var_82 = 0U;
                break;
            }
            var_39 = *(uint64_t *)(*(uint64_t *)(var_38 + 16UL) + (var_37 << 3UL));
            *var_19 = var_39;
            var_40 = (uint32_t)*(unsigned char *)((**(uint64_t **)var_9 + (var_39 << 4UL)) + 8UL);
            *var_20 = var_40;
            var_41 = *var_5;
            if (*var_19 != *(uint64_t *)(var_41 + 16UL)) {
                if (*var_6 != *(uint64_t *)(var_41 + 24UL)) {
                    var_89 = *var_18 + 1UL;
                    *var_18 = var_89;
                    var_37 = var_89;
                    local_sp_2 = local_sp_6;
                    continue;
                }
            }
            if (var_40 == 4U) {
                var_89 = *var_18 + 1UL;
                *var_18 = var_89;
                var_37 = var_89;
                local_sp_2 = local_sp_6;
                continue;
            }
            *var_22 = ((*var_15 * 40UL) + *(uint64_t *)(*var_3 + 216UL));
            *var_23 = *var_15;
            while (1U)
                {
                    local_sp_5 = local_sp_4;
                    if (*var_19 == **(uint64_t **)var_21) {
                        *var_23 = (*var_23 + 1UL);
                        var_88 = *var_22;
                        *var_22 = (var_88 + 40UL);
                        local_sp_4 = local_sp_5;
                        local_sp_6 = local_sp_5;
                        if (*(unsigned char *)(var_88 + 32UL) == '\x00') {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                    var_42 = *var_22;
                    var_43 = *(uint64_t *)(var_42 + 24UL) - *(uint64_t *)(var_42 + 16UL);
                    *var_24 = var_43;
                    *var_25 = (var_43 + *var_6);
                    var_44 = (*var_24 == 0UL);
                    var_45 = *var_10;
                    if (var_44) {
                        storemerge_in_in = *(uint64_t *)(((*var_19 * 24UL) + *(uint64_t *)(var_45 + 40UL)) + 16UL);
                    } else {
                        storemerge_in_in = *(uint64_t *)(var_45 + 24UL) + (*var_19 << 3UL);
                    }
                    storemerge = *(uint64_t *)storemerge_in_in;
                    *var_26 = storemerge;
                    var_46 = *(uint64_t *)(*var_5 + 24UL);
                    var_47 = *var_25;
                    var_48 = *(uint64_t *)(**(uint64_t **)var_4 + (var_47 << 3UL));
                    var_49 = var_48 + 8UL;
                    var_50 = local_sp_4 + (-8L);
                    *(uint64_t *)var_50 = 4298932UL;
                    var_51 = indirect_placeholder(storemerge, var_49);
                    local_sp_5 = var_50;
                    var_52 = *var_5 + 32UL;
                    var_53 = *var_25;
                    var_54 = *var_26;
                    var_55 = *var_6;
                    var_56 = *var_19;
                    var_57 = local_sp_4 + (-16L);
                    *(uint64_t *)var_57 = 4298992UL;
                    var_58 = indirect_placeholder_11(var_55, var_56, var_52, var_54, var_53);
                    local_sp_1 = var_57;
                    local_sp_5 = var_57;
                    if ((long)var_47 <= (long)var_46 & var_48 != 0UL & var_51 != 0UL & (uint64_t)(unsigned char)var_58 != 0UL) {
                        if (*var_17 != 0UL) {
                            var_59 = *var_5;
                            var_60 = *(uint64_t *)(var_59 + 8UL);
                            *var_17 = *(uint64_t *)var_59;
                            *var_27 = var_60;
                            var_61 = *(uint64_t *)(var_59 + 24UL);
                            *var_28 = *(uint64_t *)(var_59 + 16UL);
                            *var_29 = var_61;
                            var_62 = *(uint64_t *)(var_59 + 40UL);
                            *var_31 = *(uint64_t *)(var_59 + 32UL);
                            *var_32 = var_62;
                            *var_33 = *(uint64_t *)(var_59 + 48UL);
                            var_63 = *var_5 + 32UL;
                            var_64 = local_sp_4 + (-24L);
                            *(uint64_t *)var_64 = 4299129UL;
                            var_65 = indirect_placeholder(var_63, var_30);
                            var_66 = (uint32_t)var_65;
                            *var_34 = var_66;
                            local_sp_1 = var_64;
                            var_82 = var_66;
                            local_sp_3 = var_64;
                            if (var_66 != 0U) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        *var_28 = *var_19;
                        *var_29 = *var_6;
                        var_67 = *var_23;
                        var_68 = local_sp_1 + (-8L);
                        *(uint64_t *)var_68 = 4299202UL;
                        var_69 = indirect_placeholder(var_67, var_30);
                        var_70 = (unsigned char)var_69;
                        *var_35 = var_70;
                        local_sp_3 = var_68;
                        if (var_70 != '\x01') {
                            *var_34 = 12U;
                            loop_state_var = 0U;
                            break;
                        }
                        *var_36 = *(uint64_t *)(*var_17 + (*var_6 << 3UL));
                        var_71 = *var_3;
                        var_72 = local_sp_1 + (-16L);
                        *(uint64_t *)var_72 = 4299285UL;
                        var_73 = indirect_placeholder(var_16, var_71);
                        var_74 = (uint32_t)var_73;
                        *var_34 = var_74;
                        local_sp_0 = var_72;
                        var_82 = var_74;
                        local_sp_3 = var_72;
                        if (var_74 != 0U) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_75 = *(uint64_t *)(*var_5 + 8UL);
                        if (var_75 != 0UL) {
                            var_76 = *var_6 + 1UL;
                            var_77 = *var_17;
                            var_78 = *var_10;
                            var_79 = local_sp_1 + (-24L);
                            *(uint64_t *)var_79 = 4299364UL;
                            var_80 = indirect_placeholder_10(var_76, var_77, var_75, var_78);
                            var_81 = (uint32_t)var_80;
                            *var_34 = var_81;
                            local_sp_0 = var_79;
                            var_82 = var_81;
                            local_sp_3 = var_79;
                            if (var_81 != 0U) {
                                loop_state_var = 0U;
                                break;
                            }
                        }
                        *(uint64_t *)((*var_6 << 3UL) + *var_17) = *var_36;
                        var_84 = *var_23;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4299440UL;
                        var_85 = indirect_placeholder(var_84, var_30);
                        var_86 = var_85 + (-1L);
                        var_87 = local_sp_0 + (-16L);
                        *(uint64_t *)var_87 = 4299466UL;
                        indirect_placeholder_16(var_86, var_30);
                        *var_22 = ((*var_23 * 40UL) + *(uint64_t *)(*var_3 + 216UL));
                        local_sp_5 = var_87;
                    }
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    var_83 = var_82;
    if (*var_17 != 0UL) {
        *(uint64_t *)(local_sp_3 + (-8L)) = 4299612UL;
        indirect_placeholder_2();
        var_83 = *var_34;
    }
    storemerge13 = (uint64_t)var_83;
    return storemerge13;
}
