typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_152_ret_type;
struct indirect_placeholder_153_ret_type;
struct indirect_placeholder_152_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_153_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_152_ret_type indirect_placeholder_152(uint64_t param_0);
extern struct indirect_placeholder_153_ret_type indirect_placeholder_153(uint64_t param_0);
uint64_t bb_re_copy_regs(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_24;
    struct indirect_placeholder_152_ret_type var_25;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t *var_11;
    uint64_t local_sp_0;
    uint64_t rax_0;
    uint64_t var_32;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t *var_21;
    uint64_t var_22;
    struct indirect_placeholder_153_ret_type var_23;
    uint64_t **var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-88L);
    var_4 = var_0 + (-64L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint64_t *)(var_0 + (-72L));
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-80L));
    *var_7 = rdx;
    var_8 = (uint32_t *)(var_0 + (-84L));
    *var_8 = (uint32_t)rcx;
    var_9 = (uint32_t *)(var_0 + (-12L));
    *var_9 = 1U;
    var_10 = *var_7 + 1UL;
    var_11 = (uint64_t *)(var_0 + (-32L));
    *var_11 = var_10;
    local_sp_0 = var_3;
    var_31 = 0UL;
    rax_0 = 0UL;
    switch (*var_8) {
      case 0U:
        {
            var_22 = var_10 << 3UL;
            *(uint64_t *)(var_0 + (-96L)) = 4282978UL;
            var_23 = indirect_placeholder_153(var_22);
            *(uint64_t *)(*var_5 + 8UL) = var_23.field_0;
            if (*(uint64_t *)(*var_5 + 8UL) == 0UL) {
                return rax_0;
            }
            var_24 = *var_11 << 3UL;
            *(uint64_t *)(var_0 + (-104L)) = 4283034UL;
            var_25 = indirect_placeholder_152(var_24);
            *(uint64_t *)(*var_5 + 16UL) = var_25.field_0;
            if (*(uint64_t *)(*var_5 + 16UL) != 0UL) {
                *(uint64_t *)(var_0 + (-112L)) = 4283080UL;
                indirect_placeholder_2();
                return rax_0;
            }
            **(uint64_t **)var_4 = *var_11;
        }
        break;
      case 1U:
        {
            var_12 = (uint64_t **)var_4;
            var_13 = helper_cc_compute_c_wrapper(**var_12 - var_10, var_10, var_2, 17U);
            if ((uint64_t)(unsigned char)var_13 != 0UL) {
                var_14 = *var_11 << 3UL;
                var_15 = *(uint64_t *)(*var_5 + 8UL);
                *(uint64_t *)(var_0 + (-96L)) = 4283176UL;
                var_16 = indirect_placeholder(var_14, var_15);
                var_17 = (uint64_t *)(var_0 + (-40L));
                *var_17 = var_16;
                if (var_16 == 0UL) {
                    return rax_0;
                }
                var_18 = *var_11 << 3UL;
                var_19 = *(uint64_t *)(*var_5 + 16UL);
                *(uint64_t *)(var_0 + (-104L)) = 4283237UL;
                var_20 = indirect_placeholder(var_18, var_19);
                var_21 = (uint64_t *)(var_0 + (-48L));
                *var_21 = var_20;
                if (var_20 != 0UL) {
                    *(uint64_t *)(var_0 + (-112L)) = 4283269UL;
                    indirect_placeholder_2();
                    return rax_0;
                }
                *(uint64_t *)(*var_5 + 8UL) = *var_17;
                *(uint64_t *)(*var_5 + 16UL) = *var_21;
                **var_12 = *var_11;
            }
        }
        break;
      default:
        {
            var_27 = **(uint64_t **)var_4;
            var_28 = *var_7;
            var_29 = helper_cc_compute_c_wrapper(var_27 - var_28, var_28, var_2, 17U);
            if (var_29 != 0UL) {
                *(uint64_t *)(local_sp_0 + (-8L)) = 4283388UL;
                indirect_placeholder_2();
            }
            *var_9 = 2U;
        }
        break;
    }
    var_30 = (uint64_t *)(var_0 + (-24L));
    *var_30 = 0UL;
    var_32 = var_31;
    while ((long)var_31 >= (long)*var_7)
        {
            *(uint64_t *)((var_31 << 3UL) + *(uint64_t *)(*var_5 + 8UL)) = *(uint64_t *)(*var_6 + (var_31 << 4UL));
            var_35 = *var_30;
            *(uint64_t *)((var_35 << 3UL) + *(uint64_t *)(*var_5 + 16UL)) = *(uint64_t *)((*var_6 + (var_35 << 4UL)) + 8UL);
            var_36 = *var_30 + 1UL;
            *var_30 = var_36;
            var_31 = var_36;
            var_32 = var_31;
        }
    while (**(uint64_t **)var_4 <= var_32)
        {
            *(uint64_t *)(*(uint64_t *)(*var_5 + 16UL) + (var_32 << 3UL)) = 18446744073709551615UL;
            *(uint64_t *)(*(uint64_t *)(*var_5 + 8UL) + (*var_30 << 3UL)) = 18446744073709551615UL;
            var_34 = *var_30 + 1UL;
            *var_30 = var_34;
            var_32 = var_34;
        }
    var_33 = (uint64_t)*var_9;
    rax_0 = var_33;
    return rax_0;
}
