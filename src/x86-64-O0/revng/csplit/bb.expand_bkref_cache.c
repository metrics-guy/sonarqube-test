typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_204_ret_type;
struct indirect_placeholder_204_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_rbx(void);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_204_ret_type indirect_placeholder_204(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_expand_bkref_cache(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_23;
    uint64_t var_24;
    uint32_t *var_25;
    uint64_t var_26;
    uint32_t *var_27;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint32_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint32_t rax_1_shrunk;
    uint32_t var_57;
    uint64_t var_28;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint64_t var_54;
    struct indirect_placeholder_204_ret_type var_55;
    uint64_t var_56;
    uint32_t var_49;
    uint32_t storemerge10;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    unsigned char var_48;
    uint32_t rax_0_in;
    uint64_t var_62;
    uint64_t local_sp_2;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint32_t var_71;
    bool var_72;
    uint64_t var_34;
    uint64_t var_35;
    bool var_36;
    uint64_t var_37;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_50;
    uint64_t var_51;
    uint32_t var_52;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t var_73;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t *var_21;
    unsigned char *var_22;
    uint64_t **var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-144L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-152L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-160L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-168L));
    *var_6 = rcx;
    var_7 = (uint32_t *)(var_0 + (-172L));
    *var_7 = (uint32_t)r8;
    var_8 = *(uint64_t *)(*var_3 + 152UL);
    var_9 = (uint64_t *)(var_0 + (-40L));
    *var_9 = var_8;
    var_10 = *var_5;
    var_11 = *var_3;
    var_12 = var_0 + (-192L);
    *(uint64_t *)var_12 = 4308874UL;
    var_13 = indirect_placeholder(var_10, var_11);
    var_14 = (uint64_t *)(var_0 + (-48L));
    *var_14 = var_13;
    rax_1_shrunk = 0U;
    var_28 = var_13;
    local_sp_2 = var_12;
    if (var_13 == 18446744073709551615UL) {
        return (uint64_t)rax_1_shrunk;
    }
    var_15 = var_0 + (-32L);
    var_16 = (uint64_t *)var_15;
    var_17 = (uint64_t *)(var_0 + (-56L));
    var_18 = (uint64_t *)(var_0 + (-64L));
    var_19 = var_0 + (-136L);
    var_20 = var_0 + (-80L);
    var_21 = (uint32_t *)var_20;
    var_22 = (unsigned char *)(var_0 + (-65L));
    var_23 = var_0 + (-104L);
    var_24 = var_0 + (-72L);
    var_25 = (uint32_t *)var_24;
    var_26 = var_0 + (-76L);
    var_27 = (uint32_t *)var_26;
    while (1U)
        {
            *var_16 = ((var_28 * 40UL) + *(uint64_t *)(*var_3 + 216UL));
            local_sp_3 = local_sp_2;
            while (1U)
                {
                    var_29 = (uint64_t **)var_15;
                    var_30 = **var_29;
                    var_31 = *var_4;
                    var_32 = local_sp_3 + (-8L);
                    *(uint64_t *)var_32 = 4308960UL;
                    var_33 = indirect_placeholder(var_30, var_31);
                    local_sp_1 = var_32;
                    if (var_33 == 0UL) {
                        var_73 = *var_16;
                        *var_16 = (var_73 + 40UL);
                        local_sp_3 = local_sp_1;
                        if (*(unsigned char *)(var_73 + 32UL) == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_34 = *var_16;
                    var_35 = (*(uint64_t *)(var_34 + 24UL) + *var_5) - *(uint64_t *)(var_34 + 16UL);
                    *var_17 = var_35;
                    var_36 = (var_35 == *var_5);
                    var_37 = *var_9;
                    if (var_36) {
                        var_58 = **(uint64_t **)(((**var_29 * 24UL) + *(uint64_t *)(var_37 + 40UL)) + 16UL);
                        *var_18 = var_58;
                        var_59 = *var_4;
                        var_60 = local_sp_3 + (-16L);
                        *(uint64_t *)var_60 = 4309089UL;
                        var_61 = indirect_placeholder(var_58, var_59);
                        local_sp_1 = var_60;
                        if (var_61 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                    var_38 = *(uint64_t *)((**var_29 << 3UL) + *(uint64_t *)(var_37 + 24UL));
                    *var_18 = var_38;
                    var_39 = *(uint64_t *)(*(uint64_t *)(*var_3 + 184UL) + (*var_17 << 3UL));
                    if (var_39 == 0UL) {
                        var_50 = local_sp_3 + (-16L);
                        *(uint64_t *)var_50 = 4309576UL;
                        var_51 = indirect_placeholder(var_38, var_19);
                        var_52 = (uint32_t)var_51;
                        *var_21 = var_52;
                        local_sp_0 = var_50;
                        rax_1_shrunk = var_52;
                        if (var_52 != 0U) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    var_40 = var_39 + 8UL;
                    var_41 = local_sp_3 + (-16L);
                    *(uint64_t *)var_41 = 4309405UL;
                    var_42 = indirect_placeholder(var_38, var_40);
                    local_sp_1 = var_41;
                    if (var_42 != 0UL) {
                        var_73 = *var_16;
                        *var_16 = (var_73 + 40UL);
                        local_sp_3 = local_sp_1;
                        if (*(unsigned char *)(var_73 + 32UL) == '\x00') {
                            continue;
                        }
                        loop_state_var = 0U;
                        break;
                    }
                    var_43 = *(uint64_t *)(*(uint64_t *)(*var_3 + 184UL) + (*var_17 << 3UL)) + 8UL;
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4309461UL;
                    var_44 = indirect_placeholder(var_43, var_19);
                    *var_21 = (uint32_t)var_44;
                    var_45 = *var_18;
                    var_46 = local_sp_3 + (-32L);
                    *(uint64_t *)var_46 = 4309483UL;
                    var_47 = indirect_placeholder(var_45, var_19);
                    var_48 = (unsigned char)var_47;
                    *var_22 = var_48;
                    local_sp_0 = var_46;
                    if ((*var_21 == 0U) && (var_48 == '\x01')) {
                        var_53 = (*var_17 << 3UL) + *(uint64_t *)(*var_3 + 184UL);
                        var_54 = *var_9;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4309652UL;
                        var_55 = indirect_placeholder_204(var_19, var_54, var_20, var_53);
                        *(uint64_t *)var_55.field_1 = var_55.field_0;
                        var_56 = local_sp_0 + (-16L);
                        *(uint64_t *)var_56 = 4309667UL;
                        indirect_placeholder_2();
                        local_sp_1 = var_56;
                        var_57 = *var_21;
                        rax_1_shrunk = var_57;
                        if (*(uint64_t *)(*(uint64_t *)(*var_3 + 184UL) + (*var_17 << 3UL)) != 0UL & var_57 == 0U) {
                            loop_state_var = 0U;
                            break;
                        }
                    }
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4309529UL;
                    indirect_placeholder_2();
                    var_49 = *var_21;
                    storemerge10 = (var_49 == 0U) ? 12U : var_49;
                    *var_21 = storemerge10;
                    rax_1_shrunk = storemerge10;
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 1U:
                {
                    var_62 = *var_18;
                    *(uint64_t *)(local_sp_3 + (-24L)) = 4309117UL;
                    var_63 = indirect_placeholder(var_62, var_23);
                    *var_21 = (uint32_t)var_63;
                    var_64 = (uint64_t)*var_7;
                    var_65 = *var_6;
                    var_66 = *var_9;
                    *(uint64_t *)(local_sp_3 + (-32L)) = 4309149UL;
                    var_67 = indirect_placeholder_10(var_64, var_65, var_23, var_66);
                    *var_25 = (uint32_t)var_67;
                    var_68 = *var_4;
                    *(uint64_t *)(local_sp_3 + (-40L)) = 4309174UL;
                    var_69 = indirect_placeholder(var_23, var_68);
                    *var_27 = (uint32_t)var_69;
                    var_70 = local_sp_3 + (-48L);
                    *(uint64_t *)var_70 = 4309189UL;
                    indirect_placeholder_2();
                    var_71 = *var_21;
                    var_72 = (var_71 == 0U);
                    rax_0_in = var_71;
                    local_sp_2 = var_70;
                    if (!var_72) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (*var_25 != 0U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    if (*var_27 != 0U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_28 = *var_14;
                    continue;
                }
                break;
              case 0U:
                {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    if (var_72) {
        rax_0_in = *(uint32_t *)((*var_25 == 0U) ? var_26 : var_24);
    }
    *var_21 = rax_0_in;
    rax_1_shrunk = rax_0_in;
}
