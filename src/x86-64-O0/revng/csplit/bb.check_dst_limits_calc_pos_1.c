typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_11(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
typedef _Bool bool;
uint64_t bb_check_dst_limits_calc_pos_1(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8) {
    uint64_t var_36;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t *var_13;
    uint64_t *var_14;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t *var_17;
    uint32_t *var_18;
    uint64_t rax_0;
    uint64_t var_38;
    uint64_t local_sp_2;
    uint64_t local_sp_4;
    uint16_t *var_39;
    uint64_t var_19;
    uint64_t local_sp_0;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    unsigned char var_23;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t local_sp_1;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint32_t var_37;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-120L);
    var_3 = (uint64_t *)(var_0 + (-80L));
    *var_3 = rdi;
    var_4 = (uint32_t *)(var_0 + (-84L));
    *var_4 = (uint32_t)rsi;
    var_5 = (uint64_t *)(var_0 + (-96L));
    *var_5 = rdx;
    var_6 = (uint64_t *)(var_0 + (-104L));
    *var_6 = rcx;
    var_7 = (uint64_t *)(var_0 + (-112L));
    *var_7 = r8;
    var_8 = *(uint64_t *)(*var_3 + 152UL);
    var_9 = var_0 + (-32L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    var_11 = (*var_6 * 24UL) + *(uint64_t *)(var_8 + 48UL);
    var_12 = (uint64_t *)(var_0 + (-40L));
    *var_12 = var_11;
    var_13 = (uint64_t *)(var_0 + (-16L));
    *var_13 = 0UL;
    var_14 = (uint64_t *)(var_0 + (-48L));
    var_15 = var_0 + (-24L);
    var_16 = (uint64_t *)var_15;
    var_17 = (uint64_t *)(var_0 + (-56L));
    var_18 = (uint32_t *)(var_0 + (-60L));
    var_19 = 0UL;
    local_sp_0 = var_2;
    rax_0 = 4294967295UL;
    while (1U)
        {
            var_20 = *var_12;
            local_sp_1 = local_sp_0;
            local_sp_4 = local_sp_0;
            if ((long)var_19 >= (long)*(uint64_t *)(var_20 + 8UL)) {
                rax_0 = (uint64_t)((*var_4 >> 1U) & 1U);
                break;
            }
            var_21 = *(uint64_t *)(*(uint64_t *)(var_20 + 16UL) + (var_19 << 3UL));
            *var_14 = var_21;
            var_22 = **(uint64_t **)var_9 + (var_21 << 4UL);
            var_23 = *(unsigned char *)(var_22 + 8UL);
            var_24 = (uint32_t)(uint64_t)var_23;
            if ((uint64_t)(var_24 + (-9)) == 0UL) {
                rax_0 = 0UL;
                if ((*var_4 & 2U) != 0U & *var_5 == *(uint64_t *)var_22) {
                    break;
                }
            }
            if (var_23 > '\t') {
                var_42 = *var_13 + 1UL;
                *var_13 = var_42;
                var_19 = var_42;
                local_sp_0 = local_sp_4;
                continue;
            }
            if ((uint64_t)(var_24 + (-4)) != 0UL) {
                if ((uint64_t)(var_24 + (-8)) != 0UL & (*var_4 & 1U) != 0U & *var_5 == *(uint64_t *)var_22) {
                    break;
                }
            }
            var_25 = *var_7;
            if (var_25 != 18446744073709551615UL) {
                var_26 = (var_25 * 40UL) + *(uint64_t *)(*var_3 + 216UL);
                *var_16 = var_26;
                var_27 = var_26;
                while (1U)
                    {
                        var_28 = **(uint64_t **)var_15;
                        local_sp_2 = local_sp_1;
                        if (*var_14 == var_28) {
                            var_40 = *var_16;
                            var_41 = var_40 + 40UL;
                            *var_16 = var_41;
                            var_27 = var_41;
                            local_sp_1 = local_sp_2;
                            local_sp_4 = local_sp_2;
                            if (*(unsigned char *)(var_40 + 32UL) == '\x00') {
                                continue;
                            }
                            loop_state_var = 1U;
                            break;
                        }
                        var_29 = *var_5;
                        if ((long)var_29 <= (long)63UL) {
                            if (((1UL << (var_29 & 63UL)) & (uint64_t)*(uint16_t *)(var_27 + 34UL)) != 0UL) {
                                var_40 = *var_16;
                                var_41 = var_40 + 40UL;
                                *var_16 = var_41;
                                var_27 = var_41;
                                local_sp_1 = local_sp_2;
                                local_sp_4 = local_sp_2;
                                if (*(unsigned char *)(var_40 + 32UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_30 = **(uint64_t **)(((var_28 * 24UL) + *(uint64_t *)(*var_10 + 40UL)) + 16UL);
                        *var_17 = var_30;
                        if (var_30 == *var_6) {
                            return ((*var_4 & 1U) == 0U) ? 0UL : 4294967295UL;
                        }
                        var_31 = *var_7;
                        var_32 = *var_5;
                        var_33 = (uint64_t)*var_4;
                        var_34 = *var_3;
                        var_35 = local_sp_1 + (-8L);
                        *(uint64_t *)var_35 = 4297042UL;
                        var_36 = indirect_placeholder_11(var_30, var_32, var_33, var_34, var_31);
                        var_37 = (uint32_t)var_36;
                        *var_18 = var_37;
                        local_sp_2 = var_35;
                        switch_state_var = 0;
                        switch (var_37) {
                          case 0U:
                            {
                                rax_0 = 0UL;
                                if ((*var_4 & 2U) != 0U) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_38 = *var_5;
                                if ((long)var_38 > (long)63UL) {
                                    var_39 = (uint16_t *)(*var_16 + 34UL);
                                    *var_39 = (*var_39 & ((uint16_t)(1UL << (var_38 & 63UL)) ^ (unsigned short)65535U));
                                }
                                var_40 = *var_16;
                                var_41 = var_40 + 40UL;
                                *var_16 = var_41;
                                var_27 = var_41;
                                local_sp_1 = local_sp_2;
                                local_sp_4 = local_sp_2;
                                if (*(unsigned char *)(var_40 + 32UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          case 4294967295U:
                            {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                          default:
                            {
                                var_38 = *var_5;
                                if ((long)var_38 > (long)63UL) {
                                    var_39 = (uint16_t *)(*var_16 + 34UL);
                                    *var_39 = (*var_39 & ((uint16_t)(1UL << (var_38 & 63UL)) ^ (unsigned short)65535U));
                                }
                                var_40 = *var_16;
                                var_41 = var_40 + 40UL;
                                *var_16 = var_41;
                                var_27 = var_41;
                                local_sp_1 = local_sp_2;
                                local_sp_4 = local_sp_2;
                                if (*(unsigned char *)(var_40 + 32UL) == '\x00') {
                                    continue;
                                }
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            break;
                        }
                        if (switch_state_var)
                            break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 0U:
                    {
                        switch_state_var = 1;
                        break;
                    }
                    break;
                  case 1U:
                    {
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        }
    return rax_0;
}
