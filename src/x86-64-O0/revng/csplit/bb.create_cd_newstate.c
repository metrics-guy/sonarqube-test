typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_124_ret_type;
struct indirect_placeholder_125_ret_type;
struct indirect_placeholder_124_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_125_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_124_ret_type indirect_placeholder_124(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_125_ret_type indirect_placeholder_125(uint64_t param_0);
typedef _Bool bool;
uint64_t bb_create_cd_newstate(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9, uint64_t r10, uint64_t rbx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    struct indirect_placeholder_124_ret_type var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t rax_0;
    uint64_t var_31;
    unsigned char *var_45;
    uint32_t var_46;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    unsigned char var_17;
    unsigned char *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint32_t *var_22;
    uint32_t *var_23;
    uint64_t local_sp_2;
    uint64_t var_24;
    uint64_t local_sp_0;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_32;
    uint32_t var_33;
    unsigned char *var_34;
    unsigned char var_35;
    unsigned char *var_37;
    unsigned char *var_36;
    uint32_t var_38;
    uint64_t var_39;
    struct indirect_placeholder_125_ret_type var_40;
    uint64_t var_41;
    uint64_t local_sp_1;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint32_t *var_15;
    uint32_t var_16;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-64L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-76L));
    *var_5 = (uint32_t)rdx;
    var_6 = (uint64_t *)(var_0 + (-88L));
    *var_6 = rcx;
    var_7 = (uint64_t *)(var_0 + (-24L));
    *var_7 = 0UL;
    *(uint64_t *)(var_0 + (-96L)) = 4251425UL;
    var_8 = indirect_placeholder_124(rcx, 1UL, 112UL, r8, r9, r10, rbx);
    var_9 = var_8.field_0;
    var_10 = (uint64_t *)(var_0 + (-32L));
    *var_10 = var_9;
    var_31 = 0UL;
    var_24 = 0UL;
    rax_0 = 0UL;
    if (var_9 == 0UL) {
        return rax_0;
    }
    var_11 = var_9 + 8UL;
    var_12 = *var_4;
    var_13 = var_0 + (-104L);
    *(uint64_t *)var_13 = 4251478UL;
    var_14 = indirect_placeholder(var_12, var_11);
    var_15 = (uint32_t *)(var_0 + (-36L));
    var_16 = (uint32_t)var_14;
    *var_15 = var_16;
    local_sp_0 = var_13;
    if (var_16 == 0U) {
        var_17 = (unsigned char)*var_5 & '\x0f';
        var_18 = (unsigned char *)(*var_10 + 104UL);
        *var_18 = ((*var_18 & '\xf0') | var_17);
        var_19 = *var_10;
        *(uint64_t *)(var_19 + 80UL) = (var_19 + 8UL);
        var_20 = (uint64_t *)(var_0 + (-16L));
        *var_20 = 0UL;
        var_21 = (uint64_t *)(var_0 + (-48L));
        var_22 = (uint32_t *)(var_0 + (-52L));
        var_23 = (uint32_t *)(var_0 + (-56L));
        while (1U)
            {
                var_25 = *var_4;
                local_sp_1 = local_sp_0;
                local_sp_2 = local_sp_0;
                if ((long)var_24 >= (long)*(uint64_t *)(var_25 + 8UL)) {
                    var_26 = *var_6;
                    var_27 = *var_10;
                    var_28 = *var_3;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4252075UL;
                    var_29 = indirect_placeholder_9(var_26, var_27, var_28);
                    var_30 = (uint32_t)var_29;
                    *var_15 = var_30;
                    if (var_30 != 0U) {
                        var_31 = *var_10;
                        loop_state_var = 1U;
                        break;
                    }
                    *(uint64_t *)(local_sp_0 + (-16L)) = 4252105UL;
                    indirect_placeholder_2();
                    *var_10 = 0UL;
                    loop_state_var = 1U;
                    break;
                }
                var_32 = (*(uint64_t *)(*(uint64_t *)(var_25 + 16UL) + (var_24 << 3UL)) << 4UL) + **(uint64_t **)var_2;
                *var_21 = var_32;
                *var_22 = (uint32_t)*(unsigned char *)(var_32 + 8UL);
                var_33 = (uint32_t)((uint16_t)(*(uint32_t *)(*var_21 + 8UL) >> 8U) & (unsigned short)1023U);
                *var_23 = var_33;
                if ((*var_22 == 1U) && (var_33 == 0U)) {
                    var_50 = *var_20 + 1UL;
                    *var_20 = var_50;
                    var_24 = var_50;
                    local_sp_0 = local_sp_2;
                    continue;
                }
                var_34 = (unsigned char *)(*var_10 + 104UL);
                var_35 = *var_34;
                *var_34 = (((var_35 | (*(unsigned char *)(*var_21 + 10UL) << '\x01')) & ' ') | (var_35 & '\xdf'));
                switch_state_var = 0;
                switch (*var_22) {
                  case 2U:
                    {
                        var_37 = (unsigned char *)(*var_10 + 104UL);
                        *var_37 = (*var_37 | '\x10');
                    }
                    break;
                  case 4U:
                    {
                        var_36 = (unsigned char *)(*var_10 + 104UL);
                        *var_36 = (*var_36 | '@');
                    }
                    break;
                  default:
                    {
                        var_38 = *var_23;
                        var_46 = var_38;
                        if (var_38 != 0U) {
                            var_39 = *var_10;
                            if (*(uint64_t *)(var_39 + 80UL) != (var_39 + 8UL)) {
                                *(uint64_t *)(local_sp_0 + (-8L)) = 4251803UL;
                                var_40 = indirect_placeholder_125(24UL);
                                *(uint64_t *)(*var_10 + 80UL) = var_40.field_0;
                                var_41 = *(uint64_t *)(*var_10 + 80UL);
                                if (var_41 != 0UL) {
                                    *(uint64_t *)(local_sp_0 + (-16L)) = 4251845UL;
                                    indirect_placeholder_2();
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                var_42 = *var_4;
                                var_43 = local_sp_0 + (-16L);
                                *(uint64_t *)var_43 = 4251878UL;
                                var_44 = indirect_placeholder(var_42, var_41);
                                local_sp_1 = var_43;
                                if ((uint64_t)(uint32_t)var_44 != 0UL) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                *var_7 = 0UL;
                                var_45 = (unsigned char *)(*var_10 + 104UL);
                                *var_45 = (*var_45 | '\x80');
                                var_46 = *var_23;
                            }
                            local_sp_2 = local_sp_1;
                            if ((var_46 & 1U) != 0U) {
                                if ((var_46 & 2U) != 0U) {
                                    if ((var_46 & 16U) != 0U) {
                                        if ((var_46 & 64U) == 0U) {
                                            var_50 = *var_20 + 1UL;
                                            *var_20 = var_50;
                                            var_24 = var_50;
                                            local_sp_0 = local_sp_2;
                                            continue;
                                        }
                                        if ((*var_5 & 4U) == 0U) {
                                            var_50 = *var_20 + 1UL;
                                            *var_20 = var_50;
                                            var_24 = var_50;
                                            local_sp_0 = local_sp_2;
                                            continue;
                                        }
                                        var_47 = *var_20 - *var_7;
                                        var_48 = *var_10 + 8UL;
                                        var_49 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_49 = 4252021UL;
                                        indirect_placeholder_16(var_47, var_48);
                                        *var_7 = (*var_7 + 1UL);
                                        local_sp_2 = var_49;
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    if ((*var_5 & 2U) != 0U) {
                                        if ((var_46 & 64U) == 0U) {
                                            var_50 = *var_20 + 1UL;
                                            *var_20 = var_50;
                                            var_24 = var_50;
                                            local_sp_0 = local_sp_2;
                                            continue;
                                        }
                                        if ((*var_5 & 4U) == 0U) {
                                            var_50 = *var_20 + 1UL;
                                            *var_20 = var_50;
                                            var_24 = var_50;
                                            local_sp_0 = local_sp_2;
                                            continue;
                                        }
                                    }
                                    var_47 = *var_20 - *var_7;
                                    var_48 = *var_10 + 8UL;
                                    var_49 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_49 = 4252021UL;
                                    indirect_placeholder_16(var_47, var_48);
                                    *var_7 = (*var_7 + 1UL);
                                    local_sp_2 = var_49;
                                    var_50 = *var_20 + 1UL;
                                    *var_20 = var_50;
                                    var_24 = var_50;
                                    local_sp_0 = local_sp_2;
                                    continue;
                                }
                                if ((*var_5 & 1U) != 0U) {
                                    var_47 = *var_20 - *var_7;
                                    var_48 = *var_10 + 8UL;
                                    var_49 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_49 = 4252021UL;
                                    indirect_placeholder_16(var_47, var_48);
                                    *var_7 = (*var_7 + 1UL);
                                    local_sp_2 = var_49;
                                    var_50 = *var_20 + 1UL;
                                    *var_20 = var_50;
                                    var_24 = var_50;
                                    local_sp_0 = local_sp_2;
                                    continue;
                                }
                                if ((var_46 & 16U) != 0U) {
                                    if ((var_46 & 64U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    if ((*var_5 & 4U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    var_47 = *var_20 - *var_7;
                                    var_48 = *var_10 + 8UL;
                                    var_49 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_49 = 4252021UL;
                                    indirect_placeholder_16(var_47, var_48);
                                    *var_7 = (*var_7 + 1UL);
                                    local_sp_2 = var_49;
                                    var_50 = *var_20 + 1UL;
                                    *var_20 = var_50;
                                    var_24 = var_50;
                                    local_sp_0 = local_sp_2;
                                    continue;
                                }
                                if ((*var_5 & 2U) != 0U) {
                                    if ((var_46 & 64U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    if ((*var_5 & 4U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                }
                                var_47 = *var_20 - *var_7;
                                var_48 = *var_10 + 8UL;
                                var_49 = local_sp_1 + (-8L);
                                *(uint64_t *)var_49 = 4252021UL;
                                indirect_placeholder_16(var_47, var_48);
                                *var_7 = (*var_7 + 1UL);
                                local_sp_2 = var_49;
                                var_50 = *var_20 + 1UL;
                                *var_20 = var_50;
                                var_24 = var_50;
                                local_sp_0 = local_sp_2;
                                continue;
                            }
                            if ((*var_5 & 1U) != 0U) {
                                if ((var_46 & 2U) == 0U) {
                                    if ((*var_5 & 1U) != 0U) {
                                        if ((var_46 & 16U) != 0U) {
                                            if ((var_46 & 64U) == 0U) {
                                                var_50 = *var_20 + 1UL;
                                                *var_20 = var_50;
                                                var_24 = var_50;
                                                local_sp_0 = local_sp_2;
                                                continue;
                                            }
                                            if ((*var_5 & 4U) != 0U) {
                                                var_50 = *var_20 + 1UL;
                                                *var_20 = var_50;
                                                var_24 = var_50;
                                                local_sp_0 = local_sp_2;
                                                continue;
                                            }
                                        }
                                        if ((*var_5 & 2U) != 0U) {
                                            if ((var_46 & 64U) != 0U) {
                                                var_50 = *var_20 + 1UL;
                                                *var_20 = var_50;
                                                var_24 = var_50;
                                                local_sp_0 = local_sp_2;
                                                continue;
                                            }
                                            if ((*var_5 & 4U) != 0U) {
                                                var_50 = *var_20 + 1UL;
                                                *var_20 = var_50;
                                                var_24 = var_50;
                                                local_sp_0 = local_sp_2;
                                                continue;
                                            }
                                        }
                                    }
                                }
                                if ((var_46 & 16U) != 0U) {
                                    if ((var_46 & 64U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    if ((*var_5 & 4U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    var_47 = *var_20 - *var_7;
                                    var_48 = *var_10 + 8UL;
                                    var_49 = local_sp_1 + (-8L);
                                    *(uint64_t *)var_49 = 4252021UL;
                                    indirect_placeholder_16(var_47, var_48);
                                    *var_7 = (*var_7 + 1UL);
                                    local_sp_2 = var_49;
                                    var_50 = *var_20 + 1UL;
                                    *var_20 = var_50;
                                    var_24 = var_50;
                                    local_sp_0 = local_sp_2;
                                    continue;
                                }
                                if ((*var_5 & 2U) != 0U) {
                                    if ((var_46 & 64U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                    if ((*var_5 & 4U) == 0U) {
                                        var_50 = *var_20 + 1UL;
                                        *var_20 = var_50;
                                        var_24 = var_50;
                                        local_sp_0 = local_sp_2;
                                        continue;
                                    }
                                }
                                var_47 = *var_20 - *var_7;
                                var_48 = *var_10 + 8UL;
                                var_49 = local_sp_1 + (-8L);
                                *(uint64_t *)var_49 = 4252021UL;
                                indirect_placeholder_16(var_47, var_48);
                                *var_7 = (*var_7 + 1UL);
                                local_sp_2 = var_49;
                                var_50 = *var_20 + 1UL;
                                *var_20 = var_50;
                                var_24 = var_50;
                                local_sp_0 = local_sp_2;
                                continue;
                            }
                            var_47 = *var_20 - *var_7;
                            var_48 = *var_10 + 8UL;
                            var_49 = local_sp_1 + (-8L);
                            *(uint64_t *)var_49 = 4252021UL;
                            indirect_placeholder_16(var_47, var_48);
                            *var_7 = (*var_7 + 1UL);
                            local_sp_2 = var_49;
                        }
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        rax_0 = var_31;
    } else {
        *(uint64_t *)(var_0 + (-112L)) = 4251508UL;
        indirect_placeholder_2();
    }
}
