typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_group_nodes_into_DFAstates_ret_type;
struct bb_group_nodes_into_DFAstates_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_16(uint64_t param_0, uint64_t param_1);
struct bb_group_nodes_into_DFAstates_ret_type bb_group_nodes_into_DFAstates(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t local_sp_6;
    uint64_t *var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint32_t *var_15;
    uint32_t *var_16;
    uint64_t var_17;
    unsigned char *var_18;
    uint64_t *var_19;
    uint64_t *var_20;
    uint64_t *var_21;
    uint64_t *var_22;
    uint64_t *var_23;
    uint64_t *var_24;
    uint64_t *var_25;
    uint32_t *var_26;
    unsigned char *var_27;
    uint64_t var_28;
    uint64_t rcx2_12;
    uint64_t var_129;
    uint64_t local_sp_14;
    uint64_t local_sp_12;
    uint64_t rcx2_17;
    uint64_t var_119;
    uint64_t local_sp_11;
    uint64_t local_sp_0;
    uint64_t rcx2_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t var_10;
    uint64_t var_80;
    uint64_t var_88;
    uint64_t rcx2_15;
    uint64_t local_sp_8;
    uint64_t rcx2_18;
    uint64_t rcx2_11;
    uint64_t var_89;
    uint64_t var_120;
    uint64_t local_sp_9;
    uint64_t var_126;
    uint64_t local_sp_1;
    uint64_t local_sp_4;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t rcx2_2;
    uint64_t var_54;
    uint32_t var_55;
    uint64_t var_36;
    uint64_t *_pre_phi285;
    uint64_t local_sp_2;
    uint64_t var_37;
    uint64_t local_sp_3;
    uint64_t rcx2_1;
    uint64_t *var_43;
    uint64_t *_pre_phi281;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t local_sp_5;
    uint32_t var_51;
    uint64_t local_sp_7;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_56;
    uint32_t var_72;
    uint64_t var_71;
    bool var_57;
    uint64_t var_64;
    uint64_t rcx2_3;
    uint64_t rcx2_5;
    uint64_t var_65;
    uint64_t *var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_58;
    uint64_t rcx2_4;
    uint64_t rcx2_6;
    uint64_t *var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_73;
    bool var_74;
    uint64_t rcx2_10;
    uint64_t var_81;
    uint64_t rcx2_7;
    uint64_t rcx2_9;
    uint64_t var_82;
    uint64_t *var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_75;
    uint64_t rcx2_8;
    uint64_t *var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_121;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint32_t var_125;
    uint64_t local_sp_10;
    uint64_t var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t var_94;
    uint64_t rcx2_13;
    uint64_t var_95;
    uint64_t rcx2_14;
    uint64_t local_sp_13;
    uint64_t var_101;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint32_t var_113;
    uint64_t var_114;
    uint64_t var_115;
    uint64_t var_116;
    uint64_t var_117;
    unsigned char var_118;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t var_104;
    uint64_t *var_105;
    uint64_t var_106;
    uint64_t var_130;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_99;
    uint64_t var_100;
    uint64_t rcx2_16;
    uint64_t var_29;
    uint64_t storemerge;
    struct bb_group_nodes_into_DFAstates_ret_type mrv;
    struct bb_group_nodes_into_DFAstates_ret_type mrv1;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_38;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_39;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_131;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = var_0 + (-8L);
    *(uint64_t *)var_2 = var_1;
    var_3 = var_0 + (-224L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-232L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-240L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-248L));
    *var_7 = rcx;
    var_8 = *var_5 + 8UL;
    var_9 = (uint64_t *)(var_0 + (-88L));
    *var_9 = var_8;
    var_10 = var_0 + (-256L);
    *(uint64_t *)var_10 = 4312445UL;
    indirect_placeholder_2();
    var_11 = (uint64_t *)(var_0 + (-40L));
    *var_11 = 0UL;
    var_12 = (uint64_t *)(var_0 + (-16L));
    *var_12 = 0UL;
    var_13 = var_0 + (-96L);
    var_14 = (uint64_t *)var_13;
    var_15 = (uint32_t *)(var_0 + (-100L));
    var_16 = (uint32_t *)(var_0 + (-104L));
    var_17 = var_0 + (-152L);
    var_18 = (unsigned char *)(var_0 + (-105L));
    var_19 = (uint64_t *)(var_0 + (-48L));
    var_20 = (uint64_t *)(var_0 + (-24L));
    var_21 = (uint64_t *)(var_0 + (-56L));
    var_22 = (uint64_t *)(var_0 + (-64L));
    var_23 = (uint64_t *)(var_0 + (-32L));
    var_24 = (uint64_t *)(var_0 + (-80L));
    var_25 = (uint64_t *)(var_0 + (-72L));
    var_26 = (uint32_t *)(var_0 + (-112L));
    var_27 = (unsigned char *)(var_0 + (-113L));
    var_28 = 0UL;
    var_88 = 0UL;
    var_126 = 0UL;
    var_64 = 0UL;
    var_58 = 0UL;
    var_81 = 0UL;
    var_75 = 0UL;
    var_95 = 0UL;
    local_sp_13 = var_10;
    var_101 = 0UL;
    rcx2_16 = rcx;
    storemerge = 18446744073709551615UL;
    while (1U)
        {
            var_29 = *var_9;
            rcx2_17 = rcx2_16;
            local_sp_14 = local_sp_13;
            if ((long)var_28 >= (long)*(uint64_t *)(var_29 + 8UL)) {
                storemerge = *var_11;
                loop_state_var = 1U;
                break;
            }
            switch (*var_15) {
              case 1U:
                {
                    var_49 = (uint64_t)**(unsigned char **)var_13;
                    var_50 = local_sp_13 + (-8L);
                    *(uint64_t *)var_50 = 4312577UL;
                    indirect_placeholder_16(var_49, var_17);
                    local_sp_5 = var_50;
                }
                break;
              case 3U:
                {
                    var_46 = **(uint64_t **)var_13;
                    var_47 = local_sp_13 + (-8L);
                    *(uint64_t *)var_47 = 4312613UL;
                    var_48 = indirect_placeholder(var_46, var_17);
                    local_sp_5 = var_47;
                    rcx2_2 = var_48;
                }
                break;
              case 7U:
                {
                    var_34 = local_sp_13 + (-8L);
                    *(uint64_t *)var_34 = 4312819UL;
                    indirect_placeholder_2();
                    var_35 = (uint64_t *)(*var_4 + 216UL);
                    _pre_phi285 = var_35;
                    local_sp_2 = var_34;
                    if ((*var_35 & 64UL) == 0UL) {
                        var_36 = local_sp_13 + (-16L);
                        *(uint64_t *)var_36 = 4312861UL;
                        indirect_placeholder_16(10UL, var_17);
                        _pre_phi285 = (uint64_t *)(*var_4 + 216UL);
                        local_sp_2 = var_36;
                    }
                    local_sp_5 = local_sp_2;
                    if ((signed char)(unsigned char)*_pre_phi285 > '\xff') {
                        var_37 = local_sp_2 + (-8L);
                        *(uint64_t *)var_37 = 4312905UL;
                        indirect_placeholder_16(0UL, var_17);
                        local_sp_5 = var_37;
                    }
                }
                break;
              case 5U:
                {
                    var_38 = *var_4;
                    if ((int)*(uint32_t *)(var_38 + 180UL) > (int)1U) {
                        var_40 = *(uint64_t *)(var_38 + 120UL);
                        var_41 = local_sp_13 + (-8L);
                        *(uint64_t *)var_41 = 4312675UL;
                        var_42 = indirect_placeholder(var_40, var_17);
                        local_sp_3 = var_41;
                        rcx2_1 = var_42;
                    } else {
                        var_39 = local_sp_13 + (-8L);
                        *(uint64_t *)var_39 = 4312692UL;
                        indirect_placeholder_2();
                        local_sp_3 = var_39;
                    }
                    var_43 = (uint64_t *)(*var_4 + 216UL);
                    _pre_phi281 = var_43;
                    local_sp_4 = local_sp_3;
                    rcx2_2 = rcx2_1;
                    if ((*var_43 & 64UL) == 0UL) {
                        var_44 = local_sp_3 + (-8L);
                        *(uint64_t *)var_44 = 4312734UL;
                        indirect_placeholder_16(10UL, var_17);
                        _pre_phi281 = (uint64_t *)(*var_4 + 216UL);
                        local_sp_4 = var_44;
                    }
                    local_sp_5 = local_sp_4;
                    if ((signed char)(unsigned char)*_pre_phi281 > '\xff') {
                        var_45 = local_sp_4 + (-8L);
                        *(uint64_t *)var_45 = 4312782UL;
                        indirect_placeholder_16(0UL, var_17);
                        local_sp_5 = var_45;
                    }
                }
                break;
            }
            var_51 = *var_16;
            local_sp_6 = local_sp_5;
            rcx2_18 = rcx2_2;
            var_55 = var_51;
            local_sp_7 = local_sp_5;
            rcx2_3 = rcx2_2;
            rcx2_4 = rcx2_2;
            rcx2_6 = rcx2_2;
            rcx2_10 = rcx2_2;
            if (var_51 == 0U) {
                *var_20 = 0UL;
                local_sp_8 = local_sp_7;
                rcx2_11 = rcx2_10;
                while (1U)
                    {
                        var_89 = *var_11;
                        rcx2_12 = rcx2_11;
                        var_119 = var_89;
                        var_120 = var_88;
                        local_sp_9 = local_sp_8;
                        local_sp_10 = local_sp_8;
                        rcx2_13 = rcx2_11;
                        if ((long)var_88 >= (long)var_89) {
                            loop_state_var = 0U;
                            break;
                        }
                        if (*var_15 != 1U) {
                            var_90 = (uint64_t)**(unsigned char **)var_13;
                            var_91 = var_88 << 5UL;
                            var_92 = *var_7 + var_91;
                            var_93 = local_sp_8 + (-8L);
                            *(uint64_t *)var_93 = 4313679UL;
                            var_94 = indirect_placeholder(var_90, var_92);
                            local_sp_10 = var_93;
                            rcx2_13 = var_91;
                            local_sp_12 = var_93;
                            rcx2_15 = var_91;
                            if ((uint64_t)(unsigned char)var_94 != 1UL) {
                                var_130 = *var_20 + 1UL;
                                *var_20 = var_130;
                                var_88 = var_130;
                                local_sp_8 = local_sp_12;
                                rcx2_11 = rcx2_15;
                                continue;
                            }
                        }
                        *var_22 = 0UL;
                        *var_23 = 0UL;
                        rcx2_14 = rcx2_13;
                        local_sp_11 = local_sp_10;
                        local_sp_12 = local_sp_10;
                        rcx2_15 = rcx2_14;
                        while ((long)var_95 <= (long)3UL)
                            {
                                var_96 = var_95 << 3UL;
                                var_97 = var_96 + var_2;
                                var_98 = *(uint64_t *)(var_97 + (-144L));
                                var_99 = (*var_20 << 5UL) + *var_7;
                                *(uint64_t *)(var_97 + (-208L)) = (var_98 & *(uint64_t *)(var_96 + var_99));
                                *var_22 = (*var_22 | *(uint64_t *)(((*var_23 << 3UL) + var_2) + (-208L)));
                                var_100 = *var_23 + 1UL;
                                *var_23 = var_100;
                                var_95 = var_100;
                                rcx2_14 = var_99;
                                rcx2_15 = rcx2_14;
                            }
                        if (*var_22 == 0UL) {
                            var_130 = *var_20 + 1UL;
                            *var_20 = var_130;
                            var_88 = var_130;
                            local_sp_8 = local_sp_12;
                            rcx2_11 = rcx2_15;
                            continue;
                        }
                        *var_24 = 0UL;
                        *var_25 = 0UL;
                        *var_23 = 0UL;
                        while ((long)var_101 <= (long)3UL)
                            {
                                var_102 = var_101 << 3UL;
                                var_103 = var_102 + var_2;
                                *(uint64_t *)(var_103 + (-176L)) = (*(uint64_t *)(var_102 + ((*var_20 << 5UL) + *var_7)) & (*(uint64_t *)(var_103 + (-144L)) ^ (-1L)));
                                *var_25 = (*var_25 | *(uint64_t *)(((*var_23 << 3UL) + var_2) + (-176L)));
                                var_104 = *var_23 << 3UL;
                                var_105 = (uint64_t *)((var_104 + var_2) + (-144L));
                                *var_105 = (*var_105 & (*(uint64_t *)(var_104 + ((*var_20 << 5UL) + *var_7)) ^ (-1L)));
                                *var_24 = (*var_24 | *(uint64_t *)(((*var_23 << 3UL) + var_2) + (-144L)));
                                var_106 = *var_23 + 1UL;
                                *var_23 = var_106;
                                var_101 = var_106;
                            }
                        if (*var_25 != 0UL) {
                            *(uint64_t *)(local_sp_10 + (-8L)) = 4314054UL;
                            indirect_placeholder_2();
                            *(uint64_t *)(local_sp_10 + (-16L)) = 4314093UL;
                            indirect_placeholder_2();
                            var_107 = *var_20 * 24UL;
                            var_108 = *var_6;
                            var_109 = var_108 + var_107;
                            var_110 = var_108 + (*var_11 * 24UL);
                            var_111 = local_sp_10 + (-24L);
                            *(uint64_t *)var_111 = 4314165UL;
                            var_112 = indirect_placeholder(var_109, var_110);
                            var_113 = (uint32_t)var_112;
                            *var_26 = var_113;
                            local_sp_0 = var_111;
                            rcx2_0 = var_109;
                            local_sp_11 = var_111;
                            if (var_113 != 0U) {
                                loop_state_var = 2U;
                                break;
                            }
                            *var_11 = (*var_11 + 1UL);
                        }
                        var_114 = *(uint64_t *)(*(uint64_t *)(*var_9 + 16UL) + (*var_12 << 3UL));
                        var_115 = *var_6 + (*var_20 * 24UL);
                        var_116 = local_sp_11 + (-8L);
                        *(uint64_t *)var_116 = 4314255UL;
                        var_117 = indirect_placeholder(var_114, var_115);
                        var_118 = (unsigned char)var_117;
                        *var_27 = var_118;
                        rcx2_12 = var_114;
                        local_sp_12 = var_116;
                        local_sp_0 = var_116;
                        rcx2_0 = var_114;
                        rcx2_15 = var_114;
                        local_sp_9 = var_116;
                        if (var_118 != '\x01') {
                            loop_state_var = 2U;
                            break;
                        }
                        if (*var_24 != 0UL) {
                            loop_state_var = 1U;
                            break;
                        }
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                  case 0U:
                    {
                        switch (loop_state_var) {
                          case 1U:
                            {
                                var_119 = *var_11;
                                var_120 = *var_20;
                            }
                            break;
                          case 0U:
                            {
                            }
                            break;
                        }
                        local_sp_14 = local_sp_9;
                        rcx2_18 = rcx2_12;
                        if (var_120 != var_119) {
                            *(uint64_t *)(local_sp_9 + (-8L)) = 4314365UL;
                            indirect_placeholder_2();
                            var_121 = *(uint64_t *)(*(uint64_t *)(*var_9 + 16UL) + (*var_12 << 3UL));
                            var_122 = *var_6 + (*var_11 * 24UL);
                            var_123 = local_sp_9 + (-16L);
                            *(uint64_t *)var_123 = 4314428UL;
                            var_124 = indirect_placeholder(var_121, var_122);
                            var_125 = (uint32_t)var_124;
                            *var_26 = var_125;
                            local_sp_0 = var_123;
                            rcx2_0 = var_121;
                            rcx2_18 = var_121;
                            if (var_125 != 0U) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            *var_11 = (*var_11 + 1UL);
                            var_129 = local_sp_9 + (-24L);
                            *(uint64_t *)var_129 = 4314466UL;
                            indirect_placeholder_2();
                            local_sp_14 = var_129;
                        }
                    }
                    break;
                  case 2U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            } else {
                if ((var_51 & 32U) != 0U) {
                    *(uint64_t *)(local_sp_5 + (-8L)) = 4312945UL;
                    var_52 = indirect_placeholder(10UL, var_17);
                    *var_18 = (unsigned char)var_52;
                    var_53 = local_sp_5 + (-16L);
                    *(uint64_t *)var_53 = 4312963UL;
                    indirect_placeholder_2();
                    local_sp_14 = var_53;
                    if (*var_18 != '\x00') {
                        var_131 = *var_12 + 1UL;
                        *var_12 = var_131;
                        var_28 = var_131;
                        local_sp_13 = local_sp_14;
                        rcx2_16 = rcx2_18;
                        continue;
                    }
                    var_54 = local_sp_5 + (-24L);
                    *(uint64_t *)var_54 = 4312993UL;
                    indirect_placeholder_16(10UL, var_17);
                    var_55 = *var_16;
                    local_sp_6 = var_54;
                }
                var_72 = var_55;
                local_sp_7 = local_sp_6;
                local_sp_14 = local_sp_6;
                if ((signed char)(unsigned char)var_55 <= '\xff') {
                    var_56 = local_sp_6 + (-8L);
                    *(uint64_t *)var_56 = 4313020UL;
                    indirect_placeholder_2();
                    local_sp_14 = var_56;
                    var_131 = *var_12 + 1UL;
                    *var_12 = var_131;
                    var_28 = var_131;
                    local_sp_13 = local_sp_14;
                    rcx2_16 = rcx2_18;
                    continue;
                }
                if ((var_55 & 4U) == 0U) {
                    rcx2_7 = rcx2_6;
                    rcx2_8 = rcx2_6;
                    rcx2_10 = rcx2_6;
                    rcx2_18 = rcx2_6;
                    if ((var_72 & 8U) != 0U) {
                        *var_21 = 0UL;
                        if (*var_15 != 1U) {
                            if ((*(unsigned char *)(*var_14 + 10UL) & '@') != '\x00') {
                                var_73 = local_sp_6 + (-8L);
                                *(uint64_t *)var_73 = 4313378UL;
                                indirect_placeholder_2();
                                local_sp_14 = var_73;
                                var_131 = *var_12 + 1UL;
                                *var_12 = var_131;
                                var_28 = var_131;
                                local_sp_13 = local_sp_14;
                                rcx2_16 = rcx2_18;
                                continue;
                            }
                        }
                        var_74 = ((int)*(uint32_t *)(*var_4 + 180UL) > (int)1U);
                        *var_20 = 0UL;
                        if (!var_74) {
                            rcx2_9 = rcx2_8;
                            while ((long)var_75 <= (long)3UL)
                                {
                                    var_76 = (uint64_t *)(((var_75 << 3UL) + var_2) + (-144L));
                                    var_77 = *var_76;
                                    var_78 = *var_4;
                                    var_79 = var_75 + 22UL;
                                    *var_76 = (var_77 & (*(uint64_t *)(((var_79 << 3UL) + var_78) + 8UL) ^ (-1L)));
                                    *var_21 = (*var_21 | *(uint64_t *)(((*var_20 << 3UL) + var_2) + (-144L)));
                                    var_80 = *var_20 + 1UL;
                                    *var_20 = var_80;
                                    var_75 = var_80;
                                    rcx2_8 = var_79;
                                    rcx2_9 = rcx2_8;
                                }
                        }
                        rcx2_9 = rcx2_7;
                        while ((long)var_81 <= (long)3UL)
                            {
                                var_82 = var_81 << 3UL;
                                var_83 = (uint64_t *)((var_82 + var_2) + (-144L));
                                var_84 = *var_83;
                                var_85 = *var_4;
                                var_86 = *(uint64_t *)(((var_82 + 176UL) + var_85) + 8UL);
                                *var_83 = (var_84 & ((*(uint64_t *)(*(uint64_t *)(var_85 + 120UL) + var_82) & var_86) ^ (-1L)));
                                *var_21 = (*var_21 | *(uint64_t *)(((*var_20 << 3UL) + var_2) + (-144L)));
                                var_87 = *var_20 + 1UL;
                                *var_20 = var_87;
                                var_81 = var_87;
                                rcx2_7 = var_86;
                                rcx2_9 = rcx2_7;
                            }
                        rcx2_10 = rcx2_9;
                        rcx2_18 = rcx2_9;
                        if (*var_21 != 0UL) {
                            var_131 = *var_12 + 1UL;
                            *var_12 = var_131;
                            var_28 = var_131;
                            local_sp_13 = local_sp_14;
                            rcx2_16 = rcx2_18;
                            continue;
                        }
                    }
                }
                *var_19 = 0UL;
                if (*var_15 != 1U) {
                    if ((*(unsigned char *)(*var_14 + 10UL) & '@') != '\x00') {
                        var_71 = local_sp_6 + (-8L);
                        *(uint64_t *)var_71 = 4313083UL;
                        indirect_placeholder_2();
                        local_sp_14 = var_71;
                        var_131 = *var_12 + 1UL;
                        *var_12 = var_131;
                        var_28 = var_131;
                        local_sp_13 = local_sp_14;
                        rcx2_16 = rcx2_18;
                        continue;
                    }
                }
                var_57 = ((int)*(uint32_t *)(*var_4 + 180UL) > (int)1U);
                *var_20 = 0UL;
                if (!var_57) {
                    rcx2_5 = rcx2_4;
                    while ((long)var_58 <= (long)3UL)
                        {
                            var_59 = (uint64_t *)(((var_58 << 3UL) + var_2) + (-144L));
                            var_60 = *var_59;
                            var_61 = *var_4;
                            var_62 = var_58 + 22UL;
                            *var_59 = (var_60 & *(uint64_t *)(((var_62 << 3UL) + var_61) + 8UL));
                            *var_19 = (*var_19 | *(uint64_t *)(((*var_20 << 3UL) + var_2) + (-144L)));
                            var_63 = *var_20 + 1UL;
                            *var_20 = var_63;
                            var_58 = var_63;
                            rcx2_4 = var_62;
                            rcx2_5 = rcx2_4;
                        }
                }
                rcx2_5 = rcx2_3;
                while ((long)var_64 <= (long)3UL)
                    {
                        var_65 = var_64 << 3UL;
                        var_66 = (uint64_t *)((var_65 + var_2) + (-144L));
                        var_67 = *var_66;
                        var_68 = *var_4;
                        var_69 = *(uint64_t *)(((var_65 + 176UL) + var_68) + 8UL);
                        *var_66 = (var_67 & (var_69 | (*(uint64_t *)(*(uint64_t *)(var_68 + 120UL) + var_65) ^ (-1L))));
                        *var_19 = (*var_19 | *(uint64_t *)(((*var_20 << 3UL) + var_2) + (-144L)));
                        var_70 = *var_20 + 1UL;
                        *var_20 = var_70;
                        var_64 = var_70;
                        rcx2_3 = var_69;
                        rcx2_5 = rcx2_3;
                    }
                rcx2_6 = rcx2_5;
                rcx2_18 = rcx2_5;
                if (*var_19 != 0UL) {
                    var_131 = *var_12 + 1UL;
                    *var_12 = var_131;
                    var_28 = var_131;
                    local_sp_13 = local_sp_14;
                    rcx2_16 = rcx2_18;
                    continue;
                }
                var_72 = *var_16;
            }
        }
    switch (loop_state_var) {
      case 1U:
        {
            mrv.field_0 = storemerge;
            mrv1 = mrv;
            mrv1.field_1 = rcx2_17;
            return mrv1;
        }
        break;
      case 0U:
        {
            *var_20 = 0UL;
            local_sp_1 = local_sp_0;
            rcx2_17 = rcx2_0;
            while ((long)var_126 >= (long)*var_11)
                {
                    var_127 = local_sp_1 + (-8L);
                    *(uint64_t *)var_127 = 4314566UL;
                    indirect_placeholder_2();
                    var_128 = *var_20 + 1UL;
                    *var_20 = var_128;
                    var_126 = var_128;
                    local_sp_1 = var_127;
                }
        }
        break;
    }
}
