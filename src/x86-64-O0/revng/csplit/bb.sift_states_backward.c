typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_sift_states_backward(uint64_t rsi, uint64_t rdi) {
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint32_t var_12;
    uint32_t rax_0_shrunk;
    uint64_t *var_19;
    uint64_t var_27;
    uint64_t local_sp_0;
    uint64_t var_20;
    uint64_t local_sp_2;
    uint32_t storemerge;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint32_t var_26;
    uint64_t var_29;
    uint64_t var_28;
    uint64_t local_sp_1;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint32_t var_33;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-64L));
    *var_2 = rdi;
    var_3 = var_0 + (-72L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-16L));
    *var_5 = 0U;
    var_6 = *(uint64_t *)(*var_4 + 24UL);
    var_7 = (uint64_t *)(var_0 + (-24L));
    *var_7 = var_6;
    var_8 = *(uint64_t *)(*var_4 + 16UL);
    var_9 = var_0 + (-56L);
    *(uint64_t *)(var_0 + (-80L)) = 4293393UL;
    var_10 = indirect_placeholder(var_8, var_9);
    var_11 = (uint32_t *)(var_0 + (-12L));
    var_12 = (uint32_t)var_10;
    *var_11 = var_12;
    storemerge = 0U;
    rax_0_shrunk = var_12;
    if (var_12 == 0U) {
        return (uint64_t)rax_0_shrunk;
    }
    var_13 = *var_7;
    var_14 = *var_4;
    var_15 = *var_2;
    var_16 = var_0 + (-88L);
    *(uint64_t *)var_16 = 4293443UL;
    var_17 = indirect_placeholder_10(var_9, var_13, var_14, var_15);
    var_18 = (uint32_t)var_17;
    *var_11 = var_18;
    local_sp_0 = var_16;
    local_sp_2 = var_16;
    rax_0_shrunk = 0U;
    if (var_18 != 0U) {
        var_19 = (uint64_t *)(var_0 + (-48L));
        while (1U)
            {
                var_20 = *var_7;
                local_sp_1 = local_sp_0;
                local_sp_2 = local_sp_0;
                if ((long)var_20 <= (long)0UL) {
                    *var_11 = 0U;
                    loop_state_var = 1U;
                    break;
                }
                if (*(uint64_t *)(**(uint64_t **)var_3 + (var_20 << 3UL)) == 0UL) {
                    storemerge = *var_5 + 1U;
                }
                *var_5 = storemerge;
                if ((int)*(uint32_t *)(*var_2 + 224UL) < (int)storemerge) {
                    *var_19 = 0UL;
                    var_21 = *var_7 + (-1L);
                    *var_7 = var_21;
                    var_22 = *var_2;
                    var_27 = var_22;
                    var_28 = var_21;
                    if (*(uint64_t *)(*(uint64_t *)(var_22 + 184UL) + (var_21 << 3UL)) != 0UL) {
                        var_23 = *var_4;
                        var_24 = local_sp_0 + (-8L);
                        *(uint64_t *)var_24 = 4293648UL;
                        var_25 = indirect_placeholder_10(var_9, var_21, var_23, var_22);
                        var_26 = (uint32_t)var_25;
                        *var_11 = var_26;
                        local_sp_1 = var_24;
                        local_sp_2 = var_24;
                        if (var_26 != 0U) {
                            loop_state_var = 1U;
                            break;
                        }
                        var_27 = *var_2;
                        var_28 = *var_7;
                    }
                    var_29 = *var_4;
                    var_30 = local_sp_1 + (-8L);
                    *(uint64_t *)var_30 = 4293690UL;
                    var_31 = indirect_placeholder_10(var_9, var_28, var_29, var_27);
                    var_32 = (uint32_t)var_31;
                    *var_11 = var_32;
                    local_sp_0 = var_30;
                    local_sp_2 = var_30;
                    if (var_32 == 0U) {
                        continue;
                    }
                    loop_state_var = 1U;
                    break;
                }
                *(uint64_t *)(local_sp_0 + (-8L)) = 4293559UL;
                indirect_placeholder_2();
                *(uint64_t *)(local_sp_0 + (-16L)) = 4293571UL;
                indirect_placeholder_2();
                loop_state_var = 0U;
                break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                break;
            }
            break;
          case 0U:
            {
                return (uint64_t)rax_0_shrunk;
            }
            break;
        }
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4293747UL;
    indirect_placeholder_2();
    var_33 = *var_11;
    rax_0_shrunk = var_33;
}
