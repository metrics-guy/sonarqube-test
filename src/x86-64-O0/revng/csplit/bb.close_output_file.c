typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_93_ret_type;
struct indirect_placeholder_92_ret_type;
struct indirect_placeholder_95_ret_type;
struct indirect_placeholder_94_ret_type;
struct indirect_placeholder_97_ret_type;
struct indirect_placeholder_96_ret_type;
struct indirect_placeholder_93_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_92_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_95_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_94_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_97_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_96_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern uint64_t init_rbx(void);
extern struct indirect_placeholder_93_ret_type indirect_placeholder_93(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_92_ret_type indirect_placeholder_92(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_95_ret_type indirect_placeholder_95(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_94_ret_type indirect_placeholder_94(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_97_ret_type indirect_placeholder_97(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_96_ret_type indirect_placeholder_96(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_close_output_file(void) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t rbp_0;
    uint64_t var_31;
    struct indirect_placeholder_93_ret_type var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rbp_1;
    uint64_t var_15;
    struct indirect_placeholder_95_ret_type var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    bool var_26;
    uint64_t var_27;
    unsigned char *var_28;
    uint32_t var_29;
    uint32_t *var_30;
    uint64_t var_37;
    uint64_t var_6;
    struct indirect_placeholder_97_ret_type var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_5;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = *(uint64_t *)4404408UL;
    rbp_1 = var_3;
    if (var_4 == 0UL) {
        return;
    }
    var_5 = var_0 + (-192L);
    *(uint64_t *)var_5 = 4210293UL;
    indirect_placeholder_2();
    local_sp_1 = var_5;
    if ((uint64_t)(uint32_t)var_4 == 0UL) {
        var_6 = *(uint64_t *)4404416UL;
        *(uint64_t *)(var_0 + (-200L)) = 4210317UL;
        var_7 = indirect_placeholder_97(var_6, 4UL);
        var_8 = var_7.field_0;
        var_9 = var_7.field_1;
        var_10 = var_7.field_2;
        *(uint64_t *)(var_0 + (-208L)) = 4210345UL;
        indirect_placeholder_96(0UL, var_8, 4371578UL, 0UL, 0UL, var_9, var_10);
        *(uint64_t *)4404408UL = 0UL;
        var_11 = var_0 + (-216L);
        *(uint64_t *)var_11 = 4210361UL;
        var_12 = indirect_placeholder_8(var_3);
        local_sp_1 = var_11;
        rbp_1 = var_12;
    }
    var_13 = local_sp_1 + (-8L);
    *(uint64_t *)var_13 = 4210376UL;
    var_14 = indirect_placeholder_1();
    local_sp_0 = var_13;
    rbp_0 = rbp_1;
    if ((uint64_t)(uint32_t)var_14 == 0UL) {
        var_15 = *(uint64_t *)4404416UL;
        *(uint64_t *)(local_sp_1 + (-16L)) = 4210405UL;
        var_16 = indirect_placeholder_95(var_15, 3UL, 0UL);
        var_17 = var_16.field_0;
        var_18 = var_16.field_1;
        var_19 = var_16.field_2;
        *(uint64_t *)(local_sp_1 + (-24L)) = 4210413UL;
        indirect_placeholder_2();
        var_20 = (uint64_t)*(uint32_t *)var_17;
        *(uint64_t *)(local_sp_1 + (-32L)) = 4210440UL;
        indirect_placeholder_94(0UL, var_17, 4371345UL, var_20, 0UL, var_18, var_19);
        *(uint64_t *)4404408UL = 0UL;
        var_21 = local_sp_1 + (-40L);
        *(uint64_t *)var_21 = 4210456UL;
        var_22 = indirect_placeholder_8(rbp_1);
        local_sp_0 = var_21;
        rbp_0 = var_22;
    }
    var_23 = *(uint64_t *)4404400UL;
    if (var_23 != 0UL) {
        if (*(unsigned char *)4404434UL != '\x00') {
            *(uint64_t *)(local_sp_0 + (-8L)) = 4210512UL;
            indirect_placeholder_2();
            var_24 = *(uint64_t *)4404416UL;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4210527UL;
            var_25 = indirect_placeholder_8(var_24);
            var_26 = ((uint64_t)(uint32_t)var_25 == 0UL);
            var_27 = (var_25 & (-256L)) | var_26;
            var_28 = (unsigned char *)(rbp_0 + (-17L));
            *var_28 = var_26;
            *(uint64_t *)(local_sp_0 + (-24L)) = 4210540UL;
            indirect_placeholder_2();
            var_29 = *(uint32_t *)var_27;
            var_30 = (uint32_t *)(rbp_0 + (-24L));
            *var_30 = var_29;
            *(uint32_t *)4404392UL = (*(uint32_t *)4404392UL - (uint32_t)*var_28);
            *(uint64_t *)(local_sp_0 + (-32L)) = 4210590UL;
            indirect_placeholder_2();
            if (*var_28 == '\x01') {
                var_31 = *(uint64_t *)4404416UL;
                *(uint64_t *)(local_sp_0 + (-40L)) = 4210626UL;
                var_32 = indirect_placeholder_93(var_31, 3UL, 0UL);
                var_33 = var_32.field_0;
                var_34 = var_32.field_1;
                var_35 = var_32.field_2;
                var_36 = (uint64_t)*var_30;
                *(uint64_t *)(local_sp_0 + (-48L)) = 4210657UL;
                indirect_placeholder_92(0UL, var_33, 4371345UL, var_36, 0UL, var_34, var_35);
            }
            *(uint64_t *)4404408UL = 0UL;
            return;
        }
    }
    if (*(unsigned char *)4404432UL != '\x01') {
        var_37 = rbp_0 + (-48L);
        *(uint64_t *)(local_sp_0 + (-8L)) = 4210695UL;
        indirect_placeholder(var_37, var_23);
        *(uint64_t *)(local_sp_0 + (-16L)) = 4210723UL;
        indirect_placeholder_2();
    }
    *(uint64_t *)4404408UL = 0UL;
}
