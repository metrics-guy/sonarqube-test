typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_duplicate_node_closure_ret_type;
struct indirect_placeholder_142_ret_type;
struct bb_duplicate_node_closure_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_142_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_113(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_142_ret_type indirect_placeholder_142(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
struct bb_duplicate_node_closure_ret_type bb_duplicate_node_closure(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r8, uint64_t r9, uint64_t r10, uint64_t rbx) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint32_t var_9;
    uint64_t var_73;
    uint32_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t *var_14;
    uint64_t *var_15;
    uint64_t *var_16;
    unsigned char *var_17;
    uint32_t *var_18;
    uint64_t var_19;
    uint64_t r910_2;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    unsigned char var_77;
    uint64_t r89_2;
    uint64_t local_sp_0;
    uint64_t r89_0;
    uint64_t r1011_2;
    uint64_t r910_0;
    uint64_t rbx12_2;
    uint64_t r1011_0;
    uint64_t rbx12_0;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_62;
    struct indirect_placeholder_142_ret_type var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint32_t var_69;
    uint64_t var_54;
    uint64_t var_55;
    unsigned char var_56;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    unsigned char var_49;
    uint64_t r910_3;
    uint64_t r1011_3;
    uint64_t rbx12_3;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    unsigned char var_40;
    uint64_t local_sp_1;
    uint64_t r89_1;
    uint64_t r910_1;
    uint64_t r1011_1;
    uint64_t rbx12_1;
    uint64_t var_88;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    uint64_t var_85;
    uint64_t var_86;
    unsigned char var_87;
    uint64_t local_sp_2;
    uint64_t **var_20;
    uint64_t var_21;
    uint64_t var_22;
    bool var_23;
    uint64_t var_24;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_25;
    uint64_t var_41;
    uint64_t rax_0;
    uint64_t r89_3;
    struct bb_duplicate_node_closure_ret_type mrv;
    struct bb_duplicate_node_closure_ret_type mrv1;
    struct bb_duplicate_node_closure_ret_type mrv2;
    struct bb_duplicate_node_closure_ret_type mrv3;
    struct bb_duplicate_node_closure_ret_type mrv4;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    unsigned char var_31;
    uint32_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-104L);
    var_3 = var_0 + (-64L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    var_5 = (uint64_t *)(var_0 + (-72L));
    *var_5 = rsi;
    var_6 = (uint64_t *)(var_0 + (-80L));
    *var_6 = rdx;
    var_7 = (uint64_t *)(var_0 + (-88L));
    *var_7 = rcx;
    var_8 = (uint32_t *)(var_0 + (-92L));
    var_9 = (uint32_t)r8;
    *var_8 = var_9;
    var_10 = (uint32_t *)(var_0 + (-28L));
    *var_10 = var_9;
    var_11 = *var_5;
    var_12 = (uint64_t *)(var_0 + (-16L));
    *var_12 = var_11;
    var_13 = *var_6;
    var_14 = (uint64_t *)(var_0 + (-24L));
    *var_14 = var_13;
    var_15 = (uint64_t *)(var_0 + (-40L));
    var_16 = (uint64_t *)(var_0 + (-48L));
    var_17 = (unsigned char *)(var_0 + (-49L));
    var_18 = (uint32_t *)(var_0 + (-56L));
    var_19 = var_13;
    r910_2 = r9;
    r89_2 = r8;
    r1011_2 = r10;
    rbx12_2 = rbx;
    local_sp_2 = var_2;
    rax_0 = 12UL;
    while (1U)
        {
            var_20 = (uint64_t **)var_3;
            var_21 = **var_20;
            var_22 = *var_12;
            var_23 = (*(unsigned char *)((var_21 + (var_22 << 4UL)) + 8UL) == '\x04');
            var_24 = *var_4;
            r89_0 = r89_2;
            r910_0 = r910_2;
            r1011_0 = r1011_2;
            rbx12_0 = rbx12_2;
            r910_3 = r910_2;
            r1011_3 = r1011_2;
            rbx12_3 = rbx12_2;
            r89_1 = r89_2;
            r910_1 = r910_2;
            r1011_1 = r1011_2;
            rbx12_1 = rbx12_2;
            r89_3 = r89_2;
            if (!var_23) {
                *var_15 = *(uint64_t *)(*(uint64_t *)(var_24 + 24UL) + (var_22 << 3UL));
                *(uint64_t *)(((*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL)) + 8UL) = 0UL;
                var_78 = (uint64_t)*var_10;
                var_79 = *var_15;
                var_80 = *var_4;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4262277UL;
                var_81 = indirect_placeholder_113(var_78, var_79, var_80, r89_2, r910_2, r1011_2, rbx12_2);
                *var_16 = var_81;
                if (var_81 != 18446744073709551615UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_82 = *(uint64_t *)(*var_4 + 24UL);
                *(uint64_t *)((*var_14 << 3UL) + var_82) = *(uint64_t *)((*var_12 << 3UL) + var_82);
                var_83 = (*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL);
                var_84 = *var_16;
                var_85 = local_sp_2 + (-16L);
                *(uint64_t *)var_85 = 4262396UL;
                var_86 = indirect_placeholder(var_84, var_83);
                var_87 = (unsigned char)var_86;
                *var_17 = var_87;
                local_sp_1 = var_85;
                if (var_87 != '\x01') {
                    loop_state_var = 1U;
                    break;
                }
                *var_12 = *var_15;
                var_88 = *var_16;
                *var_14 = var_88;
                var_19 = var_88;
                r910_2 = r910_1;
                r89_2 = r89_1;
                r1011_2 = r1011_1;
                rbx12_2 = rbx12_1;
                local_sp_2 = local_sp_1;
                continue;
            }
            var_25 = (var_22 * 24UL) + *(uint64_t *)(var_24 + 40UL);
            rax_0 = 0UL;
            switch_state_var = 0;
            switch (*(uint64_t *)(var_25 + 8UL)) {
              case 1UL:
                {
                    *var_15 = **(uint64_t **)(var_25 + 16UL);
                    *(uint64_t *)(((*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL)) + 8UL) = 0UL;
                    var_26 = *var_12;
                    if (var_26 != *var_7) {
                        var_27 = *var_14;
                        if (var_27 != var_26) {
                            var_28 = (var_27 * 24UL) + *(uint64_t *)(*var_4 + 40UL);
                            var_29 = *var_15;
                            *(uint64_t *)(local_sp_2 + (-8L)) = 4262696UL;
                            var_30 = indirect_placeholder(var_29, var_28);
                            var_31 = (unsigned char)var_30;
                            *var_17 = var_31;
                            if (var_31 != '\x01') {
                                loop_state_var = 1U;
                                switch_state_var = 1;
                                break;
                            }
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_32 = *var_10 | (uint32_t)((uint16_t)(*(uint32_t *)((**var_20 + (var_26 << 4UL)) + 8UL) >> 8U) & (unsigned short)1023U);
                    *var_10 = var_32;
                    var_33 = (uint64_t)var_32;
                    var_34 = *var_15;
                    var_35 = *var_4;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4262784UL;
                    var_36 = indirect_placeholder_113(var_33, var_34, var_35, r89_2, r910_2, r1011_2, rbx12_2);
                    *var_16 = var_36;
                    if (var_36 != 18446744073709551615UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_37 = (*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL);
                    var_38 = local_sp_2 + (-16L);
                    *(uint64_t *)var_38 = 4262858UL;
                    var_39 = indirect_placeholder(var_36, var_37);
                    var_40 = (unsigned char)var_39;
                    *var_17 = var_40;
                    local_sp_1 = var_38;
                    if (var_40 != '\x01') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    *var_12 = *var_15;
                    var_88 = *var_16;
                    *var_14 = var_88;
                    var_19 = var_88;
                    r910_2 = r910_1;
                    r89_2 = r89_1;
                    r1011_2 = r1011_1;
                    rbx12_2 = rbx12_1;
                    local_sp_2 = local_sp_1;
                    continue;
                }
                break;
              case 0UL:
                {
                    var_41 = *(uint64_t *)(var_24 + 24UL);
                    *(uint64_t *)((var_19 << 3UL) + var_41) = *(uint64_t *)((var_22 << 3UL) + var_41);
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    *var_15 = **(uint64_t **)(var_25 + 16UL);
                    *(uint64_t *)(((*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL)) + 8UL) = 0UL;
                    var_42 = (uint64_t)*var_10;
                    var_43 = *var_15;
                    var_44 = *var_4;
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4262987UL;
                    var_45 = indirect_placeholder_9(var_42, var_43, var_44);
                    *var_16 = var_45;
                    if (var_45 == 18446744073709551615UL) {
                        var_50 = (uint64_t)*var_10;
                        var_51 = *var_15;
                        var_52 = *var_4;
                        *(uint64_t *)(local_sp_2 + (-16L)) = 4263024UL;
                        var_53 = indirect_placeholder_113(var_50, var_51, var_52, r89_2, r910_2, r1011_2, rbx12_2);
                        *var_16 = var_53;
                        if (var_53 != 18446744073709551615UL) {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_54 = (*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL);
                        *(uint64_t *)(local_sp_2 + (-24L)) = 4263098UL;
                        var_55 = indirect_placeholder(var_53, var_54);
                        var_56 = (unsigned char)var_55;
                        *var_17 = var_56;
                        if (var_56 != '\x01') {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        var_57 = (uint64_t)*var_10;
                        var_58 = *var_7;
                        var_59 = *var_16;
                        var_60 = *var_15;
                        var_61 = *var_4;
                        var_62 = local_sp_2 + (-32L);
                        *(uint64_t *)var_62 = 4263156UL;
                        var_63 = indirect_placeholder_142(var_58, var_59, var_60, var_61, var_57, r910_2, r1011_2, rbx12_2);
                        var_64 = var_63.field_0;
                        var_65 = var_63.field_1;
                        var_66 = var_63.field_2;
                        var_67 = var_63.field_3;
                        var_68 = var_63.field_4;
                        var_69 = (uint32_t)var_64;
                        *var_18 = var_69;
                        local_sp_0 = var_62;
                        r89_0 = var_65;
                        r910_0 = var_66;
                        r1011_0 = var_67;
                        rbx12_0 = var_68;
                        r910_3 = var_66;
                        r1011_3 = var_67;
                        rbx12_3 = var_68;
                        r89_3 = var_65;
                        if (var_69 != 0U) {
                            rax_0 = (uint64_t)var_69;
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                    }
                    var_46 = (*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL);
                    var_47 = local_sp_2 + (-16L);
                    *(uint64_t *)var_47 = 4263226UL;
                    var_48 = indirect_placeholder(var_45, var_46);
                    var_49 = (unsigned char)var_48;
                    *var_17 = var_49;
                    local_sp_0 = var_47;
                    if (var_49 != '\x01') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_70 = *(uint64_t *)(*(uint64_t *)(((*var_12 * 24UL) + *(uint64_t *)(*var_4 + 40UL)) + 16UL) + 8UL);
                    *var_15 = var_70;
                    var_71 = (uint64_t)*var_10;
                    var_72 = *var_4;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4263316UL;
                    var_73 = indirect_placeholder_113(var_71, var_70, var_72, r89_0, r910_0, r1011_0, rbx12_0);
                    *var_16 = var_73;
                    r910_3 = r910_0;
                    r1011_3 = r1011_0;
                    rbx12_3 = rbx12_0;
                    r89_1 = r89_0;
                    r910_1 = r910_0;
                    r1011_1 = r1011_0;
                    rbx12_1 = rbx12_0;
                    r89_3 = r89_0;
                    if (var_73 != 18446744073709551615UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_74 = (*var_14 * 24UL) + *(uint64_t *)(*var_4 + 40UL);
                    var_75 = local_sp_0 + (-16L);
                    *(uint64_t *)var_75 = 4263387UL;
                    var_76 = indirect_placeholder(var_73, var_74);
                    var_77 = (unsigned char)var_76;
                    *var_17 = var_77;
                    local_sp_1 = var_75;
                    if (var_77 == '\x01') {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    *var_12 = *var_15;
                    var_88 = *var_16;
                    *var_14 = var_88;
                    var_19 = var_88;
                    r910_2 = r910_1;
                    r89_2 = r89_1;
                    r1011_2 = r1011_1;
                    rbx12_2 = rbx12_1;
                    local_sp_2 = local_sp_1;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
        }
        break;
      case 1U:
        {
            mrv.field_0 = rax_0;
            mrv1 = mrv;
            mrv1.field_1 = r89_3;
            mrv2 = mrv1;
            mrv2.field_2 = r910_3;
            mrv3 = mrv2;
            mrv3.field_3 = r1011_3;
            mrv4 = mrv3;
            mrv4.field_4 = rbx12_3;
            return mrv4;
        }
        break;
    }
}
