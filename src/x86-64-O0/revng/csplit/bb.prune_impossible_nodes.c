typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_166_ret_type;
struct indirect_placeholder_167_ret_type;
struct indirect_placeholder_166_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
struct indirect_placeholder_167_ret_type {
    uint64_t field_0;
    uint64_t field_1;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_10(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_13(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_166_ret_type indirect_placeholder_166(uint64_t param_0);
extern struct indirect_placeholder_167_ret_type indirect_placeholder_167(uint64_t param_0);
uint64_t bb_prune_impossible_nodes(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t storemerge;
    uint32_t *_pre_phi115;
    uint64_t local_sp_0;
    uint64_t var_53;
    uint64_t var_24;
    uint64_t var_40;
    uint64_t var_39;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t local_sp_1;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t *var_45;
    uint32_t *var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint32_t *var_54;
    uint32_t *_pre_phi113;
    uint64_t local_sp_2;
    bool var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_166_ret_type var_22;
    uint64_t var_23;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint32_t *var_51;
    uint64_t var_52;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_167_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t *var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-128L));
    *var_2 = rdi;
    var_3 = *(uint64_t *)(rdi + 152UL);
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = var_3;
    var_5 = var_0 + (-48L);
    var_6 = (uint64_t *)var_5;
    *var_6 = 0UL;
    var_7 = *(uint64_t *)(*var_2 + 168UL);
    var_8 = (uint64_t *)(var_0 + (-24L));
    *var_8 = var_7;
    var_9 = *(uint64_t *)(*var_2 + 176UL);
    var_10 = (uint64_t *)(var_0 + (-16L));
    *var_10 = var_9;
    var_11 = *var_8;
    storemerge = 12UL;
    if (var_11 > 2305843009213693950UL) {
        return storemerge;
    }
    var_12 = (var_11 << 3UL) + 8UL;
    var_13 = var_0 + (-144L);
    *(uint64_t *)var_13 = 4287232UL;
    var_14 = indirect_placeholder_167(var_12);
    var_15 = var_14.field_0;
    var_16 = var_0 + (-40L);
    var_17 = (uint64_t *)var_16;
    *var_17 = var_15;
    local_sp_2 = var_13;
    if (var_15 == 0UL) {
        var_54 = (uint32_t *)(var_0 + (-28L));
        *var_54 = 12U;
        _pre_phi113 = var_54;
    } else {
        var_18 = (*(uint64_t *)(*var_4 + 152UL) == 0UL);
        var_19 = *var_8;
        if (var_18) {
            var_46 = *var_10;
            var_47 = *var_6;
            var_48 = var_0 + (-120L);
            *(uint64_t *)(var_0 + (-152L)) = 4287730UL;
            indirect_placeholder_13(var_46, var_47, var_15, var_48, var_19);
            var_49 = *var_2;
            *(uint64_t *)(var_0 + (-160L)) = 4287749UL;
            var_50 = indirect_placeholder(var_48, var_49);
            var_51 = (uint32_t *)(var_0 + (-28L));
            *var_51 = (uint32_t)var_50;
            var_52 = var_0 + (-168L);
            *(uint64_t *)var_52 = 4287764UL;
            indirect_placeholder_2();
            _pre_phi115 = var_51;
            local_sp_0 = var_52;
            _pre_phi113 = var_51;
            local_sp_2 = var_52;
            if (*var_51 != 0U) {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4287897UL;
                indirect_placeholder_2();
                *(uint64_t *)(local_sp_2 + (-16L)) = 4287909UL;
                indirect_placeholder_2();
                storemerge = (uint64_t)*_pre_phi113;
                return storemerge;
            }
            if (**(uint64_t **)var_16 != 0UL) {
                *var_51 = 1U;
                *(uint64_t *)(local_sp_2 + (-8L)) = 4287897UL;
                indirect_placeholder_2();
                *(uint64_t *)(local_sp_2 + (-16L)) = 4287909UL;
                indirect_placeholder_2();
                storemerge = (uint64_t)*_pre_phi113;
                return storemerge;
            }
        }
        var_20 = (var_19 << 3UL) + 8UL;
        var_21 = var_0 + (-152L);
        *(uint64_t *)var_21 = 4287304UL;
        var_22 = indirect_placeholder_166(var_20);
        var_23 = var_22.field_0;
        *var_6 = var_23;
        local_sp_1 = var_21;
        local_sp_2 = var_21;
        if (var_23 != 0UL) {
            var_45 = (uint32_t *)(var_0 + (-28L));
            *var_45 = 12U;
            _pre_phi113 = var_45;
            *(uint64_t *)(local_sp_2 + (-8L)) = 4287897UL;
            indirect_placeholder_2();
            *(uint64_t *)(local_sp_2 + (-16L)) = 4287909UL;
            indirect_placeholder_2();
            storemerge = (uint64_t)*_pre_phi113;
            return storemerge;
        }
        var_24 = var_0 + (-120L);
        var_25 = (uint32_t *)(var_0 + (-28L));
        _pre_phi115 = var_25;
        _pre_phi113 = var_25;
        while (1U)
            {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4287369UL;
                indirect_placeholder_2();
                var_26 = *var_8;
                var_27 = *var_10;
                var_28 = *var_6;
                var_29 = *var_17;
                *(uint64_t *)(local_sp_1 + (-16L)) = 4287400UL;
                indirect_placeholder_13(var_27, var_28, var_29, var_24, var_26);
                var_30 = *var_2;
                *(uint64_t *)(local_sp_1 + (-24L)) = 4287419UL;
                var_31 = indirect_placeholder(var_24, var_30);
                *var_25 = (uint32_t)var_31;
                var_32 = local_sp_1 + (-32L);
                *(uint64_t *)var_32 = 4287434UL;
                indirect_placeholder_2();
                local_sp_2 = var_32;
                if (*var_25 != 0U) {
                    loop_state_var = 0U;
                    break;
                }
                if (**(uint64_t **)var_16 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                if (**(uint64_t **)var_5 != 0UL) {
                    loop_state_var = 1U;
                    break;
                }
                var_39 = *var_8;
                while (1U)
                    {
                        var_40 = var_39 + (-1L);
                        *var_8 = var_40;
                        var_39 = var_40;
                        if ((long)var_40 <= (long)18446744073709551615UL) {
                            *var_25 = 1U;
                            loop_state_var = 0U;
                            break;
                        }
                        var_41 = *var_2;
                        var_42 = *(uint64_t *)(*(uint64_t *)(var_41 + 184UL) + (var_40 << 3UL));
                        if (var_42 != 0UL) {
                            continue;
                        }
                        if ((*(unsigned char *)(var_42 + 104UL) & '\x10') == '\x00') {
                            continue;
                        }
                        loop_state_var = 1U;
                        break;
                    }
                switch_state_var = 0;
                switch (loop_state_var) {
                  case 1U:
                    {
                        var_43 = local_sp_1 + (-40L);
                        *(uint64_t *)var_43 = 4287619UL;
                        var_44 = indirect_placeholder_9(var_40, var_42, var_41);
                        *var_10 = var_44;
                        local_sp_1 = var_43;
                        continue;
                    }
                    break;
                  case 0U:
                    {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    break;
                }
                if (switch_state_var)
                    break;
            }
        switch (loop_state_var) {
          case 1U:
            {
                var_33 = *var_8 + 1UL;
                var_34 = *var_6;
                var_35 = *var_17;
                var_36 = *var_4;
                *(uint64_t *)(local_sp_1 + (-40L)) = 4287656UL;
                var_37 = indirect_placeholder_10(var_33, var_34, var_35, var_36);
                *var_25 = (uint32_t)var_37;
                var_38 = local_sp_1 + (-48L);
                *(uint64_t *)var_38 = 4287671UL;
                indirect_placeholder_2();
                *var_6 = 0UL;
                local_sp_0 = var_38;
                local_sp_2 = var_38;
                if (*var_25 != 0U) {
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4287897UL;
                    indirect_placeholder_2();
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4287909UL;
                    indirect_placeholder_2();
                    storemerge = (uint64_t)*_pre_phi113;
                    return storemerge;
                }
                var_53 = local_sp_0 + (-8L);
                *(uint64_t *)var_53 = 4287819UL;
                indirect_placeholder_2();
                *(uint64_t *)(*var_2 + 184UL) = *var_17;
                *var_17 = 0UL;
                *(uint64_t *)(*var_2 + 176UL) = *var_10;
                *(uint64_t *)(*var_2 + 168UL) = *var_8;
                *_pre_phi115 = 0U;
                _pre_phi113 = _pre_phi115;
                local_sp_2 = var_53;
            }
            break;
          case 0U:
            {
                *(uint64_t *)(local_sp_2 + (-8L)) = 4287897UL;
                indirect_placeholder_2();
                *(uint64_t *)(local_sp_2 + (-16L)) = 4287909UL;
                indirect_placeholder_2();
                storemerge = (uint64_t)*_pre_phi113;
                return storemerge;
            }
            break;
        }
    }
}
