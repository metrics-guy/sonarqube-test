typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_9(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_8(uint64_t param_0);
extern void indirect_placeholder_12(uint64_t param_0);
uint64_t bb_re_string_reconstruct(uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint32_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_172;
    uint64_t storemerge21;
    uint64_t var_151;
    uint64_t var_152;
    uint64_t var_33;
    uint64_t var_157;
    uint64_t local_sp_11;
    uint32_t var_84;
    uint32_t rax_1;
    uint32_t var_85;
    uint64_t local_sp_5;
    uint64_t var_75;
    uint64_t local_sp_4;
    uint64_t var_156;
    uint64_t local_sp_0;
    uint64_t var_159;
    uint64_t var_158;
    uint64_t local_sp_1;
    uint64_t *var_160;
    uint64_t *var_161;
    uint64_t var_146;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t var_149;
    uint64_t var_150;
    uint64_t var_143;
    uint64_t *var_144;
    uint64_t *var_145;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_142;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t *var_134;
    uint64_t var_135;
    uint64_t var_121;
    uint64_t var_120;
    uint64_t var_141;
    uint64_t local_sp_2;
    uint64_t *_pre_phi402;
    uint64_t var_136;
    uint64_t var_137;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t var_140;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_15;
    uint64_t var_14;
    uint64_t local_sp_3;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t *var_19;
    uint64_t var_20;
    uint64_t local_sp_12;
    uint64_t var_21;
    uint64_t local_sp_9;
    uint64_t *var_100;
    uint64_t var_101;
    uint64_t *var_102;
    uint64_t *var_103;
    uint64_t var_115;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t *var_109;
    uint64_t var_110;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    uint64_t var_116;
    uint64_t _pre395;
    uint64_t _pre396;
    uint64_t _pre_phi408;
    uint64_t *_pre_phi406;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t var_122;
    uint64_t var_123;
    uint64_t var_124;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t var_127;
    uint64_t var_128;
    uint64_t var_129;
    uint64_t var_153;
    uint64_t var_154;
    uint64_t var_155;
    uint64_t var_22;
    uint64_t *var_23;
    uint64_t var_24;
    uint64_t var_26;
    uint64_t var_25;
    uint64_t var_27;
    uint64_t var_39;
    uint32_t *var_40;
    uint64_t var_41;
    uint64_t **var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_45;
    uint64_t *var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t *var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint64_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint32_t *var_61;
    uint32_t var_62;
    uint32_t var_63;
    uint32_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t *var_72;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_76;
    uint32_t var_77;
    uint32_t var_82;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t local_sp_6;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t var_83;
    uint64_t local_sp_7;
    uint64_t local_sp_8;
    uint64_t var_91;
    uint64_t *var_92;
    uint64_t *_pre_phi412;
    uint64_t *var_93;
    uint64_t var_99;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t *var_96;
    uint64_t var_97;
    uint64_t _pre391;
    uint64_t var_98;
    uint64_t local_sp_10;
    uint64_t var_162;
    uint32_t var_34;
    uint64_t *var_163;
    unsigned char var_28;
    uint32_t *var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint32_t var_32;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t *var_164;
    uint64_t *var_165;
    uint64_t var_166;
    bool var_167;
    uint64_t *var_168;
    uint64_t var_169;
    uint32_t *var_170;
    uint32_t var_171;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    var_4 = var_0 + (-184L);
    var_5 = var_0 + (-160L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-168L));
    *var_7 = rsi;
    var_8 = (uint32_t *)(var_0 + (-172L));
    *var_8 = (uint32_t)rdx;
    var_9 = *var_6;
    var_10 = *(uint64_t *)(var_9 + 40UL);
    var_11 = *var_7;
    rax_0 = 1U;
    storemerge21 = 0UL;
    rax_1 = 1U;
    local_sp_4 = var_4;
    var_146 = 0UL;
    var_136 = 0UL;
    var_15 = var_9;
    local_sp_3 = var_4;
    var_94 = 0UL;
    if ((long)var_11 < (long)var_10) {
        var_12 = var_11 - var_10;
        var_13 = (uint64_t *)(var_0 + (-16L));
        *var_13 = var_12;
        _pre_phi402 = var_13;
    } else {
        if ((int)*(uint32_t *)(var_9 + 144UL) > (int)1U) {
            var_14 = var_0 + (-192L);
            *(uint64_t *)var_14 = 4239784UL;
            indirect_placeholder_2();
            var_15 = *var_6;
            local_sp_3 = var_14;
        }
        *(uint64_t *)(var_15 + 88UL) = *(uint64_t *)(var_15 + 80UL);
        var_16 = *var_6;
        *(uint64_t *)(var_16 + 104UL) = *(uint64_t *)(var_16 + 96UL);
        *(uint64_t *)(*var_6 + 48UL) = 0UL;
        *(uint64_t *)(*var_6 + 40UL) = 0UL;
        *(uint64_t *)(*var_6 + 56UL) = 0UL;
        *(unsigned char *)(*var_6 + 140UL) = (unsigned char)'\x00';
        *(uint32_t *)(*var_6 + 112UL) = (((*var_8 & 1U) == 0U) ? 6U : 4U);
        var_17 = *var_6;
        local_sp_4 = local_sp_3;
        if (*(unsigned char *)(var_17 + 139UL) == '\x00') {
            *(uint64_t *)(var_17 + 8UL) = **(uint64_t **)var_5;
        }
        var_18 = *var_7;
        var_19 = (uint64_t *)(var_0 + (-16L));
        *var_19 = var_18;
        _pre_phi402 = var_19;
    }
    var_20 = *_pre_phi402;
    local_sp_5 = local_sp_4;
    local_sp_12 = local_sp_4;
    if (var_20 == 0UL) {
        *(uint64_t *)(*var_6 + 40UL) = *var_7;
        var_164 = (uint64_t *)(*var_6 + 88UL);
        *var_164 = (*var_164 - *_pre_phi402);
        var_165 = (uint64_t *)(*var_6 + 104UL);
        *var_165 = (*var_165 - *_pre_phi402);
        var_166 = *var_6;
        if ((int)*(uint32_t *)(var_166 + 144UL) <= (int)1U) {
            if (*(unsigned char *)(var_166 + 139UL) == '\x00') {
                *(uint64_t *)(var_166 + 48UL) = *(uint64_t *)(var_166 + 88UL);
            } else {
                if (*(unsigned char *)(var_166 + 136UL) == '\x00') {
                    *(uint64_t *)(local_sp_12 + (-8L)) = 4242834UL;
                    indirect_placeholder_12(var_166);
                } else {
                    if (*(uint64_t *)(var_166 + 120UL) == 0UL) {
                        *(uint64_t *)(local_sp_12 + (-8L)) = 4242867UL;
                        indirect_placeholder_12(var_166);
                    }
                }
            }
            *(uint64_t *)(*var_6 + 72UL) = 0UL;
            return storemerge21;
        }
        var_167 = (*(unsigned char *)(var_166 + 136UL) == '\x00');
        var_168 = (uint64_t *)(local_sp_12 + (-8L));
        if (!var_167) {
            *var_168 = 4242732UL;
            var_169 = indirect_placeholder_8(var_166);
            var_170 = (uint32_t *)(var_0 + (-116L));
            var_171 = (uint32_t)var_169;
            *var_170 = var_171;
            if (var_171 != 0U) {
                var_172 = (uint64_t)var_171;
                storemerge21 = var_172;
                return storemerge21;
            }
        }
        *var_168 = 4242777UL;
        indirect_placeholder_12(var_166);
    } else {
        var_21 = *var_6;
        if ((long)var_20 < (long)*(uint64_t *)(var_21 + 56UL)) {
            if (*(unsigned char *)(var_21 + 140UL) == '\x00') {
                var_151 = var_20 + (-1L);
                var_152 = (uint64_t)*var_8;
                var_153 = local_sp_4 + (-8L);
                *(uint64_t *)var_153 = 4241079UL;
                var_154 = indirect_placeholder_9(var_152, var_151, var_21);
                *(uint32_t *)(*var_6 + 112UL) = (uint32_t)var_154;
                var_155 = *var_6;
                var_157 = var_155;
                local_sp_0 = var_153;
                if ((int)*(uint32_t *)(var_155 + 144UL) > (int)1U) {
                    var_156 = local_sp_4 + (-16L);
                    *(uint64_t *)var_156 = 4241174UL;
                    indirect_placeholder_2();
                    var_157 = *var_6;
                    local_sp_0 = var_156;
                }
                var_159 = var_157;
                local_sp_1 = local_sp_0;
                if (*(unsigned char *)(var_157 + 139UL) != '\x00') {
                    var_158 = local_sp_0 + (-8L);
                    *(uint64_t *)var_158 = 4241258UL;
                    indirect_placeholder_2();
                    var_159 = *var_6;
                    local_sp_1 = var_158;
                }
                var_160 = (uint64_t *)(var_159 + 48UL);
                *var_160 = (*var_160 - *_pre_phi402);
                var_161 = (uint64_t *)(*var_6 + 56UL);
                *var_161 = (*var_161 - *_pre_phi402);
                local_sp_11 = local_sp_1;
            } else {
                var_100 = (uint64_t *)(var_0 + (-24L));
                *var_100 = 0UL;
                var_101 = *(uint64_t *)(*var_6 + 48UL);
                var_102 = (uint64_t *)(var_0 + (-32L));
                *var_102 = var_101;
                var_103 = (uint64_t *)(var_0 + (-40L));
                var_104 = *var_100;
                var_105 = var_101;
                while (1U)
                    {
                        var_106 = var_104 + var_105;
                        var_107 = (uint64_t)((long)(var_106 + (var_106 >> 63UL)) >> (long)1UL);
                        *var_103 = var_107;
                        var_108 = *var_6;
                        var_109 = (uint64_t *)(var_108 + 24UL);
                        var_110 = *var_109;
                        var_111 = var_107 << 3UL;
                        var_112 = *(uint64_t *)(var_110 + var_111);
                        var_113 = *_pre_phi402;
                        var_115 = var_107;
                        _pre_phi408 = var_111;
                        _pre_phi406 = var_109;
                        var_117 = var_113;
                        var_118 = var_107;
                        var_119 = var_108;
                        if ((long)var_113 < (long)var_112) {
                            *var_102 = var_107;
                            var_116 = *var_100;
                        } else {
                            if ((long)var_113 <= (long)var_112) {
                                break;
                            }
                            var_114 = var_107 + 1UL;
                            *var_100 = var_114;
                            var_115 = *var_102;
                            var_116 = var_114;
                        }
                        var_104 = var_116;
                        var_105 = var_115;
                        if ((long)var_116 < (long)var_115) {
                            continue;
                        }
                        _pre395 = *var_6;
                        _pre396 = *var_103;
                        _pre_phi408 = _pre396 << 3UL;
                        _pre_phi406 = (uint64_t *)(_pre395 + 24UL);
                        var_117 = *_pre_phi402;
                        var_118 = _pre396;
                        var_119 = _pre395;
                        break;
                    }
                var_121 = var_119;
                var_122 = var_118;
                if ((long)var_117 > (long)*(uint64_t *)(*_pre_phi406 + _pre_phi408)) {
                    var_120 = var_118 + 1UL;
                    *var_103 = var_120;
                    var_121 = *var_6;
                    var_122 = var_120;
                }
                var_123 = var_122 + (-1L);
                var_124 = (uint64_t)*var_8;
                var_125 = local_sp_4 + (-8L);
                *(uint64_t *)var_125 = 4240264UL;
                var_126 = indirect_placeholder_9(var_124, var_123, var_121);
                *(uint32_t *)(*var_6 + 112UL) = (uint32_t)var_126;
                var_127 = *var_6;
                var_128 = *(uint64_t *)(var_127 + 48UL);
                var_129 = *_pre_phi402;
                local_sp_2 = var_125;
                if ((long)var_129 < (long)var_128) {
                    *(uint64_t *)(var_127 + 88UL) = ((*(uint64_t *)(var_127 + 80UL) - *var_7) + var_129);
                    var_130 = *var_6;
                    *(uint64_t *)(var_130 + 104UL) = ((*(uint64_t *)(var_130 + 96UL) - *var_7) + *_pre_phi402);
                    *(unsigned char *)(*var_6 + 140UL) = (unsigned char)'\x00';
                    var_131 = *var_103;
                    var_132 = var_131;
                    while ((long)var_131 <= (long)0UL)
                        {
                            if (*_pre_phi402 == *(uint64_t *)(*(uint64_t *)(*var_6 + 24UL) + ((var_131 << 3UL) + (-8L)))) {
                                break;
                            }
                            var_142 = var_131 + (-1L);
                            *var_103 = var_142;
                            var_131 = var_142;
                            var_132 = var_131;
                        }
                    var_133 = *var_6;
                    var_134 = (uint64_t *)(var_133 + 48UL);
                    var_135 = *var_134;
                    while ((long)var_132 >= (long)var_135)
                        {
                            if (*(uint32_t *)(*(uint64_t *)(var_133 + 16UL) + (var_132 << 2UL)) == 4294967295U) {
                                break;
                            }
                            var_141 = var_132 + 1UL;
                            *var_103 = var_141;
                            var_132 = var_141;
                            var_133 = *var_6;
                            var_134 = (uint64_t *)(var_133 + 48UL);
                            var_135 = *var_134;
                        }
                    if (var_132 == var_135) {
                        *var_134 = 0UL;
                    } else {
                        *var_134 = (*(uint64_t *)(*(uint64_t *)(var_133 + 24UL) + (var_132 << 3UL)) - *_pre_phi402);
                        if (*(uint64_t *)(*var_6 + 48UL) != 0UL) {
                            *var_100 = 0UL;
                            var_137 = *var_6;
                            while ((long)var_136 >= (long)*(uint64_t *)(var_137 + 48UL))
                                {
                                    *(uint32_t *)(*(uint64_t *)(var_137 + 16UL) + (var_136 << 2UL)) = 4294967295U;
                                    var_139 = *var_100 + 1UL;
                                    *var_100 = var_139;
                                    var_136 = var_139;
                                    var_137 = *var_6;
                                }
                            var_138 = local_sp_4 + (-16L);
                            *(uint64_t *)var_138 = 4241020UL;
                            indirect_placeholder_2();
                            local_sp_2 = var_138;
                        }
                    }
                    var_140 = *var_6;
                    *(uint64_t *)(var_140 + 56UL) = *(uint64_t *)(var_140 + 48UL);
                    local_sp_11 = local_sp_2;
                } else {
                    if (*var_103 != var_129 & var_129 != *(uint64_t *)(*(uint64_t *)(var_127 + 24UL) + (var_129 << 3UL))) {
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4240411UL;
                        indirect_placeholder_2();
                        var_143 = local_sp_4 + (-24L);
                        *(uint64_t *)var_143 = 4240473UL;
                        indirect_placeholder_2();
                        var_144 = (uint64_t *)(*var_6 + 48UL);
                        *var_144 = (*var_144 - *_pre_phi402);
                        var_145 = (uint64_t *)(*var_6 + 56UL);
                        *var_145 = (*var_145 - *_pre_phi402);
                        *var_100 = 0UL;
                        local_sp_11 = var_143;
                        var_147 = *var_6;
                        while ((long)var_146 >= (long)*(uint64_t *)(var_147 + 48UL))
                            {
                                var_148 = *(uint64_t *)(var_147 + 24UL);
                                var_149 = *_pre_phi402;
                                *(uint64_t *)(var_148 + (var_146 << 3UL)) = (*(uint64_t *)(var_148 + ((var_149 + var_146) << 3UL)) - var_149);
                                var_150 = *var_100 + 1UL;
                                *var_100 = var_150;
                                var_146 = var_150;
                                var_147 = *var_6;
                            }
                        var_162 = *var_6;
                        local_sp_12 = local_sp_11;
                        if (*(unsigned char *)(var_162 + 139UL) == '\x00') {
                            var_163 = (uint64_t *)(var_162 + 8UL);
                            *var_163 = (*var_163 + *_pre_phi402);
                        }
                        *(uint64_t *)(*var_6 + 40UL) = *var_7;
                        var_164 = (uint64_t *)(*var_6 + 88UL);
                        *var_164 = (*var_164 - *_pre_phi402);
                        var_165 = (uint64_t *)(*var_6 + 104UL);
                        *var_165 = (*var_165 - *_pre_phi402);
                        var_166 = *var_6;
                        if ((int)*(uint32_t *)(var_166 + 144UL) <= (int)1U) {
                            if (*(unsigned char *)(var_166 + 139UL) == '\x00') {
                                *(uint64_t *)(var_166 + 48UL) = *(uint64_t *)(var_166 + 88UL);
                            } else {
                                if (*(unsigned char *)(var_166 + 136UL) == '\x00') {
                                    *(uint64_t *)(local_sp_12 + (-8L)) = 4242834UL;
                                    indirect_placeholder_12(var_166);
                                } else {
                                    if (*(uint64_t *)(var_166 + 120UL) == 0UL) {
                                        *(uint64_t *)(local_sp_12 + (-8L)) = 4242867UL;
                                        indirect_placeholder_12(var_166);
                                    }
                                }
                            }
                            *(uint64_t *)(*var_6 + 72UL) = 0UL;
                            return storemerge21;
                        }
                        var_167 = (*(unsigned char *)(var_166 + 136UL) == '\x00');
                        var_168 = (uint64_t *)(local_sp_12 + (-8L));
                        if (!var_167) {
                            *var_168 = 4242732UL;
                            var_169 = indirect_placeholder_8(var_166);
                            var_170 = (uint32_t *)(var_0 + (-116L));
                            var_171 = (uint32_t)var_169;
                            *var_170 = var_171;
                            if (var_171 != 0U) {
                                var_172 = (uint64_t)var_171;
                                storemerge21 = var_172;
                                return storemerge21;
                            }
                        }
                        *var_168 = 4242777UL;
                        indirect_placeholder_12(var_166);
                    }
                }
            }
        } else {
            var_22 = *(uint64_t *)(var_21 + 48UL);
            var_23 = (uint64_t *)(var_0 + (-88L));
            *var_23 = var_22;
            var_24 = *var_6;
            var_26 = var_24;
            if (*(unsigned char *)(var_24 + 140UL) == '\x00') {
                *(uint64_t *)(var_24 + 88UL) = ((*(uint64_t *)(var_24 + 80UL) - *var_7) + *_pre_phi402);
                var_25 = *var_6;
                *(uint64_t *)(var_25 + 104UL) = ((*(uint64_t *)(var_25 + 96UL) - *var_7) + *_pre_phi402);
                *(unsigned char *)(*var_6 + 140UL) = (unsigned char)'\x00';
                var_26 = *var_6;
            }
            *(uint64_t *)(var_26 + 48UL) = 0UL;
            var_27 = *var_6;
            if ((int)*(uint32_t *)(var_27 + 144UL) > (int)1U) {
                var_39 = var_0 + (-120L);
                var_40 = (uint32_t *)var_39;
                *var_40 = 4294967295U;
                var_41 = *var_6;
                if (*(unsigned char *)(var_41 + 137UL) == '\x00') {
                    var_77 = *var_40;
                    var_82 = var_77;
                    local_sp_6 = local_sp_5;
                    if (var_77 == 4294967295U) {
                        var_78 = *var_7;
                        var_79 = *var_6;
                        var_80 = local_sp_5 + (-8L);
                        *(uint64_t *)var_80 = 4242027UL;
                        var_81 = indirect_placeholder_9(var_39, var_78, var_79);
                        *(uint64_t *)(*var_6 + 48UL) = (var_81 - *var_7);
                        var_82 = *var_40;
                        local_sp_6 = var_80;
                    }
                    var_85 = var_82;
                    local_sp_7 = local_sp_6;
                    if (var_82 == 4294967295U) {
                        var_86 = *var_23 + (-1L);
                        var_87 = (uint64_t)*var_8;
                        var_88 = *var_6;
                        var_89 = local_sp_6 + (-8L);
                        *(uint64_t *)var_89 = 4242088UL;
                        var_90 = indirect_placeholder_9(var_87, var_86, var_88);
                        *(uint32_t *)(*var_6 + 112UL) = (uint32_t)var_90;
                        local_sp_9 = var_89;
                    } else {
                        if (*(unsigned char *)(*var_6 + 142UL) == '\x00') {
                            var_83 = local_sp_6 + (-8L);
                            *(uint64_t *)var_83 = 4242137UL;
                            indirect_placeholder_2();
                            local_sp_7 = var_83;
                            local_sp_8 = var_83;
                            var_84 = *var_40;
                            var_85 = var_84;
                            if (var_82 != 0U & var_84 != 95U) {
                                rax_1 = 0U;
                                local_sp_8 = local_sp_7;
                                if (var_85 == 10U) {
                                } else {
                                    rax_1 = 2U;
                                    if (*(unsigned char *)(*var_6 + 141UL) == '\x00') {
                                    }
                                }
                            }
                        } else {
                            rax_1 = 0U;
                            local_sp_8 = local_sp_7;
                            if (var_85 == 10U) {
                            } else {
                                rax_1 = 2U;
                                if (*(unsigned char *)(*var_6 + 141UL) == '\x00') {
                                }
                            }
                        }
                        *(uint32_t *)(*var_6 + 112UL) = rax_1;
                        local_sp_9 = local_sp_8;
                    }
                    var_91 = *var_6;
                    var_92 = (uint64_t *)(var_91 + 48UL);
                    _pre_phi412 = var_92;
                    var_99 = var_91;
                    local_sp_10 = local_sp_9;
                    if (*var_92 != 0UL) {
                        var_93 = (uint64_t *)(var_0 + (-48L));
                        *var_93 = 0UL;
                        var_95 = *var_6;
                        var_96 = (uint64_t *)(var_95 + 48UL);
                        _pre_phi412 = var_96;
                        var_99 = var_95;
                        while ((long)var_94 >= (long)*var_96)
                            {
                                *(uint32_t *)(*(uint64_t *)(var_95 + 16UL) + (var_94 << 2UL)) = 4294967295U;
                                var_98 = *var_93 + 1UL;
                                *var_93 = var_98;
                                var_94 = var_98;
                                var_95 = *var_6;
                                var_96 = (uint64_t *)(var_95 + 48UL);
                                _pre_phi412 = var_96;
                                var_99 = var_95;
                            }
                        if (*(unsigned char *)(var_95 + 139UL) == '\x00') {
                            var_97 = local_sp_9 + (-8L);
                            *(uint64_t *)var_97 = 4242336UL;
                            indirect_placeholder_2();
                            _pre391 = *var_6;
                            _pre_phi412 = (uint64_t *)(_pre391 + 48UL);
                            var_99 = _pre391;
                            local_sp_10 = var_97;
                        }
                    }
                    *(uint64_t *)(var_99 + 56UL) = *_pre_phi412;
                    local_sp_11 = local_sp_10;
                } else {
                    var_42 = (uint64_t **)var_5;
                    var_43 = *(uint64_t *)(var_41 + 40UL) + **var_42;
                    var_44 = (uint64_t *)(var_0 + (-96L));
                    *var_44 = var_43;
                    var_45 = var_43 + (*_pre_phi402 - (uint64_t)*(uint32_t *)(*var_6 + 144UL));
                    var_46 = (uint64_t *)(var_0 + (-64L));
                    *var_46 = var_45;
                    var_47 = **var_42;
                    var_48 = helper_cc_compute_c_wrapper(var_45 - var_47, var_47, var_2, 17U);
                    if (var_48 == 0UL) {
                        *var_46 = **var_42;
                    }
                    var_49 = *var_44 + (*_pre_phi402 + (-1L));
                    var_50 = var_0 + (-56L);
                    var_51 = (uint64_t *)var_50;
                    *var_51 = var_49;
                    var_52 = var_49;
                    while (1U)
                        {
                            var_53 = *var_46;
                            var_54 = helper_cc_compute_c_wrapper(var_52 - var_53, var_53, var_2, 17U);
                            if (var_54 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            if ((uint64_t)(((uint32_t)(uint64_t)(**(unsigned char **)var_50 & '\xc0') + (-128)) & (-64)) == 0UL) {
                                var_76 = *var_51 + (-1L);
                                *var_51 = var_76;
                                var_52 = var_76;
                                continue;
                            }
                            var_55 = (*var_44 + *(uint64_t *)(*var_6 + 88UL)) - *var_51;
                            var_56 = (uint64_t *)(var_0 + (-104L));
                            *var_56 = var_55;
                            var_57 = *var_51;
                            var_58 = (uint64_t *)(var_0 + (-72L));
                            *var_58 = var_57;
                            if (*(uint64_t *)(*var_6 + 120UL) != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_59 = *var_56;
                            var_60 = ((long)var_59 > (long)6UL) ? 6UL : var_59;
                            var_61 = (uint32_t *)(var_0 + (-76L));
                            var_62 = (uint32_t)var_60;
                            *var_61 = var_62;
                            var_63 = var_62;
                            loop_state_var = 2U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            *(uint64_t *)(local_sp_4 + (-8L)) = 4241836UL;
                            indirect_placeholder_2();
                            var_67 = *var_56;
                            var_68 = *var_58;
                            var_69 = var_0 + (-132L);
                            var_70 = local_sp_4 + (-16L);
                            *(uint64_t *)var_70 = 4241860UL;
                            var_71 = indirect_placeholder_9(var_67, var_68, var_69);
                            var_72 = (uint64_t *)(var_0 + (-112L));
                            *var_72 = var_71;
                            var_73 = (*var_44 + *_pre_phi402) - *var_51;
                            var_74 = helper_cc_compute_c_wrapper(var_71 - var_73, var_73, var_2, 17U);
                            local_sp_5 = var_70;
                            if (var_74 != 0UL & *var_72 > 18446744073709551613UL) {
                                var_75 = local_sp_4 + (-24L);
                                *(uint64_t *)var_75 = 4241921UL;
                                indirect_placeholder_2();
                                *(uint64_t *)(*var_6 + 48UL) = (*var_72 + (*var_51 - (*var_44 + *_pre_phi402)));
                                *var_40 = *(uint32_t *)var_69;
                                local_sp_5 = var_75;
                            }
                        }
                        break;
                      case 2U:
                        {
                            var_64 = var_63 + (-1);
                            *var_61 = var_64;
                            while ((int)var_64 <= (int)4294967295U)
                                {
                                    var_65 = *(uint64_t *)(*var_6 + 120UL);
                                    var_66 = (uint64_t)var_64;
                                    *(unsigned char *)((var_3 + var_66) + (-130L)) = *(unsigned char *)(var_65 + (uint64_t)*(unsigned char *)(*var_51 + var_66));
                                    var_63 = *var_61;
                                    var_64 = var_63 + (-1);
                                    *var_61 = var_64;
                                }
                            *var_58 = (var_0 + (-138L));
                        }
                        break;
                    }
                }
            } else {
                var_28 = *(unsigned char *)(((*_pre_phi402 + *(uint64_t *)(var_27 + 40UL)) + (-1L)) + **(uint64_t **)var_5);
                var_29 = (uint32_t *)(var_0 + (-80L));
                *var_29 = (uint32_t)var_28;
                *(uint64_t *)(*var_6 + 56UL) = 0UL;
                var_30 = *var_6;
                var_31 = *(uint64_t *)(var_30 + 120UL);
                var_33 = var_30;
                if (var_31 == 0UL) {
                    var_34 = *var_29;
                } else {
                    var_32 = (uint32_t)*(unsigned char *)(var_31 + (uint64_t)*var_29);
                    *var_29 = var_32;
                    var_33 = *var_6;
                    var_34 = var_32;
                }
                var_35 = (uint64_t)var_34;
                var_36 = *(uint64_t *)(var_33 + 128UL);
                var_37 = local_sp_4 + (-8L);
                *(uint64_t *)var_37 = 4242497UL;
                var_38 = indirect_placeholder(var_35, var_36);
                local_sp_11 = var_37;
                if ((uint64_t)(unsigned char)var_38 != 0UL) {
                    rax_0 = 0U;
                    if (*var_29 == 10U) {
                    } else {
                        rax_0 = 2U;
                        if (*(unsigned char *)(*var_6 + 141UL) == '\x00') {
                        }
                    }
                }
                *(uint32_t *)(*var_6 + 112UL) = rax_0;
                var_162 = *var_6;
                local_sp_12 = local_sp_11;
                if (*(unsigned char *)(var_162 + 139UL) == '\x00') {
                    var_163 = (uint64_t *)(var_162 + 8UL);
                    *var_163 = (*var_163 + *_pre_phi402);
                }
            }
        }
    }
}
