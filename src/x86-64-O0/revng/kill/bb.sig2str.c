typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_rax(void);
uint64_t bb_sig2str(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint32_t *var_5;
    uint32_t var_18;
    bool var_19;
    uint64_t var_20;
    uint32_t *_cast2;
    uint32_t var_21;
    uint32_t *var_22;
    uint32_t *_pre_phi36;
    uint32_t var_23;
    uint32_t *var_24;
    uint32_t var_25;
    uint64_t rax_0;
    uint32_t var_6;
    uint64_t rax_1;
    uint32_t *var_10;
    uint32_t var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint32_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_7;
    uint64_t var_8;
    uint32_t var_9;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rax();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = (uint32_t *)(var_0 + (-44L));
    *var_3 = (uint32_t)rdi;
    var_4 = (uint64_t *)(var_0 + (-56L));
    *var_4 = rsi;
    var_5 = (uint32_t *)(var_0 + (-12L));
    *var_5 = 0U;
    rax_0 = 4294967295UL;
    var_6 = 0U;
    rax_1 = var_1;
    while (1U)
        {
            if (var_6 > 33U) {
                var_7 = *(uint32_t *)(((uint64_t)var_6 * 12UL) + 4289312UL);
                var_8 = (uint64_t)var_7;
                rax_0 = 0UL;
                rax_1 = var_8;
                if ((uint64_t)(*var_3 - var_7) == 0UL) {
                    var_9 = var_6 + 1U;
                    *var_5 = var_9;
                    var_6 = var_9;
                    continue;
                }
                *(uint64_t *)(var_0 + (-64L)) = 4220289UL;
                indirect_placeholder();
                loop_state_var = 0U;
                break;
            }
            *(uint64_t *)(var_0 + (-64L)) = 4220314UL;
            indirect_placeholder();
            var_10 = (uint32_t *)(var_0 + (-20L));
            var_11 = (uint32_t)rax_1;
            *var_10 = var_11;
            *(uint64_t *)(var_0 + (-72L)) = 4220322UL;
            indirect_placeholder();
            var_12 = (uint32_t *)(var_0 + (-24L));
            *var_12 = var_11;
            var_13 = *var_10;
            var_14 = (uint64_t)var_13;
            var_15 = *var_3;
            var_16 = var_14 << 32UL;
            var_17 = (uint64_t)var_15 << 32UL;
            if ((long)var_16 <= (long)var_17) {
                loop_state_var = 0U;
                break;
            }
            if ((long)var_17 <= (long)((uint64_t)var_11 << 32UL)) {
                loop_state_var = 0U;
                break;
            }
            var_18 = var_11 - var_13;
            var_19 = ((long)(uint64_t)((long)(((uint64_t)((long)((uint64_t)(var_18 + (uint32_t)(((uint64_t)var_18 >> 31UL) & 1UL)) << 32UL) >> (long)33UL) + var_14) << 32UL) >> (long)32UL) < (long)(uint64_t)var_15);
            var_20 = *var_4;
            _cast2 = (uint32_t *)var_20;
            rax_0 = 0UL;
            if (!var_19) {
                *_cast2 = 1095586898U;
                *(uint16_t *)(var_20 + 4UL) = (uint16_t)(unsigned short)88U;
                var_23 = *var_12;
                var_24 = (uint32_t *)(var_0 + (-16L));
                *var_24 = var_23;
                _pre_phi36 = var_24;
                loop_state_var = 1U;
                break;
            }
            *_cast2 = 1229804626U;
            *(uint16_t *)(var_20 + 4UL) = (uint16_t)(unsigned short)78U;
            var_21 = *var_10;
            var_22 = (uint32_t *)(var_0 + (-16L));
            *var_22 = var_21;
            _pre_phi36 = var_22;
            loop_state_var = 1U;
            break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            return rax_0;
        }
        break;
      case 1U:
        {
            var_25 = *var_3 - *_pre_phi36;
            *(uint32_t *)(var_0 + (-28L)) = var_25;
            if (var_25 == 0U) {
                *(uint64_t *)(var_0 + (-80L)) = 4220467UL;
                indirect_placeholder();
            }
        }
        break;
    }
}
