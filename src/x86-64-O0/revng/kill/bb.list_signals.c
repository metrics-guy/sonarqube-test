typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
uint64_t bb_list_signals(uint64_t rsi, uint64_t rdi) {
    uint32_t *var_45;
    uint64_t var_46;
    uint64_t local_sp_9;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    unsigned char *var_4;
    uint32_t *var_5;
    uint64_t var_51;
    uint64_t local_sp_0;
    uint32_t var_52;
    uint32_t var_47;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t local_sp_8;
    bool var_42;
    uint64_t var_43;
    uint64_t *var_44;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t local_sp_2;
    uint32_t var_35;
    uint32_t var_27;
    uint64_t local_sp_6;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_17;
    uint64_t local_sp_4;
    uint32_t var_18;
    uint32_t var_13;
    uint32_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    uint32_t var_9;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t local_sp_5;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t local_sp_7;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint32_t var_10;
    uint64_t var_36;
    uint32_t *var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t var_41;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-72L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rsi;
    var_4 = (unsigned char *)(var_0 + (-60L));
    *var_4 = (unsigned char)rdi;
    var_5 = (uint32_t *)(var_0 + (-16L));
    *var_5 = 0U;
    local_sp_9 = var_2;
    var_47 = 1U;
    local_sp_8 = var_2;
    var_27 = 1U;
    var_13 = 1U;
    var_9 = 1U;
    local_sp_5 = var_2;
    if (*var_4 == '\x00') {
        if (*var_3 == 0UL) {
            var_45 = (uint32_t *)(var_0 + (-12L));
            *var_45 = 1U;
            var_46 = var_0 + (-56L);
            while ((int)var_47 <= (int)64U)
                {
                    var_48 = (uint64_t)var_47;
                    var_49 = local_sp_9 + (-8L);
                    *(uint64_t *)var_49 = 4205358UL;
                    var_50 = indirect_placeholder_1(var_46, var_48);
                    local_sp_0 = var_49;
                    if ((uint64_t)(uint32_t)var_50 == 0UL) {
                        var_51 = local_sp_9 + (-16L);
                        *(uint64_t *)var_51 = 4205374UL;
                        indirect_placeholder();
                        local_sp_0 = var_51;
                    }
                    var_52 = *var_45 + 1U;
                    *var_45 = var_52;
                    var_47 = var_52;
                    local_sp_9 = local_sp_0;
                }
        }
        var_36 = var_0 + (-56L);
        var_37 = (uint32_t *)(var_0 + (-12L));
        var_38 = **(uint64_t **)var_2;
        while (var_38 != 0UL)
            {
                var_39 = local_sp_8 + (-8L);
                *(uint64_t *)var_39 = 4205240UL;
                var_40 = indirect_placeholder_1(var_36, var_38);
                var_41 = (uint32_t)var_40;
                *var_37 = var_41;
                local_sp_1 = var_39;
                if ((int)var_41 > (int)4294967295U) {
                    *var_5 = 1U;
                } else {
                    var_42 = ((uint64_t)(((uint32_t)(uint64_t)***(unsigned char ***)var_2 + (-48)) & (-2)) > 9UL);
                    var_43 = local_sp_8 + (-16L);
                    var_44 = (uint64_t *)var_43;
                    local_sp_1 = var_43;
                    if (var_42) {
                        *var_44 = 4205313UL;
                        indirect_placeholder();
                    } else {
                        *var_44 = 4205291UL;
                        indirect_placeholder();
                    }
                }
                *var_3 = (*var_3 + 8UL);
                local_sp_8 = local_sp_1;
                var_38 = **(uint64_t **)var_2;
            }
    }
    var_6 = (uint32_t *)(var_0 + (-20L));
    *var_6 = 0U;
    var_7 = (uint32_t *)(var_0 + (-24L));
    *var_7 = 1U;
    var_8 = (uint32_t *)(var_0 + (-12L));
    *var_8 = 1U;
    while ((int)var_9 <= (int)6U)
        {
            *var_7 = (*var_7 + 1U);
            var_10 = *var_8 * 10U;
            *var_8 = var_10;
            var_9 = var_10;
        }
    *var_8 = 1U;
    var_11 = var_0 + (-56L);
    var_12 = (uint64_t *)(var_0 + (-32L));
    local_sp_6 = local_sp_5;
    local_sp_7 = local_sp_5;
    while ((int)var_13 <= (int)64U)
        {
            var_14 = (uint64_t)var_13;
            var_15 = local_sp_5 + (-8L);
            *(uint64_t *)var_15 = 4205007UL;
            var_16 = indirect_placeholder_1(var_11, var_14);
            local_sp_4 = var_15;
            var_17 = local_sp_5 + (-16L);
            *(uint64_t *)var_17 = 4205023UL;
            indirect_placeholder();
            *var_12 = var_11;
            local_sp_4 = var_17;
            if ((uint64_t)(uint32_t)var_16 != 0UL & var_11 > (uint64_t)*var_6) {
                *var_6 = (uint32_t)var_11;
            }
            var_18 = *var_8 + 1U;
            *var_8 = var_18;
            var_13 = var_18;
            local_sp_5 = local_sp_4;
            local_sp_6 = local_sp_5;
            local_sp_7 = local_sp_5;
        }
    if (*var_3 != 0UL) {
        *var_8 = 1U;
        while ((int)var_27 <= (int)64U)
            {
                var_28 = (uint64_t)var_27;
                var_29 = local_sp_7 + (-8L);
                *(uint64_t *)var_29 = 4205170UL;
                var_30 = indirect_placeholder_1(var_11, var_28);
                local_sp_2 = var_29;
                if ((uint64_t)(uint32_t)var_30 == 0UL) {
                    var_31 = (uint64_t)*var_6;
                    var_32 = (uint64_t)*var_7;
                    var_33 = (uint64_t)*var_8;
                    var_34 = local_sp_7 + (-16L);
                    *(uint64_t *)var_34 = 4205194UL;
                    indirect_placeholder_4(var_11, var_31, var_33, var_32);
                    local_sp_2 = var_34;
                }
                var_35 = *var_8 + 1U;
                *var_8 = var_35;
                var_27 = var_35;
                local_sp_7 = local_sp_2;
            }
    }
    var_19 = **(uint64_t **)var_2;
    while (var_19 != 0UL)
        {
            var_20 = local_sp_6 + (-8L);
            *(uint64_t *)var_20 = 4205084UL;
            var_21 = indirect_placeholder_1(var_11, var_19);
            var_22 = (uint32_t)var_21;
            *var_8 = var_22;
            local_sp_3 = var_20;
            if ((int)var_22 > (int)4294967295U) {
                var_23 = (uint64_t)*var_6;
                var_24 = (uint64_t)*var_7;
                var_25 = (uint64_t)var_22;
                var_26 = local_sp_6 + (-16L);
                *(uint64_t *)var_26 = 4205122UL;
                indirect_placeholder_4(var_11, var_23, var_25, var_24);
                local_sp_3 = var_26;
            } else {
                *var_5 = 1U;
            }
            *var_3 = (*var_3 + 8UL);
            local_sp_6 = local_sp_3;
            var_19 = **(uint64_t **)var_2;
        }
    return (uint64_t)*var_5;
}
