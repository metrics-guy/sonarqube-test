typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder_1(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0);
uint64_t bb_operand2sig(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t local_sp_5;
    uint64_t var_40;
    uint64_t local_sp_6;
    uint64_t storemerge10;
    uint64_t local_sp_0;
    uint64_t var_30;
    uint64_t local_sp_2;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t local_sp_1;
    uint64_t var_34;
    uint32_t *_pre_phi73;
    unsigned char *var_25;
    unsigned char **var_21;
    unsigned char var_22;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_12;
    uint64_t local_sp_4;
    uint64_t var_11;
    uint32_t storemerge;
    uint32_t *var_13;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_41;
    struct indirect_placeholder_15_ret_type var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t *var_8;
    uint32_t *var_9;
    uint64_t var_10;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint64_t var_19;
    uint64_t *var_20;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-64L);
    var_3 = (uint64_t *)var_2;
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = (uint64_t)((uint32_t)(uint64_t)**(unsigned char **)var_2 + (-48));
    storemerge10 = 4294967295UL;
    storemerge = 4294967295U;
    if (var_5 > 9UL) {
        var_14 = *var_3;
        var_15 = var_0 + (-80L);
        *(uint64_t *)var_15 = 4206734UL;
        var_16 = indirect_placeholder_3(var_14);
        var_17 = var_0 + (-24L);
        var_18 = (uint64_t *)var_17;
        *var_18 = var_16;
        var_19 = var_0 + (-16L);
        var_20 = (uint64_t *)var_19;
        *var_20 = var_16;
        local_sp_2 = var_15;
        var_21 = (unsigned char **)var_19;
        var_22 = **var_21;
        while (var_22 != '\x00')
            {
                var_23 = (uint64_t)(uint32_t)(uint64_t)var_22;
                var_24 = local_sp_2 + (-8L);
                *(uint64_t *)var_24 = 4206770UL;
                indirect_placeholder();
                local_sp_2 = var_24;
                if (var_23 == 0UL) {
                    var_25 = *var_21;
                    *var_25 = (*var_25 + '\xe0');
                }
                *var_20 = (*var_20 + 1UL);
                var_21 = (unsigned char **)var_19;
                var_22 = **var_21;
            }
        var_26 = var_0 + (-40L);
        var_27 = *var_18;
        var_28 = local_sp_2 + (-8L);
        *(uint64_t *)var_28 = 4206828UL;
        var_29 = indirect_placeholder_1(var_26, var_27);
        local_sp_0 = var_28;
        local_sp_1 = var_28;
        if ((uint64_t)(uint32_t)var_29 != 0UL) {
            if (**(unsigned char **)var_17 == 'S') {
                *(uint32_t *)var_26 = 4294967295U;
                local_sp_1 = local_sp_0;
            } else {
                var_30 = *var_18;
                if (*(unsigned char *)(var_30 + 1UL) == 'I') {
                    *(uint32_t *)var_26 = 4294967295U;
                    local_sp_1 = local_sp_0;
                } else {
                    if (*(unsigned char *)(var_30 + 2UL) == 'G') {
                        *(uint32_t *)var_26 = 4294967295U;
                        local_sp_1 = local_sp_0;
                    } else {
                        var_31 = var_30 + 3UL;
                        var_32 = local_sp_2 + (-16L);
                        *(uint64_t *)var_32 = 4206896UL;
                        var_33 = indirect_placeholder_1(var_26, var_31);
                        local_sp_0 = var_32;
                        local_sp_1 = var_32;
                        if ((uint64_t)(uint32_t)var_33 == 0UL) {
                            *(uint32_t *)var_26 = 4294967295U;
                            local_sp_1 = local_sp_0;
                        }
                    }
                }
            }
        }
        var_34 = local_sp_1 + (-8L);
        *(uint64_t *)var_34 = 4206919UL;
        indirect_placeholder();
        _pre_phi73 = (uint32_t *)var_26;
        local_sp_5 = var_34;
    } else {
        *(uint64_t *)(var_0 + (-80L)) = 4206578UL;
        indirect_placeholder();
        *(uint32_t *)var_5 = 0U;
        var_6 = *var_3;
        var_7 = var_0 + (-88L);
        *(uint64_t *)var_7 = 4206608UL;
        indirect_placeholder();
        var_8 = (uint64_t *)(var_0 + (-32L));
        *var_8 = var_6;
        var_9 = (uint32_t *)(var_0 + (-36L));
        *var_9 = (uint32_t)var_6;
        var_10 = var_0 + (-48L);
        local_sp_4 = var_7;
        var_11 = var_0 + (-96L);
        *(uint64_t *)var_11 = 4206645UL;
        indirect_placeholder();
        local_sp_4 = var_11;
        if (*var_3 != *(uint64_t *)var_10 & **(unsigned char **)var_10 != '\x00' & *(volatile uint32_t *)(uint32_t *)0UL == 0U) {
            var_12 = *var_9;
            storemerge = (*var_8 == (uint64_t)var_12) ? var_12 : 4294967295U;
        }
        var_13 = (uint32_t *)(var_0 + (-40L));
        *var_13 = storemerge;
        _pre_phi73 = var_13;
        local_sp_5 = local_sp_4;
        if (storemerge == 4294967295U) {
            *var_13 = ((((int)storemerge > (int)254U) ? 255U : 127U) & storemerge);
        }
    }
    var_35 = *_pre_phi73;
    local_sp_6 = local_sp_5;
    if ((int)var_35 >= (int)0U) {
        var_36 = (uint64_t)var_35;
        var_37 = *var_4;
        var_38 = local_sp_5 + (-8L);
        *(uint64_t *)var_38 = 4206943UL;
        var_39 = indirect_placeholder_1(var_37, var_36);
        local_sp_6 = var_38;
        if ((uint64_t)(uint32_t)var_39 != 0UL) {
            var_40 = (uint64_t)*_pre_phi73;
            storemerge10 = var_40;
            return storemerge10;
        }
    }
    var_41 = *var_3;
    *(uint64_t *)(local_sp_6 + (-8L)) = 4206959UL;
    var_42 = indirect_placeholder_15(var_41);
    var_43 = var_42.field_0;
    var_44 = var_42.field_1;
    var_45 = var_42.field_2;
    *(uint64_t *)(local_sp_6 + (-16L)) = 4206987UL;
    indirect_placeholder_7(0UL, var_43, 4274643UL, 0UL, 0UL, var_44, var_45);
    return storemerge10;
}
