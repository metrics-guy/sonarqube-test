typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_42_ret_type;
struct indirect_placeholder_43_ret_type;
struct indirect_placeholder_42_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_43_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern struct indirect_placeholder_42_ret_type indirect_placeholder_42(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_43_ret_type indirect_placeholder_43(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
uint64_t bb_restricted_chown(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r9, uint64_t r8) {
    struct indirect_placeholder_42_ret_type var_29;
    uint64_t var_0;
    uint64_t var_1;
    uint32_t *var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint32_t *var_7;
    uint32_t *var_8;
    uint32_t var_50;
    uint32_t storemerge;
    uint32_t rax_3_shrunk;
    uint64_t rax_1;
    uint64_t var_45;
    uint64_t local_sp_0;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t rax_0;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint32_t var_40;
    uint32_t var_41;
    uint32_t var_42;
    uint32_t var_43;
    uint64_t var_44;
    uint32_t var_46;
    uint32_t *var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint32_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t local_sp_1;
    uint64_t rax_2;
    uint32_t spec_select;
    uint64_t var_20;
    uint32_t var_21;
    uint64_t var_22;
    uint32_t var_9;
    uint32_t var_32;
    uint64_t local_sp_2;
    uint64_t var_33;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    struct indirect_placeholder_43_ret_type var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t *var_18;
    uint32_t var_19;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint32_t *)(var_0 + (-172L));
    *var_2 = (uint32_t)rdi;
    var_3 = (uint64_t *)(var_0 + (-184L));
    *var_3 = rsi;
    var_4 = var_0 + (-192L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdx;
    *(uint32_t *)(var_0 + (-176L)) = (uint32_t)rcx;
    *(uint32_t *)(var_0 + (-196L)) = (uint32_t)r8;
    var_6 = (uint32_t *)(var_0 + (-200L));
    *var_6 = (uint32_t)r9;
    var_7 = (uint32_t *)(var_0 + (-12L));
    *var_7 = 2U;
    var_8 = (uint32_t *)(var_0 + (-16L));
    *var_8 = 2304U;
    rax_3_shrunk = 5U;
    var_9 = 2304U;
    if (*var_6 != 4294967295U) {
        if (*(uint32_t *)(var_0 | 8UL) == 4294967295U) {
            return (uint64_t)rax_3_shrunk;
        }
    }
    switch ((uint32_t)((uint16_t)*(uint32_t *)(*var_5 + 24UL) & (unsigned short)61440U)) {
      case 16384U:
        {
            *var_8 = 67840U;
            var_9 = 67840U;
        }
        break;
      case 32768U:
        {
            var_10 = (uint64_t)var_9;
            var_11 = *var_3;
            var_12 = (uint64_t)*var_2;
            var_13 = var_0 + (-208L);
            *(uint64_t *)var_13 = 4207592UL;
            var_14 = indirect_placeholder_43(0UL, var_11, var_10, var_12, var_11, r9, r8);
            var_15 = var_14.field_0;
            var_16 = var_14.field_2;
            var_17 = var_14.field_3;
            var_18 = (uint32_t *)(var_0 + (-20L));
            var_19 = (uint32_t)var_15;
            *var_18 = var_19;
            var_32 = var_19;
            local_sp_2 = var_13;
            if ((int)var_19 <= (int)4294967295U) {
                var_20 = var_0 + (-216L);
                *(uint64_t *)var_20 = 4207606UL;
                indirect_placeholder_1();
                var_21 = *(uint32_t *)var_15;
                var_22 = (uint64_t)var_21;
                local_sp_1 = var_20;
                rax_2 = var_22;
                if ((uint64_t)(var_21 + (-13)) != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207685UL;
                    indirect_placeholder_1();
                    spec_select = (*(uint32_t *)rax_2 == 13U) ? 5U : 6U;
                    rax_3_shrunk = spec_select;
                    return (uint64_t)rax_3_shrunk;
                }
                var_23 = (uint32_t)((uint16_t)*(uint32_t *)(*var_5 + 24UL) & (unsigned short)61440U);
                var_24 = (uint64_t)var_23;
                rax_2 = var_24;
                if ((uint64_t)((var_23 + (-32768)) & (-4096)) != 0UL) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207685UL;
                    indirect_placeholder_1();
                    spec_select = (*(uint32_t *)rax_2 == 13U) ? 5U : 6U;
                    rax_3_shrunk = spec_select;
                    return (uint64_t)rax_3_shrunk;
                }
                var_25 = (uint64_t)(*var_8 | 1U);
                var_26 = *var_3;
                var_27 = (uint64_t)*var_2;
                var_28 = var_0 + (-224L);
                *(uint64_t *)var_28 = 4207671UL;
                var_29 = indirect_placeholder_42(0UL, var_26, var_25, var_27, var_26, var_16, var_17);
                var_30 = var_29.field_0;
                var_31 = (uint32_t)var_30;
                *var_18 = var_31;
                local_sp_1 = var_28;
                rax_2 = var_30;
                var_32 = var_31;
                local_sp_2 = var_28;
                if ((int)var_31 <= (int)4294967295U) {
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4207685UL;
                    indirect_placeholder_1();
                    spec_select = (*(uint32_t *)rax_2 == 13U) ? 5U : 6U;
                    rax_3_shrunk = spec_select;
                    return (uint64_t)rax_3_shrunk;
                }
            }
            var_33 = local_sp_2 + (-8L);
            *(uint64_t *)var_33 = 4207732UL;
            indirect_placeholder_1();
            local_sp_0 = var_33;
            if (var_32 == 0U) {
                var_34 = (uint64_t)var_32;
                *var_7 = 6U;
                rax_1 = var_34;
            } else {
                var_35 = *(uint64_t *)(*var_5 + 8UL);
                var_36 = *(uint64_t *)(var_0 + (-160L));
                rax_0 = var_36;
                if (var_35 == var_36) {
                    *var_7 = 4U;
                    rax_1 = rax_0;
                } else {
                    var_37 = **(uint64_t **)var_4;
                    var_38 = *(uint64_t *)(var_0 + (-168L));
                    rax_0 = var_38;
                    if (var_37 == var_38) {
                        *var_7 = 4U;
                        rax_1 = rax_0;
                    } else {
                        var_39 = *var_6;
                        if (var_39 == 4294967295U) {
                            var_40 = *(uint32_t *)(var_0 + (-140L));
                            rax_1 = (uint64_t)var_40;
                            if ((uint64_t)(var_39 - var_40) != 0UL) {
                                var_41 = *(uint32_t *)(var_0 | 8UL);
                                if (var_41 == 4294967295U) {
                                    var_42 = *(uint32_t *)(var_0 + (-136L));
                                    rax_1 = (uint64_t)var_42;
                                    if ((uint64_t)(var_41 - var_42) != 0UL) {
                                        var_43 = *var_18;
                                        var_44 = local_sp_2 + (-16L);
                                        *(uint64_t *)var_44 = 4207863UL;
                                        indirect_placeholder_1();
                                        local_sp_0 = var_44;
                                        if (var_43 != 0U) {
                                            var_50 = *var_18;
                                            *(uint64_t *)(local_sp_2 + (-24L)) = 4207877UL;
                                            indirect_placeholder_1();
                                            storemerge = (var_50 == 0U) ? 2U : 6U;
                                            *var_7 = storemerge;
                                            rax_3_shrunk = storemerge;
                                            return (uint64_t)rax_3_shrunk;
                                        }
                                        var_45 = (uint64_t)var_43;
                                        *var_7 = 6U;
                                        rax_1 = var_45;
                                    }
                                } else {
                                    var_43 = *var_18;
                                    var_44 = local_sp_2 + (-16L);
                                    *(uint64_t *)var_44 = 4207863UL;
                                    indirect_placeholder_1();
                                    local_sp_0 = var_44;
                                    if (var_43 != 0U) {
                                        var_50 = *var_18;
                                        *(uint64_t *)(local_sp_2 + (-24L)) = 4207877UL;
                                        indirect_placeholder_1();
                                        storemerge = (var_50 == 0U) ? 2U : 6U;
                                        *var_7 = storemerge;
                                        rax_3_shrunk = storemerge;
                                        return (uint64_t)rax_3_shrunk;
                                    }
                                    var_45 = (uint64_t)var_43;
                                    *var_7 = 6U;
                                    rax_1 = var_45;
                                }
                            }
                        } else {
                            var_41 = *(uint32_t *)(var_0 | 8UL);
                            if (var_41 == 4294967295U) {
                                var_42 = *(uint32_t *)(var_0 + (-136L));
                                rax_1 = (uint64_t)var_42;
                                if ((uint64_t)(var_41 - var_42) != 0UL) {
                                    var_43 = *var_18;
                                    var_44 = local_sp_2 + (-16L);
                                    *(uint64_t *)var_44 = 4207863UL;
                                    indirect_placeholder_1();
                                    local_sp_0 = var_44;
                                    if (var_43 != 0U) {
                                        var_50 = *var_18;
                                        *(uint64_t *)(local_sp_2 + (-24L)) = 4207877UL;
                                        indirect_placeholder_1();
                                        storemerge = (var_50 == 0U) ? 2U : 6U;
                                        *var_7 = storemerge;
                                        rax_3_shrunk = storemerge;
                                        return (uint64_t)rax_3_shrunk;
                                    }
                                    var_45 = (uint64_t)var_43;
                                    *var_7 = 6U;
                                    rax_1 = var_45;
                                }
                            } else {
                                var_43 = *var_18;
                                var_44 = local_sp_2 + (-16L);
                                *(uint64_t *)var_44 = 4207863UL;
                                indirect_placeholder_1();
                                local_sp_0 = var_44;
                                if (var_43 != 0U) {
                                    var_50 = *var_18;
                                    *(uint64_t *)(local_sp_2 + (-24L)) = 4207877UL;
                                    indirect_placeholder_1();
                                    storemerge = (var_50 == 0U) ? 2U : 6U;
                                    *var_7 = storemerge;
                                    rax_3_shrunk = storemerge;
                                    return (uint64_t)rax_3_shrunk;
                                }
                                var_45 = (uint64_t)var_43;
                                *var_7 = 6U;
                                rax_1 = var_45;
                            }
                        }
                    }
                }
            }
            *(uint64_t *)(local_sp_0 + (-8L)) = 4207913UL;
            indirect_placeholder_1();
            var_46 = *(uint32_t *)rax_1;
            var_47 = (uint32_t *)(var_0 + (-24L));
            *var_47 = var_46;
            var_48 = (uint64_t)*var_18;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4207928UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_0 + (-24L)) = 4207933UL;
            indirect_placeholder_1();
            *(uint32_t *)var_48 = *var_47;
            var_49 = *var_7;
            rax_3_shrunk = var_49;
        }
        break;
    }
}
