typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_164_ret_type;
struct indirect_placeholder_163_ret_type;
struct indirect_placeholder_165_ret_type;
struct indirect_placeholder_164_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_163_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_165_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_117(uint64_t param_0);
extern struct indirect_placeholder_164_ret_type indirect_placeholder_164(uint64_t param_0, uint64_t param_1);
extern struct indirect_placeholder_163_ret_type indirect_placeholder_163(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_165_ret_type indirect_placeholder_165(uint64_t param_0, uint64_t param_1);
uint64_t bb_excise(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi) {
    uint64_t var_32;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint32_t spec_select;
    uint64_t *var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    unsigned char var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t rax_2;
    uint64_t var_38;
    uint64_t local_sp_1;
    uint64_t var_35;
    uint32_t var_36;
    uint64_t var_37;
    uint64_t rax_1;
    uint64_t var_22;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t local_sp_0;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint16_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint32_t var_30;
    uint64_t var_31;
    uint64_t var_42;
    struct indirect_placeholder_164_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t rax_3;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_49;
    uint64_t var_12;
    uint32_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_3 = (uint64_t *)(var_0 + (-192L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-200L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-208L));
    *var_5 = rdx;
    var_6 = (unsigned char *)(var_0 + (-212L));
    var_7 = (unsigned char)rcx;
    *var_6 = var_7;
    spec_select = (var_7 == '\x00') ? 0U : 512U;
    *(uint32_t *)(var_0 + (-28L)) = spec_select;
    var_8 = *(uint64_t *)(*var_4 + 48UL);
    var_9 = (uint64_t)*(uint32_t *)(*var_3 + 44UL);
    var_10 = (uint64_t)spec_select;
    *(uint64_t *)(var_0 + (-224L)) = 4214457UL;
    var_11 = indirect_placeholder_6(var_10, var_8, var_9);
    rax_3 = 2UL;
    if ((uint64_t)(uint32_t)var_11 == 0UL) {
        if (*(unsigned char *)(*var_5 + 25UL) == '\x00') {
            var_49 = *(uint64_t *)(*var_4 + 56UL);
            *(uint64_t *)(var_0 + (-232L)) = 4214500UL;
            indirect_placeholder_165(var_49, 4UL);
            *(uint64_t *)(var_0 + (-240L)) = 4214537UL;
            indirect_placeholder_1();
        }
    } else {
        var_12 = var_0 + (-232L);
        *(uint64_t *)var_12 = 4214552UL;
        indirect_placeholder_1();
        var_13 = *(uint32_t *)var_11;
        local_sp_3 = var_12;
        rax_2 = (uint64_t)var_13;
        if ((uint64_t)(var_13 + (-30)) != 0UL) {
            var_14 = *(uint64_t *)(*var_4 + 48UL);
            var_15 = (uint64_t)*(uint32_t *)(*var_3 + 44UL);
            var_16 = var_0 + (-184L);
            var_17 = var_0 + (-240L);
            *(uint64_t *)var_17 = 4214597UL;
            var_18 = indirect_placeholder_6(var_16, var_14, var_15);
            local_sp_2 = var_17;
            rax_1 = var_18;
            if ((uint64_t)(uint32_t)var_18 == 0UL) {
                var_22 = local_sp_2 + (-8L);
                *(uint64_t *)var_22 = 4214618UL;
                indirect_placeholder_1();
                *(uint32_t *)rax_1 = 30U;
                local_sp_3 = var_22;
                rax_2 = rax_1;
            } else {
                var_19 = var_0 + (-248L);
                *(uint64_t *)var_19 = 4214606UL;
                indirect_placeholder_1();
                var_20 = *(uint32_t *)var_18;
                var_21 = (uint64_t)var_20;
                local_sp_2 = var_19;
                rax_1 = var_21;
                local_sp_3 = var_19;
                rax_2 = var_21;
                if ((uint64_t)(var_20 + (-2)) == 0UL) {
                    var_22 = local_sp_2 + (-8L);
                    *(uint64_t *)var_22 = 4214618UL;
                    indirect_placeholder_1();
                    *(uint32_t *)rax_1 = 30U;
                    local_sp_3 = var_22;
                    rax_2 = rax_1;
                }
            }
        }
        *(uint64_t *)(local_sp_3 + (-8L)) = 4214629UL;
        indirect_placeholder_1();
        var_23 = (uint64_t)*(uint32_t *)rax_2;
        var_24 = *var_5;
        var_25 = local_sp_3 + (-16L);
        *(uint64_t *)var_25 = 4214648UL;
        var_26 = indirect_placeholder(var_23, var_24);
        local_sp_1 = var_25;
        if ((uint64_t)(unsigned char)var_26 != 0UL) {
            var_27 = *(uint16_t *)(*var_4 + 112UL);
            var_28 = (uint64_t)var_27;
            rax_3 = 4UL;
            if ((uint64_t)(var_27 + (unsigned short)65532U) != 0UL) {
                var_29 = local_sp_3 + (-24L);
                *(uint64_t *)var_29 = 4214684UL;
                indirect_placeholder_1();
                var_30 = *(uint32_t *)var_28;
                var_31 = (uint64_t)var_30;
                local_sp_0 = var_29;
                if ((uint64_t)(var_30 + (-39)) == 0UL) {
                    var_32 = local_sp_3 + (-32L);
                    *(uint64_t *)var_32 = 4214696UL;
                    indirect_placeholder_1();
                    var_33 = *(uint32_t *)var_31;
                    var_34 = (uint64_t)var_33;
                    local_sp_0 = var_32;
                    if ((uint64_t)(var_33 + (-21)) == 0UL) {
                        var_35 = local_sp_3 + (-40L);
                        *(uint64_t *)var_35 = 4214708UL;
                        indirect_placeholder_1();
                        var_36 = *(uint32_t *)var_34;
                        var_37 = (uint64_t)var_36;
                        local_sp_0 = var_35;
                        if ((uint64_t)(var_36 + (-20)) == 0UL) {
                            var_38 = local_sp_3 + (-48L);
                            *(uint64_t *)var_38 = 4214720UL;
                            indirect_placeholder_1();
                            local_sp_0 = var_38;
                            local_sp_1 = var_38;
                            if (*(uint32_t *)var_37 != 17U) {
                                var_39 = *(uint32_t *)(*var_4 + 64UL);
                                var_40 = (uint64_t)var_39;
                                local_sp_1 = local_sp_0;
                                if ((uint64_t)(var_39 + (-1)) == 0UL) {
                                    var_41 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_41 = 4214762UL;
                                    indirect_placeholder_1();
                                    *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                                    local_sp_1 = var_41;
                                } else {
                                    if ((uint64_t)(var_39 + (-13)) == 0UL) {
                                        var_41 = local_sp_0 + (-8L);
                                        *(uint64_t *)var_41 = 4214762UL;
                                        indirect_placeholder_1();
                                        *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                                        local_sp_1 = var_41;
                                    }
                                }
                            }
                        } else {
                            var_39 = *(uint32_t *)(*var_4 + 64UL);
                            var_40 = (uint64_t)var_39;
                            local_sp_1 = local_sp_0;
                            if ((uint64_t)(var_39 + (-1)) == 0UL) {
                                var_41 = local_sp_0 + (-8L);
                                *(uint64_t *)var_41 = 4214762UL;
                                indirect_placeholder_1();
                                *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                                local_sp_1 = var_41;
                            } else {
                                if ((uint64_t)(var_39 + (-13)) == 0UL) {
                                    var_41 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_41 = 4214762UL;
                                    indirect_placeholder_1();
                                    *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                                    local_sp_1 = var_41;
                                }
                            }
                        }
                    } else {
                        var_39 = *(uint32_t *)(*var_4 + 64UL);
                        var_40 = (uint64_t)var_39;
                        local_sp_1 = local_sp_0;
                        if ((uint64_t)(var_39 + (-1)) == 0UL) {
                            var_41 = local_sp_0 + (-8L);
                            *(uint64_t *)var_41 = 4214762UL;
                            indirect_placeholder_1();
                            *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                            local_sp_1 = var_41;
                        } else {
                            if ((uint64_t)(var_39 + (-13)) == 0UL) {
                                var_41 = local_sp_0 + (-8L);
                                *(uint64_t *)var_41 = 4214762UL;
                                indirect_placeholder_1();
                                *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                                local_sp_1 = var_41;
                            }
                        }
                    }
                } else {
                    var_39 = *(uint32_t *)(*var_4 + 64UL);
                    var_40 = (uint64_t)var_39;
                    local_sp_1 = local_sp_0;
                    if ((uint64_t)(var_39 + (-1)) == 0UL) {
                        var_41 = local_sp_0 + (-8L);
                        *(uint64_t *)var_41 = 4214762UL;
                        indirect_placeholder_1();
                        *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                        local_sp_1 = var_41;
                    } else {
                        if ((uint64_t)(var_39 + (-13)) == 0UL) {
                            var_41 = local_sp_0 + (-8L);
                            *(uint64_t *)var_41 = 4214762UL;
                            indirect_placeholder_1();
                            *(uint32_t *)var_40 = *(uint32_t *)(*var_4 + 64UL);
                            local_sp_1 = var_41;
                        }
                    }
                }
            }
            var_42 = *(uint64_t *)(*var_4 + 56UL);
            *(uint64_t *)(local_sp_1 + (-8L)) = 4214798UL;
            var_43 = indirect_placeholder_164(var_42, 4UL);
            var_44 = var_43.field_0;
            var_45 = var_43.field_1;
            var_46 = var_43.field_2;
            *(uint64_t *)(local_sp_1 + (-16L)) = 4214806UL;
            indirect_placeholder_1();
            var_47 = (uint64_t)*(uint32_t *)var_44;
            *(uint64_t *)(local_sp_1 + (-24L)) = 4214833UL;
            indirect_placeholder_163(0UL, var_44, 4361968UL, var_47, 0UL, var_45, var_46);
            var_48 = *var_4;
            *(uint64_t *)(local_sp_1 + (-32L)) = 4214848UL;
            indirect_placeholder_117(var_48);
        }
    }
    return rax_3;
}
