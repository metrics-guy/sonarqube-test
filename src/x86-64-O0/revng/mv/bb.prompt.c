typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_186_ret_type;
struct indirect_placeholder_187_ret_type;
struct indirect_placeholder_188_ret_type;
struct indirect_placeholder_186_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_187_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_188_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_1(void);
extern uint64_t indirect_placeholder_6(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_3(uint64_t param_0);
extern uint64_t indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern uint64_t indirect_placeholder_12(void);
extern void indirect_placeholder_117(uint64_t param_0);
extern struct indirect_placeholder_186_ret_type indirect_placeholder_186(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_187_ret_type indirect_placeholder_187(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_188_ret_type indirect_placeholder_188(uint64_t param_0, uint64_t param_1);
uint64_t bb_prompt(uint64_t rcx, uint64_t rdx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint64_t *var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t *var_6;
    uint64_t var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint32_t var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_54;
    uint64_t var_55;
    uint64_t rax_0;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t local_sp_0;
    uint64_t var_58;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t local_sp_3;
    uint64_t var_40;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    unsigned char var_26;
    uint64_t local_sp_1;
    uint32_t *var_27;
    uint64_t local_sp_2;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t var_41;
    uint64_t var_42;
    struct indirect_placeholder_188_ret_type var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t *var_47;
    uint64_t var_16;
    uint64_t *var_17;
    uint64_t var_18;
    uint32_t storemerge3;
    uint32_t *var_19;
    uint32_t *var_20;
    unsigned char *var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-224L));
    *var_2 = rdi;
    var_3 = (uint64_t *)(var_0 + (-232L));
    *var_3 = rsi;
    var_4 = var_0 + (-248L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rcx;
    var_6 = (uint32_t *)(var_0 + (-240L));
    *var_6 = (uint32_t)r8;
    var_7 = var_0 + (-256L);
    var_8 = (uint64_t *)var_7;
    *var_8 = r9;
    var_9 = (unsigned char *)(var_0 + (-236L));
    *var_9 = (unsigned char)rdx;
    var_10 = *(uint32_t *)(*var_2 + 44UL);
    var_11 = (uint32_t *)(var_0 + (-28L));
    *var_11 = var_10;
    var_12 = *(uint64_t *)(*var_3 + 56UL);
    var_13 = (uint64_t *)(var_0 + (-40L));
    *var_13 = var_12;
    var_14 = *(uint64_t *)(*var_3 + 48UL);
    var_15 = (uint64_t *)(var_0 + (-48L));
    *var_15 = var_14;
    rax_0 = 3UL;
    if (*var_8 == 0UL) {
        **(uint32_t **)var_7 = 2U;
    }
    var_16 = var_0 + (-216L);
    var_17 = (uint64_t *)(var_0 + (-56L));
    *var_17 = var_16;
    var_18 = var_0 + (-272L);
    *(uint64_t *)var_18 = 4213305UL;
    indirect_placeholder_117(var_16);
    storemerge3 = (*var_9 == '\x00') ? 0U : 4U;
    var_19 = (uint32_t *)(var_0 + (-12L));
    *var_19 = storemerge3;
    var_20 = (uint32_t *)(var_0 + (-16L));
    *var_20 = 0U;
    var_21 = (unsigned char *)(var_0 + (-17L));
    *var_21 = (unsigned char)'\x00';
    local_sp_1 = var_18;
    if (*var_8 == 0UL) {
        var_22 = *var_15;
        var_23 = (uint64_t)*var_11;
        var_24 = var_0 + (-280L);
        *(uint64_t *)var_24 = 4213367UL;
        var_25 = indirect_placeholder_33(var_22, var_23, r9, r8);
        var_26 = (unsigned char)var_25;
        *var_21 = var_26;
        **(uint32_t **)var_7 = ((var_26 == '\x00') ? 3U : 4U);
        local_sp_1 = var_24;
    }
    local_sp_2 = local_sp_1;
    if (*(uint64_t *)(*var_3 + 32UL) == 0UL) {
        return rax_0;
    }
    rax_0 = 2UL;
    if (*(uint32_t *)(*var_5 + 4UL) == 5U) {
        return;
    }
    var_27 = (uint32_t *)(var_0 + (-24L));
    *var_27 = 0U;
    rax_0 = 4UL;
    if (**(unsigned char **)var_4 != '\x01') {
        var_28 = *var_5;
        if (*(uint32_t *)(var_28 + 4UL) == 3U) {
            if (*(unsigned char *)(var_28 + 24UL) != '\x00' & *var_19 == 10U) {
                var_29 = *var_17;
                var_30 = *var_15;
                var_31 = (uint64_t)*var_11;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213529UL;
                var_32 = indirect_placeholder_6(var_29, var_30, var_31);
                *var_20 = (uint32_t)var_32;
                var_33 = local_sp_1 + (-16L);
                *(uint64_t *)var_33 = 4213537UL;
                indirect_placeholder_1();
                *var_27 = *(uint32_t *)var_32;
                local_sp_2 = var_33;
            }
        } else {
            if (*var_19 == 10U) {
                var_29 = *var_17;
                var_30 = *var_15;
                var_31 = (uint64_t)*var_11;
                *(uint64_t *)(local_sp_1 + (-8L)) = 4213529UL;
                var_32 = indirect_placeholder_6(var_29, var_30, var_31);
                *var_20 = (uint32_t)var_32;
                var_33 = local_sp_1 + (-16L);
                *(uint64_t *)var_33 = 4213537UL;
                indirect_placeholder_1();
                *var_27 = *(uint32_t *)var_32;
                local_sp_2 = var_33;
            }
        }
    }
    var_34 = *var_20;
    local_sp_3 = local_sp_2;
    if (var_34 != 0U) {
        if (*(uint32_t *)(*var_5 + 4UL) != 3U) {
            return rax_0;
        }
    }
    if ((int)var_34 < (int)0U) {
        if ((int)*var_20 >= (int)0U) {
            if (*(uint32_t *)(*var_5 + 4UL) == 3U) {
                return rax_0;
            }
        }
        var_42 = *var_13;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4213809UL;
        var_43 = indirect_placeholder_188(var_42, 4UL);
        var_44 = var_43.field_0;
        var_45 = var_43.field_1;
        var_46 = var_43.field_2;
        var_47 = (uint64_t *)(var_0 + (-64L));
        *var_47 = var_44;
        if ((int)*var_20 > (int)4294967295U) {
            rax_0 = 3UL;
            if (*var_19 != 4U) {
                if (*var_6 != 2U & *var_21 != '\x01') {
                    var_49 = local_sp_3 + (-16L);
                    *(uint64_t *)var_49 = 4213936UL;
                    indirect_placeholder_1();
                    local_sp_0 = var_49;
                    *(uint64_t *)(local_sp_0 + (-8L)) = 4214086UL;
                    var_58 = indirect_placeholder_12();
                    if ((uint64_t)(unsigned char)var_58 == 1UL) {
                    }
                    return rax_0;
                }
            }
            var_50 = *var_17;
            var_51 = *var_15;
            var_52 = (uint64_t)*var_11;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4213964UL;
            var_53 = indirect_placeholder_33(256UL, var_50, var_51, var_52);
            if ((uint64_t)(uint32_t)var_53 != 0UL) {
                *(uint64_t *)(local_sp_3 + (-24L)) = 4213973UL;
                indirect_placeholder_1();
                var_54 = (uint64_t)*(uint32_t *)var_53;
                var_55 = *var_47;
                *(uint64_t *)(local_sp_3 + (-32L)) = 4214004UL;
                indirect_placeholder_186(0UL, var_55, 4361968UL, var_54, 0UL, var_45, var_46);
                return rax_0;
            }
            var_56 = *var_17;
            *(uint64_t *)(local_sp_3 + (-24L)) = 4214023UL;
            indirect_placeholder_3(var_56);
            var_57 = local_sp_3 + (-32L);
            *(uint64_t *)var_57 = 4214081UL;
            indirect_placeholder_1();
            local_sp_0 = var_57;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4214086UL;
            var_58 = indirect_placeholder_12();
            if ((uint64_t)(unsigned char)var_58 == 1UL) {
            }
        } else {
            var_48 = (uint64_t)*var_27;
            *(uint64_t *)(local_sp_3 + (-16L)) = 4213851UL;
            indirect_placeholder_187(0UL, var_44, 4361968UL, var_48, 0UL, var_45, var_46);
        }
    } else {
        if (*var_19 != 0U) {
            var_35 = *var_17;
            var_36 = *var_15;
            var_37 = (uint64_t)*var_11;
            var_38 = local_sp_2 + (-8L);
            *(uint64_t *)var_38 = 4213602UL;
            var_39 = indirect_placeholder_33(256UL, var_35, var_36, var_37);
            local_sp_3 = var_38;
            if ((uint64_t)(uint32_t)var_39 == 0UL) {
                switch ((uint32_t)((uint16_t)*(uint32_t *)(*var_17 + 24UL) & (unsigned short)61440U)) {
                  case 40960U:
                    {
                        *var_19 = 10U;
                    }
                    break;
                  case 16384U:
                    {
                        *var_19 = 4U;
                    }
                    break;
                  default:
                    {
                        break;
                    }
                    break;
                }
            } else {
                *var_20 = 4294967295U;
                var_40 = local_sp_2 + (-16L);
                *(uint64_t *)var_40 = 4213674UL;
                indirect_placeholder_1();
                *var_27 = *(uint32_t *)var_39;
                local_sp_3 = var_40;
            }
        }
    }
}
