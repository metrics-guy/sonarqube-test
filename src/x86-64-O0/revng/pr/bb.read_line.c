typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_read_line_ret_type;
struct indirect_placeholder_15_ret_type;
struct indirect_placeholder_16_ret_type;
struct indirect_placeholder_17_ret_type;
struct indirect_placeholder_18_ret_type;
struct indirect_placeholder_19_ret_type;
struct indirect_placeholder_20_ret_type;
struct bb_read_line_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_15_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_16_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_17_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_18_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_19_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_20_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t init_r10(void);
extern uint64_t init_rbx(void);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern void indirect_placeholder_12(uint64_t param_0);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern uint64_t init_rsi(void);
extern uint64_t init_rcx(void);
extern void indirect_placeholder_3(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern struct indirect_placeholder_15_ret_type indirect_placeholder_15(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_16_ret_type indirect_placeholder_16(void);
extern struct indirect_placeholder_17_ret_type indirect_placeholder_17(void);
extern struct indirect_placeholder_18_ret_type indirect_placeholder_18(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_19_ret_type indirect_placeholder_19(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern struct indirect_placeholder_20_ret_type indirect_placeholder_20(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
typedef _Bool bool;
struct bb_read_line_ret_type bb_read_line(uint64_t rdi) {
    uint64_t var_81;
    struct indirect_placeholder_15_ret_type var_66;
    struct indirect_placeholder_19_ret_type var_54;
    struct indirect_placeholder_18_ret_type var_40;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t *var_9;
    uint64_t **var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t *var_13;
    uint32_t var_14;
    uint32_t *var_15;
    uint64_t local_sp_4;
    uint64_t rbx_4;
    uint64_t r9_7;
    uint64_t var_73;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_80;
    uint64_t local_sp_0;
    uint64_t r10_0;
    uint64_t r8_6;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rcx_0;
    uint64_t r10_6;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint32_t var_72;
    uint64_t var_77;
    uint64_t var_78;
    uint32_t var_79;
    uint64_t local_sp_1;
    uint64_t r9_0;
    uint64_t rbx_0;
    uint64_t r8_0;
    uint64_t r9_2;
    uint64_t rbx_1;
    uint64_t var_62;
    uint32_t var_63;
    uint64_t rdi6_0;
    uint64_t var_85;
    uint64_t local_sp_2;
    uint64_t var_86;
    struct indirect_placeholder_16_ret_type var_87;
    uint64_t local_sp_3;
    uint64_t r9_1;
    uint64_t r8_1;
    uint64_t r10_1;
    uint64_t var_88;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint32_t var_21;
    uint64_t local_sp_5;
    uint64_t var_55;
    uint64_t r8_2;
    uint64_t r10_2;
    uint64_t rdi6_1;
    uint64_t local_sp_10;
    uint64_t local_sp_6;
    uint64_t var_31;
    struct indirect_placeholder_17_ret_type var_32;
    uint64_t r9_3;
    uint64_t r8_3;
    uint64_t r10_3;
    uint64_t rdi6_2;
    uint64_t local_sp_8;
    uint32_t var_33;
    uint32_t *var_34;
    uint32_t *var_35;
    uint64_t var_36;
    uint64_t *var_37;
    uint64_t var_38;
    uint64_t local_sp_7;
    uint64_t r9_4;
    uint64_t rbx_2;
    uint64_t r8_4;
    uint64_t r10_4;
    uint64_t rdi6_3;
    uint32_t storemerge;
    uint64_t var_39;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    uint64_t r9_5;
    uint64_t rbx_3;
    uint64_t r8_5;
    uint64_t r10_5;
    uint64_t rdi6_4;
    uint32_t var_47;
    uint64_t var_48;
    uint32_t var_49;
    uint64_t local_sp_9;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t var_52;
    uint64_t rdi6_5;
    uint64_t var_53;
    uint64_t r9_6;
    uint64_t local_sp_11;
    uint64_t var_56;
    uint64_t rbx_5;
    uint64_t rax_0;
    uint64_t rcx_1;
    uint64_t r8_7;
    uint64_t r10_7;
    struct bb_read_line_ret_type mrv;
    struct bb_read_line_ret_type mrv1;
    struct bb_read_line_ret_type mrv2;
    struct bb_read_line_ret_type mrv3;
    struct bb_read_line_ret_type mrv4;
    struct bb_read_line_ret_type mrv5;
    uint64_t var_57;
    uint32_t *var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    struct indirect_placeholder_20_ret_type var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t var_82;
    uint64_t var_83;
    uint32_t var_84;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_r9();
    var_2 = init_rbx();
    var_3 = init_rbp();
    var_4 = init_rcx();
    var_5 = init_r8();
    var_6 = init_r10();
    var_7 = init_rsi();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    var_8 = var_0 + (-48L);
    var_9 = (uint64_t *)var_8;
    *var_9 = rdi;
    var_10 = (uint64_t **)var_8;
    var_11 = **var_10;
    var_12 = var_0 + (-64L);
    *(uint64_t *)var_12 = 4215665UL;
    indirect_placeholder();
    var_13 = (uint32_t *)(var_0 + (-12L));
    *var_13 = (uint32_t)var_11;
    var_14 = *(uint32_t *)4338888UL;
    var_15 = (uint32_t *)(var_0 + (-36L));
    *var_15 = var_14;
    local_sp_4 = var_12;
    r9_7 = var_1;
    r9_2 = var_1;
    rbx_1 = var_2;
    rdi6_0 = var_11;
    r9_1 = var_1;
    r8_1 = var_5;
    r10_1 = var_6;
    r8_2 = var_5;
    r10_2 = var_6;
    storemerge = 0U;
    rbx_5 = var_2;
    rax_0 = 1UL;
    rcx_1 = var_4;
    r8_7 = var_5;
    r10_7 = var_6;
    var_16 = **var_10;
    var_17 = var_0 + (-72L);
    *(uint64_t *)var_17 = 4215710UL;
    indirect_placeholder();
    var_18 = (uint32_t)var_16;
    *var_13 = var_18;
    local_sp_4 = var_17;
    rdi6_0 = var_16;
    if (*var_13 != 12U & *(unsigned char *)(*var_9 + 57UL) != '\x00' & var_18 == 10U) {
        var_19 = **var_10;
        var_20 = var_0 + (-80L);
        *(uint64_t *)var_20 = 4215734UL;
        indirect_placeholder();
        *var_13 = (uint32_t)var_19;
        local_sp_4 = var_20;
        rdi6_0 = var_19;
    }
    *(unsigned char *)(*var_9 + 57UL) = (unsigned char)'\x00';
    var_21 = *var_13;
    local_sp_5 = local_sp_4;
    rdi6_1 = rdi6_0;
    if (var_21 == 12U) {
        var_82 = **var_10;
        var_83 = local_sp_4 + (-8L);
        *(uint64_t *)var_83 = 4215801UL;
        indirect_placeholder();
        var_84 = (uint32_t)var_82;
        *var_13 = var_84;
        local_sp_2 = var_83;
        if (var_84 == 10U) {
            var_85 = local_sp_4 + (-16L);
            *(uint64_t *)var_85 = 4215830UL;
            indirect_placeholder();
            local_sp_2 = var_85;
        }
        *(unsigned char *)4338851UL = (unsigned char)'\x01';
        local_sp_3 = local_sp_2;
        if (*(unsigned char *)4338855UL == '\x00') {
            if (*(unsigned char *)4338401UL == '\x01') {
                *(unsigned char *)4338968UL = (unsigned char)'\x01';
                var_86 = local_sp_2 + (-8L);
                *(uint64_t *)var_86 = 4215874UL;
                var_87 = indirect_placeholder_16();
                local_sp_3 = var_86;
                r9_1 = var_87.field_0;
                r8_1 = var_87.field_1;
                r10_1 = var_87.field_2;
            } else {
                if (*(unsigned char *)4338853UL == '\x00') {
                    *(unsigned char *)4338854UL = (unsigned char)'\x01';
                }
            }
        } else {
            if (*(unsigned char *)4338853UL == '\x00') {
                *(unsigned char *)4338854UL = (unsigned char)'\x01';
            }
        }
        var_88 = *var_9;
        *(uint64_t *)(local_sp_3 + (-8L)) = 4215906UL;
        indirect_placeholder_12(var_88);
        r9_7 = r9_1;
        r8_7 = r8_1;
        r10_7 = r10_1;
    } else {
        rax_0 = 0UL;
        if ((int)var_21 > (int)12U) {
            var_22 = *var_9;
            *(uint64_t *)(local_sp_4 + (-8L)) = 4215928UL;
            indirect_placeholder_12(var_22);
            mrv.field_0 = r9_7;
            mrv1 = mrv;
            mrv1.field_1 = rbx_5;
            mrv2 = mrv1;
            mrv2.field_2 = rax_0;
            mrv3 = mrv2;
            mrv3.field_3 = rcx_1;
            mrv4 = mrv3;
            mrv4.field_4 = r8_7;
            mrv5 = mrv4;
            mrv5.field_5 = r10_7;
            return mrv5;
        }
        var_23 = (uint64_t)(uint32_t)((int)(var_21 << 24U) >> (int)24U);
        var_24 = local_sp_4 + (-8L);
        *(uint64_t *)var_24 = 4215951UL;
        var_25 = indirect_placeholder_20(var_1, var_2, var_4, var_5, var_6, var_7, var_23);
        var_26 = var_25.field_0;
        var_27 = var_25.field_1;
        var_28 = var_25.field_3;
        var_29 = var_25.field_4;
        var_30 = var_25.field_5;
        *(uint32_t *)(var_0 + (-16L)) = (uint32_t)var_25.field_2;
        r9_2 = var_26;
        rbx_1 = var_27;
        local_sp_5 = var_24;
        r8_2 = var_28;
        r10_2 = var_29;
        rdi6_1 = var_30;
        rbx_4 = rbx_1;
        r9_7 = r9_2;
        r8_6 = r8_2;
        r10_6 = r10_2;
        local_sp_10 = local_sp_5;
        local_sp_6 = local_sp_5;
        r9_3 = r9_2;
        r8_3 = r8_2;
        r10_3 = r10_2;
        rdi6_2 = rdi6_1;
        rbx_2 = rbx_1;
        rbx_3 = rbx_1;
        r9_6 = r9_2;
        rbx_5 = rbx_1;
        r8_7 = r8_2;
        r10_7 = r10_2;
        if (*(unsigned char *)4338864UL != '\x00') {
            if ((long)((uint64_t)*(uint32_t *)4338888UL << 32UL) <= (long)((uint64_t)*(uint32_t *)4338868UL << 32UL)) {
                *(uint32_t *)4338888UL = *var_15;
                mrv.field_0 = r9_7;
                mrv1 = mrv;
                mrv1.field_1 = rbx_5;
                mrv2 = mrv1;
                mrv2.field_2 = rax_0;
                mrv3 = mrv2;
                mrv3.field_3 = rcx_1;
                mrv4 = mrv3;
                mrv4.field_4 = r8_7;
                mrv5 = mrv4;
                mrv5.field_5 = r10_7;
                return mrv5;
            }
        }
        if (*(uint64_t *)(*var_9 + 32UL) != 4213281UL) {
            *(unsigned char *)4338968UL = (unsigned char)'\x01';
            if (*(unsigned char *)4338855UL != '\x00' & *(unsigned char *)4338401UL == '\x01') {
                var_31 = local_sp_5 + (-8L);
                *(uint64_t *)var_31 = 4216060UL;
                var_32 = indirect_placeholder_17();
                local_sp_6 = var_31;
                r9_3 = var_32.field_0;
                r8_3 = var_32.field_1;
                r10_3 = var_32.field_2;
                rdi6_2 = var_32.field_3;
            }
            local_sp_8 = local_sp_6;
            local_sp_7 = local_sp_6;
            r9_4 = r9_3;
            r8_4 = r8_3;
            r10_4 = r10_3;
            rdi6_3 = rdi6_2;
            r9_5 = r9_3;
            r8_5 = r8_3;
            r10_5 = r10_3;
            rdi6_4 = rdi6_2;
            if (*(unsigned char *)4338848UL != '\x00' & *(unsigned char *)4338849UL != '\x00') {
                var_33 = *(uint32_t *)4338960UL;
                var_34 = (uint32_t *)(var_0 + (-40L));
                *var_34 = var_33;
                *(uint32_t *)4338960UL = 0U;
                var_35 = (uint32_t *)(var_0 + (-20L));
                *var_35 = 1U;
                var_36 = *(uint64_t *)4338800UL;
                var_37 = (uint64_t *)(var_0 + (-32L));
                *var_37 = var_36;
                var_38 = var_36;
                local_sp_8 = local_sp_7;
                r9_5 = r9_4;
                rbx_3 = rbx_2;
                r8_5 = r8_4;
                r10_5 = r10_4;
                rdi6_4 = rdi6_3;
                while ((long)((uint64_t)*var_35 << 32UL) <= (long)((uint64_t)*var_34 << 32UL))
                    {
                        var_39 = local_sp_7 + (-8L);
                        *(uint64_t *)var_39 = 4216141UL;
                        var_40 = indirect_placeholder_18(r9_4, rbx_2, r8_4, r10_4, var_38);
                        var_41 = var_40.field_0;
                        var_42 = var_40.field_1;
                        var_43 = var_40.field_2;
                        var_44 = var_40.field_3;
                        var_45 = var_40.field_4;
                        *(uint32_t *)4338960UL = (*(uint32_t *)4338960UL + 1U);
                        *var_35 = (*var_35 + 1U);
                        var_46 = *var_37 + 64UL;
                        *var_37 = var_46;
                        var_38 = var_46;
                        local_sp_7 = var_39;
                        r9_4 = var_41;
                        rbx_2 = var_42;
                        r8_4 = var_43;
                        r10_4 = var_44;
                        rdi6_3 = var_45;
                        local_sp_8 = local_sp_7;
                        r9_5 = r9_4;
                        rbx_3 = rbx_2;
                        r8_5 = r8_4;
                        r10_5 = r10_4;
                        rdi6_4 = rdi6_3;
                    }
                *(uint32_t *)4338964UL = *(uint32_t *)(*var_9 + 52UL);
                if (*(unsigned char *)4338864UL != '\x00') {
                    storemerge = *(uint32_t *)4338868UL;
                }
                *(uint32_t *)4338876UL = storemerge;
                *(unsigned char *)4338849UL = (unsigned char)'\x00';
            }
            var_47 = *(uint32_t *)4338956UL;
            var_48 = (uint64_t)var_47;
            var_49 = *(uint32_t *)4338964UL;
            rbx_4 = rbx_3;
            r8_6 = r8_5;
            r10_6 = r10_5;
            local_sp_9 = local_sp_8;
            rdi6_5 = rdi6_4;
            r9_6 = r9_5;
            if ((long)(var_48 << 32UL) < (long)((uint64_t)var_49 << 32UL)) {
                var_50 = (uint64_t)(var_49 - var_47);
                var_51 = local_sp_8 + (-8L);
                *(uint64_t *)var_51 = 4216267UL;
                var_52 = indirect_placeholder_13(var_50);
                *(uint32_t *)4338964UL = 0U;
                local_sp_9 = var_51;
                rdi6_5 = var_52;
            }
            local_sp_10 = local_sp_9;
            if (*(unsigned char *)4338953UL == '\x00') {
                var_53 = local_sp_9 + (-8L);
                *(uint64_t *)var_53 = 4216293UL;
                var_54 = indirect_placeholder_19(r9_5, rbx_3, r8_5, r10_5, rdi6_5);
                rbx_4 = var_54.field_1;
                r8_6 = var_54.field_2;
                r10_6 = var_54.field_3;
                local_sp_10 = var_53;
                r9_6 = var_54.field_0;
            }
        }
        var_55 = *var_9;
        r9_7 = r9_6;
        r10_0 = r10_6;
        r9_0 = r9_6;
        rbx_0 = rbx_4;
        r8_0 = r8_6;
        local_sp_11 = local_sp_10;
        rbx_5 = rbx_4;
        r8_7 = r8_6;
        r10_7 = r10_6;
        if (*(unsigned char *)(var_55 + 56UL) == '\x00') {
            var_56 = local_sp_10 + (-8L);
            *(uint64_t *)var_56 = 4216317UL;
            indirect_placeholder_12(var_55);
            local_sp_11 = var_56;
        }
        *(unsigned char *)4338850UL = (unsigned char)'\x00';
        if (*var_13 != 10U) {
            var_57 = *(uint64_t *)4339024UL;
            var_58 = (uint32_t *)(var_0 + (-16L));
            var_59 = (uint64_t)*var_58;
            var_60 = *var_9;
            var_61 = local_sp_11 + (-8L);
            *(uint64_t *)var_61 = 4216364UL;
            indirect_placeholder_3(var_57, var_59, var_60);
            local_sp_1 = var_61;
            rcx_0 = var_59;
            rax_0 = 0UL;
            while (1U)
                {
                    var_62 = **var_10;
                    *(uint64_t *)(local_sp_1 + (-8L)) = 4216379UL;
                    indirect_placeholder();
                    var_63 = (uint32_t)var_62;
                    *var_13 = var_63;
                    r9_7 = r9_0;
                    rbx_5 = rbx_0;
                    rcx_1 = rcx_0;
                    r8_7 = r8_0;
                    r10_7 = r10_0;
                    if (var_63 != 12U) {
                        var_77 = **var_10;
                        var_78 = local_sp_1 + (-16L);
                        *(uint64_t *)var_78 = 4216431UL;
                        indirect_placeholder();
                        var_79 = (uint32_t)var_77;
                        *var_13 = var_79;
                        local_sp_0 = var_78;
                        if (var_79 != 10U) {
                            loop_state_var = 0U;
                            break;
                        }
                        var_80 = local_sp_1 + (-24L);
                        *(uint64_t *)var_80 = 4216460UL;
                        indirect_placeholder();
                        local_sp_0 = var_80;
                        loop_state_var = 0U;
                        break;
                    }
                    if ((int)var_63 <= (int)12U) {
                        *var_15 = *(uint32_t *)4338888UL;
                        var_65 = (uint64_t)(uint32_t)((int)(*var_13 << 24U) >> (int)24U);
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4216538UL;
                        var_66 = indirect_placeholder_15(r9_0, rbx_0, rcx_0, r8_0, r10_0, rcx_0, var_65);
                        var_67 = var_66.field_0;
                        var_68 = var_66.field_1;
                        var_69 = var_66.field_2;
                        var_70 = var_66.field_3;
                        var_71 = var_66.field_4;
                        var_72 = (uint32_t)var_69;
                        *var_58 = var_72;
                        r9_7 = var_67;
                        r10_0 = var_71;
                        r9_0 = var_67;
                        rbx_0 = var_68;
                        r8_0 = var_70;
                        rbx_5 = var_68;
                        r8_7 = var_70;
                        r10_7 = var_71;
                        if (*(unsigned char *)4338864UL != '\x00') {
                            if ((long)((uint64_t)*(uint32_t *)4338888UL << 32UL) <= (long)((uint64_t)*(uint32_t *)4338868UL << 32UL)) {
                                *(uint32_t *)4338888UL = *var_15;
                                loop_state_var = 1U;
                                break;
                            }
                        }
                        var_73 = *(uint64_t *)4339024UL;
                        var_74 = (uint64_t)var_72;
                        var_75 = *var_9;
                        var_76 = local_sp_1 + (-24L);
                        *(uint64_t *)var_76 = 4216608UL;
                        indirect_placeholder_3(var_73, var_74, var_75);
                        local_sp_1 = var_76;
                        rcx_0 = var_74;
                        continue;
                    }
                    switch_state_var = 0;
                    switch (var_63) {
                      case 4294967295U:
                        {
                            var_64 = *var_9;
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4216509UL;
                            indirect_placeholder_12(var_64);
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      case 10U:
                        {
                            loop_state_var = 1U;
                            switch_state_var = 1;
                            break;
                        }
                        break;
                      default:
                        {
                            *var_15 = *(uint32_t *)4338888UL;
                            var_65 = (uint64_t)(uint32_t)((int)(*var_13 << 24U) >> (int)24U);
                            *(uint64_t *)(local_sp_1 + (-16L)) = 4216538UL;
                            var_66 = indirect_placeholder_15(r9_0, rbx_0, rcx_0, r8_0, r10_0, rcx_0, var_65);
                            var_67 = var_66.field_0;
                            var_68 = var_66.field_1;
                            var_69 = var_66.field_2;
                            var_70 = var_66.field_3;
                            var_71 = var_66.field_4;
                            var_72 = (uint32_t)var_69;
                            *var_58 = var_72;
                            r9_7 = var_67;
                            r10_0 = var_71;
                            r9_0 = var_67;
                            rbx_0 = var_68;
                            r8_0 = var_70;
                            rbx_5 = var_68;
                            r8_7 = var_70;
                            r10_7 = var_71;
                            if (*(unsigned char *)4338864UL != '\x00') {
                                if ((long)((uint64_t)*(uint32_t *)4338888UL << 32UL) <= (long)((uint64_t)*(uint32_t *)4338868UL << 32UL)) {
                                    *(uint32_t *)4338888UL = *var_15;
                                    loop_state_var = 1U;
                                    switch_state_var = 1;
                                    break;
                                }
                            }
                            var_73 = *(uint64_t *)4339024UL;
                            var_74 = (uint64_t)var_72;
                            var_75 = *var_9;
                            var_76 = local_sp_1 + (-24L);
                            *(uint64_t *)var_76 = 4216608UL;
                            indirect_placeholder_3(var_73, var_74, var_75);
                            local_sp_1 = var_76;
                            rcx_0 = var_74;
                            continue;
                        }
                        break;
                    }
                    if (switch_state_var)
                        break;
                }
            if (*(unsigned char *)4338853UL != '\x00') {
                *(unsigned char *)4338854UL = (unsigned char)'\x01';
            }
            var_81 = *var_9;
            *(uint64_t *)(local_sp_0 + (-8L)) = 4216490UL;
            indirect_placeholder_12(var_81);
        }
    }
}
