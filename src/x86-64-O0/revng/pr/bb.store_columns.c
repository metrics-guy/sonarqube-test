typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_5_ret_type;
struct indirect_placeholder_6_ret_type;
struct indirect_placeholder_5_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_6_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_4(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_5_ret_type indirect_placeholder_5(uint64_t param_0);
extern struct indirect_placeholder_6_ret_type indirect_placeholder_6(uint64_t param_0);
void bb_store_columns(uint64_t r9, uint64_t rbx, uint64_t rcx, uint64_t r8, uint64_t r10) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t *var_3;
    uint32_t *var_4;
    bool var_5;
    uint32_t var_6;
    uint64_t rbx2_2;
    uint64_t var_25;
    uint64_t var_26;
    struct indirect_placeholder_5_ret_type var_27;
    uint64_t local_sp_0;
    uint64_t r91_0;
    uint64_t rcx3_0;
    uint64_t r84_0;
    uint64_t r105_0;
    uint64_t var_28;
    uint32_t *_pre_phi44;
    uint64_t local_sp_3;
    uint32_t *var_29;
    uint64_t var_30;
    uint32_t var_31;
    uint64_t var_32;
    uint32_t *var_7;
    uint32_t var_8;
    uint32_t *var_9;
    uint32_t *var_10;
    uint64_t var_11;
    uint64_t *var_12;
    uint64_t var_13;
    uint64_t var_15;
    uint32_t *var_16;
    uint64_t var_17;
    uint64_t local_sp_1;
    uint64_t rcx3_3;
    uint64_t r91_1;
    uint64_t r84_3;
    uint64_t rbx2_0;
    uint64_t r105_3;
    uint64_t rcx3_1;
    uint64_t r84_1;
    uint64_t r105_1;
    uint32_t var_18;
    uint32_t var_19;
    uint64_t r91_3;
    uint64_t local_sp_2;
    uint64_t r91_2;
    uint64_t rbx2_1;
    uint64_t rcx3_2;
    uint64_t r84_2;
    uint64_t r105_2;
    uint64_t var_20;
    uint64_t var_21;
    struct indirect_placeholder_6_ret_type var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_14;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = (uint32_t *)(var_0 + (-20L));
    *var_3 = 0U;
    *(uint32_t *)4338816UL = 0U;
    var_4 = (uint32_t *)(var_0 + (-24L));
    *var_4 = 0U;
    var_5 = (*(unsigned char *)4338859UL == '\x00');
    var_6 = *(uint32_t *)4338428UL;
    local_sp_1 = var_2;
    r91_1 = r9;
    rbx2_0 = rbx;
    rcx3_1 = rcx;
    r84_1 = r8;
    r105_1 = r10;
    if (var_5) {
        var_8 = var_6 + (-1);
        var_9 = (uint32_t *)(var_0 + (-28L));
        *var_9 = var_8;
        _pre_phi44 = var_9;
    } else {
        var_7 = (uint32_t *)(var_0 + (-28L));
        *var_7 = var_6;
        _pre_phi44 = var_7;
    }
    var_10 = (uint32_t *)(var_0 + (-12L));
    *var_10 = 1U;
    var_11 = *(uint64_t *)4338800UL;
    var_12 = (uint64_t *)var_2;
    *var_12 = var_11;
    var_13 = var_11;
    while ((long)((uint64_t)*var_10 << 32UL) <= (long)((uint64_t)*_pre_phi44 << 32UL))
        {
            *(uint32_t *)(var_13 + 44UL) = 0U;
            *var_10 = (*var_10 + 1U);
            var_14 = *var_12 + 64UL;
            *var_12 = var_14;
            var_13 = var_14;
        }
    *var_10 = 1U;
    var_15 = *(uint64_t *)4338800UL;
    *var_12 = var_15;
    var_16 = (uint32_t *)(var_0 + (-16L));
    var_17 = var_15;
    local_sp_2 = local_sp_1;
    r91_2 = r91_1;
    rbx2_1 = rbx2_0;
    rcx3_2 = rcx3_1;
    r84_2 = r84_1;
    r105_2 = r105_1;
    while ((long)((uint64_t)*var_10 << 32UL) <= (long)((uint64_t)*_pre_phi44 << 32UL))
        {
            *(uint32_t *)(var_17 + 40UL) = *var_3;
            var_18 = *(uint32_t *)4338860UL;
            *var_16 = var_18;
            var_19 = var_18;
            rbx2_2 = rbx2_1;
            local_sp_3 = local_sp_2;
            local_sp_1 = local_sp_2;
            rcx3_3 = rcx3_2;
            r91_1 = r91_2;
            r84_3 = r84_2;
            rbx2_0 = rbx2_1;
            r105_3 = r105_2;
            rcx3_1 = rcx3_2;
            r84_1 = r84_2;
            r105_1 = r105_2;
            r91_3 = r91_2;
            while (var_19 != 0U)
                {
                    if (*(uint32_t *)4338904UL == 0U) {
                        break;
                    }
                    if (*(uint32_t *)(*var_12 + 16UL) == 0U) {
                        var_33 = *var_16 + (-1);
                        *var_16 = var_33;
                        var_19 = var_33;
                        local_sp_2 = local_sp_3;
                        r91_2 = r91_3;
                        rbx2_1 = rbx2_2;
                        rcx3_2 = rcx3_3;
                        r84_2 = r84_3;
                        r105_2 = r105_3;
                        rbx2_2 = rbx2_1;
                        local_sp_3 = local_sp_2;
                        local_sp_1 = local_sp_2;
                        rcx3_3 = rcx3_2;
                        r91_1 = r91_2;
                        r84_3 = r84_2;
                        rbx2_0 = rbx2_1;
                        r105_3 = r105_2;
                        rcx3_1 = rcx3_2;
                        r84_1 = r84_2;
                        r105_1 = r105_2;
                        r91_3 = r91_2;
                        continue;
                    }
                    *(uint32_t *)4338888UL = 0U;
                    var_20 = *var_12;
                    var_21 = local_sp_2 + (-8L);
                    *(uint64_t *)var_21 = 4212935UL;
                    var_22 = indirect_placeholder_6(var_20);
                    var_23 = var_22.field_0;
                    var_24 = var_22.field_1;
                    rbx2_2 = var_24;
                    local_sp_0 = var_21;
                    r91_0 = var_23;
                    rcx3_0 = var_22.field_3;
                    r84_0 = var_22.field_4;
                    r105_0 = var_22.field_5;
                    if ((uint64_t)(unsigned char)var_22.field_2 == 1UL) {
                        var_25 = *var_12;
                        var_26 = local_sp_2 + (-16L);
                        *(uint64_t *)var_26 = 4212954UL;
                        var_27 = indirect_placeholder_5(var_25);
                        local_sp_0 = var_26;
                        r91_0 = var_27.field_0;
                        rcx3_0 = var_27.field_1;
                        r84_0 = var_27.field_2;
                        r105_0 = var_27.field_3;
                    }
                    var_28 = *var_12;
                    local_sp_3 = local_sp_0;
                    rcx3_3 = rcx3_0;
                    r84_3 = r84_0;
                    r105_3 = r105_0;
                    r91_3 = r91_0;
                    if (*(uint32_t *)(var_28 + 16UL) != 0U) {
                        if ((uint64_t)(*var_4 - *(uint32_t *)4338816UL) != 0UL) {
                            var_33 = *var_16 + (-1);
                            *var_16 = var_33;
                            var_19 = var_33;
                            local_sp_2 = local_sp_3;
                            r91_2 = r91_3;
                            rbx2_1 = rbx2_2;
                            rcx3_2 = rcx3_3;
                            r84_2 = r84_3;
                            r105_2 = r105_3;
                            rbx2_2 = rbx2_1;
                            local_sp_3 = local_sp_2;
                            local_sp_1 = local_sp_2;
                            rcx3_3 = rcx3_2;
                            r91_1 = r91_2;
                            r84_3 = r84_2;
                            rbx2_0 = rbx2_1;
                            r105_3 = r105_2;
                            rcx3_1 = rcx3_2;
                            r84_1 = r84_2;
                            r105_1 = r105_2;
                            r91_3 = r91_2;
                            continue;
                        }
                    }
                    var_29 = (uint32_t *)(var_28 + 44UL);
                    *var_29 = (*var_29 + 1U);
                    *(uint32_t *)(((uint64_t)*var_3 << 2UL) + *(uint64_t *)4338832UL) = *var_4;
                    var_30 = *(uint64_t *)4338840UL;
                    var_31 = *var_3;
                    var_32 = (uint64_t)var_31;
                    *var_3 = (var_31 + 1U);
                    *(uint32_t *)((var_32 << 2UL) + var_30) = *(uint32_t *)4338888UL;
                    *var_4 = *(uint32_t *)4338816UL;
                    rcx3_3 = var_30;
                }
            *var_10 = (*var_10 + 1U);
            var_34 = *var_12 + 64UL;
            *var_12 = var_34;
            var_17 = var_34;
            local_sp_2 = local_sp_1;
            r91_2 = r91_1;
            rbx2_1 = rbx2_0;
            rcx3_2 = rcx3_1;
            r84_2 = r84_1;
            r105_2 = r105_1;
        }
    *(uint32_t *)(((uint64_t)*var_3 << 2UL) + *(uint64_t *)4338832UL) = *var_4;
    if (*(unsigned char *)4338859UL == '\x00') {
        return;
    }
    var_35 = (uint64_t)*var_3;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4213156UL;
    indirect_placeholder_4(r91_1, rbx2_0, rcx3_1, r84_1, r105_1, var_35);
    return;
}
