typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_33_ret_type;
struct helper_idivl_EAX_wrapper_ret_type;
struct type_6;
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct helper_idivl_EAX_wrapper_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint32_t field_3;
    uint64_t field_4;
    uint64_t field_5;
    uint32_t field_6;
    uint64_t field_7;
    uint32_t field_8;
    uint32_t field_9;
    unsigned char field_10;
    uint32_t field_11;
    uint32_t field_12;
};
struct type_6 {
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t init_state_0x9018(void);
extern uint32_t init_state_0x9010(void);
extern uint64_t init_state_0x8408(void);
extern uint64_t init_state_0x8328(void);
extern uint64_t init_state_0x82d8(void);
extern uint32_t init_state_0x9080(void);
extern uint32_t init_state_0x8248(void);
extern uint64_t indirect_placeholder_13(uint64_t param_0);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct helper_idivl_EAX_wrapper_ret_type helper_idivl_EAX_wrapper(struct type_6 *param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7, uint64_t param_8, uint64_t param_9, uint64_t param_10, uint64_t param_11, uint64_t param_12, uint64_t param_13, uint32_t param_14, uint64_t param_15, uint64_t param_16, uint64_t param_17, uint32_t param_18, uint32_t param_19);
void bb_init_parameters(uint64_t r9, uint64_t rbx, uint64_t rcx, uint64_t r8, uint64_t r10, uint64_t rsi, uint64_t rdi) {
    uint32_t var_37;
    uint64_t rcx3_160;
    uint64_t var_38;
    uint64_t var_39;
    uint32_t var_41;
    struct helper_idivl_EAX_wrapper_ret_type var_40;
    uint64_t var_42;
    uint64_t local_sp_0;
    uint64_t var_43;
    uint64_t local_sp_1;
    uint32_t var_44;
    uint64_t rax_0;
    uint64_t var_45;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint32_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint32_t var_7;
    uint32_t var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint32_t *var_11;
    uint32_t *var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t var_16;
    uint32_t var_17;
    bool var_18;
    unsigned char var_19;
    uint64_t state_0x82d8_1;
    uint32_t state_0x9080_1;
    uint32_t state_0x8248_1;
    uint64_t state_0x9018_1;
    uint32_t *var_20;
    bool var_21;
    uint64_t var_22;
    struct helper_idivl_EAX_wrapper_ret_type var_23;
    uint64_t state_0x9018_0;
    uint32_t state_0x9010_0;
    uint64_t state_0x82d8_0;
    uint32_t state_0x9080_0;
    uint32_t state_0x8248_0;
    uint64_t storemerge_in;
    uint32_t storemerge;
    uint32_t state_0x9010_1;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t var_26;
    bool var_27;
    uint32_t *var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    bool var_34;
    uint32_t *var_35;
    uint32_t var_36;
    uint64_t var_46;
    uint32_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_state_0x9018();
    var_3 = init_state_0x9010();
    var_4 = init_state_0x8408();
    var_5 = init_state_0x8328();
    var_6 = init_state_0x82d8();
    var_7 = init_state_0x9080();
    var_8 = init_state_0x8248();
    var_9 = var_0 + (-8L);
    *(uint64_t *)var_9 = var_1;
    var_10 = var_0 + (-40L);
    var_11 = (uint32_t *)(var_0 + (-28L));
    *var_11 = (uint32_t)rdi;
    var_12 = (uint32_t *)(var_0 + (-12L));
    *var_12 = 0U;
    var_13 = *(uint32_t *)4338404UL + (-10);
    var_14 = (uint64_t)var_13;
    *(uint32_t *)4338860UL = var_13;
    var_15 = helper_cc_compute_all_wrapper(var_14, 0UL, 0UL, 24U);
    rcx3_160 = 0UL;
    local_sp_0 = var_10;
    state_0x82d8_1 = var_6;
    state_0x9080_1 = var_7;
    state_0x8248_1 = var_8;
    state_0x9018_1 = var_2;
    state_0x9018_0 = var_2;
    state_0x9010_0 = var_3;
    state_0x82d8_0 = var_6;
    state_0x9080_0 = var_7;
    state_0x8248_0 = var_8;
    state_0x9010_1 = var_3;
    if ((uint64_t)(((unsigned char)(var_15 >> 4UL) ^ (unsigned char)var_15) & '\xc0') == 0UL) {
        *(unsigned char *)4338400UL = (unsigned char)'\x00';
        *(unsigned char *)4338853UL = (unsigned char)'\x01';
    }
    if (*(unsigned char *)4338400UL == '\x01') {
        *(uint32_t *)4338860UL = *(uint32_t *)4338404UL;
    }
    if (*(unsigned char *)4338946UL == '\x00') {
        var_16 = *(uint32_t *)4338860UL;
        *(uint32_t *)4338860UL = (uint32_t)(uint64_t)((long)((uint64_t)((uint32_t)((uint64_t)var_16 >> 31UL) + var_16) << 32UL) >> (long)33UL);
    }
    var_17 = *var_11;
    if (var_17 == 0U) {
        *(unsigned char *)4338848UL = (unsigned char)'\x00';
    } else {
        if (*(unsigned char *)4338848UL == '\x00') {
            *(uint32_t *)4338428UL = var_17;
        }
    }
    if (*(unsigned char *)4338401UL == '\x00') {
        *(unsigned char *)4338859UL = (unsigned char)'\x01';
    }
    if ((int)*(uint32_t *)4338428UL > (int)1U) {
        *(unsigned char *)4338401UL = (unsigned char)'\x00';
    } else {
        var_18 = (*(unsigned char *)4338953UL == '\x01');
        var_19 = *(unsigned char *)4338865UL;
        if (var_18) {
            *(uint64_t *)4338464UL = *(uint64_t *)(var_19 == '\x00') ? 4338472UL : 4338480UL;
            *(uint32_t *)4338956UL = 1U;
            *(unsigned char *)4338953UL = (unsigned char)'\x01';
        } else {
            if (var_19 != '\x01' & *(uint32_t *)4338956UL != 1U & **(unsigned char **)4338464UL == '\t') {
                *(uint64_t *)4338464UL = *(uint64_t *)4338472UL;
            }
        }
        *(unsigned char *)4338864UL = (unsigned char)'\x01';
        *(unsigned char *)4338873UL = (unsigned char)'\x01';
    }
    if (*(unsigned char *)4338865UL == '\x00') {
        *(unsigned char *)4338864UL = (unsigned char)'\x00';
    }
    if (*(unsigned char *)4338924UL != '\x00') {
        var_20 = (uint32_t *)(var_0 + (-16L));
        *var_20 = 8U;
        *(uint32_t *)4338444UL = *(uint32_t *)4338452UL;
        var_21 = (*(unsigned char *)4338440UL == '\t');
        var_22 = (uint64_t)*(uint32_t *)4338456UL;
        if (var_21) {
            var_23 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), (uint64_t)*var_20, 4208286UL, r9, rbx, var_22, var_9, rcx, (uint64_t)(uint32_t)(uint64_t)((long)(var_22 << 32UL) >> (long)63UL), r8, r10, rsi, rdi, var_2, var_3, var_4, var_5, var_6, var_7, var_8);
            state_0x9018_0 = var_23.field_5;
            state_0x9010_0 = var_23.field_6;
            state_0x82d8_0 = var_23.field_7;
            state_0x9080_0 = var_23.field_8;
            state_0x8248_0 = var_23.field_9;
            storemerge_in = (uint64_t)(*var_20 - (uint32_t)var_23.field_4) + (uint64_t)*(uint32_t *)4338456UL;
        } else {
            storemerge_in = var_22 + 1UL;
        }
        storemerge = (uint32_t)storemerge_in;
        *(uint32_t *)4338928UL = storemerge;
        state_0x82d8_1 = state_0x82d8_0;
        state_0x9080_1 = state_0x9080_0;
        state_0x8248_1 = state_0x8248_0;
        state_0x9018_1 = state_0x9018_0;
        state_0x9010_1 = state_0x9010_0;
        if (*(unsigned char *)4338848UL == '\x00') {
            *var_12 = storemerge;
        }
    }
    var_24 = (uint64_t)((long)(((uint64_t)*(uint32_t *)4338428UL << 32UL) + (-4294967296L)) >> (long)32UL) * (uint64_t)*(uint32_t *)4338956UL;
    var_25 = (uint32_t)var_24;
    var_26 = helper_cc_compute_all_wrapper((uint64_t)var_25, (uint64_t)((uint32_t)((int)var_25 >> (int)31U) - (uint32_t)(var_24 >> 32UL)), 0UL, 4U);
    var_27 = ((uint64_t)((uint16_t)var_26 & (unsigned short)2048U) == 0UL);
    var_28 = (uint32_t *)(var_0 + (-20L));
    var_29 = var_25;
    if (var_27) {
        *var_28 = var_25;
    } else {
        *var_28 = 2147483647U;
        var_29 = 2147483647U;
    }
    var_30 = (uint64_t)(*(uint32_t *)4338408UL - *var_12);
    var_31 = (uint64_t)var_29;
    var_32 = var_30 - var_31;
    var_33 = helper_cc_compute_all_wrapper(var_32, var_31, 0UL, 16U);
    var_34 = ((uint64_t)((uint16_t)var_33 & (unsigned short)2048U) == 0UL);
    var_35 = (uint32_t *)(var_0 + (-24L));
    var_36 = (uint32_t)var_32;
    var_37 = var_36;
    if (var_34) {
        *var_35 = var_36;
    } else {
        *var_35 = 0U;
        var_37 = 0U;
        rcx3_160 = 1UL;
    }
    var_38 = (uint64_t)var_37;
    var_39 = (uint64_t)*(uint32_t *)4338428UL;
    var_40 = helper_idivl_EAX_wrapper((struct type_6 *)(0UL), var_39, 4208455UL, r9, rbx, var_38, var_9, rcx3_160, (uint64_t)(uint32_t)(uint64_t)((long)(var_38 << 32UL) >> (long)63UL), r8, r10, var_39, rdi, state_0x9018_1, state_0x9010_1, var_4, var_5, state_0x82d8_1, state_0x9080_1, state_0x8248_1);
    var_41 = (uint32_t)var_40.field_1;
    *(uint32_t *)4338868UL = var_41;
    var_42 = helper_cc_compute_all_wrapper((uint64_t)var_41, 0UL, 0UL, 24U);
    if ((uint64_t)(((unsigned char)(var_42 >> 4UL) ^ (unsigned char)var_42) & '\xc0') == 0UL) {
        var_43 = var_0 + (-48L);
        *(uint64_t *)var_43 = 4208498UL;
        indirect_placeholder_33(r9, 0UL, rcx3_160, 4308293UL, r8, 0UL, 1UL);
        local_sp_0 = var_43;
    }
    local_sp_1 = local_sp_0;
    if (*(unsigned char *)4338924UL == '\x00') {
        *(uint64_t *)(local_sp_1 + (-8L)) = 4208584UL;
        indirect_placeholder();
        var_47 = *(uint32_t *)4338416UL;
        var_48 = (uint64_t)(((int)var_47 > (int)8U) ? var_47 : 8U);
        *(uint64_t *)(local_sp_1 + (-16L)) = 4208611UL;
        var_49 = indirect_placeholder_13(var_48);
        *(uint64_t *)4339024UL = var_49;
        return;
    }
    *(uint64_t *)(local_sp_0 + (-8L)) = 4208524UL;
    indirect_placeholder();
    var_44 = *(uint32_t *)4338456UL;
    rax_0 = (var_44 > 11U) ? ((uint64_t)var_44 + 1UL) : 12UL;
    var_45 = local_sp_0 + (-16L);
    *(uint64_t *)var_45 = 4208562UL;
    var_46 = indirect_placeholder_13(rax_0);
    *(uint64_t *)4338936UL = var_46;
    local_sp_1 = var_45;
    *(uint64_t *)(local_sp_1 + (-8L)) = 4208584UL;
    indirect_placeholder();
    var_47 = *(uint32_t *)4338416UL;
    var_48 = (uint64_t)(((int)var_47 > (int)8U) ? var_47 : 8U);
    *(uint64_t *)(local_sp_1 + (-16L)) = 4208611UL;
    var_49 = indirect_placeholder_13(var_48);
    *(uint64_t *)4339024UL = var_49;
    return;
}
