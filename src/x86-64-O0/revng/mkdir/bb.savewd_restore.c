typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
uint64_t bb_savewd_restore(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint32_t **var_5;
    uint32_t var_6;
    uint64_t local_sp_2;
    uint32_t *var_18;
    uint32_t var_19;
    uint32_t var_20;
    uint64_t var_21;
    uint64_t rax_0;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t local_sp_1_be;
    uint64_t var_24;
    uint32_t var_17;
    uint64_t var_8;
    uint32_t var_9;
    uint32_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint32_t var_7;
    uint32_t var_13;
    uint32_t *var_14;
    uint32_t var_16;
    uint64_t var_15;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t rax_1;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    var_3 = var_0 + (-32L);
    var_4 = (uint64_t *)var_3;
    *var_4 = rdi;
    *(uint32_t *)(var_0 + (-36L)) = (uint32_t)rsi;
    var_5 = (uint32_t **)var_3;
    var_6 = **var_5;
    local_sp_0 = var_2;
    rax_0 = 0UL;
    local_sp_2 = var_2;
    rax_1 = (uint64_t)var_6;
    if ((uint64_t)(var_6 + (-4)) != 0UL) {
        if (var_6 <= 4U) {
            *(uint64_t *)(var_0 + (-48L)) = 4216671UL;
            indirect_placeholder_2();
            return rax_0;
        }
        if ((uint64_t)(var_6 + (-3)) != 0UL) {
            var_13 = *(uint32_t *)(*var_4 + 4UL);
            var_14 = (uint32_t *)(var_0 + (-12L));
            *var_14 = var_13;
            var_16 = var_13;
            if (var_13 == 0U) {
                var_15 = var_0 + (-48L);
                *(uint64_t *)var_15 = 4216523UL;
                indirect_placeholder_2();
                var_16 = *var_14;
                local_sp_0 = var_15;
            }
            var_17 = var_16;
            local_sp_1 = local_sp_0;
            if ((int)var_16 <= (int)0U) {
                *(uint64_t *)(local_sp_1 + (-8L)) = 4216597UL;
                indirect_placeholder_2();
                while ((int)var_17 >= (int)0U)
                    {
                        var_22 = (uint64_t)var_17;
                        var_23 = local_sp_1 + (-16L);
                        *(uint64_t *)var_23 = 4216540UL;
                        indirect_placeholder_2();
                        local_sp_1_be = var_23;
                        if (*(uint32_t *)var_22 == 4U) {
                            var_24 = local_sp_1 + (-24L);
                            *(uint64_t *)var_24 = 4216572UL;
                            indirect_placeholder_2();
                            local_sp_1_be = var_24;
                        }
                        var_17 = *var_14;
                        local_sp_1 = local_sp_1_be;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4216597UL;
                        indirect_placeholder_2();
                    }
                *(uint32_t *)(*var_4 + 4UL) = 4294967295U;
                var_18 = (uint32_t *)(var_0 + (-20L));
                var_19 = *var_18;
                var_20 = var_19;
                if ((var_19 & 127U) != 0U) {
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4216635UL;
                    indirect_placeholder_2();
                    var_20 = *var_18;
                }
                var_21 = (uint64_t)(uint32_t)(unsigned char)(var_20 >> 8U);
                rax_0 = var_21;
            }
            return rax_0;
        }
        if ((uint64_t)(var_6 & (-4)) != 0UL) {
            *(uint64_t *)(var_0 + (-48L)) = 4216671UL;
            indirect_placeholder_2();
            return rax_0;
        }
        if ((uint64_t)(var_6 & (-2)) == 0UL) {
            return rax_0;
        }
        if ((uint64_t)(var_6 + (-2)) != 0UL) {
            *(uint64_t *)(var_0 + (-48L)) = 4216671UL;
            indirect_placeholder_2();
            return rax_0;
        }
        var_7 = *(uint32_t *)(*var_4 + 4UL);
        *(uint64_t *)(var_0 + (-48L)) = 4216410UL;
        indirect_placeholder_2();
        if (var_7 != 0U) {
            **var_5 = 1U;
            return rax_0;
        }
        var_8 = (uint64_t)var_7;
        *(uint64_t *)(var_0 + (-56L)) = 4216434UL;
        indirect_placeholder_2();
        var_9 = *(uint32_t *)var_8;
        var_10 = (uint32_t *)(var_0 + (-16L));
        *var_10 = var_9;
        var_11 = var_0 + (-64L);
        *(uint64_t *)var_11 = 4216453UL;
        indirect_placeholder_2();
        **var_5 = 4U;
        var_12 = *var_4;
        *(uint32_t *)(var_12 + 4UL) = *var_10;
        local_sp_2 = var_11;
        rax_1 = var_12;
    }
    *(uint64_t *)(local_sp_2 + (-8L)) = 4216478UL;
    indirect_placeholder_2();
    *(uint32_t *)rax_1 = *(uint32_t *)(*var_4 + 4UL);
    rax_0 = 4294967295UL;
    return rax_0;
}
