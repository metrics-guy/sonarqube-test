typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
uint64_t bb_dirchownmod(uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi, uint64_t rdi, uint64_t r9) {
    uint64_t local_sp_4;
    uint32_t var_58;
    uint32_t var_64;
    bool var_59;
    uint64_t *var_60;
    uint32_t var_61;
    uint32_t *var_62;
    uint64_t rax_0;
    uint64_t var_0;
    uint64_t var_1;
    uint32_t *var_2;
    uint64_t *var_3;
    uint32_t *var_4;
    uint32_t *var_5;
    uint32_t *var_6;
    uint32_t *var_7;
    uint32_t var_8;
    uint64_t local_sp_0;
    uint32_t var_56;
    uint32_t var_57;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint32_t var_38;
    uint64_t rax_4;
    uint32_t var_39;
    uint32_t var_40;
    uint32_t var_41;
    uint64_t var_42;
    uint64_t rax_2;
    uint64_t local_sp_2;
    uint32_t *var_14;
    uint32_t var_15;
    uint32_t var_16;
    uint32_t *var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint32_t *var_21;
    uint32_t var_22;
    uint64_t rax_3;
    uint32_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    uint64_t local_sp_3;
    uint32_t var_26;
    uint32_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    bool var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint32_t var_43;
    uint32_t var_44;
    uint32_t var_45;
    uint32_t var_46;
    uint32_t var_47;
    uint32_t var_48;
    uint32_t var_49;
    uint64_t rax_5;
    uint64_t var_50;
    uint64_t var_51;
    bool var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t *var_55;
    uint64_t var_63;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint32_t *)(var_0 + (-188L));
    *var_2 = (uint32_t)rdi;
    var_3 = (uint64_t *)(var_0 + (-200L));
    *var_3 = rsi;
    var_4 = (uint32_t *)(var_0 + (-192L));
    *var_4 = (uint32_t)rdx;
    var_5 = (uint32_t *)(var_0 + (-204L));
    *var_5 = (uint32_t)rcx;
    var_6 = (uint32_t *)(var_0 + (-208L));
    *var_6 = (uint32_t)r8;
    var_7 = (uint32_t *)(var_0 + (-212L));
    *var_7 = (uint32_t)r9;
    var_8 = *var_2;
    if ((int)var_8 > (int)4294967295U) {
        var_12 = (uint64_t)var_8;
        var_13 = var_0 + (-224L);
        *(uint64_t *)var_13 = 4227693UL;
        indirect_placeholder_2();
        rax_2 = var_12;
        local_sp_2 = var_13;
    } else {
        var_9 = *var_3;
        var_10 = var_0 + (-224L);
        *(uint64_t *)var_10 = 4227668UL;
        var_11 = indirect_placeholder_4(var_9);
        rax_2 = var_11;
        local_sp_2 = var_10;
    }
    var_14 = (uint32_t *)(var_0 + (-12L));
    var_15 = (uint32_t)rax_2;
    *var_14 = var_15;
    local_sp_3 = local_sp_2;
    var_57 = var_15;
    rax_5 = rax_2;
    local_sp_4 = local_sp_2;
    if (var_15 != 0U) {
        var_16 = *(uint32_t *)(var_0 + (-160L));
        var_17 = (uint32_t *)(var_0 + (-20L));
        *var_17 = var_16;
        var_18 = (uint32_t)((uint16_t)var_16 & (unsigned short)61440U);
        var_19 = (uint64_t)var_18;
        rax_3 = var_19;
        var_57 = 4294967295U;
        rax_5 = var_19;
        if ((uint64_t)((var_18 + (-16384)) & (-4096)) == 0UL) {
            var_20 = local_sp_2 + (-8L);
            *(uint64_t *)var_20 = 4227735UL;
            indirect_placeholder_2();
            *(uint32_t *)var_19 = 20U;
            *var_14 = 4294967295U;
            local_sp_4 = var_20;
        } else {
            var_21 = (uint32_t *)(var_0 + (-16L));
            *var_21 = 0U;
            var_22 = *var_5;
            if (var_22 == 4294967295U) {
                var_23 = *(uint32_t *)(var_0 + (-156L));
                var_24 = (uint64_t)var_23;
                rax_3 = var_24;
                if ((uint64_t)(var_22 - var_23) == 0UL) {
                    var_25 = *var_6;
                    rax_4 = rax_3;
                    var_26 = *(uint32_t *)(var_0 + (-152L));
                    rax_4 = (uint64_t)var_26;
                    if (var_25 != 4294967295U & (uint64_t)(var_25 - var_26) != 0UL) {
                        var_27 = *var_2;
                        if ((int)var_27 < (int)0U) {
                            var_28 = (uint64_t)var_27;
                            var_29 = local_sp_2 + (-8L);
                            *(uint64_t *)var_29 = 4227850UL;
                            indirect_placeholder_2();
                            rax_1 = var_28;
                            local_sp_1 = var_29;
                        } else {
                            var_30 = (*var_4 == 4294967295U);
                            var_31 = (uint64_t)*var_6;
                            var_32 = (uint64_t)var_22;
                            var_33 = *var_3;
                            var_34 = local_sp_2 + (-8L);
                            var_35 = (uint64_t *)var_34;
                            local_sp_1 = var_34;
                            if (var_30) {
                                *var_35 = 4227921UL;
                                var_37 = indirect_placeholder(var_31, var_32, var_33);
                                rax_1 = var_37;
                            } else {
                                *var_35 = 4227890UL;
                                var_36 = indirect_placeholder(var_31, var_32, var_33);
                                rax_1 = var_36;
                            }
                        }
                        var_38 = (uint32_t)rax_1;
                        *var_14 = var_38;
                        rax_4 = rax_1;
                        local_sp_3 = local_sp_1;
                        var_39 = *var_17;
                        var_40 = var_39 & 73U;
                        rax_4 = (uint64_t)var_40;
                        if (var_38 != 0U & var_40 == 0U) {
                            var_41 = (uint32_t)((uint16_t)var_39 & (unsigned short)3072U);
                            var_42 = (uint64_t)var_41;
                            *var_21 = var_41;
                            rax_4 = var_42;
                        }
                    }
                } else {
                    var_27 = *var_2;
                    if ((int)var_27 < (int)0U) {
                        var_28 = (uint64_t)var_27;
                        var_29 = local_sp_2 + (-8L);
                        *(uint64_t *)var_29 = 4227850UL;
                        indirect_placeholder_2();
                        rax_1 = var_28;
                        local_sp_1 = var_29;
                    } else {
                        var_30 = (*var_4 == 4294967295U);
                        var_31 = (uint64_t)*var_6;
                        var_32 = (uint64_t)var_22;
                        var_33 = *var_3;
                        var_34 = local_sp_2 + (-8L);
                        var_35 = (uint64_t *)var_34;
                        local_sp_1 = var_34;
                        if (var_30) {
                            *var_35 = 4227890UL;
                            var_36 = indirect_placeholder(var_31, var_32, var_33);
                            rax_1 = var_36;
                        } else {
                            *var_35 = 4227921UL;
                            var_37 = indirect_placeholder(var_31, var_32, var_33);
                            rax_1 = var_37;
                        }
                    }
                    var_38 = (uint32_t)rax_1;
                    *var_14 = var_38;
                    rax_4 = rax_1;
                    local_sp_3 = local_sp_1;
                    var_39 = *var_17;
                    var_40 = var_39 & 73U;
                    rax_4 = (uint64_t)var_40;
                    if (var_38 != 0U & var_40 == 0U) {
                        var_41 = (uint32_t)((uint16_t)var_39 & (unsigned short)3072U);
                        var_42 = (uint64_t)var_41;
                        *var_21 = var_41;
                        rax_4 = var_42;
                    }
                }
            } else {
                var_25 = *var_6;
                rax_4 = rax_3;
                var_26 = *(uint32_t *)(var_0 + (-152L));
                rax_4 = (uint64_t)var_26;
                if (var_25 != 4294967295U & (uint64_t)(var_25 - var_26) != 0UL) {
                    var_27 = *var_2;
                    if ((int)var_27 < (int)0U) {
                        var_28 = (uint64_t)var_27;
                        var_29 = local_sp_2 + (-8L);
                        *(uint64_t *)var_29 = 4227850UL;
                        indirect_placeholder_2();
                        rax_1 = var_28;
                        local_sp_1 = var_29;
                    } else {
                        var_30 = (*var_4 == 4294967295U);
                        var_31 = (uint64_t)*var_6;
                        var_32 = (uint64_t)var_22;
                        var_33 = *var_3;
                        var_34 = local_sp_2 + (-8L);
                        var_35 = (uint64_t *)var_34;
                        local_sp_1 = var_34;
                        if (var_30) {
                            *var_35 = 4227890UL;
                            var_36 = indirect_placeholder(var_31, var_32, var_33);
                            rax_1 = var_36;
                        } else {
                            *var_35 = 4227921UL;
                            var_37 = indirect_placeholder(var_31, var_32, var_33);
                            rax_1 = var_37;
                        }
                    }
                    var_38 = (uint32_t)rax_1;
                    *var_14 = var_38;
                    rax_4 = rax_1;
                    local_sp_3 = local_sp_1;
                    var_39 = *var_17;
                    var_40 = var_39 & 73U;
                    rax_4 = (uint64_t)var_40;
                    if (var_38 != 0U & var_40 == 0U) {
                        var_41 = (uint32_t)((uint16_t)var_39 & (unsigned short)3072U);
                        var_42 = (uint64_t)var_41;
                        *var_21 = var_41;
                        rax_4 = var_42;
                    }
                }
            }
            var_43 = *var_14;
            var_57 = var_43;
            rax_5 = rax_4;
            local_sp_4 = local_sp_3;
            var_44 = *var_17;
            var_45 = *var_7;
            var_46 = (var_44 ^ var_45) | *var_21;
            var_47 = *(uint32_t *)(var_0 | 8UL);
            var_48 = var_46 & var_47;
            var_57 = 0U;
            rax_5 = (uint64_t)var_48;
            if (var_43 != 0U & var_48 != 0U) {
                *(uint32_t *)(var_0 + (-24L)) = ((uint32_t)(((uint16_t)var_44 & ((uint16_t)var_47 ^ (unsigned short)4095U)) & (unsigned short)4095U) | var_45);
                var_49 = *var_2;
                if ((int)var_49 < (int)0U) {
                    var_50 = (uint64_t)var_49;
                    var_51 = local_sp_3 + (-8L);
                    *(uint64_t *)var_51 = 4228025UL;
                    indirect_placeholder_2();
                    rax_0 = var_50;
                    local_sp_0 = var_51;
                } else {
                    var_52 = (*var_4 == 4294967295U);
                    var_53 = *var_3;
                    var_54 = local_sp_3 + (-8L);
                    var_55 = (uint64_t *)var_54;
                    rax_0 = var_53;
                    local_sp_0 = var_54;
                    if (var_52) {
                        *var_55 = 4228078UL;
                        indirect_placeholder_2();
                    } else {
                        *var_55 = 4228056UL;
                        indirect_placeholder_2();
                    }
                }
                var_56 = (uint32_t)rax_0;
                *var_14 = var_56;
                var_57 = var_56;
                rax_5 = rax_0;
                local_sp_4 = local_sp_0;
            }
        }
    }
    var_58 = *var_2;
    var_64 = var_57;
    if ((int)var_58 < (int)0U) {
        return (uint64_t)var_64;
    }
    var_59 = (var_57 == 0U);
    var_60 = (uint64_t *)(local_sp_4 + (-8L));
    var_64 = var_58;
    if (var_59) {
        *var_60 = 4228109UL;
        indirect_placeholder_2();
        *var_14 = var_58;
    } else {
        *var_60 = 4228119UL;
        indirect_placeholder_2();
        var_61 = *(uint32_t *)rax_5;
        var_62 = (uint32_t *)(var_0 + (-28L));
        *var_62 = var_61;
        var_63 = (uint64_t)*var_2;
        *(uint64_t *)(local_sp_4 + (-16L)) = 4228137UL;
        indirect_placeholder_2();
        *(uint64_t *)(local_sp_4 + (-24L)) = 4228142UL;
        indirect_placeholder_2();
        *(uint32_t *)var_63 = *var_62;
        var_64 = *var_14;
    }
    return (uint64_t)var_64;
}
