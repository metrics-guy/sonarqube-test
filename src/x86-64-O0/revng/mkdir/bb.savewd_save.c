typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_savewd_save(uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rdi, uint64_t r9) {
    uint64_t var_9;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t *var_5;
    uint32_t **var_6;
    uint32_t var_7;
    uint64_t var_18;
    uint64_t storemerge;
    uint32_t *_cast4;
    uint32_t *_cast2;
    uint64_t var_15;
    uint64_t local_sp_0;
    uint64_t local_sp_1;
    uint64_t var_12;
    uint32_t var_13;
    uint64_t var_14;
    uint64_t var_8;
    uint32_t *var_10;
    uint32_t var_11;
    uint32_t var_16;
    uint32_t var_17;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_2;
    var_3 = var_0 + (-40L);
    var_4 = var_0 + (-32L);
    var_5 = (uint64_t *)var_4;
    *var_5 = rdi;
    var_6 = (uint32_t **)var_4;
    var_7 = **var_6;
    local_sp_1 = var_3;
    storemerge = 0UL;
    if (var_7 > 5U) {
        *(uint64_t *)(var_0 + (-48L)) = 4215895UL;
        indirect_placeholder_2();
    } else {
        var_8 = helper_cc_compute_c_wrapper((uint64_t)var_7 + (-4L), 4UL, var_1, 16U);
        if (var_8 != 0UL) {
            if ((uint64_t)(var_7 + (-3)) == 0UL) {
                var_16 = *(uint32_t *)(*var_5 + 4UL);
                *(uint64_t *)(local_sp_1 + (-8L)) = 4215806UL;
                indirect_placeholder_2();
                *(uint32_t *)(*var_5 + 4UL) = var_16;
                var_17 = *(uint32_t *)(*var_5 + 4UL);
                var_18 = helper_cc_compute_all_wrapper((uint64_t)var_17, 0UL, 0UL, 24U);
                storemerge = 1UL;
                if ((int)var_16 <= (int)4294967295U & var_17 != 0U & (uint64_t)(((unsigned char)(var_18 >> 4UL) ^ (unsigned char)var_18) & '\xc0') == 0UL) {
                    _cast4 = (uint32_t *)*var_5;
                    *_cast4 = 4U;
                    *(uint64_t *)(local_sp_1 + (-16L)) = 4215859UL;
                    indirect_placeholder_2();
                    *(uint32_t *)(*var_5 + 4UL) = *_cast4;
                    storemerge = 0UL;
                }
            } else {
                if ((uint64_t)(var_7 & (-4)) != 0UL) {
                    *(uint64_t *)(var_0 + (-48L)) = 4215895UL;
                    indirect_placeholder_2();
                    return storemerge;
                }
                if (var_7 != 0U) {
                    if ((uint64_t)((var_7 + (-1)) & (-2)) == 0UL) {
                        return storemerge;
                    }
                    *(uint64_t *)(var_0 + (-48L)) = 4215895UL;
                    indirect_placeholder_2();
                    return storemerge;
                }
                *(uint64_t *)(var_0 + (-48L)) = 4215682UL;
                var_9 = indirect_placeholder_40(0UL, rcx, rdx, r8, 2097152UL, 4284176UL, r9);
                var_10 = (uint32_t *)(var_0 + (-12L));
                var_11 = (uint32_t)var_9;
                *var_10 = var_11;
                if ((int)var_11 >= (int)0U) {
                    **var_6 = 1U;
                    *(uint32_t *)(*var_5 + 4UL) = *var_10;
                    return storemerge;
                }
                var_12 = var_0 + (-56L);
                *(uint64_t *)var_12 = 4215721UL;
                indirect_placeholder_2();
                var_13 = *(uint32_t *)var_9;
                var_14 = (uint64_t)var_13;
                local_sp_0 = var_12;
                if ((uint64_t)(var_13 + (-13)) != 0UL) {
                    var_15 = var_0 + (-64L);
                    *(uint64_t *)var_15 = 4215733UL;
                    indirect_placeholder_2();
                    local_sp_0 = var_15;
                    if (*(uint32_t *)var_14 != 116U) {
                        _cast2 = (uint32_t *)*var_5;
                        *_cast2 = 4U;
                        *(uint64_t *)(var_0 + (-72L)) = 4215755UL;
                        indirect_placeholder_2();
                        *(uint32_t *)(*var_5 + 4UL) = *_cast2;
                        return storemerge;
                    }
                }
                **var_6 = 3U;
                *(uint32_t *)(*var_5 + 4UL) = 4294967295U;
                local_sp_1 = local_sp_0;
            }
        }
    }
}
