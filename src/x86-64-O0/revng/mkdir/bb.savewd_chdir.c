typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t init_rax(void);
uint64_t bb_savewd_chdir(uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi, uint64_t rdi, uint64_t r9) {
    uint64_t var_28;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint32_t *var_8;
    uint64_t *var_9;
    uint32_t *var_10;
    uint32_t *var_11;
    uint64_t rdx2_1;
    uint64_t rax_0;
    uint64_t local_sp_0;
    uint32_t var_34;
    uint64_t rax_4;
    uint32_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t var_33;
    uint32_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint32_t var_18;
    uint64_t var_19;
    uint32_t var_20;
    uint64_t rdx2_0;
    uint64_t rax_1;
    uint64_t local_sp_1;
    uint64_t var_21;
    uint32_t var_22;
    uint64_t var_23;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t rax_2;
    uint64_t local_sp_2;
    uint64_t local_sp_4;
    uint32_t var_24;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t var_27;
    uint32_t var_43;
    uint32_t *var_44;
    uint64_t var_45;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_cc_src2();
    var_2 = init_rax();
    var_3 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_3;
    var_4 = var_0 + (-56L);
    var_5 = var_0 + (-32L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rdi;
    var_7 = (uint64_t *)(var_0 + (-40L));
    *var_7 = rsi;
    var_8 = (uint32_t *)(var_0 + (-44L));
    *var_8 = (uint32_t)rdx;
    var_9 = (uint64_t *)var_4;
    *var_9 = rcx;
    var_10 = (uint32_t *)(var_0 + (-12L));
    *var_10 = 4294967295U;
    var_11 = (uint32_t *)(var_0 + (-16L));
    *var_11 = 0U;
    rdx2_1 = rdx;
    var_20 = 0U;
    rax_1 = 0UL;
    rax_2 = var_2;
    local_sp_2 = var_4;
    if (*var_9 != 0UL) {
        var_12 = (uint64_t)((uint32_t)((uint64_t)*var_8 << 17UL) & 131072U) | 2164992UL;
        var_13 = var_0 + (-64L);
        *(uint64_t *)var_13 = 4215989UL;
        indirect_placeholder_2();
        *var_10 = 0U;
        var_14 = *var_9;
        rdx2_0 = var_12;
        local_sp_1 = var_13;
        if (var_14 == 0UL) {
            var_15 = (uint32_t *)var_14;
            *var_15 = 0U;
            var_16 = var_0 + (-72L);
            *(uint64_t *)var_16 = 4216013UL;
            indirect_placeholder_2();
            var_17 = *var_9 + 4UL;
            var_18 = *var_15;
            var_19 = (uint64_t)var_18;
            *(uint32_t *)var_17 = var_18;
            var_20 = *var_10;
            rdx2_0 = var_17;
            rax_1 = var_19;
            local_sp_1 = var_16;
        }
        rdx2_1 = rdx2_0;
        rax_2 = rax_1;
        local_sp_2 = local_sp_1;
        var_21 = local_sp_1 + (-8L);
        *(uint64_t *)var_21 = 4216036UL;
        indirect_placeholder_2();
        var_22 = *(uint32_t *)rax_1;
        var_23 = (uint64_t)var_22;
        rax_2 = var_23;
        local_sp_2 = var_21;
        if ((int)var_20 <= (int)4294967295U & (uint64_t)(var_22 + (-13)) == 0UL) {
            *var_11 = 4294967295U;
        }
    }
    rax_4 = rax_2;
    local_sp_4 = local_sp_2;
    if (*var_11 == 0U) {
        if ((int)*var_10 >= (int)0U & *var_9 == 0UL) {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4216295UL;
            indirect_placeholder_2();
            var_43 = *(uint32_t *)rax_4;
            var_44 = (uint32_t *)(var_0 + (-20L));
            *var_44 = var_43;
            var_45 = (uint64_t)*var_10;
            *(uint64_t *)(local_sp_4 + (-16L)) = 4216310UL;
            indirect_placeholder_2();
            *(uint64_t *)(local_sp_4 + (-24L)) = 4216315UL;
            indirect_placeholder_2();
            *(uint32_t *)var_45 = *var_44;
        }
        return (uint64_t)*var_11;
    }
    if ((int)*var_10 >= (int)0U) {
        var_24 = *var_8 & 2U;
        var_25 = (uint64_t)var_24;
        rax_4 = var_25;
        if (var_24 != 0U) {
            if ((int)*var_10 >= (int)0U & *var_9 == 0UL) {
                *(uint64_t *)(local_sp_4 + (-8L)) = 4216295UL;
                indirect_placeholder_2();
                var_43 = *(uint32_t *)rax_4;
                var_44 = (uint32_t *)(var_0 + (-20L));
                *var_44 = var_43;
                var_45 = (uint64_t)*var_10;
                *(uint64_t *)(local_sp_4 + (-16L)) = 4216310UL;
                indirect_placeholder_2();
                *(uint64_t *)(local_sp_4 + (-24L)) = 4216315UL;
                indirect_placeholder_2();
                *(uint32_t *)var_45 = *var_44;
            }
            return (uint64_t)*var_11;
        }
    }
    var_26 = *var_6;
    var_27 = local_sp_2 + (-8L);
    *(uint64_t *)var_27 = 4216092UL;
    var_28 = indirect_placeholder_33(rcx, rdx2_1, r8, var_26, r9);
    rax_4 = var_28;
    local_sp_4 = var_27;
    if ((uint64_t)(unsigned char)var_28 == 0UL) {
        *var_9 = 0UL;
        *var_11 = 4294967294U;
    } else {
        var_29 = *var_10;
        if ((int)var_29 > (int)4294967295U) {
            var_32 = (uint64_t)var_29;
            var_33 = local_sp_2 + (-16L);
            *(uint64_t *)var_33 = 4216146UL;
            indirect_placeholder_2();
            rax_0 = var_32;
            local_sp_0 = var_33;
        } else {
            var_30 = *var_7;
            var_31 = local_sp_2 + (-16L);
            *(uint64_t *)var_31 = 4216134UL;
            indirect_placeholder_2();
            rax_0 = var_30;
            local_sp_0 = var_31;
        }
        var_34 = (uint32_t)rax_0;
        *var_11 = var_34;
        rax_4 = rax_0;
        local_sp_4 = local_sp_0;
        if (var_34 != 0U) {
            var_35 = **(uint32_t **)var_5;
            var_36 = (uint64_t)var_35;
            rax_4 = var_36;
            if (var_35 > 5U) {
                var_42 = local_sp_0 + (-8L);
                *(uint64_t *)var_42 = 4216268UL;
                indirect_placeholder_2();
                local_sp_4 = var_42;
            } else {
                var_37 = helper_cc_compute_c_wrapper(var_36 + (-4L), 4UL, var_1, 16U);
                if (var_37 != 0UL) {
                    if ((uint64_t)(var_35 + (-3)) == 0UL) {
                        var_39 = *(uint32_t *)(*var_6 + 4UL);
                        var_40 = (uint64_t)var_39;
                        rax_4 = var_40;
                        if (var_39 == 0U) {
                            var_41 = local_sp_0 + (-8L);
                            *(uint64_t *)var_41 = 4216241UL;
                            indirect_placeholder_2();
                            local_sp_4 = var_41;
                        }
                    } else {
                        if ((uint64_t)(var_35 & (-4)) == 0UL) {
                            var_42 = local_sp_0 + (-8L);
                            *(uint64_t *)var_42 = 4216268UL;
                            indirect_placeholder_2();
                            local_sp_4 = var_42;
                        } else {
                            if ((uint64_t)(var_35 + (-1)) == 0UL) {
                                var_38 = *var_6;
                                *(uint32_t *)var_38 = 2U;
                                rax_4 = var_38;
                            } else {
                                if ((uint64_t)(var_35 + (-2)) == 0UL) {
                                    var_42 = local_sp_0 + (-8L);
                                    *(uint64_t *)var_42 = 4216268UL;
                                    indirect_placeholder_2();
                                    local_sp_4 = var_42;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
