typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
extern void abort(void);
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
extern uint64_t indirect_placeholder(uint64_t param_0, uint64_t param_1, uint64_t param_2);
extern uint64_t indirect_placeholder_4(uint64_t param_0);
extern uint64_t init_rbx(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_8(void);
extern uint64_t indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern void indirect_placeholder_19(uint64_t param_0);
extern void indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0);
extern void indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4);
void bb_main(uint64_t rsi, uint64_t rdi) {
    uint64_t var_61;
    struct indirect_placeholder_38_ret_type var_20;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint32_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint32_t *var_11;
    uint64_t var_12;
    uint64_t *var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint32_t *var_16;
    uint64_t local_sp_5;
    uint64_t r8_0;
    uint64_t var_53;
    struct indirect_placeholder_35_ret_type var_54;
    uint64_t var_55;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t r9_0;
    uint64_t var_59;
    uint64_t local_sp_0;
    uint64_t var_60;
    uint64_t var_62;
    uint64_t local_sp_3;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t *var_52;
    uint32_t var_38;
    uint64_t local_sp_1;
    uint64_t local_sp_2;
    uint64_t var_39;
    struct indirect_placeholder_36_ret_type var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_32;
    uint32_t var_33;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint32_t var_37;
    uint32_t *var_26;
    uint64_t var_27;
    bool var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t var_46;
    uint64_t var_17;
    uint32_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint32_t *var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t var_72;
    uint64_t local_sp_5_be;
    uint64_t local_sp_4;
    uint64_t var_66;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    bool var_70;
    uint64_t var_71;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_21;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint32_t var_25;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rbx();
    var_3 = var_0 + (-8L);
    *(uint64_t *)var_3 = var_1;
    *(uint64_t *)(var_0 + (-16L)) = var_2;
    var_4 = (uint32_t *)(var_0 + (-108L));
    *var_4 = (uint32_t)rdi;
    var_5 = var_0 + (-120L);
    var_6 = (uint64_t *)var_5;
    *var_6 = rsi;
    var_7 = (uint64_t *)(var_0 + (-32L));
    *var_7 = 0UL;
    var_8 = (uint64_t *)(var_0 + (-40L));
    *var_8 = 0UL;
    var_9 = var_0 + (-104L);
    var_10 = (uint64_t *)var_9;
    *var_10 = 0UL;
    var_11 = (uint32_t *)(var_0 + (-92L));
    *var_11 = 511U;
    var_12 = var_0 + (-88L);
    *(uint32_t *)var_12 = 0U;
    var_13 = (uint64_t *)(var_0 + (-80L));
    *var_13 = 0UL;
    *(unsigned char *)(var_0 + (-84L)) = (unsigned char)'\x00';
    var_14 = **(uint64_t **)var_5;
    *(uint64_t *)(var_0 + (-128L)) = 4205961UL;
    indirect_placeholder_19(var_14);
    *(uint64_t *)(var_0 + (-136L)) = 4205976UL;
    indirect_placeholder_2();
    var_15 = var_0 + (-144L);
    *(uint64_t *)var_15 = 4205986UL;
    indirect_placeholder_2();
    var_16 = (uint32_t *)(var_0 + (-48L));
    local_sp_5 = var_15;
    while (1U)
        {
            var_17 = *var_6;
            var_18 = (uint64_t)*var_4;
            var_19 = local_sp_5 + (-8L);
            *(uint64_t *)var_19 = 4206293UL;
            var_20 = indirect_placeholder_38(4280832UL, 4281977UL, 0UL, var_17, var_18);
            var_21 = var_20.field_0;
            var_22 = var_20.field_1;
            var_23 = var_20.field_2;
            var_24 = var_20.field_3;
            var_25 = (uint32_t)var_21;
            *var_16 = var_25;
            r8_0 = var_23;
            r9_0 = var_24;
            local_sp_2 = var_19;
            local_sp_5_be = var_19;
            local_sp_4 = var_19;
            switch_state_var = 0;
            switch (var_25) {
              case 118U:
                {
                    *var_13 = 4281865UL;
                    local_sp_5 = local_sp_5_be;
                    continue;
                }
                break;
              case 4294967295U:
                {
                    if ((uint64_t)(*var_4 - *(uint32_t *)4301656UL) == 0UL) {
                        *(uint64_t *)(local_sp_5 + (-16L)) = 4206342UL;
                        indirect_placeholder_30(0UL, var_22, 4281983UL, var_23, 0UL, 0UL, var_24);
                        *(uint64_t *)(local_sp_5 + (-24L)) = 4206352UL;
                        indirect_placeholder_7(var_3, 1UL);
                        abort();
                    }
                    if (*var_8 != 0UL) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_26 = (uint32_t *)(var_0 + (-44L));
                    *var_26 = 0U;
                    *(uint64_t *)(local_sp_5 + (-16L)) = 4206371UL;
                    var_27 = indirect_placeholder_8();
                    var_28 = ((uint64_t)(unsigned char)var_27 == 0UL);
                    var_29 = *var_8;
                    var_30 = local_sp_5 + (-24L);
                    var_31 = (uint64_t *)var_30;
                    local_sp_1 = var_30;
                    if (!var_28) {
                        *var_31 = 4206387UL;
                        var_32 = indirect_placeholder_4(var_29);
                        var_33 = (uint32_t)var_32;
                        *var_26 = var_33;
                        var_38 = var_33;
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    *var_31 = 4206404UL;
                    var_34 = indirect_placeholder_4(var_29);
                    var_35 = local_sp_5 + (-32L);
                    *(uint64_t *)var_35 = 4206412UL;
                    var_36 = indirect_placeholder_4(var_34);
                    var_37 = (uint32_t)var_36;
                    *var_26 = var_37;
                    var_38 = var_37;
                    local_sp_1 = var_35;
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    if ((int)var_25 <= (int)118U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    if (var_25 == 112U) {
                        *var_10 = 4205207UL;
                    } else {
                        if ((int)var_25 <= (int)112U) {
                            loop_state_var = 0U;
                            switch_state_var = 1;
                            break;
                        }
                        if (var_25 == 109U) {
                            *var_7 = *(uint64_t *)4302272UL;
                        } else {
                            if ((int)var_25 <= (int)109U) {
                                loop_state_var = 0U;
                                switch_state_var = 1;
                                break;
                            }
                            if (var_25 != 90U) {
                                if ((int)var_25 <= (int)90U) {
                                    loop_state_var = 0U;
                                    switch_state_var = 1;
                                    break;
                                }
                                switch_state_var = 0;
                                switch (var_25) {
                                  case 4294967166U:
                                    {
                                        *(uint64_t *)(local_sp_5 + (-16L)) = 4206191UL;
                                        indirect_placeholder_7(var_3, 0UL);
                                        abort();
                                    }
                                    break;
                                  case 4294967165U:
                                    {
                                        var_66 = *(uint64_t *)4301504UL;
                                        *(uint64_t *)(local_sp_5 + (-16L)) = 4206243UL;
                                        indirect_placeholder_37(0UL, var_66, 4280544UL, 4281961UL, 4281774UL, 0UL);
                                        var_67 = local_sp_5 + (-24L);
                                        *(uint64_t *)var_67 = 4206253UL;
                                        indirect_placeholder_2();
                                        local_sp_4 = var_67;
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                  default:
                                    {
                                        loop_state_var = 0U;
                                        switch_state_var = 1;
                                        break;
                                    }
                                    break;
                                }
                                if (switch_state_var)
                                    break;
                            }
                            var_68 = local_sp_5 + (-16L);
                            *(uint64_t *)var_68 = 4206125UL;
                            var_69 = indirect_placeholder_8();
                            var_70 = ((uint64_t)(unsigned char)var_69 == 0UL);
                            var_71 = *(uint64_t *)4302272UL;
                            local_sp_5_be = var_68;
                            if (var_70) {
                                *var_8 = var_71;
                            } else {
                                if (var_71 == 0UL) {
                                    var_72 = local_sp_5 + (-24L);
                                    *(uint64_t *)var_72 = 4206179UL;
                                    indirect_placeholder_30(0UL, var_22, 4281888UL, var_23, 0UL, 0UL, var_24);
                                    local_sp_5_be = var_72;
                                }
                            }
                        }
                    }
                    local_sp_5 = local_sp_5_be;
                    continue;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            *(uint64_t *)(local_sp_4 + (-8L)) = 4206263UL;
            indirect_placeholder_7(var_3, 1UL);
            abort();
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 1U:
                {
                    var_46 = *var_10;
                    local_sp_3 = local_sp_2;
                    if (var_46 != 0UL) {
                        if (*var_7 == 0UL) {
                            var_63 = *(uint32_t *)4301656UL;
                            var_64 = *var_6 + ((uint64_t)var_63 << 3UL);
                            var_65 = (uint64_t)(*var_4 - var_63);
                            *(uint64_t *)(local_sp_3 + (-8L)) = 4206703UL;
                            indirect_placeholder_29(var_9, 4205512UL, var_64, var_65);
                            return;
                        }
                    }
                    *(uint64_t *)(local_sp_2 + (-8L)) = 4206498UL;
                    indirect_placeholder_2();
                    var_47 = (uint32_t *)(var_0 + (-52L));
                    *var_47 = (uint32_t)var_46;
                    var_48 = local_sp_2 + (-16L);
                    *(uint64_t *)var_48 = 4206511UL;
                    indirect_placeholder_2();
                    *(uint32_t *)(var_0 + (-96L)) = *var_47;
                    var_49 = *var_7;
                    local_sp_3 = var_48;
                    if (var_49 == 0UL) {
                        *var_11 = 511U;
                    } else {
                        var_50 = local_sp_2 + (-24L);
                        *(uint64_t *)var_50 = 4206536UL;
                        var_51 = indirect_placeholder(r8_0, var_49, r9_0);
                        var_52 = (uint64_t *)(var_0 + (-64L));
                        *var_52 = var_51;
                        var_59 = var_51;
                        local_sp_0 = var_50;
                        if (var_51 == 0UL) {
                            var_53 = *var_7;
                            *(uint64_t *)(local_sp_2 + (-32L)) = 4206559UL;
                            var_54 = indirect_placeholder_35(var_53);
                            var_55 = var_54.field_0;
                            var_56 = var_54.field_1;
                            var_57 = var_54.field_2;
                            var_58 = local_sp_2 + (-40L);
                            *(uint64_t *)var_58 = 4206587UL;
                            indirect_placeholder_30(0UL, var_55, 4282050UL, var_56, 0UL, 1UL, var_57);
                            var_59 = *var_52;
                            local_sp_0 = var_58;
                        }
                        var_60 = (uint64_t)*var_47;
                        *(uint64_t *)(local_sp_0 + (-8L)) = 4206625UL;
                        var_61 = indirect_placeholder_33(var_59, var_60, var_12, 1UL, 511UL);
                        *var_11 = (uint32_t)var_61;
                        var_62 = local_sp_0 + (-16L);
                        *(uint64_t *)var_62 = 4206640UL;
                        indirect_placeholder_2();
                        local_sp_3 = var_62;
                    }
                    var_63 = *(uint32_t *)4301656UL;
                    var_64 = *var_6 + ((uint64_t)var_63 << 3UL);
                    var_65 = (uint64_t)(*var_4 - var_63);
                    *(uint64_t *)(local_sp_3 + (-8L)) = 4206703UL;
                    indirect_placeholder_29(var_9, 4205512UL, var_64, var_65);
                    return;
                }
                break;
              case 2U:
                {
                    local_sp_2 = local_sp_1;
                    if ((int)var_38 > (int)4294967295U) {
                        var_39 = *var_8;
                        *(uint64_t *)(local_sp_1 + (-8L)) = 4206433UL;
                        var_40 = indirect_placeholder_36(var_39);
                        var_41 = var_40.field_0;
                        var_42 = var_40.field_1;
                        var_43 = var_40.field_2;
                        *(uint64_t *)(local_sp_1 + (-16L)) = 4206441UL;
                        indirect_placeholder_2();
                        var_44 = (uint64_t)*(uint32_t *)var_41;
                        var_45 = local_sp_1 + (-24L);
                        *(uint64_t *)var_45 = 4206468UL;
                        indirect_placeholder_30(0UL, var_41, 4282000UL, var_42, var_44, 1UL, var_43);
                        local_sp_2 = var_45;
                        r8_0 = var_42;
                        r9_0 = var_43;
                    }
                }
                break;
            }
        }
        break;
    }
}
