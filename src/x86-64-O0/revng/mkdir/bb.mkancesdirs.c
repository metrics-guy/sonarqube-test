typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder_2(void);
extern uint64_t indirect_placeholder_5(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5);
uint64_t bb_mkancesdirs(uint64_t rcx, uint64_t rdx, uint64_t r8, uint64_t rsi, uint64_t rdi, uint64_t r9) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t var_8;
    uint64_t var_9;
    uint64_t *var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t *var_13;
    unsigned char *var_14;
    unsigned char *var_15;
    uint32_t *var_16;
    uint32_t *var_17;
    uint32_t *var_18;
    uint64_t r83_0_ph;
    uint64_t var_40;
    uint64_t var_19;
    uint32_t var_33;
    uint32_t var_41;
    uint64_t rax_0;
    uint64_t local_sp_2;
    uint32_t var_42;
    uint64_t var_43;
    uint64_t storemerge;
    uint64_t var_30;
    unsigned char var_31;
    unsigned char var_20;
    unsigned char var_21;
    bool var_22;
    bool var_23;
    uint64_t var_24;
    uint64_t local_sp_0_ph;
    uint64_t r83_1;
    uint64_t local_sp_1;
    uint64_t var_25;
    uint64_t var_26;
    uint64_t r83_2;
    uint32_t var_32;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint32_t var_39;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-88L);
    var_3 = (uint64_t *)(var_0 + (-64L));
    *var_3 = rdi;
    var_4 = (uint64_t *)(var_0 + (-72L));
    *var_4 = rsi;
    var_5 = (uint64_t *)(var_0 + (-80L));
    *var_5 = rdx;
    *(uint64_t *)var_2 = rcx;
    var_6 = var_0 + (-16L);
    var_7 = (uint64_t *)var_6;
    *var_7 = 0UL;
    var_8 = *var_3;
    var_9 = var_0 + (-24L);
    var_10 = (uint64_t *)var_9;
    *var_10 = var_8;
    var_11 = *var_3;
    var_12 = var_0 + (-32L);
    var_13 = (uint64_t *)var_12;
    *var_13 = var_11;
    var_14 = (unsigned char *)(var_0 + (-33L));
    *var_14 = (unsigned char)'\x00';
    var_15 = (unsigned char *)(var_0 + (-45L));
    var_16 = (uint32_t *)(var_0 + (-40L));
    var_17 = (uint32_t *)(var_0 + (-44L));
    var_18 = (uint32_t *)(var_0 + (-52L));
    var_41 = 4294967295U;
    r83_0_ph = r8;
    local_sp_0_ph = var_2;
    var_31 = (unsigned char)'\x00';
    while (1U)
        {
            r83_1 = r83_0_ph;
            local_sp_1 = local_sp_0_ph;
            r83_2 = r83_0_ph;
            local_sp_2 = local_sp_0_ph;
            while (1U)
                {
                    var_19 = *var_13;
                    *var_13 = (var_19 + 1UL);
                    var_20 = *(unsigned char *)var_19;
                    *var_15 = var_20;
                    if (var_20 != '\x00') {
                        storemerge = *var_10 - *var_3;
                        loop_state_var = 1U;
                        break;
                    }
                    var_21 = **(unsigned char **)var_12;
                    var_22 = (var_21 == '/');
                    var_23 = (var_20 == '/');
                    if (var_22) {
                        if (var_23) {
                            *var_7 = *var_13;
                        }
                        continue;
                    }
                    if (!((var_23 ^ 1) || (var_21 == '\x00'))) {
                        continue;
                    }
                    var_24 = *var_7;
                    if (var_24 == 0UL) {
                        continue;
                    }
                    loop_state_var = 0U;
                    break;
                }
            switch_state_var = 0;
            switch (loop_state_var) {
              case 0U:
                {
                    if ((var_24 - *var_10) != 1UL) {
                        if (**(unsigned char **)var_9 != '.') {
                            *var_10 = *var_13;
                            r83_0_ph = r83_1;
                            local_sp_0_ph = local_sp_1;
                            continue;
                        }
                    }
                    *var_16 = 0U;
                    *var_17 = 0U;
                    **(unsigned char **)var_6 = (unsigned char)'\x00';
                    var_25 = *var_7;
                    var_26 = *var_10;
                    if ((var_25 - var_26) == 2UL) {
                        if (**(unsigned char **)var_9 == '.') {
                            if (*(unsigned char *)(var_26 + 1UL) == '.') {
                                *var_14 = (unsigned char)'\x00';
                            } else {
                                var_27 = *var_3;
                                var_28 = *var_5;
                                var_29 = local_sp_0_ph + (-8L);
                                *(uint64_t *)var_29 = 4227427UL;
                                indirect_placeholder_2();
                                var_31 = (unsigned char)'\x01';
                                r83_2 = var_28;
                                local_sp_2 = var_29;
                                if ((int)(uint32_t)var_27 > (int)4294967295U) {
                                    *var_14 = (unsigned char)'\x01';
                                } else {
                                    var_30 = local_sp_0_ph + (-16L);
                                    *(uint64_t *)var_30 = 4227436UL;
                                    indirect_placeholder_2();
                                    *var_16 = *(uint32_t *)var_27;
                                    var_31 = *var_14;
                                    local_sp_2 = var_30;
                                }
                            }
                        } else {
                            var_27 = *var_3;
                            var_28 = *var_5;
                            var_29 = local_sp_0_ph + (-8L);
                            *(uint64_t *)var_29 = 4227427UL;
                            indirect_placeholder_2();
                            var_31 = (unsigned char)'\x01';
                            r83_2 = var_28;
                            local_sp_2 = var_29;
                            if ((int)(uint32_t)var_27 > (int)4294967295U) {
                                var_30 = local_sp_0_ph + (-16L);
                                *(uint64_t *)var_30 = 4227436UL;
                                indirect_placeholder_2();
                                *var_16 = *(uint32_t *)var_27;
                                var_31 = *var_14;
                                local_sp_2 = var_30;
                            } else {
                                *var_14 = (unsigned char)'\x01';
                            }
                        }
                    } else {
                        var_27 = *var_3;
                        var_28 = *var_5;
                        var_29 = local_sp_0_ph + (-8L);
                        *(uint64_t *)var_29 = 4227427UL;
                        indirect_placeholder_2();
                        var_31 = (unsigned char)'\x01';
                        r83_2 = var_28;
                        local_sp_2 = var_29;
                        if ((int)(uint32_t)var_27 > (int)4294967295U) {
                            var_30 = local_sp_0_ph + (-16L);
                            *(uint64_t *)var_30 = 4227436UL;
                            indirect_placeholder_2();
                            *var_16 = *(uint32_t *)var_27;
                            var_31 = *var_14;
                            local_sp_2 = var_30;
                        } else {
                            *var_14 = (unsigned char)'\x01';
                        }
                    }
                    r83_1 = r83_2;
                    if (var_31 == '\x00') {
                        var_33 = *var_17;
                    } else {
                        var_32 = *var_17 | 1U;
                        *var_17 = var_32;
                        var_33 = var_32;
                    }
                    var_34 = (uint64_t)var_33;
                    var_35 = *var_10;
                    var_36 = *var_4;
                    var_37 = local_sp_2 + (-8L);
                    *(uint64_t *)var_37 = 4227481UL;
                    var_38 = indirect_placeholder_5(0UL, var_34, r83_2, var_35, var_36, r9);
                    var_39 = (uint32_t)var_38;
                    *var_18 = var_39;
                    rax_0 = var_38;
                    local_sp_1 = var_37;
                    if (var_39 == 4294967295U) {
                        var_40 = *var_7;
                        *(unsigned char *)var_40 = (unsigned char)'/';
                        var_41 = *var_18;
                        rax_0 = var_40;
                    }
                    if (var_41 == 0U) {
                        *var_10 = *var_13;
                        r83_0_ph = r83_1;
                        local_sp_0_ph = local_sp_1;
                        continue;
                    }
                    if (*var_16 != 0U) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_2 + (-16L)) = 4227514UL;
                    indirect_placeholder_2();
                    var_42 = *(uint32_t *)rax_0;
                    var_43 = (uint64_t)var_42;
                    if ((uint64_t)(var_42 + (-2)) != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    *(uint64_t *)(local_sp_2 + (-24L)) = 4227526UL;
                    indirect_placeholder_2();
                    *(uint32_t *)var_43 = *var_16;
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 1U:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            storemerge = (uint64_t)*var_18;
        }
        break;
      case 1U:
        {
            return storemerge;
        }
        break;
    }
}
