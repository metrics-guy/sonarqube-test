typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct indirect_placeholder_40_ret_type;
struct indirect_placeholder_39_ret_type;
struct indirect_placeholder_28_ret_type;
struct indirect_placeholder_29_ret_type;
struct indirect_placeholder_30_ret_type;
struct indirect_placeholder_31_ret_type;
struct indirect_placeholder_32_ret_type;
struct indirect_placeholder_33_ret_type;
struct indirect_placeholder_34_ret_type;
struct indirect_placeholder_35_ret_type;
struct indirect_placeholder_36_ret_type;
struct indirect_placeholder_37_ret_type;
struct indirect_placeholder_38_ret_type;
struct indirect_placeholder_40_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_39_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_28_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_29_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_30_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_31_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_32_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_33_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_34_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_35_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_36_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_37_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
struct indirect_placeholder_38_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern void indirect_placeholder_1(void);
extern uint64_t init_r10(void);
extern void indirect_placeholder_7(uint64_t param_0, uint64_t param_1);
extern void indirect_placeholder_6(uint64_t param_0);
extern uint64_t init_rbx(void);
extern uint64_t init_r8(void);
extern uint64_t init_r9(void);
extern struct indirect_placeholder_40_ret_type indirect_placeholder_40(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_39_ret_type indirect_placeholder_39(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_28_ret_type indirect_placeholder_28(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_29_ret_type indirect_placeholder_29(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_30_ret_type indirect_placeholder_30(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_31_ret_type indirect_placeholder_31(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_32_ret_type indirect_placeholder_32(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_33_ret_type indirect_placeholder_33(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_34_ret_type indirect_placeholder_34(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_35_ret_type indirect_placeholder_35(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6, uint64_t param_7);
extern struct indirect_placeholder_36_ret_type indirect_placeholder_36(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3);
extern struct indirect_placeholder_37_ret_type indirect_placeholder_37(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_38_ret_type indirect_placeholder_38(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
void bb_system_join(uint64_t rsi, uint64_t rdi) {
    struct indirect_placeholder_39_ret_type var_21;
    uint64_t local_sp_17;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    uint64_t var_9;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    struct indirect_placeholder_40_ret_type var_13;
    uint64_t var_14;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t var_18;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_22;
    uint64_t var_23;
    uint64_t var_24;
    uint64_t var_25;
    uint64_t local_sp_0;
    uint64_t r8_8;
    uint64_t *var_29;
    uint64_t var_147;
    uint64_t var_148;
    uint64_t local_sp_18;
    uint64_t local_sp_2;
    uint64_t local_sp_1;
    uint64_t r8_6;
    uint64_t var_135;
    uint64_t var_136;
    uint64_t local_sp_15;
    uint64_t local_sp_14;
    uint64_t local_sp_7;
    uint64_t local_sp_12_ph;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t local_sp_9;
    uint64_t local_sp_8;
    uint64_t local_sp_12_be;
    uint64_t r8_0;
    uint64_t r9_0;
    uint64_t r10_0;
    uint64_t local_sp_5;
    uint64_t var_100;
    uint64_t local_sp_3;
    uint64_t *_pre_phi;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t local_sp_4;
    uint64_t var_104;
    uint64_t var_105;
    uint64_t var_110;
    uint64_t var_106;
    uint64_t var_107;
    uint64_t var_108;
    uint64_t var_109;
    uint64_t var_36;
    uint64_t r8_5_be;
    uint64_t r9_5_be;
    uint64_t r10_5_be;
    uint64_t rbx_3_be;
    uint64_t local_sp_12;
    uint64_t r8_2;
    uint64_t var_94;
    uint64_t var_95;
    uint64_t var_96;
    uint64_t var_97;
    uint64_t var_98;
    struct indirect_placeholder_28_ret_type var_99;
    uint64_t local_sp_10;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t local_sp_6;
    uint64_t r8_1;
    uint64_t r9_1;
    uint64_t r10_1;
    uint64_t r9_2;
    uint64_t r10_2;
    uint64_t rbx_0;
    uint64_t var_86;
    uint64_t var_87;
    struct indirect_placeholder_29_ret_type var_88;
    uint64_t var_89;
    uint64_t var_90;
    uint64_t r8_3;
    uint64_t var_80;
    uint64_t var_81;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t var_84;
    struct indirect_placeholder_30_ret_type var_85;
    uint64_t r8_5;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    struct indirect_placeholder_31_ret_type var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_111;
    uint64_t var_112;
    uint64_t var_113;
    uint64_t var_114;
    struct indirect_placeholder_32_ret_type var_115;
    uint64_t var_116;
    uint64_t var_117;
    uint64_t var_118;
    uint64_t var_119;
    uint64_t r9_3;
    uint64_t r10_3;
    uint64_t rbx_1;
    uint64_t var_72;
    uint64_t var_73;
    struct indirect_placeholder_33_ret_type var_74;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_33;
    uint64_t var_34;
    struct indirect_placeholder_34_ret_type var_35;
    uint64_t storemerge;
    uint64_t local_sp_11;
    uint64_t r8_4;
    uint64_t r9_4;
    uint64_t r10_4;
    uint64_t rbx_2;
    uint64_t *_pre_phi324;
    uint64_t var_37;
    struct indirect_placeholder_35_ret_type var_38;
    uint64_t storemerge22;
    uint64_t storemerge21;
    uint64_t storemerge20;
    uint64_t *var_26;
    bool var_27;
    uint64_t *var_28;
    uint64_t *var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t r8_5_ph;
    uint64_t r9_5_ph;
    uint64_t r10_5_ph;
    uint64_t rbx_3_ph;
    uint64_t var_39;
    uint64_t var_40;
    uint32_t *var_41;
    unsigned char *var_42;
    uint64_t *var_43;
    unsigned char *var_44;
    uint64_t *var_45;
    uint64_t *var_46;
    uint64_t *var_47;
    uint64_t *var_48;
    uint64_t *var_49;
    uint64_t r9_5;
    uint64_t r10_5;
    uint64_t rbx_3;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t **var_52;
    uint64_t var_53;
    uint64_t **var_54;
    uint64_t var_55;
    uint64_t var_56;
    struct indirect_placeholder_36_ret_type var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t var_60;
    uint64_t var_61;
    uint32_t var_62;
    uint64_t var_120;
    uint64_t *var_121;
    unsigned char *var_122;
    unsigned char var_123;
    bool var_124;
    uint64_t local_sp_13;
    uint64_t var_125;
    uint64_t var_126;
    uint64_t r9_6;
    uint64_t r10_6;
    uint64_t rbx_4;
    uint64_t var_127;
    uint64_t var_128;
    struct indirect_placeholder_37_ret_type var_129;
    uint64_t var_130;
    uint64_t var_131;
    uint64_t var_132;
    uint64_t var_133;
    uint64_t var_134;
    uint64_t r8_7;
    uint64_t r9_7;
    uint64_t r10_7;
    uint64_t rbx_5;
    bool var_137;
    uint64_t local_sp_16;
    uint64_t var_138;
    uint64_t var_139;
    uint64_t r9_8;
    uint64_t r10_8;
    uint64_t rbx_6;
    uint64_t var_140;
    uint64_t var_141;
    struct indirect_placeholder_38_ret_type var_142;
    uint64_t var_143;
    uint64_t var_144;
    uint64_t var_145;
    uint64_t var_146;
    uint64_t var_149;
    unsigned int loop_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r8();
    var_4 = init_r9();
    var_5 = init_r10();
    var_6 = init_rbx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_7 = (uint64_t *)(var_0 + (-160L));
    *var_7 = rdi;
    var_8 = (uint64_t *)(var_0 + (-168L));
    *var_8 = rsi;
    var_9 = *var_7;
    *(uint64_t *)(var_0 + (-176L)) = 4208220UL;
    indirect_placeholder_7(2UL, var_9);
    var_10 = *var_8;
    *(uint64_t *)(var_0 + (-184L)) = 4208240UL;
    indirect_placeholder_7(2UL, var_10);
    var_11 = var_0 + (-104L);
    *(uint64_t *)(var_0 + (-192L)) = 4208252UL;
    indirect_placeholder_6(var_11);
    var_12 = *var_7;
    *(uint64_t *)(var_0 + (-200L)) = 4208279UL;
    var_13 = indirect_placeholder_40(1UL, var_3, var_4, var_11, var_12, var_5, var_6);
    var_14 = var_13.field_1;
    var_15 = var_13.field_2;
    var_16 = var_13.field_3;
    var_17 = var_13.field_4;
    var_18 = var_0 + (-136L);
    *(uint64_t *)(var_0 + (-208L)) = 4208291UL;
    indirect_placeholder_6(var_18);
    var_19 = *var_8;
    var_20 = var_0 + (-216L);
    *(uint64_t *)var_20 = 4208318UL;
    var_21 = indirect_placeholder_39(2UL, var_14, var_15, var_18, var_19, var_16, var_17);
    var_22 = var_21.field_1;
    var_23 = var_21.field_2;
    var_24 = var_21.field_3;
    var_25 = var_21.field_4;
    local_sp_12_ph = var_20;
    var_100 = 0UL;
    var_103 = 0UL;
    storemerge = 4306048UL;
    r8_4 = var_22;
    r9_4 = var_23;
    r10_4 = var_24;
    rbx_2 = var_25;
    storemerge22 = 0UL;
    storemerge21 = 0UL;
    storemerge20 = 4306048UL;
    r8_5_ph = var_22;
    r9_5_ph = var_23;
    r10_5_ph = var_24;
    rbx_3_ph = var_25;
    var_123 = (unsigned char)'\x00';
    if (*(unsigned char *)4305968UL != '\x00') {
        if (*(uint64_t *)var_11 == 0UL) {
            storemerge22 = *(uint64_t *)(**(uint64_t **)(var_0 + (-88L)) + 24UL);
        }
        *(uint64_t *)4305976UL = storemerge22;
        if (*(uint64_t *)var_18 != 0UL) {
            storemerge21 = *(uint64_t *)(**(uint64_t **)(var_0 + (-120L)) + 24UL);
        }
        *(uint64_t *)4305984UL = storemerge21;
    }
    if (*(unsigned char *)4306097UL == '\x00') {
        _pre_phi324 = (uint64_t *)var_11;
        _pre_phi = (uint64_t *)var_18;
    } else {
        var_26 = (uint64_t *)var_11;
        var_27 = (*var_26 == 0UL);
        _pre_phi324 = var_26;
        if (var_27) {
            var_28 = (uint64_t *)var_18;
            _pre_phi = var_28;
            if (*var_28 != 0UL) {
                if (var_27) {
                    storemerge20 = **(uint64_t **)(var_0 + (-88L));
                }
                var_29 = (uint64_t *)(var_0 + (-48L));
                *var_29 = storemerge20;
                var_30 = (uint64_t *)var_18;
                _pre_phi = var_30;
                if (*var_30 == 0UL) {
                    storemerge = **(uint64_t **)(var_0 + (-120L));
                }
                *(uint64_t *)(var_0 + (-56L)) = storemerge;
                var_31 = *var_29;
                var_32 = var_0 + (-224L);
                *(uint64_t *)var_32 = 4208507UL;
                indirect_placeholder_7(storemerge, var_31);
                *(uint64_t *)4305888UL = 0UL;
                *(uint64_t *)4305896UL = 0UL;
                local_sp_11 = var_32;
                if (*var_26 == 0UL) {
                    var_33 = *var_7;
                    var_34 = var_0 + (-232L);
                    *(uint64_t *)var_34 = 4208567UL;
                    var_35 = indirect_placeholder_34(1UL, 1UL, var_22, var_23, var_11, var_33, var_24, var_25);
                    local_sp_11 = var_34;
                    r8_4 = var_35.field_1;
                    r9_4 = var_35.field_2;
                    r10_4 = var_35.field_3;
                    rbx_2 = var_35.field_4;
                }
                local_sp_12_ph = local_sp_11;
                r8_5_ph = r8_4;
                r9_5_ph = r9_4;
                r10_5_ph = r10_4;
                rbx_3_ph = rbx_2;
                if (*var_30 == 0UL) {
                    var_36 = *var_8;
                    var_37 = local_sp_11 + (-8L);
                    *(uint64_t *)var_37 = 4208609UL;
                    var_38 = indirect_placeholder_35(2UL, 1UL, r8_4, r9_4, var_18, var_36, r10_4, rbx_2);
                    local_sp_12_ph = var_37;
                    r8_5_ph = var_38.field_1;
                    r9_5_ph = var_38.field_2;
                    r10_5_ph = var_38.field_3;
                    rbx_3_ph = var_38.field_4;
                }
            }
        } else {
            if (var_27) {
                storemerge20 = **(uint64_t **)(var_0 + (-88L));
            }
            var_29 = (uint64_t *)(var_0 + (-48L));
            *var_29 = storemerge20;
            var_30 = (uint64_t *)var_18;
            _pre_phi = var_30;
            if (*var_30 == 0UL) {
                storemerge = **(uint64_t **)(var_0 + (-120L));
            }
            *(uint64_t *)(var_0 + (-56L)) = storemerge;
            var_31 = *var_29;
            var_32 = var_0 + (-224L);
            *(uint64_t *)var_32 = 4208507UL;
            indirect_placeholder_7(storemerge, var_31);
            *(uint64_t *)4305888UL = 0UL;
            *(uint64_t *)4305896UL = 0UL;
            local_sp_11 = var_32;
            if (*var_26 == 0UL) {
                var_33 = *var_7;
                var_34 = var_0 + (-232L);
                *(uint64_t *)var_34 = 4208567UL;
                var_35 = indirect_placeholder_34(1UL, 1UL, var_22, var_23, var_11, var_33, var_24, var_25);
                local_sp_11 = var_34;
                r8_4 = var_35.field_1;
                r9_4 = var_35.field_2;
                r10_4 = var_35.field_3;
                rbx_2 = var_35.field_4;
            }
            local_sp_12_ph = local_sp_11;
            r8_5_ph = r8_4;
            r9_5_ph = r9_4;
            r10_5_ph = r10_4;
            rbx_3_ph = rbx_2;
            if (*var_30 == 0UL) {
                var_36 = *var_8;
                var_37 = local_sp_11 + (-8L);
                *(uint64_t *)var_37 = 4208609UL;
                var_38 = indirect_placeholder_35(2UL, 1UL, r8_4, r9_4, var_18, var_36, r10_4, rbx_2);
                local_sp_12_ph = var_37;
                r8_5_ph = var_38.field_1;
                r9_5_ph = var_38.field_2;
                r10_5_ph = var_38.field_3;
                rbx_3_ph = var_38.field_4;
            }
        }
    }
    var_39 = var_0 + (-120L);
    var_40 = var_0 + (-88L);
    var_41 = (uint32_t *)(var_0 + (-60L));
    var_42 = (unsigned char *)(var_0 + (-9L));
    var_43 = (uint64_t *)var_40;
    var_44 = (unsigned char *)(var_0 + (-10L));
    var_45 = (uint64_t *)var_39;
    var_46 = (uint64_t *)(var_0 + (-24L));
    var_47 = (uint64_t *)(var_0 + (-32L));
    var_48 = (uint64_t *)(var_0 + (-72L));
    var_49 = (uint64_t *)(var_0 + (-80L));
    local_sp_12 = local_sp_12_ph;
    r8_5 = r8_5_ph;
    r9_5 = r9_5_ph;
    r10_5 = r10_5_ph;
    rbx_3 = rbx_3_ph;
    r8_6 = r8_5;
    local_sp_15 = local_sp_12;
    rbx_1 = rbx_3;
    local_sp_13 = local_sp_12;
    r9_6 = r9_5;
    r10_6 = r10_5;
    rbx_4 = rbx_3;
    r8_7 = r8_5;
    r9_7 = r9_5;
    r10_7 = r10_5;
    rbx_5 = rbx_3;
    while (*_pre_phi324 != 0UL)
        {
            var_50 = *(uint64_t *)4305544UL;
            var_51 = *(uint64_t *)4305536UL;
            var_52 = (uint64_t **)var_39;
            var_53 = **var_52;
            var_54 = (uint64_t **)var_40;
            var_55 = **var_54;
            var_56 = local_sp_12 + (-8L);
            *(uint64_t *)var_56 = 4208650UL;
            var_57 = indirect_placeholder_36(var_50, var_51, var_53, var_55);
            var_58 = var_57.field_0;
            var_59 = var_57.field_1;
            var_60 = var_57.field_2;
            var_61 = var_57.field_3;
            var_62 = (uint32_t)var_58;
            *var_41 = var_62;
            local_sp_9 = var_56;
            local_sp_8 = var_56;
            local_sp_10 = var_56;
            r8_3 = var_59;
            r9_3 = var_60;
            r10_3 = var_61;
            if ((int)var_62 > (int)4294967295U) {
                if ((int)var_62 > (int)0U) {
                    if (*(unsigned char *)4305954UL != '\x00') {
                        var_111 = **var_52;
                        var_112 = local_sp_12 + (-16L);
                        *(uint64_t *)var_112 = 4208768UL;
                        indirect_placeholder_7(var_111, 4306048UL);
                        local_sp_9 = var_112;
                    }
                    var_113 = *var_8;
                    var_114 = local_sp_9 + (-8L);
                    *(uint64_t *)var_114 = 4208797UL;
                    var_115 = indirect_placeholder_32(2UL, 1UL, var_59, var_60, var_18, var_113, var_61, rbx_3);
                    var_116 = var_115.field_1;
                    var_117 = var_115.field_2;
                    var_118 = var_115.field_3;
                    var_119 = var_115.field_4;
                    *(unsigned char *)4305956UL = (unsigned char)'\x01';
                    local_sp_12_be = var_114;
                    r8_5_be = var_116;
                    r9_5_be = var_117;
                    r10_5_be = var_118;
                    rbx_3_be = var_119;
                } else {
                    *var_42 = (unsigned char)'\x00';
                    while (1U)
                        {
                            var_72 = *var_7;
                            var_73 = local_sp_10 + (-8L);
                            *(uint64_t *)var_73 = 4208842UL;
                            var_74 = indirect_placeholder_33(1UL, 0UL, r8_3, r9_3, var_11, var_72, r10_3, rbx_1);
                            var_75 = var_74.field_0;
                            var_76 = var_74.field_4;
                            local_sp_6 = var_73;
                            rbx_0 = var_76;
                            rbx_1 = var_76;
                            if ((uint64_t)(unsigned char)var_75 != 1UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            var_80 = *(uint64_t *)4305544UL;
                            var_81 = *(uint64_t *)4305536UL;
                            var_82 = **var_52;
                            var_83 = *(uint64_t *)(*var_43 + ((*_pre_phi324 << 3UL) + (-8L)));
                            var_84 = local_sp_10 + (-16L);
                            *(uint64_t *)var_84 = 4208918UL;
                            var_85 = indirect_placeholder_30(var_80, var_81, var_82, var_83);
                            local_sp_6 = var_84;
                            local_sp_10 = var_84;
                            if ((uint64_t)(uint32_t)var_85.field_0 != 0UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            r8_3 = var_85.field_1;
                            r9_3 = var_85.field_2;
                            r10_3 = var_85.field_3;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            r8_1 = var_85.field_1;
                            r9_1 = var_85.field_2;
                            r10_1 = var_85.field_3;
                        }
                        break;
                      case 1U:
                        {
                            var_77 = var_74.field_1;
                            var_78 = var_74.field_2;
                            var_79 = var_74.field_3;
                            *var_42 = (unsigned char)'\x01';
                            *_pre_phi324 = (*_pre_phi324 + 1UL);
                            r8_1 = var_77;
                            r9_1 = var_78;
                            r10_1 = var_79;
                        }
                        break;
                    }
                    *var_44 = (unsigned char)'\x00';
                    local_sp_7 = local_sp_6;
                    r8_2 = r8_1;
                    r9_2 = r9_1;
                    r10_2 = r10_1;
                    while (1U)
                        {
                            var_86 = *var_8;
                            var_87 = local_sp_7 + (-8L);
                            *(uint64_t *)var_87 = 4208955UL;
                            var_88 = indirect_placeholder_29(2UL, 0UL, r8_2, r9_2, var_18, var_86, r10_2, rbx_0);
                            var_89 = var_88.field_0;
                            var_90 = var_88.field_4;
                            local_sp_2 = var_87;
                            rbx_3_be = var_90;
                            rbx_0 = var_90;
                            if ((uint64_t)(unsigned char)var_89 != 1UL) {
                                loop_state_var = 0U;
                                break;
                            }
                            var_94 = *(uint64_t *)4305544UL;
                            var_95 = *(uint64_t *)4305536UL;
                            var_96 = *(uint64_t *)(*var_45 + ((*_pre_phi << 3UL) + (-8L)));
                            var_97 = **var_54;
                            var_98 = local_sp_7 + (-16L);
                            *(uint64_t *)var_98 = 4209031UL;
                            var_99 = indirect_placeholder_28(var_94, var_95, var_96, var_97);
                            local_sp_2 = var_98;
                            local_sp_7 = var_98;
                            if ((uint64_t)(uint32_t)var_99.field_0 != 0UL) {
                                loop_state_var = 1U;
                                break;
                            }
                            r8_2 = var_99.field_1;
                            r9_2 = var_99.field_2;
                            r10_2 = var_99.field_3;
                            continue;
                        }
                    switch (loop_state_var) {
                      case 0U:
                        {
                            var_91 = var_88.field_1;
                            var_92 = var_88.field_2;
                            var_93 = var_88.field_3;
                            *var_44 = (unsigned char)'\x01';
                            *_pre_phi = (*_pre_phi + 1UL);
                            r8_0 = var_91;
                            r9_0 = var_92;
                            r10_0 = var_93;
                        }
                        break;
                      case 1U:
                        {
                            r8_0 = var_99.field_1;
                            r9_0 = var_99.field_2;
                            r10_0 = var_99.field_3;
                        }
                        break;
                    }
                    local_sp_5 = local_sp_2;
                    local_sp_3 = local_sp_2;
                    r8_5_be = r8_0;
                    r9_5_be = r9_0;
                    r10_5_be = r10_0;
                    if (*(unsigned char *)4305955UL != '\x00') {
                        *var_46 = 0UL;
                        var_101 = *_pre_phi324 + (-1L);
                        var_102 = helper_cc_compute_c_wrapper(var_100 - var_101, var_101, var_2, 17U);
                        local_sp_4 = local_sp_3;
                        local_sp_5 = local_sp_3;
                        while (var_102 != 0UL)
                            {
                                *var_47 = 0UL;
                                var_104 = *_pre_phi + (-1L);
                                var_105 = helper_cc_compute_c_wrapper(var_103 - var_104, var_104, var_2, 17U);
                                local_sp_3 = local_sp_4;
                                while (var_105 != 0UL)
                                    {
                                        var_106 = *(uint64_t *)(*var_45 + (*var_47 << 3UL));
                                        var_107 = *(uint64_t *)(*var_43 + (*var_46 << 3UL));
                                        var_108 = local_sp_4 + (-8L);
                                        *(uint64_t *)var_108 = 4209113UL;
                                        indirect_placeholder_7(var_106, var_107);
                                        var_109 = *var_47 + 1UL;
                                        *var_47 = var_109;
                                        var_103 = var_109;
                                        local_sp_4 = var_108;
                                        var_104 = *_pre_phi + (-1L);
                                        var_105 = helper_cc_compute_c_wrapper(var_103 - var_104, var_104, var_2, 17U);
                                        local_sp_3 = local_sp_4;
                                    }
                                var_110 = *var_46 + 1UL;
                                *var_46 = var_110;
                                var_100 = var_110;
                                var_101 = *_pre_phi324 + (-1L);
                                var_102 = helper_cc_compute_c_wrapper(var_100 - var_101, var_101, var_2, 17U);
                                local_sp_4 = local_sp_3;
                                local_sp_5 = local_sp_3;
                            }
                    }
                    local_sp_12_be = local_sp_5;
                    if (*var_42 == '\x01') {
                        *_pre_phi324 = 0UL;
                    } else {
                        *var_48 = **var_54;
                        **var_54 = *(uint64_t *)(((*_pre_phi324 << 3UL) + (-8L)) + *var_43);
                        *(uint64_t *)(((*_pre_phi324 << 3UL) + (-8L)) + *var_43) = *var_48;
                        *_pre_phi324 = 1UL;
                    }
                    if (*var_44 == '\x01') {
                        *_pre_phi = 0UL;
                    } else {
                        *var_49 = **var_52;
                        **var_52 = *(uint64_t *)(((*_pre_phi << 3UL) + (-8L)) + *var_45);
                        *(uint64_t *)(((*_pre_phi << 3UL) + (-8L)) + *var_45) = *var_49;
                        *_pre_phi = 1UL;
                    }
                }
            } else {
                if (*(unsigned char *)4305953UL != '\x00') {
                    var_63 = **var_54;
                    var_64 = local_sp_12 + (-16L);
                    *(uint64_t *)var_64 = 4208690UL;
                    indirect_placeholder_7(4306048UL, var_63);
                    local_sp_8 = var_64;
                }
                var_65 = *var_7;
                var_66 = local_sp_8 + (-8L);
                *(uint64_t *)var_66 = 4208719UL;
                var_67 = indirect_placeholder_31(1UL, 1UL, var_59, var_60, var_11, var_65, var_61, rbx_3);
                var_68 = var_67.field_1;
                var_69 = var_67.field_2;
                var_70 = var_67.field_3;
                var_71 = var_67.field_4;
                *(unsigned char *)4305956UL = (unsigned char)'\x01';
                local_sp_12_be = var_66;
                r8_5_be = var_68;
                r9_5_be = var_69;
                r10_5_be = var_70;
                rbx_3_be = var_71;
            }
            local_sp_12 = local_sp_12_be;
            r8_5 = r8_5_be;
            r9_5 = r9_5_be;
            r10_5 = r10_5_be;
            rbx_3 = rbx_3_be;
            r8_6 = r8_5;
            local_sp_15 = local_sp_12;
            rbx_1 = rbx_3;
            local_sp_13 = local_sp_12;
            r9_6 = r9_5;
            r10_6 = r10_5;
            rbx_4 = rbx_3;
            r8_7 = r8_5;
            r9_7 = r9_5;
            r10_7 = r10_5;
            rbx_5 = rbx_3;
        }
    var_120 = var_0 + (-144L);
    var_121 = (uint64_t *)var_120;
    *var_121 = 0UL;
    var_122 = (unsigned char *)(var_0 + (-33L));
    *var_122 = (unsigned char)'\x00';
    if (*(uint32_t *)4306024UL != 2U) {
        var_123 = (unsigned char)'\x01';
        if (*(unsigned char *)4305957UL == '\x01') {
            *var_122 = (unsigned char)'\x01';
        } else {
            if (*(unsigned char *)4305958UL == '\x01') {
                *var_122 = (unsigned char)'\x01';
            }
        }
    }
    var_124 = (*(unsigned char *)4305953UL == '\x00');
    if (!(var_124 && var_123 == '\x00') & *_pre_phi324 != 0UL) {
        if (var_124) {
            var_125 = **(uint64_t **)var_40;
            var_126 = local_sp_12 + (-8L);
            *(uint64_t *)var_126 = 4209486UL;
            indirect_placeholder_7(4306048UL, var_125);
            local_sp_13 = var_126;
        }
        local_sp_14 = local_sp_13;
        if (*_pre_phi == 0UL) {
            *(unsigned char *)4305956UL = (unsigned char)'\x01';
        }
        var_127 = *var_7;
        var_128 = local_sp_14 + (-8L);
        *(uint64_t *)var_128 = 4209590UL;
        var_129 = indirect_placeholder_37(1UL, r8_6, r9_6, var_120, var_127, r10_6, rbx_4);
        var_130 = var_129.field_0;
        var_131 = var_129.field_1;
        var_132 = var_129.field_2;
        var_133 = var_129.field_3;
        var_134 = var_129.field_4;
        local_sp_1 = var_128;
        r8_6 = var_131;
        local_sp_15 = var_128;
        r9_6 = var_132;
        r10_6 = var_133;
        rbx_4 = var_134;
        r8_7 = var_131;
        r9_7 = var_132;
        r10_7 = var_133;
        rbx_5 = var_134;
        while ((uint64_t)(unsigned char)var_130 != 0UL)
            {
                if (*(unsigned char *)4305953UL == '\x00') {
                    var_135 = *var_121;
                    var_136 = local_sp_14 + (-16L);
                    *(uint64_t *)var_136 = 4209535UL;
                    indirect_placeholder_7(4306048UL, var_135);
                    local_sp_1 = var_136;
                }
                local_sp_14 = local_sp_1;
                local_sp_15 = local_sp_1;
                if (*(unsigned char *)4305957UL != '\x00') {
                    var_127 = *var_7;
                    var_128 = local_sp_14 + (-8L);
                    *(uint64_t *)var_128 = 4209590UL;
                    var_129 = indirect_placeholder_37(1UL, r8_6, r9_6, var_120, var_127, r10_6, rbx_4);
                    var_130 = var_129.field_0;
                    var_131 = var_129.field_1;
                    var_132 = var_129.field_2;
                    var_133 = var_129.field_3;
                    var_134 = var_129.field_4;
                    local_sp_1 = var_128;
                    r8_6 = var_131;
                    local_sp_15 = var_128;
                    r9_6 = var_132;
                    r10_6 = var_133;
                    rbx_4 = var_134;
                    r8_7 = var_131;
                    r9_7 = var_132;
                    r10_7 = var_133;
                    rbx_5 = var_134;
                    continue;
                }
                if (*(unsigned char *)4305953UL == '\x01') {
                    break;
                }
                var_127 = *var_7;
                var_128 = local_sp_14 + (-8L);
                *(uint64_t *)var_128 = 4209590UL;
                var_129 = indirect_placeholder_37(1UL, r8_6, r9_6, var_120, var_127, r10_6, rbx_4);
                var_130 = var_129.field_0;
                var_131 = var_129.field_1;
                var_132 = var_129.field_2;
                var_133 = var_129.field_3;
                var_134 = var_129.field_4;
                local_sp_1 = var_128;
                r8_6 = var_131;
                local_sp_15 = var_128;
                r9_6 = var_132;
                r10_6 = var_133;
                rbx_4 = var_134;
                r8_7 = var_131;
                r9_7 = var_132;
                r10_7 = var_133;
                rbx_5 = var_134;
            }
    }
    var_137 = (*(unsigned char *)4305954UL == '\x00');
    r8_8 = r8_7;
    local_sp_18 = local_sp_15;
    local_sp_16 = local_sp_15;
    r9_8 = r9_7;
    r10_8 = r10_7;
    rbx_6 = rbx_5;
    if (!var_137) {
        if (*var_122 == '\x00') {
            var_149 = *var_121;
            *(uint64_t *)(local_sp_18 + (-8L)) = 4209788UL;
            indirect_placeholder_6(var_149);
            *(uint64_t *)(local_sp_18 + (-16L)) = 4209803UL;
            indirect_placeholder_1();
            *(uint64_t *)(local_sp_18 + (-24L)) = 4209815UL;
            indirect_placeholder_6(var_11);
            *(uint64_t *)(local_sp_18 + (-32L)) = 4209827UL;
            indirect_placeholder_6(var_18);
            return;
        }
    }
    if (*_pre_phi == 0UL) {
        var_149 = *var_121;
        *(uint64_t *)(local_sp_18 + (-8L)) = 4209788UL;
        indirect_placeholder_6(var_149);
        *(uint64_t *)(local_sp_18 + (-16L)) = 4209803UL;
        indirect_placeholder_1();
        *(uint64_t *)(local_sp_18 + (-24L)) = 4209815UL;
        indirect_placeholder_6(var_11);
        *(uint64_t *)(local_sp_18 + (-32L)) = 4209827UL;
        indirect_placeholder_6(var_18);
        return;
    }
    if (var_137) {
        var_138 = **(uint64_t **)var_39;
        var_139 = local_sp_15 + (-8L);
        *(uint64_t *)var_139 = 4209662UL;
        indirect_placeholder_7(var_138, 4306048UL);
        local_sp_16 = var_139;
    }
    local_sp_17 = local_sp_16;
    if (*_pre_phi324 == 0UL) {
        *(unsigned char *)4305956UL = (unsigned char)'\x01';
    }
    var_140 = *var_8;
    var_141 = local_sp_17 + (-8L);
    *(uint64_t *)var_141 = 4209766UL;
    var_142 = indirect_placeholder_38(2UL, r8_8, r9_8, var_120, var_140, r10_8, rbx_6);
    var_143 = var_142.field_1;
    var_144 = var_142.field_2;
    var_145 = var_142.field_3;
    var_146 = var_142.field_4;
    local_sp_0 = var_141;
    r8_8 = var_143;
    local_sp_18 = var_141;
    r9_8 = var_144;
    r10_8 = var_145;
    rbx_6 = var_146;
    while ((uint64_t)(unsigned char)var_142.field_0 != 0UL)
        {
            if (*(unsigned char *)4305954UL == '\x00') {
                var_147 = *var_121;
                var_148 = local_sp_17 + (-16L);
                *(uint64_t *)var_148 = 4209711UL;
                indirect_placeholder_7(var_147, 4306048UL);
                local_sp_0 = var_148;
            }
            local_sp_17 = local_sp_0;
            local_sp_18 = local_sp_0;
            if (*(unsigned char *)4305958UL != '\x00') {
                var_140 = *var_8;
                var_141 = local_sp_17 + (-8L);
                *(uint64_t *)var_141 = 4209766UL;
                var_142 = indirect_placeholder_38(2UL, r8_8, r9_8, var_120, var_140, r10_8, rbx_6);
                var_143 = var_142.field_1;
                var_144 = var_142.field_2;
                var_145 = var_142.field_3;
                var_146 = var_142.field_4;
                local_sp_0 = var_141;
                r8_8 = var_143;
                local_sp_18 = var_141;
                r9_8 = var_144;
                r10_8 = var_145;
                rbx_6 = var_146;
                continue;
            }
            if (*(unsigned char *)4305954UL == '\x01') {
                break;
            }
            var_140 = *var_8;
            var_141 = local_sp_17 + (-8L);
            *(uint64_t *)var_141 = 4209766UL;
            var_142 = indirect_placeholder_38(2UL, r8_8, r9_8, var_120, var_140, r10_8, rbx_6);
            var_143 = var_142.field_1;
            var_144 = var_142.field_2;
            var_145 = var_142.field_3;
            var_146 = var_142.field_4;
            local_sp_0 = var_141;
            r8_8 = var_143;
            local_sp_18 = var_141;
            r9_8 = var_144;
            r10_8 = var_145;
            rbx_6 = var_146;
        }
    var_149 = *var_121;
    *(uint64_t *)(local_sp_18 + (-8L)) = 4209788UL;
    indirect_placeholder_6(var_149);
    *(uint64_t *)(local_sp_18 + (-16L)) = 4209803UL;
    indirect_placeholder_1();
    *(uint64_t *)(local_sp_18 + (-24L)) = 4209815UL;
    indirect_placeholder_6(var_11);
    *(uint64_t *)(local_sp_18 + (-32L)) = 4209827UL;
    indirect_placeholder_6(var_18);
    return;
}
