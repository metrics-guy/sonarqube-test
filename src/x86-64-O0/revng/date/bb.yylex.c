typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_yylex_ret_type;
struct indirect_placeholder_136_ret_type;
struct indirect_placeholder_137_ret_type;
struct bb_yylex_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_136_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_137_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern uint64_t helper_cc_compute_c_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
extern uint64_t init_r9(void);
extern uint64_t init_r8(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
extern struct indirect_placeholder_136_ret_type indirect_placeholder_136(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_137_ret_type indirect_placeholder_137(uint64_t param_0, uint64_t param_1);
struct bb_yylex_ret_type bb_yylex(uint64_t rsi, uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t var_4;
    uint64_t var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t *var_8;
    unsigned char *var_9;
    uint64_t *var_10;
    uint64_t *var_11;
    uint32_t *var_12;
    uint64_t local_sp_8;
    uint64_t rax_0;
    unsigned char ***var_13;
    unsigned char var_14;
    struct indirect_placeholder_136_ret_type var_96;
    uint64_t var_97;
    uint64_t var_98;
    uint64_t var_95;
    uint64_t var_89;
    struct indirect_placeholder_137_ret_type var_90;
    uint64_t var_91;
    uint64_t var_92;
    uint64_t var_93;
    uint64_t *var_94;
    uint64_t var_81;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t *var_79;
    uint64_t var_80;
    uint64_t local_sp_0;
    uint64_t var_82;
    uint64_t var_83;
    uint64_t *var_84;
    unsigned char var_85;
    uint64_t var_86;
    uint64_t var_87;
    uint64_t var_88;
    uint64_t *var_99;
    uint64_t var_100;
    uint64_t var_101;
    uint64_t var_102;
    uint64_t var_103;
    uint64_t *var_104;
    uint64_t var_105;
    unsigned char var_106;
    uint64_t var_109;
    uint64_t var_108;
    uint64_t var_107;
    uint64_t local_sp_5;
    uint64_t var_73;
    uint64_t spec_select;
    uint64_t local_sp_3;
    uint64_t local_sp_3_ph;
    uint64_t local_sp_2;
    uint64_t var_65;
    uint32_t var_66;
    uint32_t var_61;
    uint64_t var_55;
    uint64_t *var_56;
    uint64_t var_57;
    uint32_t var_58;
    uint32_t *var_59;
    uint32_t *var_60;
    uint64_t local_sp_1;
    uint64_t var_67;
    uint64_t var_68;
    uint64_t var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t spec_select217;
    uint64_t var_50;
    uint64_t var_51;
    uint64_t local_sp_4;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    uint64_t **_pre_phi;
    unsigned char var_22;
    uint64_t var_74;
    uint64_t var_75;
    uint64_t var_76;
    unsigned char var_23;
    uint64_t var_33;
    uint64_t var_34;
    uint64_t *var_35;
    uint64_t local_sp_6;
    unsigned __int128 var_36;
    uint64_t var_37;
    uint64_t var_38;
    bool var_39;
    bool var_40;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    bool var_44;
    uint64_t r9_1;
    uint64_t r8_1;
    struct bb_yylex_ret_type mrv;
    struct bb_yylex_ret_type mrv1;
    struct bb_yylex_ret_type mrv2;
    unsigned char **var_45;
    unsigned char var_46;
    uint64_t var_47;
    uint64_t var_48;
    uint64_t var_49;
    uint64_t local_sp_7;
    uint64_t **var_24;
    uint64_t *var_25;
    unsigned char var_26;
    uint64_t var_27;
    uint64_t var_28;
    uint64_t var_29;
    uint64_t var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_18;
    uint64_t local_sp_9;
    uint64_t var_15;
    uint64_t var_16;
    uint64_t var_17;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    var_3 = init_r9();
    var_4 = init_r8();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_5 = var_0 + (-120L);
    var_6 = var_0 + (-112L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rdi;
    var_8 = (uint64_t *)var_5;
    *var_8 = rsi;
    var_9 = (unsigned char *)(var_0 + (-9L));
    var_10 = (uint64_t *)(var_0 + (-64L));
    var_11 = (uint64_t *)(var_0 + (-80L));
    var_12 = (uint32_t *)(var_0 + (-28L));
    local_sp_8 = var_5;
    rax_0 = 63UL;
    var_61 = 2U;
    r9_1 = var_3;
    r8_1 = var_4;
    while (1U)
        {
            local_sp_9 = local_sp_8;
            var_13 = (unsigned char ***)var_5;
            var_14 = ***var_13;
            *var_9 = var_14;
            var_15 = (uint64_t)var_14;
            var_16 = local_sp_9 + (-8L);
            *(uint64_t *)var_16 = 4233897UL;
            var_17 = indirect_placeholder_16(var_15);
            local_sp_9 = var_16;
            while ((uint64_t)(unsigned char)var_17 != 0UL)
                {
                    var_18 = *(uint64_t **)var_5;
                    *var_18 = (*var_18 + 1UL);
                    var_13 = (unsigned char ***)var_5;
                    var_14 = ***var_13;
                    *var_9 = var_14;
                    var_15 = (uint64_t)var_14;
                    var_16 = local_sp_9 + (-8L);
                    *(uint64_t *)var_16 = 4233897UL;
                    var_17 = indirect_placeholder_16(var_15);
                    local_sp_9 = var_16;
                }
            var_19 = (uint64_t)*var_9;
            var_20 = local_sp_9 + (-16L);
            *(uint64_t *)var_20 = 4233912UL;
            var_21 = indirect_placeholder_16(var_19);
            local_sp_5 = var_20;
            local_sp_7 = var_20;
            if ((uint64_t)(unsigned char)var_21 == 0UL) {
                *var_12 = ((var_23 == '-') ? 4294967295U : 1U);
                var_24 = (uint64_t **)var_5;
                var_25 = *var_24;
                *var_25 = (*var_25 + 1UL);
                var_26 = ***var_13;
                *var_9 = var_26;
                var_27 = (uint64_t)var_26;
                var_28 = local_sp_7 + (-8L);
                *(uint64_t *)var_28 = 4234018UL;
                var_29 = indirect_placeholder_16(var_27);
                _pre_phi = var_24;
                local_sp_7 = var_28;
                do {
                    var_24 = (uint64_t **)var_5;
                    var_25 = *var_24;
                    *var_25 = (*var_25 + 1UL);
                    var_26 = ***var_13;
                    *var_9 = var_26;
                    var_27 = (uint64_t)var_26;
                    var_28 = local_sp_7 + (-8L);
                    *(uint64_t *)var_28 = 4234018UL;
                    var_29 = indirect_placeholder_16(var_27);
                    _pre_phi = var_24;
                    local_sp_7 = var_28;
                } while ((uint64_t)(unsigned char)var_29 != 0UL);
                var_30 = (uint64_t)*var_9;
                var_31 = local_sp_7 + (-16L);
                *(uint64_t *)var_31 = 4234033UL;
                var_32 = indirect_placeholder_16(var_30);
                local_sp_4 = var_31;
                local_sp_5 = var_31;
                if ((uint64_t)(unsigned char)var_32 != 1UL) {
                    loop_state_var = 0U;
                    switch_state_var = 1;
                    break;
                }
                local_sp_8 = local_sp_4;
                continue;
            }
            break;
        }
    switch (loop_state_var) {
      case 1U:
        {
            mrv.field_0 = rax_0;
            mrv1 = mrv;
            mrv1.field_1 = r9_1;
            mrv2 = mrv1;
            mrv2.field_2 = r8_1;
            return mrv2;
        }
        break;
      case 0U:
        {
            var_33 = **_pre_phi;
            var_34 = var_0 + (-24L);
            var_35 = (uint64_t *)var_34;
            *var_35 = var_33;
            local_sp_6 = local_sp_5;
            while (1U)
                {
                    var_36 = (unsigned __int128)*var_11 * 10ULL;
                    var_37 = (uint64_t)var_36;
                    var_38 = helper_cc_compute_all_wrapper(var_37, (uint64_t)((long)var_37 >> (long)63UL) - (uint64_t)(var_36 >> 64ULL), 0UL, 5U);
                    var_39 = ((uint64_t)((uint16_t)var_38 & (unsigned short)2048U) == 0UL);
                    *var_11 = var_37;
                    var_40 = ((int)*var_12 > (int)4294967295U);
                    var_41 = (uint64_t)*var_9;
                    var_42 = (uint64_t)((long)((var_40 ? (var_41 + 4294967248UL) : (48UL - var_41)) << 32UL) >> (long)32UL) + var_37;
                    var_43 = helper_cc_compute_all_wrapper(var_42, var_37, 0UL, 9U);
                    var_44 = ((uint64_t)((uint16_t)var_43 & (unsigned short)2048U) == 0UL);
                    *var_11 = var_42;
                    if (!(var_39 && var_44)) {
                        loop_state_var = 2U;
                        break;
                    }
                    *var_35 = (*var_35 + 1UL);
                    var_45 = (unsigned char **)var_34;
                    var_46 = **var_45;
                    *var_9 = var_46;
                    var_47 = (uint64_t)var_46;
                    var_48 = local_sp_6 + (-8L);
                    *(uint64_t *)var_48 = 4234206UL;
                    var_49 = indirect_placeholder_16(var_47);
                    local_sp_6 = var_48;
                    if ((uint64_t)(unsigned char)var_49 == 0UL) {
                        continue;
                    }
                    var_50 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(*var_35 + 1UL);
                    *(uint64_t *)(local_sp_6 + (-16L)) = 4234251UL;
                    var_51 = indirect_placeholder_16(var_50);
                    if ((uint64_t)(unsigned char)var_51 != 0UL) {
                        loop_state_var = 0U;
                        switch_state_var = 1;
                        break;
                    }
                    var_52 = *var_11;
                    var_53 = local_sp_6 + (-24L);
                    *(uint64_t *)var_53 = 4234271UL;
                    var_54 = indirect_placeholder_16(var_52);
                    local_sp_1 = var_53;
                    if ((uint64_t)(unsigned char)var_54 != 0UL) {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    var_55 = *var_11;
                    var_56 = (uint64_t *)(var_0 + (-40L));
                    *var_56 = var_55;
                    var_57 = *var_35 + 1UL;
                    *var_35 = (var_57 + 1UL);
                    var_58 = (uint32_t)*(unsigned char *)var_57 + (-48);
                    var_59 = (uint32_t *)(var_0 + (-44L));
                    *var_59 = var_58;
                    var_60 = (uint32_t *)(var_0 + (-48L));
                    *var_60 = 2U;
                    local_sp_2 = local_sp_1;
                    local_sp_3_ph = local_sp_1;
                    while ((int)var_61 <= (int)9U)
                        {
                            *var_59 = (*var_59 * 10U);
                            var_62 = (uint64_t)(uint32_t)(uint64_t)**var_45;
                            var_63 = local_sp_1 + (-8L);
                            *(uint64_t *)var_63 = 4234363UL;
                            var_64 = indirect_placeholder_16(var_62);
                            local_sp_1 = var_63;
                            if ((uint64_t)(unsigned char)var_64 == 0UL) {
                                var_65 = *var_35;
                                *var_35 = (var_65 + 1UL);
                                *var_59 = (*var_59 + ((uint32_t)*(unsigned char *)var_65 + (-48)));
                            }
                            var_66 = *var_60 + 1U;
                            *var_60 = var_66;
                            var_61 = var_66;
                            local_sp_2 = local_sp_1;
                            local_sp_3_ph = local_sp_1;
                        }
                    if ((int)*var_12 <= (int)4294967295U) {
                        loop_state_var = 1U;
                        switch_state_var = 1;
                        break;
                    }
                    var_67 = (uint64_t)(uint32_t)(uint64_t)**var_45;
                    var_68 = local_sp_2 + (-8L);
                    *(uint64_t *)var_68 = 4234448UL;
                    var_69 = indirect_placeholder_16(var_67);
                    local_sp_2 = var_68;
                    local_sp_3_ph = var_68;
                    while ((uint64_t)(unsigned char)var_69 != 0UL)
                        {
                            if (**var_45 != '0') {
                                *var_59 = (*var_59 + 1U);
                                break;
                            }
                            *var_35 = (*var_35 + 1UL);
                            var_67 = (uint64_t)(uint32_t)(uint64_t)**var_45;
                            var_68 = local_sp_2 + (-8L);
                            *(uint64_t *)var_68 = 4234448UL;
                            var_69 = indirect_placeholder_16(var_67);
                            local_sp_2 = var_68;
                            local_sp_3_ph = var_68;
                        }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
            switch (loop_state_var) {
              case 0U:
                {
                    **(unsigned char **)var_6 = (unsigned char)(*var_12 >> 31U);
                    *(uint64_t *)(*var_7 + 8UL) = *var_11;
                    *(uint64_t *)(*var_7 + 16UL) = (*var_35 - **_pre_phi);
                    **_pre_phi = *var_35;
                    spec_select217 = (*var_12 == 0U) ? 275UL : 274UL;
                    rax_0 = spec_select217;
                }
                break;
              case 1U:
                {
                    local_sp_3 = local_sp_3_ph;
                    var_70 = (uint64_t)(uint32_t)(uint64_t)**var_45;
                    var_71 = local_sp_3 + (-8L);
                    *(uint64_t *)var_71 = 4234476UL;
                    var_72 = indirect_placeholder_16(var_70);
                    local_sp_3 = var_71;
                    while ((uint64_t)(unsigned char)var_72 != 0UL)
                        {
                            *var_35 = (*var_35 + 1UL);
                            var_70 = (uint64_t)(uint32_t)(uint64_t)**var_45;
                            var_71 = local_sp_3 + (-8L);
                            *(uint64_t *)var_71 = 4234476UL;
                            var_72 = indirect_placeholder_16(var_70);
                            local_sp_3 = var_71;
                        }
                    if ((int)*var_12 <= (int)4294967295U & *var_59 != 0U) {
                        var_73 = *var_56;
                        if (var_73 != 9223372036854775808UL) {
                            mrv.field_0 = rax_0;
                            mrv1 = mrv;
                            mrv1.field_1 = r9_1;
                            mrv2 = mrv1;
                            mrv2.field_2 = r8_1;
                            return mrv2;
                        }
                        *var_56 = (var_73 + (-1L));
                        *var_59 = (1000000000U - *var_59);
                    }
                    **(uint64_t **)var_6 = *var_56;
                    *(uint64_t *)(*var_7 + 8UL) = (uint64_t)*var_59;
                    **_pre_phi = *var_35;
                    spec_select = (*var_12 == 0U) ? 277UL : 276UL;
                    rax_0 = spec_select;
                }
                break;
            }
        }
        break;
      case 2U:
        {
            while (1U)
                {
                    var_82 = helper_cc_compute_c_wrapper(var_81 - var_80, var_80, var_2, 17U);
                    if (var_82 == 0UL) {
                        var_83 = *var_79;
                        *var_79 = (var_83 + 1UL);
                        *(unsigned char *)var_83 = *var_9;
                    }
                    var_84 = *(uint64_t **)var_5;
                    *var_84 = (*var_84 + 1UL);
                    var_85 = ***var_13;
                    *var_9 = var_85;
                    var_86 = (uint64_t)var_85;
                    var_87 = local_sp_0 + (-8L);
                    *(uint64_t *)var_87 = 4234782UL;
                    var_88 = indirect_placeholder_16(var_86);
                    local_sp_0 = var_87;
                    if ((uint64_t)(unsigned char)var_88 != 0UL) {
                        if (*var_9 == '.') {
                            break;
                        }
                    }
                    var_81 = *var_79;
                    continue;
                }
            **(unsigned char **)var_78 = (unsigned char)'\x00';
            var_89 = *var_8;
            *(uint64_t *)(local_sp_0 + (-16L)) = 4234818UL;
            var_90 = indirect_placeholder_137(var_77, var_89);
            var_91 = var_90.field_0;
            var_92 = var_90.field_1;
            var_93 = var_90.field_2;
            var_94 = (uint64_t *)(var_0 + (-72L));
            *var_94 = var_91;
            if (var_91 == 0UL) {
                **(uint64_t **)var_6 = (uint64_t)*(uint32_t *)(var_91 + 12UL);
                var_95 = (uint64_t)*(uint32_t *)(*var_94 + 8UL);
                rax_0 = var_95;
            } else {
                if (*(unsigned char *)(*var_8 + 217UL) == '\x00') {
                    *(uint64_t *)(local_sp_0 + (-24L)) = 4234866UL;
                    var_96 = indirect_placeholder_136(0UL, var_92, var_93, var_77, 4339096UL, var_3, var_4);
                    var_97 = var_96.field_4;
                    var_98 = var_96.field_5;
                    r9_1 = var_97;
                    r8_1 = var_98;
                }
            }
        }
        break;
    }
}
