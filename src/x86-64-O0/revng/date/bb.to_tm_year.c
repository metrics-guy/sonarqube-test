typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct bb_to_tm_year_ret_type;
struct indirect_placeholder_160_ret_type;
struct indirect_placeholder_161_ret_type;
struct bb_to_tm_year_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_160_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
struct indirect_placeholder_161_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
    uint64_t field_4;
    uint64_t field_5;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern struct indirect_placeholder_160_ret_type indirect_placeholder_160(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_161_ret_type indirect_placeholder_161(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
struct bb_to_tm_year_ret_type bb_to_tm_year(uint64_t rcx, uint64_t rsi, uint64_t rdi, uint64_t r9, uint64_t r8) {
    uint64_t rsi7_0;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    unsigned char *var_3;
    uint64_t *var_4;
    uint64_t var_5;
    uint64_t *var_6;
    uint64_t var_11;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_160_ret_type var_10;
    uint64_t local_sp_0;
    uint64_t var_20;
    uint64_t r99_0;
    uint64_t r810_0;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t rdx_1;
    uint32_t *var_14;
    uint32_t var_15;
    uint64_t rdx_6;
    uint64_t rsi7_2;
    uint64_t r99_2;
    uint64_t r810_2;
    uint64_t storemerge3;
    struct bb_to_tm_year_ret_type mrv;
    struct bb_to_tm_year_ret_type mrv1;
    struct bb_to_tm_year_ret_type mrv2;
    struct bb_to_tm_year_ret_type mrv3;
    struct bb_to_tm_year_ret_type mrv4;
    struct bb_to_tm_year_ret_type mrv5;
    struct indirect_placeholder_161_ret_type var_22;
    uint32_t _pre_phi;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t rdx_3;
    uint32_t *var_18;
    uint32_t var_19;
    uint64_t rdx_4;
    uint64_t rcx6_2;
    uint64_t var_21;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = var_0 + (-40L);
    *(uint64_t *)var_2 = rsi;
    var_3 = (unsigned char *)(var_0 + (-28L));
    *var_3 = (unsigned char)rdi;
    var_4 = (uint64_t *)(var_0 | 16UL);
    var_5 = *var_4;
    var_6 = (uint64_t *)(var_0 + (-16L));
    *var_6 = var_5;
    rsi7_0 = rsi;
    var_11 = var_5;
    local_sp_0 = var_2;
    r99_0 = r9;
    r810_0 = r8;
    storemerge3 = 1UL;
    var_7 = var_5 + (((long)var_5 > (long)68UL) ? 1900UL : 2000UL);
    *var_6 = var_7;
    var_11 = var_7;
    if ((long)var_5 >= (long)0UL & *(uint64_t *)(var_0 | 24UL) != 2UL & *var_3 == '\x00') {
        var_8 = *var_4;
        var_9 = var_0 + (-48L);
        *(uint64_t *)var_9 = 4232663UL;
        var_10 = indirect_placeholder_160(0UL, rcx, var_7, var_8, 4339024UL, r9, r8);
        rsi7_0 = var_10.field_3;
        var_11 = *var_6;
        local_sp_0 = var_9;
        r99_0 = var_10.field_4;
        r810_0 = var_10.field_5;
    }
    rsi7_2 = rsi7_0;
    r99_2 = r99_0;
    r810_2 = r810_0;
    if ((long)var_11 > (long)18446744073709551615UL) {
        var_16 = var_11 + (-1900L);
        var_17 = helper_cc_compute_all_wrapper(var_16, 1900UL, 0UL, 17U);
        rdx_3 = ((var_11 + 2147481748UL) < 4294967296UL) ? (((uint64_t)((uint16_t)var_17 & (unsigned short)2048U) == 0UL) ? 0UL : 1UL) : 1UL;
        var_18 = *(uint32_t **)var_2;
        var_19 = (uint32_t)var_16;
        *var_18 = var_19;
        _pre_phi = var_19;
        rdx_4 = rdx_3;
    } else {
        var_12 = 18446744073709549716UL - (-1900L);
        var_13 = helper_cc_compute_all_wrapper(var_12, var_11, 0UL, 17U);
        rdx_1 = ((2147481748UL - var_11) < 4294967296UL) ? (((uint64_t)((uint16_t)var_13 & (unsigned short)2048U) == 0UL) ? 0UL : 1UL) : 1UL;
        var_14 = *(uint32_t **)var_2;
        var_15 = (uint32_t)var_12;
        *var_14 = var_15;
        _pre_phi = var_15;
        rdx_4 = rdx_1;
    }
    var_20 = (uint64_t)_pre_phi;
    rcx6_2 = var_20;
    rdx_6 = rdx_4;
    storemerge3 = 0UL;
    if (~((rdx_4 & 1UL) != 0UL & *var_3 == '\x00')) {
        mrv.field_0 = storemerge3;
        mrv1 = mrv;
        mrv1.field_1 = rcx6_2;
        mrv2 = mrv1;
        mrv2.field_2 = rdx_6;
        mrv3 = mrv2;
        mrv3.field_3 = rsi7_2;
        mrv4 = mrv3;
        mrv4.field_4 = r99_2;
        mrv5 = mrv4;
        mrv5.field_5 = r810_2;
        return mrv5;
    }
    var_21 = *var_6;
    *(uint64_t *)(local_sp_0 + (-8L)) = 4232801UL;
    var_22 = indirect_placeholder_161(0UL, var_20, rdx_4, var_21, 4339066UL, r99_0, r810_0);
    rdx_6 = var_22.field_2;
    rsi7_2 = var_22.field_3;
    r99_2 = var_22.field_4;
    r810_2 = var_22.field_5;
    rcx6_2 = var_22.field_1;
    mrv.field_0 = storemerge3;
    mrv1 = mrv;
    mrv1.field_1 = rcx6_2;
    mrv2 = mrv1;
    mrv2.field_2 = rdx_6;
    mrv3 = mrv2;
    mrv3.field_3 = rsi7_2;
    mrv4 = mrv3;
    mrv4.field_4 = r99_2;
    mrv5 = mrv4;
    mrv5.field_5 = r810_2;
    return mrv5;
}
