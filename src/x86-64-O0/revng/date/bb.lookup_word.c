typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
typedef _Bool bool;
struct bb_lookup_word_ret_type;
struct bb_lookup_word_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_2(uint64_t param_0, uint64_t param_1);
extern uint64_t indirect_placeholder_16(uint64_t param_0);
extern uint64_t init_rcx(void);
struct bb_lookup_word_ret_type bb_lookup_word(uint64_t rsi, uint64_t rdi) {
    uint64_t var_49;
    uint64_t var_54;
    unsigned char var_55;
    uint64_t var_56;
    unsigned char *var_57;
    uint64_t var_58;
    uint64_t var_59;
    uint64_t *var_60;
    unsigned char **var_61;
    unsigned char var_62;
    uint64_t var_63;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t rcx_1;
    uint64_t var_50;
    uint64_t var_51;
    bool var_52;
    uint64_t var_53;
    uint64_t var_40;
    uint64_t var_41;
    bool var_42;
    uint64_t var_43;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t rcx_0;
    uint64_t rdx_1;
    uint64_t storemerge26;
    bool var_32;
    uint64_t var_33;
    uint64_t local_sp_5;
    unsigned char storemerge;
    unsigned char *var_26;
    uint64_t var_27;
    uint64_t var_34;
    uint64_t var_35;
    uint64_t var_36;
    bool var_28;
    uint64_t var_29;
    uint64_t var_30;
    uint64_t *var_31;
    uint64_t rax_0;
    uint64_t rcx_2;
    uint64_t rdx_2;
    struct bb_lookup_word_ret_type mrv;
    struct bb_lookup_word_ret_type mrv1;
    struct bb_lookup_word_ret_type mrv2;
    uint64_t local_sp_7;
    uint64_t local_sp_6;
    unsigned char **var_8;
    unsigned char var_9;
    uint64_t var_15;
    uint64_t *var_16;
    uint64_t **var_17;
    uint64_t var_18;
    bool var_19;
    uint64_t var_20;
    uint64_t var_21;
    uint64_t *var_22;
    uint64_t *var_25;
    bool var_23;
    uint64_t var_24;
    uint64_t var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    uint64_t var_6;
    uint64_t *var_7;
    uint64_t local_sp_3;
    uint64_t local_sp_1;
    uint64_t local_sp_0;
    uint64_t var_39;
    unsigned char *_cast10;
    uint64_t local_sp_2;
    uint64_t var_44;
    uint64_t var_45;
    uint64_t var_46;
    bool var_47;
    uint64_t var_48;
    unsigned int loop_state_var;
    bool switch_state_var;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_rcx();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-72L);
    var_4 = (uint64_t *)(var_0 + (-64L));
    *var_4 = rdi;
    var_5 = (uint64_t *)var_3;
    *var_5 = rsi;
    var_6 = var_0 + (-16L);
    var_7 = (uint64_t *)var_6;
    *var_7 = rsi;
    var_54 = 4338592UL;
    rcx_1 = var_2;
    storemerge = (unsigned char)'\x00';
    rcx_2 = var_2;
    local_sp_6 = var_3;
    var_8 = (unsigned char **)var_6;
    var_9 = **var_8;
    local_sp_7 = local_sp_6;
    while (var_9 != '\x00')
        {
            var_10 = (uint64_t)(uint32_t)(uint64_t)var_9;
            *(uint64_t *)(local_sp_6 + (-8L)) = 4233067UL;
            var_11 = indirect_placeholder_16(var_10);
            var_12 = (uint64_t)(unsigned char)var_11;
            var_13 = local_sp_6 + (-16L);
            *(uint64_t *)var_13 = 4233077UL;
            var_14 = indirect_placeholder_16(var_12);
            **var_8 = (unsigned char)var_14;
            *var_7 = (*var_7 + 1UL);
            local_sp_6 = var_13;
            var_8 = (unsigned char **)var_6;
            var_9 = **var_8;
            local_sp_7 = local_sp_6;
        }
    var_15 = var_0 + (-32L);
    var_16 = (uint64_t *)var_15;
    *var_16 = 4336032UL;
    while (1U)
        {
            var_17 = (uint64_t **)var_15;
            var_18 = **var_17;
            var_19 = (var_18 == 0UL);
            var_20 = *var_5;
            var_21 = local_sp_7 + (-8L);
            var_22 = (uint64_t *)var_21;
            local_sp_5 = var_21;
            rdx_2 = var_18;
            local_sp_7 = var_21;
            if (var_19) {
                *var_22 = 4233133UL;
                indirect_placeholder();
                var_23 = ((uint64_t)(uint32_t)var_20 == 0UL);
                var_24 = *var_16;
                rax_0 = var_24;
                if (!var_23) {
                    loop_state_var = 0U;
                    break;
                }
                *var_16 = (var_24 + 16UL);
                continue;
            }
            *var_22 = 4233175UL;
            indirect_placeholder();
            var_25 = (uint64_t *)(var_0 + (-48L));
            *var_25 = var_20;
            switch_state_var = 0;
            switch (var_20) {
              case 4UL:
                {
                    if (*(unsigned char *)(*var_5 + 3UL) != '.') {
                        loop_state_var = 2U;
                        switch_state_var = 1;
                        break;
                    }
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
              case 3UL:
                {
                    loop_state_var = 2U;
                    switch_state_var = 1;
                    break;
                }
                break;
              default:
                {
                    loop_state_var = 1U;
                    switch_state_var = 1;
                    break;
                }
                break;
            }
            if (switch_state_var)
                break;
        }
    switch (loop_state_var) {
      case 0U:
        {
            mrv.field_0 = rax_0;
            mrv1 = mrv;
            mrv1.field_1 = rcx_2;
            mrv2 = mrv1;
            mrv2.field_2 = rdx_2;
            return mrv2;
        }
        break;
      case 2U:
      case 1U:
        {
            switch (loop_state_var) {
              case 2U:
                {
                    storemerge = (unsigned char)'\x01';
                }
                break;
              case 1U:
                {
                    var_26 = (unsigned char *)(var_0 + (-49L));
                    *var_26 = (storemerge & '\x01');
                    *var_16 = 4336320UL;
                    while (1U)
                        {
                            var_27 = **var_17;
                            rcx_0 = rcx_1;
                            rdx_1 = var_27;
                            rcx_2 = rcx_1;
                            if (var_27 != 0UL) {
                                var_34 = *var_5;
                                var_35 = *var_4;
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4233353UL;
                                var_36 = indirect_placeholder_2(var_34, var_35);
                                *var_16 = var_36;
                                rax_0 = var_36;
                                rdx_2 = var_34;
                                if (var_36 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                var_37 = *var_5;
                                var_38 = local_sp_5 + (-16L);
                                *(uint64_t *)var_38 = 4233393UL;
                                indirect_placeholder();
                                local_sp_0 = var_38;
                                rax_0 = 4336128UL;
                                rdx_2 = 4336112UL;
                                if ((uint64_t)(uint32_t)var_37 != 0UL) {
                                    loop_state_var = 0U;
                                    break;
                                }
                                *var_16 = 4336800UL;
                                rax_0 = 0UL;
                                loop_state_var = 1U;
                                break;
                            }
                            var_28 = (*var_26 == '\x00');
                            var_29 = *var_5;
                            var_30 = local_sp_5 + (-8L);
                            var_31 = (uint64_t *)var_30;
                            local_sp_5 = var_30;
                            if (var_28) {
                                *var_31 = 4233299UL;
                                indirect_placeholder();
                                storemerge26 = (var_29 & (-256L)) | ((uint64_t)(uint32_t)var_29 == 0UL);
                            } else {
                                *var_31 = 4233270UL;
                                indirect_placeholder();
                                rcx_0 = var_27;
                                rdx_1 = 3UL;
                                storemerge26 = (var_29 & (-256L)) | ((uint64_t)(uint32_t)var_29 == 0UL);
                            }
                            var_32 = ((uint64_t)(unsigned char)storemerge26 == 0UL);
                            var_33 = *var_16;
                            rcx_1 = rcx_0;
                            rax_0 = var_33;
                            rcx_2 = rcx_0;
                            rdx_2 = rdx_1;
                            if (!var_32) {
                                loop_state_var = 0U;
                                break;
                            }
                            *var_16 = (var_33 + 16UL);
                            continue;
                        }
                    while (1U)
                        {
                            var_39 = **var_17;
                            local_sp_1 = local_sp_0;
                            local_sp_2 = local_sp_0;
                            rdx_2 = var_39;
                            if (var_39 == 0UL) {
                                var_40 = *var_5;
                                var_41 = local_sp_0 + (-8L);
                                *(uint64_t *)var_41 = 4233439UL;
                                indirect_placeholder();
                                var_42 = ((uint64_t)(uint32_t)var_40 == 0UL);
                                var_43 = *var_16;
                                local_sp_0 = var_41;
                                rax_0 = var_43;
                                if (!var_42) {
                                    loop_state_var = 1U;
                                    break;
                                }
                                *var_16 = (var_43 + 16UL);
                                continue;
                            }
                            _cast10 = (unsigned char *)(*var_5 + (*var_25 + (-1L)));
                            if (*_cast10 != 'S') {
                                loop_state_var = 0U;
                                break;
                            }
                            *_cast10 = (unsigned char)'\x00';
                            *var_16 = 4336800UL;
                            loop_state_var = 2U;
                            break;
                        }
                    switch (loop_state_var) {
                      case 1U:
                        {
                            break;
                        }
                        break;
                      case 0U:
                        {
                            *var_16 = 4337120UL;
                            local_sp_3 = local_sp_2;
                            while (1U)
                                {
                                    var_49 = **var_17;
                                    rdx_2 = var_49;
                                    if (var_49 == 0UL) {
                                        var_50 = *var_5;
                                        var_51 = local_sp_3 + (-8L);
                                        *(uint64_t *)var_51 = 4233621UL;
                                        indirect_placeholder();
                                        var_52 = ((uint64_t)(uint32_t)var_50 == 0UL);
                                        var_53 = *var_16;
                                        local_sp_3 = var_51;
                                        rax_0 = var_53;
                                        if (!var_52) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *var_16 = (var_53 + 16UL);
                                        continue;
                                    }
                                    if (*var_25 != 1UL) {
                                        loop_state_var = 0U;
                                        break;
                                    }
                                    *var_16 = 4338592UL;
                                    while (1U)
                                        {
                                            rax_0 = var_54;
                                            if (**var_17 != 0UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            var_55 = **(unsigned char **)var_3;
                                            rdx_2 = (uint64_t)var_55;
                                            if ((uint64_t)(var_55 - ***(unsigned char ***)var_15) != 0UL) {
                                                loop_state_var = 1U;
                                                break;
                                            }
                                            var_56 = var_54 + 16UL;
                                            *var_16 = var_56;
                                            var_54 = var_56;
                                            continue;
                                        }
                                    switch_state_var = 0;
                                    switch (loop_state_var) {
                                      case 0U:
                                        {
                                            loop_state_var = 0U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                      case 1U:
                                        {
                                            loop_state_var = 1U;
                                            switch_state_var = 1;
                                            break;
                                        }
                                        break;
                                    }
                                    if (switch_state_var)
                                        break;
                                }
                            var_57 = (unsigned char *)(var_0 + (-33L));
                            *var_57 = (unsigned char)'\x00';
                            var_58 = *var_5;
                            var_59 = var_0 + (-24L);
                            var_60 = (uint64_t *)var_59;
                            *var_60 = var_58;
                            *var_7 = var_58;
                            var_61 = (unsigned char **)var_59;
                            var_62 = **var_61;
                            **var_8 = var_62;
                            while (**var_8 != '\x00')
                                {
                                    if (**var_61 == '.') {
                                        *var_57 = (unsigned char)'\x01';
                                    } else {
                                        *var_7 = (*var_7 + 1UL);
                                    }
                                    *var_60 = (*var_60 + 1UL);
                                    var_61 = (unsigned char **)var_59;
                                    var_62 = **var_61;
                                    **var_8 = var_62;
                                }
                            rdx_2 = (uint64_t)var_62;
                            if (*var_57 == '\x00') {
                                var_63 = *var_5;
                                var_64 = *var_4;
                                *(uint64_t *)(local_sp_3 + (-8L)) = 4233813UL;
                                var_65 = indirect_placeholder_2(var_63, var_64);
                                *var_16 = var_65;
                                rax_0 = var_65;
                                rdx_2 = var_63;
                            }
                        }
                        break;
                      case 2U:
                        {
                            while (1U)
                                {
                                    var_44 = **var_17;
                                    local_sp_2 = local_sp_1;
                                    rdx_2 = var_44;
                                    if (var_44 == 0UL) {
                                        var_45 = *var_5;
                                        var_46 = local_sp_1 + (-8L);
                                        *(uint64_t *)var_46 = 4233541UL;
                                        indirect_placeholder();
                                        var_47 = ((uint64_t)(uint32_t)var_45 == 0UL);
                                        var_48 = *var_16;
                                        local_sp_1 = var_46;
                                        rax_0 = var_48;
                                        if (!var_47) {
                                            loop_state_var = 1U;
                                            break;
                                        }
                                        *var_16 = (var_48 + 16UL);
                                        continue;
                                    }
                                    *(unsigned char *)(*var_5 + (*var_25 + (-1L))) = (unsigned char)'S';
                                    loop_state_var = 0U;
                                    break;
                                }
                            switch (loop_state_var) {
                              case 1U:
                                {
                                    mrv.field_0 = rax_0;
                                    mrv1 = mrv;
                                    mrv1.field_1 = rcx_2;
                                    mrv2 = mrv1;
                                    mrv2.field_2 = rdx_2;
                                    return mrv2;
                                }
                                break;
                              case 0U:
                                {
                                    *var_16 = 4337120UL;
                                    local_sp_3 = local_sp_2;
                                    while (1U)
                                        {
                                            var_49 = **var_17;
                                            rdx_2 = var_49;
                                            if (var_49 == 0UL) {
                                                var_50 = *var_5;
                                                var_51 = local_sp_3 + (-8L);
                                                *(uint64_t *)var_51 = 4233621UL;
                                                indirect_placeholder();
                                                var_52 = ((uint64_t)(uint32_t)var_50 == 0UL);
                                                var_53 = *var_16;
                                                local_sp_3 = var_51;
                                                rax_0 = var_53;
                                                if (!var_52) {
                                                    loop_state_var = 1U;
                                                    break;
                                                }
                                                *var_16 = (var_53 + 16UL);
                                                continue;
                                            }
                                            if (*var_25 != 1UL) {
                                                loop_state_var = 0U;
                                                break;
                                            }
                                            *var_16 = 4338592UL;
                                            while (1U)
                                                {
                                                    rax_0 = var_54;
                                                    if (**var_17 != 0UL) {
                                                        loop_state_var = 0U;
                                                        break;
                                                    }
                                                    var_55 = **(unsigned char **)var_3;
                                                    rdx_2 = (uint64_t)var_55;
                                                    if ((uint64_t)(var_55 - ***(unsigned char ***)var_15) != 0UL) {
                                                        loop_state_var = 1U;
                                                        break;
                                                    }
                                                    var_56 = var_54 + 16UL;
                                                    *var_16 = var_56;
                                                    var_54 = var_56;
                                                    continue;
                                                }
                                            switch_state_var = 0;
                                            switch (loop_state_var) {
                                              case 0U:
                                                {
                                                    loop_state_var = 0U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                              case 1U:
                                                {
                                                    loop_state_var = 1U;
                                                    switch_state_var = 1;
                                                    break;
                                                }
                                                break;
                                            }
                                            if (switch_state_var)
                                                break;
                                        }
                                    var_57 = (unsigned char *)(var_0 + (-33L));
                                    *var_57 = (unsigned char)'\x00';
                                    var_58 = *var_5;
                                    var_59 = var_0 + (-24L);
                                    var_60 = (uint64_t *)var_59;
                                    *var_60 = var_58;
                                    *var_7 = var_58;
                                    var_61 = (unsigned char **)var_59;
                                    var_62 = **var_61;
                                    **var_8 = var_62;
                                    while (**var_8 != '\x00')
                                        {
                                            if (**var_61 == '.') {
                                                *var_57 = (unsigned char)'\x01';
                                            } else {
                                                *var_7 = (*var_7 + 1UL);
                                            }
                                            *var_60 = (*var_60 + 1UL);
                                            var_61 = (unsigned char **)var_59;
                                            var_62 = **var_61;
                                            **var_8 = var_62;
                                        }
                                    rdx_2 = (uint64_t)var_62;
                                    if (*var_57 == '\x00') {
                                        var_63 = *var_5;
                                        var_64 = *var_4;
                                        *(uint64_t *)(local_sp_3 + (-8L)) = 4233813UL;
                                        var_65 = indirect_placeholder_2(var_63, var_64);
                                        *var_16 = var_65;
                                        rax_0 = var_65;
                                        rdx_2 = var_63;
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        break;
    }
}
