typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern uint64_t init_cc_src2(void);
extern void indirect_placeholder(void);
extern uint64_t helper_cc_compute_all_wrapper(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint32_t param_3);
uint64_t bb_human_fstype(uint64_t rdi) {
    uint64_t var_0;
    uint64_t var_1;
    uint64_t var_2;
    uint64_t var_3;
    uint64_t **var_4;
    uint64_t var_5;
    uint64_t rax_0;
    uint64_t var_6;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    var_2 = init_cc_src2();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_3 = var_0 + (-32L);
    *(uint64_t *)var_3 = rdi;
    var_4 = (uint64_t **)var_3;
    var_5 = **var_4;
    rax_0 = 4322413UL;
    if (var_5 == 4283649346UL) {
        return rax_0;
    }
    rax_0 = 4351168UL;
    if (var_5 <= 4283649346UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    rax_0 = 4322927UL;
    if (var_5 == 4266872130UL) {
        return rax_0;
    }
    rax_0 = 4322603UL;
    if (var_5 <= 4266872130UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4187351113UL) {
        return rax_0;
    }
    rax_0 = 4322907UL;
    if (var_5 <= 4187351113UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4185718668UL) {
        return rax_0;
    }
    rax_0 = 4322529UL;
    if (var_5 <= 4185718668UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4076150800UL) {
        return rax_0;
    }
    rax_0 = 4322491UL;
    if (var_5 <= 4076150800UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 3730735588UL) {
        return rax_0;
    }
    rax_0 = 4322353UL;
    if (var_5 <= 3730735588UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 3405662737UL) {
        return rax_0;
    }
    rax_0 = 4322669UL;
    if (var_5 <= 3405662737UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 3380511080UL) {
        return rax_0;
    }
    rax_0 = 4322932UL;
    if (var_5 <= 3380511080UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 3203391149UL) {
        return rax_0;
    }
    rax_0 = 4323013UL;
    if (var_5 <= 3203391149UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 3133910204UL) {
        return rax_0;
    }
    rax_0 = 4323036UL;
    if (var_5 <= 3133910204UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2881100148UL) {
        return rax_0;
    }
    rax_0 = 4322816UL;
    if (var_5 <= 2881100148UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2866260714UL) {
        return rax_0;
    }
    rax_0 = 4323020UL;
    if (var_5 <= 2866260714UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2768370933UL) {
        return rax_0;
    }
    rax_0 = 4322608UL;
    if (var_5 <= 2768370933UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2508478710UL) {
        return rax_0;
    }
    rax_0 = 4322372UL;
    if (var_5 <= 2508478710UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2435016766UL) {
        return rax_0;
    }
    rax_0 = 4322860UL;
    if (var_5 <= 2435016766UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2240043254UL) {
        return rax_0;
    }
    rax_0 = 4322829UL;
    if (var_5 <= 2240043254UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2088527475UL) {
        return rax_0;
    }
    rax_0 = 4322806UL;
    if (var_5 <= 2088527475UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2035054128UL) {
        return rax_0;
    }
    rax_0 = 4322977UL;
    if (var_5 <= 2035054128UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1953653091UL) {
        return rax_0;
    }
    rax_0 = 4322800UL;
    if (var_5 <= 1953653091UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1952539503UL) {
        return rax_0;
    }
    rax_0 = 4322418UL;
    if (var_5 <= 1952539503UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1937076805UL) {
        return rax_0;
    }
    rax_0 = 4322378UL;
    if (var_5 <= 1937076805UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1936880249UL) {
        return rax_0;
    }
    rax_0 = 4322944UL;
    if (var_5 <= 1936880249UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1936814952UL) {
        return rax_0;
    }
    rax_0 = 4322896UL;
    if (var_5 <= 1936814952UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1935894131UL) {
        return rax_0;
    }
    rax_0 = 4322781UL;
    if (var_5 <= 1935894131UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1853056627UL) {
        return rax_0;
    }
    rax_0 = 4322770UL;
    if (var_5 <= 1853056627UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1852207972UL) {
        return rax_0;
    }
    rax_0 = 4322663UL;
    if (var_5 <= 1852207972UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1799439955UL) {
        return rax_0;
    }
    rax_0 = 4322855UL;
    if (var_5 <= 1799439955UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1746473250UL) {
        return rax_0;
    }
    rax_0 = 4322885UL;
    if (var_5 <= 1746473250UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1733912937UL) {
        return rax_0;
    }
    rax_0 = 4322544UL;
    if (var_5 <= 1733912937UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1702057286UL) {
        return rax_0;
    }
    rax_0 = 4322552UL;
    if (var_5 <= 1702057286UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1702057283UL) {
        return rax_0;
    }
    rax_0 = 4322455UL;
    if (var_5 <= 1702057283UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1684300152UL) {
        return rax_0;
    }
    rax_0 = 4322461UL;
    if (var_5 <= 1684300152UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1684170528UL) {
        return rax_0;
    }
    rax_0 = 4322403UL;
    if (var_5 <= 1684170528UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1667723888UL) {
        return rax_0;
    }
    rax_0 = 4322953UL;
    if (var_5 <= 1667723888UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1650812274UL) {
        return rax_0;
    }
    rax_0 = 4322427UL;
    if (var_5 <= 1650812274UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1650812272UL) {
        return rax_0;
    }
    rax_0 = 4322342UL;
    if (var_5 <= 1650812272UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1650746742UL) {
        return rax_0;
    }
    rax_0 = 4322310UL;
    if (var_5 <= 1650746742UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1635083891UL) {
        return rax_0;
    }
    rax_0 = 4322841UL;
    if (var_5 <= 1635083891UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1634035564UL) {
        return rax_0;
    }
    rax_0 = 4322277UL;
    if (var_5 <= 1634035564UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1633904243UL) {
        return rax_0;
    }
    rax_0 = 4322272UL;
    if (var_5 <= 1633904243UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1513908720UL) {
        return rax_0;
    }
    rax_0 = 4323048UL;
    if (var_5 <= 1513908720UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1481003842UL) {
        return rax_0;
    }
    rax_0 = 4323060UL;
    if (var_5 <= 1481003842UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1479104553UL) {
        return rax_0;
    }
    rax_0 = 4323025UL;
    if (var_5 <= 1479104553UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1448756819UL) {
        return rax_0;
    }
    rax_0 = 4322995UL;
    if (var_5 <= 1448756819UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1410924800UL) {
        return rax_0;
    }
    rax_0 = 4322937UL;
    if (var_5 <= 1410924800UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1397703499UL) {
        return rax_0;
    }
    rax_0 = 4322786UL;
    if (var_5 <= 1397703499UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1397118030UL) {
        return rax_0;
    }
    rax_0 = 4323030UL;
    if (var_5 <= 1397118030UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1397114950UL) {
        return rax_0;
    }
    rax_0 = 4322292UL;
    if (var_5 <= 1397114950UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1397113167UL) {
        return rax_0;
    }
    rax_0 = 4322682UL;
    if (var_5 <= 1397113167UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1397109069UL) {
        return rax_0;
    }
    rax_0 = 4322870UL;
    if (var_5 <= 1397109069UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1382369651UL) {
        return rax_0;
    }
    rax_0 = 4322822UL;
    if (var_5 <= 1382369651UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1346981957UL) {
        return rax_0;
    }
    rax_0 = 4322577UL;
    if (var_5 <= 1346981957UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1196443219UL) {
        return rax_0;
    }
    rax_0 = 4322443UL;
    if (var_5 <= 1196443219UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1161678120UL) {
        return rax_0;
    }
    rax_0 = 4322915UL;
    if (var_5 <= 1161678120UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1128357203UL) {
        return rax_0;
    }
    rax_0 = 4322360UL;
    if (var_5 <= 1128357203UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1112100429UL) {
        return rax_0;
    }
    rax_0 = 4322337UL;
    if (var_5 <= 1112100429UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1111905073UL) {
        return rax_0;
    }
    rax_0 = 4322659UL;
    if (var_5 <= 1111905073UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 827541066UL) {
        return rax_0;
    }
    rax_0 = 4323056UL;
    if (var_5 <= 827541066UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 801189825UL) {
        return rax_0;
    }
    var_6 = helper_cc_compute_all_wrapper(var_5 + (-801189825L), 801189825UL, var_2, 17U);
    rax_0 = 4322632UL;
    if ((var_6 & 65UL) != 0UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 732765674UL) {
        return rax_0;
    }
    rax_0 = 4322436UL;
    if (var_5 <= 732765674UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 684539205UL) {
        return rax_0;
    }
    rax_0 = 4322985UL;
    if (var_5 <= 684539205UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 604313861UL) {
        return rax_0;
    }
    rax_0 = 4322349UL;
    if (var_5 <= 604313861UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 464386766UL) {
        return rax_0;
    }
    rax_0 = 4322538UL;
    if (var_5 <= 464386766UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 428016422UL) {
        return rax_0;
    }
    rax_0 = 4322746UL;
    if (var_5 <= 428016422UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 427819522UL) {
        return rax_0;
    }
    rax_0 = 4322991UL;
    if (var_5 <= 427819522UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 352400198UL) {
        return rax_0;
    }
    rax_0 = 4322322UL;
    if (var_5 <= 352400198UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 325456742UL) {
        return rax_0;
    }
    rax_0 = 4322618UL;
    if (var_5 <= 325456742UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 288389204UL) {
        return rax_0;
    }
    rax_0 = 4322675UL;
    if (var_5 <= 288389204UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 198183888UL) {
        return rax_0;
    }
    rax_0 = 4322560UL;
    if (var_5 <= 198183888UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 195894762UL) {
        return rax_0;
    }
    rax_0 = 4322296UL;
    if (var_5 <= 195894762UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 151263540UL) {
        return rax_0;
    }
    rax_0 = 4322866UL;
    if (var_5 <= 151263540UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 124082209UL) {
        return rax_0;
    }
    rax_0 = 4322626UL;
    if (var_5 <= 124082209UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19993000UL) {
        return rax_0;
    }
    rax_0 = 4322423UL;
    if (var_5 <= 19993000UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19920823UL) {
        return rax_0;
    }
    rax_0 = 4322959UL;
    if (var_5 <= 19920823UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19920822UL) {
        return rax_0;
    }
    rax_0 = 4322965UL;
    if (var_5 <= 19920822UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19920821UL) {
        return rax_0;
    }
    rax_0 = 4323042UL;
    if (var_5 <= 19920821UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19920820UL) {
        return rax_0;
    }
    rax_0 = 4323052UL;
    if (var_5 <= 19920820UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19911021UL) {
        return rax_0;
    }
    rax_0 = 4322568UL;
    if (var_5 <= 19911021UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 18225520UL) {
        return rax_0;
    }
    rax_0 = 4323008UL;
    if (var_5 <= 18225520UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 16914839UL) {
        return rax_0;
    }
    rax_0 = 4322971UL;
    if (var_5 <= 16914839UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 16914836UL) {
        return rax_0;
    }
    rax_0 = 4322389UL;
    if (var_5 <= 16914836UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 12805120UL) {
        return rax_0;
    }
    rax_0 = 4322596UL;
    if (var_5 <= 12805120UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 12648430UL) {
        return rax_0;
    }
    rax_0 = 4322500UL;
    if (var_5 <= 12648430UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4278867UL) {
        return rax_0;
    }
    rax_0 = 4322394UL;
    if (var_5 <= 4278867UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 2613483UL) {
        return rax_0;
    }
    rax_0 = 4322995UL;
    if (var_5 <= 2613483UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 72020UL) {
        return rax_0;
    }
    rax_0 = 4322482UL;
    if (var_5 <= 72020UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 61791UL) {
        return rax_0;
    }
    rax_0 = 4322514UL;
    if (var_5 <= 61791UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 61267UL) {
        return rax_0;
    }
    rax_0 = 4322524UL;
    if (var_5 <= 61267UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 61265UL) {
        return rax_0;
    }
    rax_0 = 4322287UL;
    if (var_5 <= 61265UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 44543UL) {
        return rax_0;
    }
    rax_0 = 4322282UL;
    if (var_5 <= 44543UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 44533UL) {
        return rax_0;
    }
    rax_0 = 4322999UL;
    if (var_5 <= 44533UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 40866UL) {
        return rax_0;
    }
    rax_0 = 4322791UL;
    if (var_5 <= 40866UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 40865UL) {
        return rax_0;
    }
    rax_0 = 4322836UL;
    if (var_5 <= 40865UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 40864UL) {
        return rax_0;
    }
    rax_0 = 4322642UL;
    if (var_5 <= 40864UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 38496UL) {
        return rax_0;
    }
    rax_0 = 4322653UL;
    if (var_5 <= 38496UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 29366UL) {
        return rax_0;
    }
    rax_0 = 4322879UL;
    if (var_5 <= 29366UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 29301UL) {
        return rax_0;
    }
    rax_0 = 4322766UL;
    if (var_5 <= 29301UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 26985UL) {
        return rax_0;
    }
    rax_0 = 4322504UL;
    if (var_5 <= 26985UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 24053UL) {
        return rax_0;
    }
    rax_0 = 4322759UL;
    if (var_5 <= 24053UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 22092UL) {
        return rax_0;
    }
    rax_0 = 4322923UL;
    if (var_5 <= 22092UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 20859UL) {
        return rax_0;
    }
    rax_0 = 4322739UL;
    if (var_5 <= 20859UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19802UL) {
        return rax_0;
    }
    rax_0 = 4322753UL;
    if (var_5 <= 19802UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 19780UL) {
        return rax_0;
    }
    rax_0 = 4322591UL;
    if (var_5 <= 19780UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 18520UL) {
        return rax_0;
    }
    rax_0 = 4322586UL;
    if (var_5 <= 18520UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 18475UL) {
        return rax_0;
    }
    rax_0 = 4322582UL;
    if (var_5 <= 18475UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 16964UL) {
        return rax_0;
    }
    rax_0 = 4322534UL;
    if (var_5 <= 16964UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 16390UL) {
        return rax_0;
    }
    rax_0 = 4322642UL;
    if (var_5 <= 16390UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 16388UL) {
        return rax_0;
    }
    if (var_5 <= 16388UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 16384UL) {
        return rax_0;
    }
    rax_0 = 4322775UL;
    if (var_5 <= 16384UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 13364UL) {
        return rax_0;
    }
    rax_0 = 4322719UL;
    if (var_5 <= 13364UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 9336UL) {
        return rax_0;
    }
    rax_0 = 4322710UL;
    if (var_5 <= 9336UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 9320UL) {
        return rax_0;
    }
    rax_0 = 4322475UL;
    if (var_5 <= 9320UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 7377UL) {
        return rax_0;
    }
    rax_0 = 4322693UL;
    if (var_5 <= 7377UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 5007UL) {
        return rax_0;
    }
    rax_0 = 4322687UL;
    if (var_5 <= 5007UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4991UL) {
        return rax_0;
    }
    rax_0 = 4322510UL;
    if (var_5 <= 4991UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4989UL) {
        return rax_0;
    }
    rax_0 = 4322469UL;
    if (var_5 <= 4989UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 4979UL) {
        return rax_0;
    }
    rax_0 = 4322648UL;
    if (var_5 <= 4979UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    if (var_5 == 1984UL) {
        return rax_0;
    }
    rax_0 = 4322850UL;
    if (var_5 <= 1984UL) {
        *(uint64_t *)(var_0 + (-16L)) = **var_4;
        *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
        indirect_placeholder();
        return rax_0;
    }
    switch (var_5) {
      case 47UL:
        {
            break;
        }
        break;
      case 391UL:
        {
            rax_0 = 4322315UL;
        }
        break;
      default:
        {
            *(uint64_t *)(var_0 + (-16L)) = **var_4;
            *(uint64_t *)(var_0 + (-48L)) = 4209236UL;
            indirect_placeholder();
        }
        break;
    }
}
