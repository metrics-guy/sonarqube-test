typedef unsigned char __u_char;
typedef unsigned short __u_short;
typedef unsigned int __u_int;
typedef unsigned long __u_long;
typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;
typedef long __quad_t;
typedef unsigned long __u_quad_t;
typedef long __intmax_t;
typedef unsigned long __uintmax_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned long __ino64_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef struct {
    int __val[2];
} __fsid_t;
typedef long __clock_t;
typedef unsigned long __rlim_t;
typedef unsigned long __rlim64_t;
typedef unsigned int __id_t;
typedef long __time_t;
typedef unsigned int __useconds_t;
typedef long __suseconds_t;
typedef long __suseconds64_t;
typedef int __daddr_t;
typedef int __key_t;
typedef int __clockid_t;
typedef void *__timer_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __blkcnt64_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsblkcnt64_t;
typedef unsigned long __fsfilcnt_t;
typedef unsigned long __fsfilcnt64_t;
typedef long __fsword_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef unsigned long __syscall_ulong_t;
typedef __off64_t __loff_t;
typedef char *__caddr_t;
typedef long __intptr_t;
typedef unsigned int __socklen_t;
typedef int __sig_atomic_t;
typedef __int8_t int8_t;
typedef __int16_t int16_t;
typedef __int32_t int32_t;
typedef __int64_t int64_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint32_t uint32_t;
typedef __uint64_t uint64_t;
typedef __int_least8_t int_least8_t;
typedef __int_least16_t int_least16_t;
typedef __int_least32_t int_least32_t;
typedef __int_least64_t int_least64_t;
typedef __uint_least8_t uint_least8_t;
typedef __uint_least16_t uint_least16_t;
typedef __uint_least32_t uint_least32_t;
typedef __uint_least64_t uint_least64_t;
typedef signed char int_fast8_t;
typedef long int_fast16_t;
typedef long int_fast32_t;
typedef long int_fast64_t;
typedef unsigned char uint_fast8_t;
typedef unsigned long uint_fast16_t;
typedef unsigned long uint_fast32_t;
typedef unsigned long uint_fast64_t;
typedef long intptr_t;
typedef unsigned long uintptr_t;
typedef __intmax_t intmax_t;
typedef __uintmax_t uintmax_t;
struct indirect_placeholder_60_ret_type;
struct indirect_placeholder_58_ret_type;
struct indirect_placeholder_59_ret_type;
struct indirect_placeholder_60_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
    uint64_t field_3;
};
struct indirect_placeholder_58_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
struct indirect_placeholder_59_ret_type {
    uint64_t field_0;
    uint64_t field_1;
    uint64_t field_2;
};
extern uint64_t revng_init_local_sp(uint64_t param_0);
extern uint64_t init_rbp(void);
extern void indirect_placeholder(void);
extern uint64_t indirect_placeholder_7(uint64_t param_0);
extern void indirect_placeholder_8(uint64_t param_0, uint64_t param_1, uint64_t param_2, uint64_t param_3, uint64_t param_4, uint64_t param_5, uint64_t param_6);
extern struct indirect_placeholder_60_ret_type indirect_placeholder_60(uint64_t param_0);
extern struct indirect_placeholder_58_ret_type indirect_placeholder_58(uint64_t param_0);
extern struct indirect_placeholder_59_ret_type indirect_placeholder_59(uint64_t param_0, uint64_t param_1, uint64_t param_2);
uint64_t bb_print_it(uint64_t rcx, uint64_t rdx, uint64_t rdi, uint64_t rsi, uint64_t r8) {
    uint64_t var_47;
    unsigned char var_40;
    uint32_t rax_1_in;
    uint64_t var_61;
    uint64_t var_41;
    uint64_t var_42;
    uint64_t var_43;
    uint64_t var_44;
    uint64_t var_0;
    uint64_t var_1;
    uint64_t *var_2;
    uint32_t *var_3;
    uint64_t *var_4;
    uint64_t *var_5;
    unsigned char *var_6;
    uint64_t var_7;
    uint64_t var_8;
    uint64_t var_9;
    struct indirect_placeholder_60_ret_type var_10;
    uint64_t var_11;
    uint64_t var_12;
    uint64_t var_13;
    uint64_t var_14;
    uint64_t *var_15;
    uint64_t var_16;
    uint64_t var_17;
    uint64_t *var_18;
    uint32_t *var_19;
    uint32_t *var_20;
    uint32_t *var_21;
    uint64_t *var_22;
    uint64_t var_23;
    uint64_t *var_24;
    uint32_t *var_25;
    uint64_t var_26;
    uint32_t var_45;
    unsigned char var_46;
    uint32_t rax_0_in;
    uint64_t rcx1_3;
    uint64_t local_sp_3;
    uint64_t local_sp_2;
    uint64_t r9_3;
    uint64_t r9_2;
    uint64_t rcx1_1;
    uint64_t r85_3;
    uint64_t r85_2;
    uint64_t r9_1;
    uint64_t r85_1;
    uint64_t var_80;
    uint64_t var_75;
    uint64_t var_76;
    uint64_t var_77;
    uint64_t var_78;
    uint64_t var_79;
    uint64_t var_67;
    uint64_t var_68;
    struct indirect_placeholder_58_ret_type var_69;
    uint64_t var_70;
    uint64_t var_71;
    uint64_t var_72;
    uint64_t var_73;
    uint64_t rcx1_2;
    uint64_t var_74;
    uint64_t var_62;
    uint64_t var_63;
    uint64_t local_sp_4;
    uint64_t var_64;
    uint64_t var_65;
    uint64_t var_66;
    uint64_t local_sp_5;
    unsigned char **var_27;
    unsigned char var_28;
    uint32_t var_29;
    uint64_t var_30;
    uint64_t var_31;
    uint64_t var_32;
    unsigned char var_33;
    unsigned char var_34;
    uint64_t var_35;
    unsigned char var_48;
    uint64_t var_36;
    uint64_t var_37;
    uint64_t var_38;
    uint64_t var_39;
    uint64_t local_sp_6;
    uint64_t var_55;
    uint64_t var_49;
    uint64_t var_50;
    struct indirect_placeholder_59_ret_type var_51;
    uint64_t var_52;
    uint64_t var_53;
    uint64_t var_54;
    uint64_t var_56;
    uint64_t var_57;
    uint64_t var_58;
    uint64_t var_59;
    unsigned char **var_60;
    var_0 = revng_init_local_sp(0UL);
    var_1 = init_rbp();
    *(uint64_t *)(var_0 + (-8L)) = var_1;
    var_2 = (uint64_t *)(var_0 + (-96L));
    *var_2 = rdi;
    var_3 = (uint32_t *)(var_0 + (-100L));
    *var_3 = (uint32_t)rsi;
    var_4 = (uint64_t *)(var_0 + (-112L));
    *var_4 = rdx;
    *(uint64_t *)(var_0 + (-120L)) = rcx;
    var_5 = (uint64_t *)(var_0 + (-128L));
    *var_5 = r8;
    var_6 = (unsigned char *)(var_0 + (-9L));
    *var_6 = (unsigned char)'\x00';
    var_7 = *var_2;
    *(uint64_t *)(var_0 + (-144L)) = 4214955UL;
    indirect_placeholder();
    var_8 = var_7 + 3UL;
    *(uint64_t *)(var_0 + (-56L)) = var_8;
    var_9 = var_0 + (-152L);
    *(uint64_t *)var_9 = 4214975UL;
    var_10 = indirect_placeholder_60(var_8);
    var_11 = var_10.field_0;
    var_12 = var_10.field_1;
    var_13 = var_10.field_2;
    var_14 = var_10.field_3;
    var_15 = (uint64_t *)(var_0 + (-64L));
    *var_15 = var_11;
    var_16 = *var_2;
    var_17 = var_0 + (-24L);
    var_18 = (uint64_t *)var_17;
    *var_18 = var_16;
    var_19 = (uint32_t *)(var_0 + (-36L));
    var_20 = (uint32_t *)(var_0 + (-40L));
    var_21 = (uint32_t *)(var_0 + (-44L));
    var_22 = (uint64_t *)(var_0 + (-72L));
    var_23 = var_0 + (-32L);
    var_24 = (uint64_t *)var_23;
    var_25 = (uint32_t *)(var_0 + (-76L));
    var_26 = var_16;
    rcx1_3 = var_12;
    r9_3 = var_13;
    r85_3 = var_14;
    local_sp_5 = var_9;
    var_27 = (unsigned char **)var_17;
    var_28 = **var_27;
    r9_2 = r9_3;
    rcx1_1 = rcx1_3;
    r85_2 = r85_3;
    r9_1 = r9_3;
    r85_1 = r85_3;
    local_sp_6 = local_sp_5;
    while (var_28 != '\x00')
        {
            var_29 = (uint32_t)(uint64_t)var_28;
            if ((uint64_t)(var_29 + (-37)) != 0UL) {
                if ((uint64_t)(var_29 + (-92)) == 0UL) {
                    var_30 = local_sp_5 + (-8L);
                    *(uint64_t *)var_30 = 4215966UL;
                    indirect_placeholder();
                    local_sp_2 = var_30;
                } else {
                    if (*(unsigned char *)4351137UL == '\x01') {
                        var_31 = local_sp_5 + (-8L);
                        *(uint64_t *)var_31 = 4215399UL;
                        indirect_placeholder();
                        local_sp_2 = var_31;
                    } else {
                        var_32 = var_26 + 1UL;
                        *var_18 = var_32;
                        var_33 = **var_27;
                        var_48 = var_33;
                        if (((signed char)var_33 <= '/') || ((signed char)var_33 > '7')) {
                            if (var_33 != 'x') {
                                var_36 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_32 + 1UL);
                                *(uint64_t *)(local_sp_5 + (-8L)) = 4215582UL;
                                var_37 = indirect_placeholder_7(var_36);
                                var_38 = (uint64_t)(unsigned char)var_37;
                                var_39 = local_sp_5 + (-16L);
                                *(uint64_t *)var_39 = 4215592UL;
                                indirect_placeholder();
                                local_sp_6 = var_39;
                                if (var_38 != 0UL) {
                                    var_40 = *(unsigned char *)(*var_18 + 1UL);
                                    if (((signed char)var_40 <= '`') || ((signed char)var_40 > 'f')) {
                                        rax_1_in = (uint32_t)var_40 + (-87);
                                    } else {
                                        if (((signed char)var_40 <= '@') || ((signed char)var_40 > 'F')) {
                                            rax_1_in = (uint32_t)var_40 + (-48);
                                        } else {
                                            rax_1_in = (uint32_t)var_40 + (-55);
                                        }
                                    }
                                    *var_21 = rax_1_in;
                                    var_41 = *var_18 + 1UL;
                                    *var_18 = var_41;
                                    var_42 = (uint64_t)(uint32_t)(uint64_t)*(unsigned char *)(var_41 + 1UL);
                                    *(uint64_t *)(local_sp_5 + (-24L)) = 4215744UL;
                                    var_43 = indirect_placeholder_7(var_42);
                                    var_44 = (uint64_t)(unsigned char)var_43;
                                    *(uint64_t *)(local_sp_5 + (-32L)) = 4215754UL;
                                    indirect_placeholder();
                                    if (var_44 != 0UL) {
                                        *var_18 = (*var_18 + 1UL);
                                        var_45 = *var_21 << 4U;
                                        var_46 = **var_27;
                                        if (((signed char)var_46 <= '`') || ((signed char)var_46 > 'f')) {
                                            rax_0_in = (uint32_t)var_46 + (-87);
                                        } else {
                                            if (((signed char)var_46 <= '@') || ((signed char)var_46 > 'F')) {
                                                rax_0_in = (uint32_t)var_46 + (-48);
                                            } else {
                                                rax_0_in = (uint32_t)var_46 + (-55);
                                            }
                                        }
                                        *var_21 = (rax_0_in + var_45);
                                    }
                                    var_47 = local_sp_5 + (-40L);
                                    *(uint64_t *)var_47 = 4215873UL;
                                    indirect_placeholder();
                                    local_sp_2 = var_47;
                                    var_80 = *var_18 + 1UL;
                                    *var_18 = var_80;
                                    var_26 = var_80;
                                    rcx1_3 = rcx1_1;
                                    r9_3 = r9_1;
                                    r85_3 = r85_1;
                                    local_sp_5 = local_sp_2;
                                    var_27 = (unsigned char **)var_17;
                                    var_28 = **var_27;
                                    r9_2 = r9_3;
                                    rcx1_1 = rcx1_3;
                                    r85_2 = r85_3;
                                    r9_1 = r9_3;
                                    r85_1 = r85_3;
                                    local_sp_6 = local_sp_5;
                                    continue;
                                }
                                var_48 = **var_27;
                            }
                            if (var_48 == '\x00') {
                                *(uint64_t *)(local_sp_6 + (-8L)) = 4215911UL;
                                indirect_placeholder_8(0UL, rcx1_3, 4324432UL, 0UL, 0UL, r9_3, r85_3);
                                var_55 = local_sp_6 + (-16L);
                                *(uint64_t *)var_55 = 4215921UL;
                                indirect_placeholder();
                                *var_18 = (*var_18 + (-1L));
                                local_sp_2 = var_55;
                            } else {
                                var_49 = (uint64_t)(uint32_t)(uint64_t)var_48;
                                var_50 = local_sp_6 + (-8L);
                                *(uint64_t *)var_50 = 4215945UL;
                                var_51 = indirect_placeholder_59(var_49, r9_3, r85_3);
                                var_52 = var_51.field_0;
                                var_53 = var_51.field_1;
                                var_54 = var_51.field_2;
                                local_sp_2 = var_50;
                                rcx1_1 = var_52;
                                r9_1 = var_53;
                                r85_1 = var_54;
                            }
                        } else {
                            *var_19 = ((uint32_t)var_33 + (-48));
                            *var_20 = 1U;
                            *var_18 = (*var_18 + 1UL);
                            while ((int)*var_20 <= (int)2U)
                                {
                                    var_34 = **var_27;
                                    if (((signed char)var_34 <= '/') || ((signed char)var_34 > '7')) {
                                        break;
                                    }
                                    *var_19 = (((uint32_t)var_34 + (-48)) + (*var_19 << 3U));
                                    *var_20 = (*var_20 + 1U);
                                    *var_18 = (*var_18 + 1UL);
                                }
                            var_35 = local_sp_5 + (-8L);
                            *(uint64_t *)var_35 = 4215536UL;
                            indirect_placeholder();
                            *var_18 = (*var_18 + (-1L));
                            local_sp_2 = var_35;
                        }
                    }
                }
                var_80 = *var_18 + 1UL;
                *var_18 = var_80;
                var_26 = var_80;
                rcx1_3 = rcx1_1;
                r9_3 = r9_1;
                r85_3 = r85_1;
                local_sp_5 = local_sp_2;
                var_27 = (unsigned char **)var_17;
                var_28 = **var_27;
                r9_2 = r9_3;
                rcx1_1 = rcx1_3;
                r85_2 = r85_3;
                r9_1 = r9_3;
                r85_1 = r85_3;
                local_sp_6 = local_sp_5;
                continue;
            }
            var_56 = var_26 + 1UL;
            *(uint64_t *)(local_sp_5 + (-8L)) = 4215042UL;
            indirect_placeholder();
            *var_22 = var_56;
            var_57 = *var_18 + (var_56 + 1UL);
            *var_24 = var_57;
            var_58 = local_sp_5 + (-16L);
            *(uint64_t *)var_58 = 4215082UL;
            indirect_placeholder();
            var_59 = *var_24 + var_57;
            *var_24 = var_59;
            var_60 = (unsigned char **)var_23;
            var_63 = var_59;
            local_sp_4 = var_58;
            if (**var_60 == '.') {
                var_61 = local_sp_5 + (-24L);
                *(uint64_t *)var_61 = 4215118UL;
                indirect_placeholder();
                var_62 = *var_24 + (var_59 + 2UL);
                *var_24 = var_62;
                var_63 = var_62;
                local_sp_4 = var_61;
            }
            switch (*var_25) {
              case 0U:
                {
                    *var_18 = (var_66 + (-1L));
                }
                break;
              case 37U:
                {
                    var_67 = *var_22;
                    if (var_67 != 0UL) {
                        *(unsigned char *)((var_67 + 1UL) + *var_15) = **var_60;
                        *(unsigned char *)(*var_15 + (*var_22 + 2UL)) = (unsigned char)'\x00';
                        var_68 = *var_15;
                        *(uint64_t *)(local_sp_4 + (-16L)) = 4215273UL;
                        var_69 = indirect_placeholder_58(var_68);
                        var_70 = var_69.field_0;
                        var_71 = var_69.field_1;
                        var_72 = var_69.field_2;
                        var_73 = local_sp_4 + (-24L);
                        *(uint64_t *)var_73 = 4215301UL;
                        indirect_placeholder_8(0UL, var_70, 4324408UL, 1UL, 0UL, var_71, var_72);
                        local_sp_3 = var_73;
                        rcx1_2 = var_70;
                        r9_2 = var_71;
                        r85_2 = var_72;
                    }
                    var_74 = local_sp_3 + (-8L);
                    *(uint64_t *)var_74 = 4215311UL;
                    indirect_placeholder();
                    local_sp_2 = var_74;
                    rcx1_1 = rcx1_2;
                    r9_1 = r9_2;
                    r85_1 = r85_2;
                }
                break;
            }
        }
    *(uint64_t *)(local_sp_5 + (-8L)) = 4215999UL;
    indirect_placeholder();
    *(uint64_t *)(local_sp_5 + (-16L)) = 4216024UL;
    indirect_placeholder();
    return (uint64_t)*var_6;
}
